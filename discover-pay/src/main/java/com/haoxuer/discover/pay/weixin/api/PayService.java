package com.haoxuer.discover.pay.weixin.api;

import com.haoxuer.discover.pay.weixin.domain.UnifiedOrderPayData;
import com.haoxuer.discover.pay.weixin.domain.UnifiedOrderPayBack;

public interface PayService {
  
  UnifiedOrderPayBack order(UnifiedOrderPayData data);
}
