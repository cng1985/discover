package com.haoxuer.discover.talk.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.user.data.entity.UserInfo;

import javax.persistence.*;

@Entity
@Table(name = "user_message_member")
public class UserMessageMember extends AbstractEntity {
  
  /**
   * 消息主题
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private UserMessageSubject subject;
  
  /**
   * 用户
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;
  
  /**
   * 消息数量
   */
  private Integer nums;
  
  /**
   * 0 为没有最新消息
   * 1为有最新消息
   */
  private Integer state;
  
  public UserMessageSubject getSubject() {
    return subject;
  }
  
  public void setSubject(UserMessageSubject subject) {
    this.subject = subject;
  }
  

  public Integer getNums() {
    return nums;
  }
  
  public void setNums(Integer nums) {
    this.nums = nums;
  }
  
  public Integer getState() {
    return state;
  }
  
  public void setState(Integer state) {
    this.state = state;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
