package com.haoxuer.discover.talk.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.talk.data.entity.UserMessageSubject;

/**
* Created by imake on 2019年01月29日15:40:37.
*/
public interface UserMessageSubjectDao extends BaseDao<UserMessageSubject,Long>{

	 UserMessageSubject findById(Long id);

	 UserMessageSubject save(UserMessageSubject bean);

	 UserMessageSubject updateByUpdater(Updater<UserMessageSubject> updater);

	 UserMessageSubject deleteById(Long id);
}