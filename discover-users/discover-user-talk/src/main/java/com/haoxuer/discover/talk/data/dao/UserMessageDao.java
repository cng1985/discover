package com.haoxuer.discover.talk.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.talk.data.entity.UserMessage;

/**
* Created by imake on 2019年01月29日15:40:36.
*/
public interface UserMessageDao extends BaseDao<UserMessage,Long>{

	 UserMessage findById(Long id);

	 UserMessage save(UserMessage bean);

	 UserMessage updateByUpdater(Updater<UserMessage> updater);

	 UserMessage deleteById(Long id);
}