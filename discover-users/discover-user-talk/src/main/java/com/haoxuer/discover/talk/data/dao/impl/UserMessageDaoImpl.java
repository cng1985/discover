package com.haoxuer.discover.talk.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.talk.data.dao.UserMessageDao;
import com.haoxuer.discover.talk.data.entity.UserMessage;

/**
* Created by imake on 2019年01月29日15:40:37.
*/
@Repository

public class UserMessageDaoImpl extends CriteriaDaoImpl<UserMessage, Long> implements UserMessageDao {

	@Override
	public UserMessage findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public UserMessage save(UserMessage bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public UserMessage deleteById(Long id) {
		UserMessage entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<UserMessage> getEntityClass() {
		return UserMessage.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}