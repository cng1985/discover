package com.haoxuer.discover.talk.data.so;

import java.io.Serializable;

/**
* Created by imake on 2019年01月29日15:40:37.
*/
public class UserMessageSubjectSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
