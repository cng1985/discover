package com.haoxuer.discover.talk.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.talk.data.entity.UserMessageMember;

/**
* Created by imake on 2019年01月29日15:40:37.
*/
public interface UserMessageMemberDao extends BaseDao<UserMessageMember,Long>{

	 UserMessageMember findById(Long id);

	 UserMessageMember save(UserMessageMember bean);

	 UserMessageMember updateByUpdater(Updater<UserMessageMember> updater);

	 UserMessageMember deleteById(Long id);
}