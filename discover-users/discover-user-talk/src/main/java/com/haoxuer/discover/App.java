package com.haoxuer.discover;

import com.haoxuer.discover.talk.data.entity.UserMessage;
import com.haoxuer.discover.talk.data.entity.UserMessageMember;
import com.haoxuer.discover.talk.data.entity.UserMessageSubject;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);

        File view = new File("E:\\mvnspace\\usite\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(UserMessage.class);
        cs.add(UserMessageMember.class);
        cs.add(UserMessageSubject.class);

        make.setDao(true);
        make.setService(false);
        make.setView(false);
        make.setAction(false);
        make.makes(cs);
        System.out.println("ok");
    }
}
