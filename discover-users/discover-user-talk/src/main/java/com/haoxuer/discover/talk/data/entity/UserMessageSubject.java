package com.haoxuer.discover.talk.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.user.data.entity.UserInfo;

import javax.persistence.*;


@Entity
@Table(name = "user_message_subject")
public class UserMessageSubject extends AbstractEntity {
  
  /**
   * 用户
   */
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;
  
  /**
   * 主题
   */
  private String subject;
  
  /**
   * 消息类型 1为私信 2为群聊
   */
  private Integer catalog;
  
  /**
   * 参与人数
   */
  private Integer members;
  
  /**
   * 最后消息
   */
  private String lastMessage;
  

  public String getSubject() {
    return subject;
  }
  
  public void setSubject(String subject) {
    this.subject = subject;
  }
  
  public Integer getCatalog() {
    return catalog;
  }
  
  public void setCatalog(Integer catalog) {
    this.catalog = catalog;
  }
  
  public Integer getMembers() {
    return members;
  }
  
  public void setMembers(Integer members) {
    this.members = members;
  }
  
  public String getLastMessage() {
    return lastMessage;
  }
  
  public void setLastMessage(String lastMessage) {
    this.lastMessage = lastMessage;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
