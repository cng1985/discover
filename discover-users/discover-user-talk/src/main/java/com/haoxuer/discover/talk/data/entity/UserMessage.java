package com.haoxuer.discover.talk.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.user.data.entity.UserInfo;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "user_message")
public class UserMessage extends AbstractEntity {
  
  
  /**
   * 对应的主题
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private UserMessageSubject subject;
  
  /**
   * 发消息的用户
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;
  
  /**
   * 消息
   */
  private String message;
  
  /**
   * 0为正常
   * 1为删除
   */
  private StoreState state;
  
  public UserMessageSubject getSubject() {
    return subject;
  }
  
  public void setSubject(UserMessageSubject subject) {
    this.subject = subject;
  }
  

  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }

  public StoreState getState() {
    return state;
  }

  public void setState(StoreState state) {
    this.state = state;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
