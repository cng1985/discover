package com.haoxuer.discover.talk.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.talk.data.dao.UserMessageSubjectDao;
import com.haoxuer.discover.talk.data.entity.UserMessageSubject;

/**
* Created by imake on 2019年01月29日15:40:37.
*/
@Repository

public class UserMessageSubjectDaoImpl extends CriteriaDaoImpl<UserMessageSubject, Long> implements UserMessageSubjectDao {

	@Override
	public UserMessageSubject findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public UserMessageSubject save(UserMessageSubject bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public UserMessageSubject deleteById(Long id) {
		UserMessageSubject entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<UserMessageSubject> getEntityClass() {
		return UserMessageSubject.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}