package com.haoxuer.discover.talk.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.talk.data.dao.UserMessageMemberDao;
import com.haoxuer.discover.talk.data.entity.UserMessageMember;

/**
* Created by imake on 2019年01月29日15:40:37.
*/
@Repository

public class UserMessageMemberDaoImpl extends CriteriaDaoImpl<UserMessageMember, Long> implements UserMessageMemberDao {

	@Override
	public UserMessageMember findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public UserMessageMember save(UserMessageMember bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public UserMessageMember deleteById(Long id) {
		UserMessageMember entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<UserMessageMember> getEntityClass() {
		return UserMessageMember.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}