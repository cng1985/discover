package com.haoxuer.discover.notice.data.dao;


import com.haoxuer.discover.notice.data.entity.UserNotificationTime;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationTimeDao extends BaseDao<UserNotificationTime, Long> {
  
  UserNotificationTime findById(Long id);
  
  UserNotificationTime save(UserNotificationTime bean);
  
  UserNotificationTime updateByUpdater(Updater<UserNotificationTime> updater);
  
  UserNotificationTime deleteById(Long id);
  
  UserNotificationTime findByUser(Long user);
}