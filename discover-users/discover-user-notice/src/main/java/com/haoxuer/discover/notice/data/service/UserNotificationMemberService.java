package com.haoxuer.discover.notice.data.service;

import com.haoxuer.discover.notice.data.entity.UserNotificationMember;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationMemberService {
  
  UserNotificationMember findById(Long id);
  
  UserNotificationMember save(UserNotificationMember bean);
  
  UserNotificationMember update(UserNotificationMember bean);
  
  UserNotificationMember deleteById(Long id);
  
  UserNotificationMember[] deleteByIds(Long[] ids);
  
  Page<UserNotificationMember> page(Pageable pageable);
  
  Page<UserNotificationMember> page(Pageable pageable, Object search);
  
  
  List<UserNotificationMember> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}