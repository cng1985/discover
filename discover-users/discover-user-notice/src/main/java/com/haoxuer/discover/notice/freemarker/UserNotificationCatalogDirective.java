package com.haoxuer.discover.notice.freemarker;

import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.notice.data.entity.UserNotificationCatalog;
import com.haoxuer.discover.notice.data.service.UserNotificationCatalogService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class UserNotificationCatalogDirective extends ListDirective<UserNotificationCatalog> {
  @Override
  public List<UserNotificationCatalog> list() {

    Integer size = getInt("size");
    Integer id = getInt("id");
    Pageable pagex = new Pageable();
    pagex.setPageSize(size);
    pagex.setPageNumber(1);
    pagex.getFilters().add(Filter.eq("parent.id", id));
    pagex.getOrders().add(Order.asc("code"));
    return catalogService.list(0, size, pagex.getFilters(), pagex.getOrders());
  }

  @Autowired
  private UserNotificationCatalogService catalogService;
}
