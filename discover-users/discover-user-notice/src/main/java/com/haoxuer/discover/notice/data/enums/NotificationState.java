package com.haoxuer.discover.notice.data.enums;

public enum NotificationState {
  unread, read;
  
  @Override
  public String toString() {
    if (name().equals("read")) {
      return "已读";
    } else if (name().equals("unread")) {
      return "未读";
    }
    return super.toString();
  }
}
