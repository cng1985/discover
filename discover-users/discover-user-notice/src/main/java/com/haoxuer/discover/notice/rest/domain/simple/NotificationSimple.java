package com.haoxuer.discover.notice.rest.domain.simple;

import java.io.Serializable;

public class NotificationSimple implements Serializable {
  
  private Long id;
  
  private String title;
  
  private String note;
  
  private Long date;
  
  /**
   * 阅读状态
   */
  private Integer state;
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getNote() {
    return note;
  }
  
  public void setNote(String note) {
    this.note = note;
  }
  
  public Long getDate() {
    return date;
  }
  
  public void setDate(Long date) {
    this.date = date;
  }
  
  public Integer getState() {
    return state;
  }
  
  public void setState(Integer state) {
    this.state = state;
  }
}
