package com.haoxuer.discover.notice.data.dao;


import com.haoxuer.discover.notice.data.entity.UserNotificationCatalog;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationCatalogDao extends BaseDao<UserNotificationCatalog, Integer> {
  
  UserNotificationCatalog findById(Integer id);
  
  UserNotificationCatalog save(UserNotificationCatalog bean);
  
  UserNotificationCatalog updateByUpdater(Updater<UserNotificationCatalog> updater);
  
  UserNotificationCatalog deleteById(Integer id);
}