package com.haoxuer.discover.notice.data.dao.impl;

import com.haoxuer.discover.notice.data.entity.UserNotificationMember;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.notice.data.dao.UserNotificationMemberDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
@Repository

public class UserNotificationMemberDaoImpl extends CriteriaDaoImpl<UserNotificationMember, Long> implements UserNotificationMemberDao {

  @Override
  public UserNotificationMember findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserNotificationMember save(UserNotificationMember bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserNotificationMember deleteById(Long id) {
    UserNotificationMember entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<UserNotificationMember> getEntityClass() {
    return UserNotificationMember.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}