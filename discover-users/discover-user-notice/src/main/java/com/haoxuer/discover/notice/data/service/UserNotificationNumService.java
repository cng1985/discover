package com.haoxuer.discover.notice.data.service;

import com.haoxuer.discover.notice.data.entity.UserNotificationNum;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationNumService {
  
  UserNotificationNum findById(Long id);
  
  UserNotificationNum save(UserNotificationNum bean);
  
  UserNotificationNum update(UserNotificationNum bean);
  
  UserNotificationNum deleteById(Long id);
  
  UserNotificationNum[] deleteByIds(Long[] ids);
  
  Page<UserNotificationNum> page(Pageable pageable);
  
  Page<UserNotificationNum> page(Pageable pageable, Object search);
  
  
  List<UserNotificationNum> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}