package com.haoxuer.discover.notice.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.notice.data.dao.UserNotificationMemberDao;
import com.haoxuer.discover.notice.data.entity.UserNotificationMember;
import com.haoxuer.discover.notice.data.service.UserNotificationMemberService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年01月02日11:12:13.
 */


@Scope("prototype")
@Service
@Transactional
public class UserNotificationMemberServiceImpl implements UserNotificationMemberService {
  
  private UserNotificationMemberDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public UserNotificationMember findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public UserNotificationMember save(UserNotificationMember bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationMember update(UserNotificationMember bean) {
    Updater<UserNotificationMember> updater = new Updater<UserNotificationMember>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public UserNotificationMember deleteById(Long id) {
    UserNotificationMember bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationMember[] deleteByIds(Long[] ids) {
    UserNotificationMember[] beans = new UserNotificationMember[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(UserNotificationMemberDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<UserNotificationMember> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<UserNotificationMember> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<UserNotificationMember> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}