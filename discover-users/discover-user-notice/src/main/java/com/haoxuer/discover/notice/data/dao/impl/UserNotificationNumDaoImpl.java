package com.haoxuer.discover.notice.data.dao.impl;

import com.haoxuer.discover.notice.data.entity.UserNotificationNum;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.notice.data.dao.UserNotificationNumDao;
import com.haoxuer.discover.user.data.entity.UserInfo;
import java.util.Date;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
@Repository

public class UserNotificationNumDaoImpl extends CriteriaDaoImpl<UserNotificationNum, Long> implements UserNotificationNumDao {

  @Override
  public UserNotificationNum findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserNotificationNum save(UserNotificationNum bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserNotificationNum deleteById(Long id) {
    UserNotificationNum entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public UserNotificationNum findByUser(Long user) {
    UserNotificationNum result = null;
    Finder finder = Finder.create();
    finder.append("from UserNotificationNum t where t.user.id =:uid");
    finder.setParam("uid", user);
    result = this.findOne(finder);
    if (result == null) {
      result = new UserNotificationNum();
      result.setUser(UserInfo.fromId(user));
      result.setMid(0L);
      this.add(result);
    }
    result.setLastDate(new Date());
    return result;
  }

  @Override
  protected Class<UserNotificationNum> getEntityClass() {
    return UserNotificationNum.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}