package com.haoxuer.discover.notice.controller.admin;

import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.notice.data.entity.UserNotification;
import com.haoxuer.discover.notice.data.entity.UserNotificationMember;
import com.haoxuer.discover.notice.data.service.UserNotificationMemberService;
import com.haoxuer.discover.notice.data.service.UserNotificationService;
import com.haoxuer.discover.notice.data.so.UserNotificationMemberSo;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */


@Scope("prototype")
@Controller
public class UserNotificationMemberAction {

  public static final String MODEL = "model";

  public static final String REDIRECT_LIST_HTML = "redirect:/admin/usernotificationmember/view_list.htm";

  private static final Logger log = LoggerFactory.getLogger(UserNotificationMemberAction.class);

  @Autowired
  private UserNotificationMemberService manager;

  @Autowired
  private UserNotificationService userNotificationService;


  @RequiresPermissions("usernotificationmember")
  @RequestMapping("/admin/usernotificationmember/view_list")
  public String list(Pageable pageable, UserNotificationMemberSo so, ModelMap model) {

    if (pageable == null) {
      pageable = new Pageable();
    }
    if (pageable.getOrders().isEmpty()) {
      pageable.getOrders().add(Order.desc("id"));
    }
    userNotificationService.updateNoticesByNum(UserUtil.getCurrentShiroUser().getId());
    pageable.getFilters().addAll(FilterUtils.getFilters(so));
    pageable.getFilters().add(Filter.eq("user.id", UserUtil.getCurrentShiroUser().getId()));
    Page<UserNotificationMember> pagination = manager.page(pageable);
    model.addAttribute("list", pagination.getContent());
    model.addAttribute("page", pagination);
    model.addAttribute("so", so);
    return "/admin/usernotificationmember/list";
  }

  @RequiresPermissions("usernotificationmember")
  @RequestMapping("/admin/usernotificationmember/sendlist")
  public String sendList(Pageable pageable, UserNotificationMemberSo so, ModelMap model) {

    if (pageable == null) {
      pageable = new Pageable();
    }
    if (pageable.getOrders().isEmpty()) {
      pageable.getOrders().add(Order.desc("id"));
    }
    pageable.getFilters().addAll(FilterUtils.getFilters(so));
    pageable.getFilters().add(Filter.eq("author.id", UserUtil.getCurrentShiroUser().getId()));
    Page<UserNotification> pagination = userNotificationService.page(pageable);
    model.addAttribute("list", pagination.getContent());
    model.addAttribute("page", pagination);
    model.addAttribute("so", so);
    return "/admin/usernotificationmember/sendlist";
  }

  @RequiresPermissions("usernotificationmember")
  @RequestMapping("/admin/usernotificationmember/compose")
  public String compose(ModelMap model) {
    return "/admin/usernotificationmember/compose";
  }

  @RequiresPermissions("usernotificationmember")
  @RequestMapping("/admin/usernotificationmember/sendmail")
  public String sendMail(ModelMap model, UserNotification notification, Long user) {
    notification.setStoreState(StoreState.normal);
    notification.setAuthor(UserUtil.getUser());
    userNotificationService.send(user, notification);
    return REDIRECT_LIST_HTML;
  }

  private void initRedirectData(Pageable pageable, RedirectAttributes redirectAttributes) {
    redirectAttributes.addAttribute("pageNumber", pageable.getPageNumber());
  }
}