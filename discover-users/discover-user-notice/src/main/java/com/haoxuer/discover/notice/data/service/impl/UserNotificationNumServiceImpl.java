package com.haoxuer.discover.notice.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.notice.data.dao.UserNotificationNumDao;
import com.haoxuer.discover.notice.data.entity.UserNotificationNum;
import com.haoxuer.discover.notice.data.service.UserNotificationNumService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年01月02日11:12:13.
 */


@Scope("prototype")
@Service
@Transactional
public class UserNotificationNumServiceImpl implements UserNotificationNumService {
  
  private UserNotificationNumDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public UserNotificationNum findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public UserNotificationNum save(UserNotificationNum bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationNum update(UserNotificationNum bean) {
    Updater<UserNotificationNum> updater = new Updater<UserNotificationNum>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public UserNotificationNum deleteById(Long id) {
    UserNotificationNum bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationNum[] deleteByIds(Long[] ids) {
    UserNotificationNum[] beans = new UserNotificationNum[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(UserNotificationNumDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<UserNotificationNum> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<UserNotificationNum> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<UserNotificationNum> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}