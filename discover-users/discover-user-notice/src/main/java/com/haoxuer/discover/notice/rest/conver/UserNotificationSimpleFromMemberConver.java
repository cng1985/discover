package com.haoxuer.discover.notice.rest.conver;

import com.haoxuer.discover.notice.rest.domain.simple.NotificationSimple;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.notice.data.entity.UserNotificationMember;

public class UserNotificationSimpleFromMemberConver implements Conver<NotificationSimple, UserNotificationMember> {
  @Override
  public NotificationSimple conver(UserNotificationMember source) {
    NotificationSimple result = new NotificationSimple();
    if (source.getNotification() != null) {
      result.setTitle(source.getNotification().getTitle());
      result.setNote(source.getNotification().getNote());
    }
    if (source.getAddDate() != null) {
      result.setDate(source.getAddDate().getTime());
    }
    result.setId(source.getId());
    if (source.getState() != null) {
      result.setState(source.getState().ordinal());
    } else {
      result.setState(1);
    }
    return result;
  }
}
