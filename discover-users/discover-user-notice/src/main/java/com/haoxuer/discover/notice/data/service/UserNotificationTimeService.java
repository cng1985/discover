package com.haoxuer.discover.notice.data.service;

import com.haoxuer.discover.notice.data.entity.UserNotificationTime;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationTimeService {
  
  UserNotificationTime findById(Long id);
  
  UserNotificationTime save(UserNotificationTime bean);
  
  UserNotificationTime update(UserNotificationTime bean);
  
  UserNotificationTime deleteById(Long id);
  
  UserNotificationTime[] deleteByIds(Long[] ids);
  
  Page<UserNotificationTime> page(Pageable pageable);
  
  Page<UserNotificationTime> page(Pageable pageable, Object search);
  
  
  List<UserNotificationTime> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}