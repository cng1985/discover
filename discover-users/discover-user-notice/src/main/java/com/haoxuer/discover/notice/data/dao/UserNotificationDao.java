package com.haoxuer.discover.notice.data.dao;


import com.haoxuer.discover.notice.data.entity.UserNotification;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationDao extends BaseDao<UserNotification, Long> {
  
  UserNotification findById(Long id);
  
  UserNotification save(UserNotification bean);
  
  UserNotification updateByUpdater(Updater<UserNotification> updater);
  
  UserNotification deleteById(Long id);
  
  UserNotification send(UserNotification bean, Long... users);
  
}