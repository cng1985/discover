package com.haoxuer.discover.notice.data.dao.impl;

import com.haoxuer.discover.notice.data.dao.UserNotificationTimeDao;
import com.haoxuer.discover.notice.data.entity.UserNotificationTime;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.user.data.entity.UserInfo;
import java.util.Date;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
@Repository

public class UserNotificationTimeDaoImpl extends CriteriaDaoImpl<UserNotificationTime, Long> implements UserNotificationTimeDao {

  @Override
  public UserNotificationTime findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserNotificationTime save(UserNotificationTime bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserNotificationTime deleteById(Long id) {
    UserNotificationTime entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public UserNotificationTime findByUser(Long user) {
    UserNotificationTime result = null;
    Finder finder = Finder.create();
    finder.append("from UserNotificationTime t where t.user.id =:uid");
    finder.setParam("uid", user);
    result = this.findOne(finder);
    if (result == null) {
      result = new UserNotificationTime();
      result.setUser(UserInfo.fromId(user));
      result.setLastDate(new Date());
      this.add(result);
    }
    return result;
  }

  @Override
  protected Class<UserNotificationTime> getEntityClass() {
    return UserNotificationTime.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}