package com.haoxuer.discover.notice.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.notice.data.enums.NotificationState;

import javax.persistence.*;

@Entity
@Table(name = "user_notification_member")
public class UserNotificationMember extends AbstractEntity {

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "notificationid")
  private UserNotification notification;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "uid")
  private User user;

  /**
   * 阅读状态 1为已读0为未读
   */
  private NotificationState state;

  public UserNotification getNotification() {
    return notification;
  }

  public void setNotification(UserNotification notification) {
    this.notification = notification;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public NotificationState getState() {
    return state;
  }

  public void setState(NotificationState state) {
    this.state = state;
  }


}
