package com.haoxuer.discover.notice.data.dao;


import com.haoxuer.discover.notice.data.entity.UserNotificationMember;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationMemberDao extends BaseDao<UserNotificationMember, Long> {
  
  UserNotificationMember findById(Long id);
  
  UserNotificationMember save(UserNotificationMember bean);
  
  UserNotificationMember updateByUpdater(Updater<UserNotificationMember> updater);
  
  UserNotificationMember deleteById(Long id);
}