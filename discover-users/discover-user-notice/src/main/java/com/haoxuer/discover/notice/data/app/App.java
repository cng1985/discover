package com.haoxuer.discover.notice.data.app;


import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);

    File view = new File("E:\\mvnspace\\usite\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
    make.setView(view);
    
    List<Class<?>> cs = new ArrayList<Class<?>>();

    make.setDao(false);
    make.setService(true);
    make.setView(false);
    make.setAction(false);
    make.makes(cs);
    System.out.println("ok");
  }
}
