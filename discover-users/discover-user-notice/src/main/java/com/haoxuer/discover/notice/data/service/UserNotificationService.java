package com.haoxuer.discover.notice.data.service;

import com.haoxuer.discover.notice.data.entity.UserNotification;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationService {
  
  UserNotification findById(Long id);
  
  UserNotification push(UserNotification bean);

  /**
   * 保存到草稿箱
   * @param bean
   * @return
   */
  UserNotification draft(UserNotification bean);


  
  UserNotification pull(UserNotification bean);
  
  UserNotification send(Long user, UserNotification bean);
  
  UserNotification send(UserNotification bean, Long... users);
  
  public void updateNotices(Long user);
  
  public void updateNoticesByNum(Long user);
  
  
  public void updateNotices(Long user, Integer catalog);
  
  UserNotification update(UserNotification bean);
  
  UserNotification deleteById(Long id);
  
  UserNotification[] deleteByIds(Long[] ids);
  
  Page<UserNotification> page(Pageable pageable);
  
  Page<UserNotification> page(Pageable pageable, Object search);
  
  
  List<UserNotification> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}