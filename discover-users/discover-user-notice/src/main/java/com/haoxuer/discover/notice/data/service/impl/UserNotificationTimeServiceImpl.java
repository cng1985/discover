package com.haoxuer.discover.notice.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.notice.data.dao.UserNotificationTimeDao;
import com.haoxuer.discover.notice.data.entity.UserNotificationTime;
import com.haoxuer.discover.notice.data.service.UserNotificationTimeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年01月02日11:12:13.
 */


@Scope("prototype")
@Service
@Transactional
public class UserNotificationTimeServiceImpl implements UserNotificationTimeService {
  
  private UserNotificationTimeDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public UserNotificationTime findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public UserNotificationTime save(UserNotificationTime bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationTime update(UserNotificationTime bean) {
    Updater<UserNotificationTime> updater = new Updater<UserNotificationTime>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public UserNotificationTime deleteById(Long id) {
    UserNotificationTime bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationTime[] deleteByIds(Long[] ids) {
    UserNotificationTime[] beans = new UserNotificationTime[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(UserNotificationTimeDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<UserNotificationTime> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<UserNotificationTime> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<UserNotificationTime> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}