package com.haoxuer.discover.notice.rest.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class NotificationReadRequest extends RequestUserTokenObject {
  
  private Long id;
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
}
