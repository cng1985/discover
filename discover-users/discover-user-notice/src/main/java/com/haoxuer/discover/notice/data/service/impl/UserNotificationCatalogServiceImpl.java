package com.haoxuer.discover.notice.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.notice.data.dao.UserNotificationCatalogDao;
import com.haoxuer.discover.notice.data.entity.UserNotificationCatalog;
import com.haoxuer.discover.notice.data.service.UserNotificationCatalogService;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年01月02日11:12:13.
 */


@Scope("prototype")
@Service
@Transactional
public class UserNotificationCatalogServiceImpl implements UserNotificationCatalogService {
  
  private UserNotificationCatalogDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public UserNotificationCatalog findById(Integer id) {
    return dao.findById(id);
  }
  
  @Override
  public List<UserNotificationCatalog> findByTops(Integer pid) {
    LinkedList<UserNotificationCatalog> result = new LinkedList<UserNotificationCatalog>();
    UserNotificationCatalog catalog = dao.findById(pid);
    if (catalog != null) {
      while (catalog != null && catalog.getParent() != null) {
        result.addFirst(catalog);
        catalog = dao.findById(catalog.getParentId());
      }
      result.addFirst(catalog);
    }
    return result;
  }
  
  
  @Override
  public List<UserNotificationCatalog> child(Integer id, Integer max) {
    List<Order> orders = new ArrayList<Order>();
    orders.add(Order.asc("code"));
    List<Filter> list = new ArrayList<Filter>();
    list.add(Filter.eq("parent.id", id));
    return dao.list(0, max, list, orders);
  }
  
  @Override
  @Transactional
  public UserNotificationCatalog save(UserNotificationCatalog bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationCatalog update(UserNotificationCatalog bean) {
    Updater<UserNotificationCatalog> updater = new Updater<UserNotificationCatalog>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public UserNotificationCatalog deleteById(Integer id) {
    UserNotificationCatalog bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public UserNotificationCatalog[] deleteByIds(Integer[] ids) {
    UserNotificationCatalog[] beans = new UserNotificationCatalog[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(UserNotificationCatalogDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<UserNotificationCatalog> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<UserNotificationCatalog> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<UserNotificationCatalog> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}