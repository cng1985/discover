package com.haoxuer.discover.notice.controller.rest;

import com.haoxuer.discover.notice.rest.domain.page.NotificationPage;
import com.haoxuer.discover.notice.rest.domain.request.NotificationPageRequest;
import com.haoxuer.discover.notice.rest.domain.request.NotificationReadRequest;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.notice.rest.api.NotificationApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest/sys")
@RestController
public class NotificationRestController {
  
  
  @RequestMapping("/notification/page")
  public NotificationPage page(NotificationPageRequest request) {
    return api.page(request);
  }
  
  @RequestMapping("/notification/read")
  public ResponseObject read(NotificationReadRequest request) {
    return api.read(request);
  }
  
  @Autowired
  private NotificationApi api;
}
