package com.haoxuer.discover.notice.rest.resource;

import com.haoxuer.discover.notice.rest.domain.page.NotificationPage;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.notice.data.dao.UserNotificationDao;
import com.haoxuer.discover.notice.data.dao.UserNotificationMemberDao;
import com.haoxuer.discover.notice.data.entity.UserNotificationMember;
import com.haoxuer.discover.notice.data.enums.NotificationState;
import com.haoxuer.discover.notice.rest.api.NotificationApi;
import com.haoxuer.discover.notice.rest.conver.UserNotificationSimpleFromMemberConver;
import com.haoxuer.discover.notice.rest.domain.request.NotificationPageRequest;
import com.haoxuer.discover.notice.rest.domain.request.NotificationReadRequest;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.utils.ConverResourceUtils;
import com.haoxuer.discover.user.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class NotificationResource implements NotificationApi {
  
  
  @Autowired
  private UserNotificationDao notificationDao;
  
  @Autowired
  private UserInfoDao userDao;
  
  @Autowired
  private UserNotificationMemberDao notificationMemberDao;
  
  @Override
  public NotificationPage page(NotificationPageRequest request) {
    NotificationPage result = new NotificationPage();
    Long uid = UserUtils.getApp(request.getUserToken());
    UserInfo user = userDao.findById(uid);
    if (user == null) {
      result.setMsg("该用户不存在!");
      result.setCode(-101);
      return result;
    }
    Pageable pageable = new Pageable();
    pageable.setPageSize(request.getSize());
    pageable.setPageNo(request.getNo());
    pageable.getFilters().add(Filter.eq("user.id", uid));
    Page<UserNotificationMember> page = notificationMemberDao.page(pageable);
    ConverResourceUtils.coverPage(result, page, new UserNotificationSimpleFromMemberConver());
    
    return result;
  }
  
  @Override
  public ResponseObject read(NotificationReadRequest request) {
    ResponseObject result = new ResponseObject();
    Long uid = UserUtils.getApp(request.getUserToken());
    UserInfo user = userDao.findById(uid);
    if (user == null) {
      result.setMsg("该用户不存在!");
      result.setCode(-101);
      return result;
    }
    UserNotificationMember notificationMember = notificationMemberDao.findById(request.getId());
    if (notificationMember == null) {
      result.setMsg("无效id!");
      result.setCode(-102);
      return result;
    }
    if (notificationMember.getUser() == null) {
      result.setMsg("数据异常!");
      result.setCode(-103);
      return result;
    }
    if (!notificationMember.getUser().getId().equals(uid)) {
      result.setMsg("非法操作!");
      result.setCode(-104);
      return result;
    }
    notificationMember.setState(NotificationState.read);
    return result;
  }
}
