package com.haoxuer.discover.notice.data.dao.impl;

import com.haoxuer.discover.notice.data.entity.UserNotificationCatalog;
import com.haoxuer.discover.data.core.CatalogDaoImpl;
import com.haoxuer.discover.notice.data.dao.UserNotificationCatalogDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
@Repository

public class UserNotificationCatalogDaoImpl extends CatalogDaoImpl<UserNotificationCatalog, Integer> implements UserNotificationCatalogDao {

  @Override
  public UserNotificationCatalog findById(Integer id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserNotificationCatalog save(UserNotificationCatalog bean) {

    add(bean);


    return bean;
  }

  @Override
  public UserNotificationCatalog deleteById(Integer id) {
    UserNotificationCatalog entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<UserNotificationCatalog> getEntityClass() {
    return UserNotificationCatalog.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}