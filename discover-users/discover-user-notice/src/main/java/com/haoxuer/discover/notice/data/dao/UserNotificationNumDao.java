package com.haoxuer.discover.notice.data.dao;


import com.haoxuer.discover.notice.data.entity.UserNotificationNum;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationNumDao extends BaseDao<UserNotificationNum, Long> {
  
  UserNotificationNum findById(Long id);
  
  UserNotificationNum save(UserNotificationNum bean);
  
  UserNotificationNum updateByUpdater(Updater<UserNotificationNum> updater);
  
  UserNotificationNum deleteById(Long id);
  
  UserNotificationNum findByUser(Long user);
  
}