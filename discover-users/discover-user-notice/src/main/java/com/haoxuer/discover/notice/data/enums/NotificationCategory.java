package com.haoxuer.discover.notice.data.enums;

public enum NotificationCategory {
  
  broadcast, single, push, pull;
  
  @Override
  public String toString() {
    if (name().equals("broadcast")) {
      return "广播";
    } else if (name().equals("single")) {
      return "私信";
    } else if (name().equals("push")) {
      return "广播";
    } else if (name().equals("pull")) {
      return "广播";
    }
    return super.toString();
  }
}
