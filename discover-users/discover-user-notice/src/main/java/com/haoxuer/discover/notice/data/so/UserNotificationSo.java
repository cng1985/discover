package com.haoxuer.discover.notice.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;

import java.io.Serializable;

/**
 * Created by imake on 2018年01月02日15:14:49.
 */
public class UserNotificationSo implements Serializable {

  @Search(name = "title",operator = Filter.Operator.isNull)
  private String title;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
