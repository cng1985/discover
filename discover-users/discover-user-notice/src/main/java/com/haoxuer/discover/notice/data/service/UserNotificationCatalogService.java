package com.haoxuer.discover.notice.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.notice.data.entity.UserNotificationCatalog;
import java.util.List;

/**
 * Created by imake on 2018年01月02日11:12:13.
 */
public interface UserNotificationCatalogService {
  
  UserNotificationCatalog findById(Integer id);
  
  UserNotificationCatalog save(UserNotificationCatalog bean);
  
  UserNotificationCatalog update(UserNotificationCatalog bean);
  
  UserNotificationCatalog deleteById(Integer id);
  
  UserNotificationCatalog[] deleteByIds(Integer[] ids);
  
  Page<UserNotificationCatalog> page(Pageable pageable);
  
  Page<UserNotificationCatalog> page(Pageable pageable, Object search);
  
  List<UserNotificationCatalog> findByTops(Integer pid);
  
  
  List<UserNotificationCatalog> child(Integer id, Integer max);
  
  List<UserNotificationCatalog> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}