package com.haoxuer.discover.notice.rest.api;

import com.haoxuer.discover.notice.rest.domain.page.NotificationPage;
import com.haoxuer.discover.notice.rest.domain.request.NotificationPageRequest;
import com.haoxuer.discover.notice.rest.domain.request.NotificationReadRequest;
import com.haoxuer.discover.rest.base.ResponseObject;

/**
 * 通知接口
 */
public interface NotificationApi {
  
  /**
   * 获取某个用户的通知
   *
   * @param request
   * @return
   */
  NotificationPage page(NotificationPageRequest request);
  
  
  /**
   * 某个人读了某条通知
   *
   * @param request
   * @return
   */
  ResponseObject read(NotificationReadRequest request);
}
