package com.haoxuer.discover.notice.rest.conver;

import com.haoxuer.discover.notice.rest.domain.simple.NotificationSimple;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.notice.data.entity.UserNotification;

public class NotificationSimpleConver implements Conver<NotificationSimple, UserNotification> {
  @Override
  public NotificationSimple conver(UserNotification source) {
    NotificationSimple result = new NotificationSimple();
    result.setId(source.getId());
    result.setTitle(source.getTitle());
    result.setNote(source.getNote());
    if (source.getAddDate() != null) {
      result.setDate(source.getAddDate().getTime());
    }
    return result;
  }
}
