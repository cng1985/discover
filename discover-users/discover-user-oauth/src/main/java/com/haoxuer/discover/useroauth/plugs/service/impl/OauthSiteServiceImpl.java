package com.haoxuer.discover.useroauth.plugs.service.impl;

import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.useroauth.plugs.base.OauthPlugin;
import com.haoxuer.discover.useroauth.plugs.domain.OauthInfo;
import com.haoxuer.discover.useroauth.plugs.service.OauthSiteService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.stereotype.Service;


@Service()
public class OauthSiteServiceImpl implements OauthSiteService {
  
  @Resource
  private List<OauthPlugin> oauthPlugins = new ArrayList<OauthPlugin>();
  
  @Resource
  private Map<String, OauthPlugin> oauthPluginMap = new HashMap<String, OauthPlugin>();
  
  @Override
  public List<OauthPlugin> getOauthPlugins() {
    Collections.sort(oauthPlugins);
    return oauthPlugins;
  }
  
  public List<OauthPlugin> getOauthPlugins(final boolean isEnabled) {
    List<OauthPlugin> result = new ArrayList<OauthPlugin>();
    CollectionUtils.select(oauthPlugins, new Predicate() {
      @Override
      public boolean evaluate(Object object) {
        OauthPlugin paymentPlugin = (OauthPlugin) object;
        return paymentPlugin.getIsEnabled() == isEnabled;
      }
    }, result);
    Collections.sort(result);
    return result;
  }
  
  public OauthPlugin getOauthPlugin(String id) {
    return oauthPluginMap.get(id);
  }
  
  @Override
  public List<OauthInfo> list() {
    List<OauthInfo> result = new ArrayList<>();
    List<OauthPlugin> plugins = getOauthPlugins(true);
    if (plugins != null) {
      for (OauthPlugin plugin : plugins) {
        result.add(plugin.info());
      }
    }
    return result;
  }
  
  @Override
  public OauthResponse handle(String plug, String code) {
    OauthPlugin oauthPlugin = getOauthPlugin(plug);
    if (oauthPlugin != null) {
      return oauthPlugin.handle(code);
    } else {
      return null;
    }
  }
}
