/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.useroauth.plugs.base;

import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.useroauth.plugs.domain.OauthInfo;
import com.haoxuer.discover.plug.api.IPlugin;

/**
 * Plugin - 存储
 */
public abstract class OauthPlugin extends IPlugin {


  /**
   * 通过code进行第三方登陆
   *
   * @return
   */
  public abstract OauthInfo info();
  
  
  public abstract OauthResponse handle(String code);


  @Override
  public String getBaseUrl() {
    return "admin/plugin_oauth/";
  }

  @Override
  public String getAuthor() {
    return "ada.young";
  }


  @Override
  public String getSiteUrl() {
    return "http://www.haoxuer.com";
  }
  
  
}