package com.haoxuer.discover.useroauth.plugs.weibo;

import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("weiBoPlugController")
@RequestMapping("/admin/plugin_oauth/weibo")
public class WeiBoPlugController extends PlugTemplateController {
  
  @Resource(name = "weibo_plugin")
  private WeiBoPlugin weiBoPlugin;
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return weiBoPlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return weiBoPlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_oauth";
  }
  
  @Override
  public String getSettingView() {
    return "weibo";
  }
}
