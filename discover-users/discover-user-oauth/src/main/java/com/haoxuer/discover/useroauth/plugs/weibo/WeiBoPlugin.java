package com.haoxuer.discover.useroauth.plugs.weibo;

import com.github.scribejava.apis.SinaWeiboApi20;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.user.oauth.impl.WeiBoHander;
import com.haoxuer.discover.useroauth.plugs.domain.OauthInfo;
import com.haoxuer.discover.useroauth.plugs.base.OauthPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("weibo_plugin")
public class WeiBoPlugin extends OauthPlugin {
  private Logger logger = LoggerFactory.getLogger("oauth");
  
  @Override
  public OauthInfo info() {
    OauthInfo result = new OauthInfo();
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String apikey = pluginConfig.getAttribute("apikey");
      String secret = pluginConfig.getAttribute("secret");
      String callback = pluginConfig.getAttribute("callback");
      String scope = pluginConfig.getAttribute("scope");
      String icon = pluginConfig.getAttribute("icon");
      String name = pluginConfig.getAttribute("name");
      String bg = pluginConfig.getAttribute("bg");
  
      OAuth20Service weibo = new ServiceBuilder(apikey)
          .apiSecret(secret)
          .callback(callback).scope(scope)
          .build(SinaWeiboApi20.instance());
      result.setUrl(weibo.getAuthorizationUrl());
      result.setIcon(icon);
      result.setName(name);
      result.setBg(bg);
    }
    return result;
  }
  
  @Override
  public OauthResponse handle(String code) {
    OauthResponse result = null;
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String apikey = pluginConfig.getAttribute("apikey");
      String secret = pluginConfig.getAttribute("secret");
      String callback = pluginConfig.getAttribute("callback");
      String scope = pluginConfig.getAttribute("scope");
      
      OAuth20Service weibo = new ServiceBuilder(apikey)
          .apiSecret(secret)
          .callback(callback).scope(scope)
          .build(SinaWeiboApi20.instance());
      OAuth2AccessToken tokenx = null;
      try {
        tokenx = weibo.getAccessToken(code);
      } catch (IOException e) {
        logger.error("Weibo 获取登陆信息 io错误", e);
      } catch (InterruptedException e) {
        logger.error("Weibo 获取登陆信息 Interrupted", e);
      } catch (ExecutionException e) {
        logger.error("Weibo 获取登陆信息 其他错误", e);
      }
      
      WeiBoHander hander = new WeiBoHander();
      result = hander.login(tokenx.getAccessToken(), null);
    }
    return result;
  }
  
  @Override
  public String getName() {
    return "微博第三方登陆插件";
  }
  
  @Override
  public String getVersion() {
    return "1.01";
  }
  

  @Override
  public String viewName() {
    return "weibo";
  }
}
