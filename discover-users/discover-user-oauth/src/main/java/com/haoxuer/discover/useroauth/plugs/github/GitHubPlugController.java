/*
 * 
 * 
 * 
 */
package com.haoxuer.discover.useroauth.plugs.github;

import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * github第三方登陆插件配置
 */
@Controller("gitHubPlugController")
@RequestMapping("/admin/plugin_oauth/github")
public class GitHubPlugController extends PlugTemplateController {

    @Resource(name = "github_plugin")
    private GitHubPlugin gitHubPlugin;

    @Resource(name = "pluginConfigServiceImpl")
    private PluginConfigService pluginConfigService;
    
    
    @Override
    public PluginConfig getPluginConfig() {
        return gitHubPlugin.getPluginConfig();
    }
    
    @Override
    public IPlugin getPlug() {
        return gitHubPlugin;
    }
    
    @Override
    public PluginConfigService getPluginConfigService() {
        return pluginConfigService;
    }
    
    @Override
    public String getView() {
        return "/admin/plugin_oauth";
    }
    
    @Override
    public String getSettingView() {
        return "github";
    }
}