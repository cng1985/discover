package com.haoxuer.discover.useroauth.plugs.service;

import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.useroauth.plugs.base.OauthPlugin;
import com.haoxuer.discover.useroauth.plugs.domain.OauthInfo;
import java.util.List;

public interface OauthSiteService {
  
  List<OauthPlugin> getOauthPlugins();
  
  List<OauthInfo> list();
  
  OauthResponse handle(String plug, String code);
}
