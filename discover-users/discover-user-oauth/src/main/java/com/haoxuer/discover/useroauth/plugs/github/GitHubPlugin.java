package com.haoxuer.discover.useroauth.plugs.github;

import com.github.scribejava.apis.GitHubApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.user.oauth.impl.GitHubHander;
import com.haoxuer.discover.useroauth.plugs.base.OauthPlugin;
import com.haoxuer.discover.useroauth.plugs.domain.OauthInfo;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component("github_plugin")
public class GitHubPlugin extends OauthPlugin {
  
  private Logger logger = LoggerFactory.getLogger("oauth");
  
  @Override
  public OauthInfo info() {
    OauthInfo result = new OauthInfo();
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String apikey = pluginConfig.getAttribute("apikey");
      String secret = pluginConfig.getAttribute("secret");
      String callback = pluginConfig.getAttribute("callback");
      String scope = pluginConfig.getAttribute("scope");
      String icon = pluginConfig.getAttribute("icon");
      String name = pluginConfig.getAttribute("name");
      String bg = pluginConfig.getAttribute("bg");
  
      OAuth20Service github = new ServiceBuilder(apikey)
          .apiSecret(secret)
          .callback(callback).scope(scope)
          .build(GitHubApi.instance());
      
      result.setUrl(github.getAuthorizationUrl());
      result.setIcon(icon);
      result.setName(name);
      result.setBg(bg);
  
    }
    return result;
  }
  
  
  @Override
  public OauthResponse handle(String code) {
    OauthResponse result = null;
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String apikey = pluginConfig.getAttribute("apikey");
      String secret = pluginConfig.getAttribute("secret");
      String callback = pluginConfig.getAttribute("callback");
      String scope = pluginConfig.getAttribute("scope");
      
      OAuth20Service github = new ServiceBuilder(apikey)
          .apiSecret(secret)
          .callback(callback).scope(scope)
          .build(GitHubApi.instance());
      
      OAuth2AccessToken tokenx = null;
      try {
        tokenx = github.getAccessToken(code);
      } catch (IOException e) {
        logger.error("GitHub 获取登陆信息 io错误", e);
      } catch (InterruptedException e) {
        logger.error("GitHub 获取登陆信息 Interrupted", e);
      } catch (ExecutionException e) {
        logger.error("GitHub 获取登陆信息 其他错误", e);
      }
      if (tokenx != null) {
        GitHubHander hubHander = new GitHubHander();
        result = hubHander.login(tokenx.getAccessToken(), null);
      }
    }
    return result;
  }
  
  @Override
  public String getName() {
    return "github第三方登陆插件";
  }
  
  @Override
  public String getVersion() {
    return "1.01";
  }
  

  @Override
  public String viewName() {
    return "github";
  }
}
