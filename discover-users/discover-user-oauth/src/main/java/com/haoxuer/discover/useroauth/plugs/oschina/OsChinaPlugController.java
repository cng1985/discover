package com.haoxuer.discover.useroauth.plugs.oschina;

import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller("osChinaPlugController")
@RequestMapping("/admin/plugin_oauth/oschina")
public class OsChinaPlugController extends PlugTemplateController {
  
  @Resource(name = "oschina_plugin")
  private OsChinaPlugin osChinaPlugin;
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return osChinaPlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return osChinaPlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_oauth";
  }
  
  @Override
  public String getSettingView() {
    return "oschina";
  }
}