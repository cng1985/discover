package com.haoxuer.discover.useroauth.controller.admin;

import com.haoxuer.discover.useroauth.plugs.service.OauthSiteService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller - 第三方登陆插件.
 */
@Controller("adminOauthPluginAction")
@RequestMapping("/admin/plugin_oauth")
public class OauthPluginAction {
  
  @Autowired
  private OauthSiteService oauthSiteService;
  
  /**
   * 列表.
   */
  @RequiresPermissions("plugin_oauth")
  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String list(ModelMap model) {
    model.addAttribute("plugins", oauthSiteService.getOauthPlugins());
    return "/admin/plugin_oauth/list";
  }
  
}