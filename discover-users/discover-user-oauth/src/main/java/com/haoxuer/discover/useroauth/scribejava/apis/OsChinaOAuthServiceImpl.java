package com.haoxuer.discover.useroauth.scribejava.apis;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.httpclient.HttpClient;
import com.github.scribejava.core.httpclient.HttpClientConfig;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.oauth.OAuth20Service;

public class OsChinaOAuthServiceImpl extends OAuth20Service {
  
  
  public OsChinaOAuthServiceImpl(DefaultApi20 api, String apiKey, String apiSecret, String callback, String scope, String state, String responseType, String userAgent, HttpClientConfig httpClientConfig, HttpClient httpClient) {
    super(api, apiKey, apiSecret, callback, scope, state, responseType, userAgent, httpClientConfig, httpClient);
  }
  
  @Override
  protected OAuthRequest createAccessTokenRequest(String code) {
    OAuthRequest request = super.createAccessTokenRequest(code);
//        if (!getConfig().hasGrantType()) {
//            request.addParameter(OAuthConstants.GRANT_TYPE, OAuthConstants.AUTHORIZATION_CODE);
//        }
    request.addParameter("dataType", "json");
    return request;
  }
  
  
}
