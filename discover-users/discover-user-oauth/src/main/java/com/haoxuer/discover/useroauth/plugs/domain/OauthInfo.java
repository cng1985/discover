package com.haoxuer.discover.useroauth.plugs.domain;

import com.haoxuer.discover.rest.base.ResponseObject;

public class OauthInfo extends ResponseObject {
  
  private String name;
  
  private String url;
  
  private String icon;
  
  private String bg;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getUrl() {
    return url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public String getIcon() {
    return icon;
  }
  
  public void setIcon(String icon) {
    this.icon = icon;
  }
  
  public String getBg() {
    return bg;
  }
  
  public void setBg(String bg) {
    this.bg = bg;
  }
}
