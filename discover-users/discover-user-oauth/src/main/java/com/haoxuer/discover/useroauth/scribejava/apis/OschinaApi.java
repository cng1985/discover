package com.haoxuer.discover.useroauth.scribejava.apis;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.extractors.OAuth2AccessTokenExtractor;
import com.github.scribejava.core.extractors.TokenExtractor;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Verb;

public class OschinaApi extends DefaultApi20 {
  
  private static final String AUTHORIZE_URL = "http://www.oschina.net/action/oauth2/authorize?client_id=%s&redirect_uri=%s";
  
  protected OschinaApi() {
  }
  
  private static class InstanceHolder {
    private static final OschinaApi INSTANCE = new OschinaApi();
  }
  
  public static OschinaApi instance() {
    return InstanceHolder.INSTANCE;
  }
  
  @Override
  public Verb getAccessTokenVerb() {
    return Verb.GET;
  }
  
  @Override
  public String getAccessTokenEndpoint() {
    return "http://www.oschina.net/action/openapi/token";
  }
  
  
  @Override
  public TokenExtractor<OAuth2AccessToken> getAccessTokenExtractor() {
    return OAuth2AccessTokenExtractor.instance();
  }
  
  @Override
  protected String getAuthorizationBaseUrl() {
    return "http://www.oschina.net/action/oauth2/authorize";
  }
}
