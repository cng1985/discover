package com.haoxuer.discover.userfriend.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.userfriend.data.entity.UserFriendRequest;

/**
* Created by imake on 2017年09月01日10:05:18.
*/
public interface UserFriendRequestDao extends BaseDao<UserFriendRequest,Long>{

	 UserFriendRequest findById(Long id);

	 UserFriendRequest save(UserFriendRequest bean);

	 UserFriendRequest updateByUpdater(Updater<UserFriendRequest> updater);

	 UserFriendRequest deleteById(Long id);
}