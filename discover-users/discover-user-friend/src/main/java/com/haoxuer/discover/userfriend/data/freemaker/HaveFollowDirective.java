package com.haoxuer.discover.userfriend.data.freemaker;

import com.haoxuer.discover.common.utils.DirectiveUtils;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import com.haoxuer.discover.userfriend.data.service.UserFollowService;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component("haveFollowDirective")
public class HaveFollowDirective implements TemplateDirectiveModel {

  @Autowired
  UserFollowService followService;

  @Override
  public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
    Long id = DirectiveUtils.getLong("id", params);
    Long user = UserUtil.getCurrentShiroUser().getId();
    if (!followService.haveFollow(user, id)) {
      if (body != null) {
        body.render(env.getOut());
      }
    }

  }
}
