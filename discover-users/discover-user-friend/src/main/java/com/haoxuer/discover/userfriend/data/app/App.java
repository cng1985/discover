package com.haoxuer.discover.userfriend.data.app;

import com.haoxuer.discover.userfriend.data.entity.UserFollow;
import com.haoxuer.discover.userfriend.data.entity.UserFriend;
import com.haoxuer.discover.userfriend.data.entity.UserFriendRequest;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        File file = new File("E:\\mvnspace\\xjob\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);
        make.setView(file);
        make.setDao(true);
        make.setService(true);
        make.setAction(false);
        make.setView(false);
        // UserOauthToken.
        make.makes(UserFollow.class);
        make.makes(UserFriend.class);
        make.makes(UserFriendRequest.class);

    }
}
