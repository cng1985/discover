package com.haoxuer.discover.userfriend.data.dao.impl;

import com.haoxuer.discover.userfriend.data.dao.UserFollowDao;
import com.haoxuer.discover.userfriend.data.entity.UserFollow;
import com.haoxuer.discover.data.core.Finder;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;

/**
 * Created by imake on 2017年09月01日10:05:18.
 */
@Repository

public class UserFollowDaoImpl extends CriteriaDaoImpl<UserFollow, Long> implements UserFollowDao {

  @Override
  public UserFollow findById(Long id, Long friendid) {
    Finder finder = Finder.create();
    finder.append("from UserFollow u where u.user.id =:uid");
    finder.setParam("uid", id);
    finder.append("  and u.follower.id =:fid");
    finder.setParam("fid", friendid);
    return findOne(finder);
  }

  @Override
  public UserFollow findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserFollow save(UserFollow bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserFollow deleteById(Long id) {
    UserFollow entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public Boolean haveFollow(Long user, Long follower) {
    Boolean result = new Boolean(false);
    UserFollow follow = findById(user, follower);
    if (follow!=null){
      result=true;
    }
    return result;
  }

  @Override
  protected Class<UserFollow> getEntityClass() {
    return UserFollow.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}