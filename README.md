﻿# discover

[![maven](https://img.shields.io/maven-central/v/com.haoxuer.discover/discover-website.svg)](http://mvnrepository.com/artifact/com.haoxuer.discover/discover-website)
[![QQ](https://img.shields.io/badge/chat-on%20QQ-ff69b4.svg?style=flat-square)](//shang.qq.com/wpa/qunwpa?idkey=d1a308945e4b2ff8aeb1711c2c7914342dae15e9ce7041e94756ab355430dc78)
[![Apache-2.0](https://img.shields.io/hexpm/l/plug.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)
[![使用IntelliJ IDEA开发维护](https://img.shields.io/badge/IntelliJ%20IDEA-提供支持-blue.svg)](https://www.jetbrains.com/idea/)
[![GitHub forks](https://img.shields.io/github/stars/haoxuer/discover.svg?style=social&logo=github&label=Stars)](https://github.com/haoxuer/discover)

> discover 是一个集成spring mvc+spring+hibernate的基础功能工程

### 定义功能
>一个模块由后台管理，接口，freemarker标签组成，给前端更大的自由。前端可以通过标签或者接口构建页面和功能

### 技术选型：

* **服务端**

* SSH (Spring、SpringMVC、Hibernate）
* 安全权限 Shiro
* 搜索工具 Lucene
* 缓存 Ehcache
* 视图模板 freemarker 
* 其它 Jsoup、gson

### 编码规范
1.项目结构规范
```
com.{公司域名}.{主工程}
com.{公司域名}.{主工程}.{子工程}


```
2.包结构规范
```
//主包结构
com.{公司域名}.{主工程}.{子工程}
com.{公司域名}.{主工程}.{子工程}.controller
com.{公司域名}.{主工程}.{子工程}.data
com.{公司域名}.{主工程}.{子工程}.freemaker
com.{公司域名}.{主工程}.{子工程}.web
com.{公司域名}.{主工程}.{子工程}.utils
com.{公司域名}.{主工程}.{子工程}.rest
com.{公司域名}.{主工程}.{子工程}.plugins
com.{公司域名}.{主工程}.{子工程}.web
com.{公司域名}.{主工程}.{子工程}.exception

//controller包子结构
com.{公司域名}.{主工程}.{子工程}.controller.admin
com.{公司域名}.{主工程}.{子工程}.controller.front
com.{公司域名}.{主工程}.{子工程}.controller.rest

//data包子结构
com.{公司域名}.{主工程}.{子工程}.data.dao
com.{公司域名}.{主工程}.{子工程}.data.entity
com.{公司域名}.{主工程}.{子工程}.data.enums
com.{公司域名}.{主工程}.{子工程}.data.service
com.{公司域名}.{主工程}.{子工程}.data.so

//rest包子结构
com.{公司域名}.{主工程}.{子工程}.rest.conver
com.{公司域名}.{主工程}.{子工程}.data.resources

```


### 使用访问
已经发布到maven中央仓库了
```
 <dependency>
      <groupId>com.haoxuer.discover</groupId>
      <artifactId>discover-website</artifactId>
      <version>2.2.3</version>
 </dependency>
```
### 交流方式

* QQ群:141837028   [discover开源QQ群](//shang.qq.com/wpa/qunwpa?idkey=d1a308945e4b2ff8aeb1711c2c7914342dae15e9ce7041e94756ab355430dc78)

* 例子网站 [ucms开源系统](https://www.haoxuer.com)

### 代码生成类CodeMake使用

```
  CodeMake make=	new CodeMake(TemplateSimpleDir.class,TemplateHibernateDir.class);
   make.setAction("com.youapp.controller.admin");//Controller类所在包
   //项目模板位置
   File view=new File("E:\\youweb\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
   make.setView(view);
   List<Class<?>> cs=new ArrayList<Class<?>>();
   cs.add(LeaveData.class);//实体对象
   make.setMenus("1,39,44");//后台菜单号
   make.setDao(false);//是否生成dao代码
   make.setService(false);//是否生成Service代码
   make.setView(false);//是否生成后台管理页面
   make.setAction(true);//是否生成Controller代码
   make.makes(cs);
```

### So类使用
#### 1.定义实体对象
```
@Entity
@Table(name = "bs_user")
public class User extends AbstractEntity {

    /**
     * 姓名
     */
    @Column(length = 50)
    private String name;
    
    /**
     * 所在学习
     */
    @Column(length = 50)
    private String school;    
    
    /**
     * 年龄
     */
    private int age;    
    
}    
```
#### 2.定义需要搜索的字段已经搜索条件
```
public class UserSo implements Serializable {

    @Search(name = "name",operator = Filter.Operator.like)
    private String name;

    Search(name = "sex", operator = Filter.Operator.eq,condition = Condition.AND)
    private String sex;
  
    public String getName() {
       return name;
    }

    public void setName(String name) {
       this.name = name;
    }
    
    public String getSex() {
      return sex;
     }
     
    public void setSex(String sex) {
       this.sex = sex;
    }   
    
    
}
```
#### 3.查询数据
```
	pageable.getFilters().addAll(FilterUtils.getFilters(so));
	Page<User> pagination = userService.page(pageable);
	
```

### 包介绍：

* com.haoxuer.discover.admin 后台管理功能
* com.haoxuer.discover.album 相册功能
* com.haoxuer.discover.area  地区功能
* com.haoxuer.discover.article 文章功能
* com.haoxuer.discover.data hibernate公共包
* com.haoxuer.discover.feed 动态功能
* com.haoxuer.discover.plugin 插件功能
* com.haoxuer.discover.question 问答功能
* com.haoxuer.discover.user.shiro shiro权限功能依赖用户模块
* com.haoxuer.discover.user 用户模块
* com.young.word 验证码生成
* com.young.security 各种安全工具

### 开源协议

如果您的网站使用了 discover, 请在网站页面页脚处保留 discover相关版权信息链接

### 已经使用的项目
* [ucms](https://gitee.com/cng1985/iwan)
* [adminstore](https://gitee.com/cng1985/adminstore)
* [umall](https://gitee.com/quhaodian/umall)
* [haodian](https://gitee.com/cng1985/iyelp)


    

