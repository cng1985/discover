package com.haoxuer.discover.chain;

import org.apache.commons.chain2.Processing;

public interface Command<Request,Response> {

  Processing execute(Request request,Response response);

}
