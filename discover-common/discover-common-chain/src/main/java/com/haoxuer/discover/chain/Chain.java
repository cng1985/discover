package com.haoxuer.discover.chain;

public interface Chain<Request,Response>  extends Command<Request,Response>  {

  <CMD extends Command> void addCommand(CMD command);

}
