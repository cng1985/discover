package com.haoxuer.discover.chain;

import org.apache.commons.chain2.Processing;

public class ExceptionCommand implements Command<DemoRequest, DemoResponse> {
  @Override
  public Processing execute(DemoRequest demoRequest, DemoResponse demoResponse) {
    if (demoResponse.getCode()==500){
      throw new DemoException();
    }
    return Processing.CONTINUE;
  }
}
