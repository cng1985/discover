package com.haoxuer.discover.chain;

import org.apache.commons.chain2.Processing;

public class TestCommand implements Command<DemoRequest, DemoResponse> {
  @Override
  public Processing execute(DemoRequest demoRequest, DemoResponse demoResponse) {
    demoResponse.setCode(500);
    return Processing.CONTINUE;
  }
}
