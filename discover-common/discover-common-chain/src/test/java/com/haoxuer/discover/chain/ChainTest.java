package com.haoxuer.discover.chain;

import com.haoxuer.discover.chain.impl.ChainBase;
import org.junit.Assert;
import org.junit.Test;

public class ChainTest {


  @Test
  public void  testOne(){

    Chain<DemoRequest,DemoResponse> chain=new ChainBase<>();

    chain.addCommand(new TestCommand());
    DemoRequest request=new DemoRequest();
    DemoResponse response=new DemoResponse();
    chain.execute(request,response);
    Assert.assertEquals(500,response.getCode());
  }


  @Test(expected= DemoException.class)
  public void  testTwo(){

    Chain<DemoRequest,DemoResponse> chain=new ChainBase<>();

    chain.addCommand(new TestCommand());
    chain.addCommand(new ExceptionCommand());


    DemoRequest request=new DemoRequest();
    DemoResponse response=new DemoResponse();
    chain.execute(request,response);
    Assert.assertEquals(500,response.getCode());
  }
}
