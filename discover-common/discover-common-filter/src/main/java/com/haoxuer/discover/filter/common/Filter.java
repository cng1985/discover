package com.haoxuer.discover.filter.common;


import com.haoxuer.discover.filter.api.FilterTemplate;

public interface Filter extends FilterTemplate<HandlerRequest, HandlerResponse,FilterChain> {

  @Override
  void doFilter(HandlerRequest request, HandlerResponse response, FilterChain filterChain);

}
