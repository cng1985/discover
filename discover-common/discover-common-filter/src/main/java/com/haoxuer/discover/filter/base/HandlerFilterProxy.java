package com.haoxuer.discover.filter.base;


import com.haoxuer.discover.filter.common.*;

public class HandlerFilterProxy implements Filter {

  private final Handler delegateServlet;

  public HandlerFilterProxy(Handler servlet) {
    this.delegateServlet = servlet;
  }


  @Override
  public void doFilter(HandlerRequest request, HandlerResponse response, FilterChain filterChain) {
    delegateServlet.service(request, response);
  }
}
