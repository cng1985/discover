package com.haoxuer.discover.filter.common;

public interface HandlerResponse {

  String getMsg();

  void setMsg(String msg);

  int getCode();

  void setCode(int code);
}
