package com.haoxuer.discover.filter.impl;


import com.haoxuer.discover.filter.api.FilterChainTemplate;
import com.haoxuer.discover.filter.api.FilterTemplate;
import com.haoxuer.discover.filter.api.HandlerTemplate;

public class HandlerFilterTemplateProxy<Request,Response,ChainTemplate extends FilterChainTemplate<Request,Response>> implements FilterTemplate<Request,Response,ChainTemplate> {

  private final HandlerTemplate delegateServlet;

  public HandlerFilterTemplateProxy(HandlerTemplate servlet) {
    this.delegateServlet = servlet;
  }

  @Override
  public void doFilter(Request request, Response response, ChainTemplate filterChain) {
    delegateServlet.service(request, response);
  }

}
