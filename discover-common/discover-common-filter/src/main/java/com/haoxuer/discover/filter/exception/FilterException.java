package com.haoxuer.discover.filter.exception;

public class FilterException extends RuntimeException {
  public FilterException(String s) {
    super(s);
  }
}
