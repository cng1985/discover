package com.haoxuer.discover.filter.common;

public class HandlerRequestWrapper implements HandlerRequest {

  private HandlerRequest request;

  public HandlerRequestWrapper(HandlerRequest request) {
    if (request == null) {
      throw new IllegalArgumentException("Request cannot be null");
    }
    this.request = request;
  }

  @Override
  public Object getAttribute(String name) {
    return request.getAttribute(name);
  }

  @Override
  public void setAttribute(String name, Object o) {
    request.setAttribute(name,o);
  }

  @Override
  public void removeAttribute(String name) {
    request.removeAttribute(name);
  }

  @Override
  public String getParameter(String name) {
    return request.getParameter(name);
  }
}
