package com.haoxuer.discover.filter.base;

import com.haoxuer.discover.filter.common.HandlerRequest;

import java.util.HashMap;
import java.util.Map;

public class RestRequest implements HandlerRequest {

  private Map<String, Object> datas = new HashMap<>();

  private Map<String, String> parameters = new HashMap<>();

  public void putParameter(String name, String value) {
    this.parameters.put(name, value);
  }

  @Override
  public Object getAttribute(String name) {
    return datas.get(name);
  }

  @Override
  public void setAttribute(String name, Object o) {
    datas.put(name, o);
  }


  @Override
  public void removeAttribute(String name) {
    datas.remove(name);
  }

  @Override
  public String getParameter(String name) {
    return parameters.get(name);
  }
}
