package com.haoxuer.discover.filter.common;

import com.haoxuer.discover.filter.api.HandlerTemplate;

public interface Handler extends HandlerTemplate<HandlerRequest, HandlerResponse> {

}
