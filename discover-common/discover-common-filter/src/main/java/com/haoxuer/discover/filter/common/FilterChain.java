package com.haoxuer.discover.filter.common;


import com.haoxuer.discover.filter.api.FilterChainTemplate;

public interface FilterChain extends FilterChainTemplate<HandlerRequest, HandlerResponse> {

}
