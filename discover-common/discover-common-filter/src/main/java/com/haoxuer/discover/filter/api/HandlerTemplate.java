package com.haoxuer.discover.filter.api;

public interface HandlerTemplate<Request,Response> {


  void service(Request request, Response response);

}
