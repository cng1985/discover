package com.haoxuer.discover.filter.common;

public interface HandlerRequest {

  Object getAttribute(String name);

  void setAttribute(String name, Object o);

  void removeAttribute(String name);

  String getParameter(String name);

}
