package com.haoxuer.discover.filter.api;


public interface FilterChainTemplate<Request,Response> {

  void doFilter(Request request, Response response);

}
