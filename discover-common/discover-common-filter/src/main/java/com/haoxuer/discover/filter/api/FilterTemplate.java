package com.haoxuer.discover.filter.api;



public interface FilterTemplate<Request,Response,ChainTemplate extends FilterChainTemplate<Request,Response>> {

  void doFilter(Request request, Response response,ChainTemplate filterChain);

}
