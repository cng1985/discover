package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.common.Handler;
import com.haoxuer.discover.filter.common.HandlerRequest;
import com.haoxuer.discover.filter.common.HandlerResponse;

public class ExeHandler implements Handler {
  @Override
  public void service(HandlerRequest request, HandlerResponse response) {
    Integer index = (Integer) request.getAttribute("index");
    if (index == null) {
      index = 1;
    }
    System.out.println("index:"+index);
    if (index < 3) {
      index++;
      request.setAttribute("index", index);
      throw new RuntimeException();
    }
  }
}
