package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.common.Handler;
import com.haoxuer.discover.filter.common.HandlerRequest;
import com.haoxuer.discover.filter.common.HandlerResponse;

public class DemoHandler implements Handler {
  @Override
  public void service(HandlerRequest request, HandlerResponse response) {
    if (request.getParameter("demo") != null) {
      response.setCode(1);
    }
  }
}
