package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.common.Filter;
import com.haoxuer.discover.filter.common.FilterChain;
import com.haoxuer.discover.filter.common.HandlerRequest;
import com.haoxuer.discover.filter.common.HandlerResponse;

public class FilterTwo implements Filter {
  @Override
  public void doFilter(HandlerRequest request, HandlerResponse response, FilterChain filterChain) {
    System.out.println("in FilterOne");
    filterChain.doFilter(request, response);
  }
}
