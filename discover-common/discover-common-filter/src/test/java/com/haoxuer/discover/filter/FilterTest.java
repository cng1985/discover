package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.base.HandlerFilterProxy;
import com.haoxuer.discover.filter.base.MockFilterChain;
import com.haoxuer.discover.filter.base.RestRequest;
import com.haoxuer.discover.filter.base.RestResponse;
import com.haoxuer.discover.filter.common.Handler;
import com.haoxuer.discover.filter.common.HandlerResponse;
import com.haoxuer.discover.filter.base.RetryFilterProxy;
import org.junit.Assert;
import org.junit.Test;

public class FilterTest {

  @Test
  public void shouldAnswerWithTrue()
  {
    MockFilterChain chain=new MockFilterChain();
    chain.addFilter(new TransactionFilter());
    chain.addFilter(new FilterOne());
    chain.addFilter(new FilterTwo());
    Handler hander=new DemoHandler();
    chain.addFilter(new HandlerFilterProxy(hander));
    RestRequest request=new RestRequest();
    request.putParameter("demo1","1");
    HandlerResponse response=new RestResponse();
    chain.doFilter(request,response);
    Assert.assertEquals(0,response.getCode());
  }
  @Test
  public void retry()
  {

    MockFilterChain chain=new MockFilterChain();

    chain.addFilter(new TransactionFilter());
    chain.addFilter(new FilterOne());
    chain.addFilter(new FilterTwo());
    Handler hander=new ExeHandler();

    RetryFilterProxy retryFilter=new RetryFilterProxy(hander);
    retryFilter.setNumOfRetries(5);
    chain.addFilter(retryFilter);

    RestRequest request=new RestRequest();
    request.putParameter("demo1","1");
    HandlerResponse response=new RestResponse();
    chain.doFilter(request,response);
    Assert.assertEquals(0,response.getCode());
  }
}
