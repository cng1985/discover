package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.common.Filter;
import com.haoxuer.discover.filter.common.FilterChain;
import com.haoxuer.discover.filter.common.HandlerRequest;
import com.haoxuer.discover.filter.common.HandlerResponse;

public class TransactionFilter implements Filter {
  @Override
  public void doFilter(HandlerRequest request, HandlerResponse response, FilterChain filterChain) {
    System.out.println("打开事物");
    filterChain.doFilter(request,response);
    System.out.println("关闭事物，提交事物!");

  }
}
