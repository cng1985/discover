package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.api.FilterChainTemplate;
import com.haoxuer.discover.filter.api.FilterTemplate;
import com.haoxuer.discover.filter.common.HandlerRequest;
import com.haoxuer.discover.filter.common.HandlerResponse;

public class RuntimeExceptionFilter implements FilterTemplate<HandlerRequest, HandlerResponse, FilterChainTemplate<HandlerRequest, HandlerResponse>> {
  @Override
  public void doFilter(HandlerRequest handlerRequest, HandlerResponse handlerResponse, FilterChainTemplate<HandlerRequest, HandlerResponse> filterChain) {
    System.out.println("ex...........");
    throw new RuntimeException();
  }
}
