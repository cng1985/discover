package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.api.HandlerTemplate;
import com.haoxuer.discover.filter.base.RestRequest;
import com.haoxuer.discover.filter.base.RestResponse;
import com.haoxuer.discover.filter.common.HandlerResponse;
import com.haoxuer.discover.filter.impl.FilterChainImpl;
import com.haoxuer.discover.filter.impl.HandlerFilterTemplateProxy;
import org.junit.Assert;
import org.junit.Test;

public class FilterChainTest {

  @Test
  public void shouldAnswerWithTrue()
  {
    FilterChainImpl chain=new FilterChainImpl();
    chain.addFilter(new TokenFilter());
    HandlerTemplate hander=(request, response)->{};
    chain.addFilter(new HandlerFilterTemplateProxy(hander));
    RestRequest request=new RestRequest();
    request.putParameter("demo1","1");
    HandlerResponse response=new RestResponse();
    chain.doFilter(request,response);
    Assert.assertEquals(0,response.getCode());
    Assert.assertEquals("i am ok",response.getMsg());
  }


  @Test(expected= RuntimeException.class)
  public void exceptions()
  {
    FilterChainImpl chain=new FilterChainImpl();
    chain.addFilter(new TokenFilter());
    chain.addFilter(new FilterTwo());
    chain.addFilter(new RuntimeExceptionFilter());


    HandlerTemplate hander=(request, response)->{};
    chain.addFilter(new HandlerFilterTemplateProxy(hander));
    RestRequest request=new RestRequest();
    request.putParameter("demo1","1");
    HandlerResponse response=new RestResponse();
    chain.doFilter(request,response);
  }
  @Test(expected = IllegalStateException.class)
  public void testExpect() {
    throw new IllegalStateException();
  }
}
