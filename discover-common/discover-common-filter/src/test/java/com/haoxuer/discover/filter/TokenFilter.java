package com.haoxuer.discover.filter;

import com.haoxuer.discover.filter.api.FilterChainTemplate;
import com.haoxuer.discover.filter.api.FilterTemplate;
import com.haoxuer.discover.filter.common.HandlerRequest;
import com.haoxuer.discover.filter.common.HandlerResponse;

public class TokenFilter implements FilterTemplate<HandlerRequest, HandlerResponse, FilterChainTemplate<HandlerRequest,HandlerResponse>> {


  @Override
  public void doFilter(HandlerRequest handlerRequest, HandlerResponse handlerResponse, FilterChainTemplate filterChain) {
    handlerRequest.setAttribute("name", "ada");
    handlerResponse.setMsg("i am ok");
    filterChain.doFilter(handlerRequest,handlerResponse);
  }
}
