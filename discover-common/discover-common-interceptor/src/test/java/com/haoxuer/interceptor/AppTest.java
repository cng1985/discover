package com.haoxuer.interceptor;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );


        SoutInterceptor interceptor=new SoutInterceptor();

        RetryInterceptor retryInterceptor=new RetryInterceptor();
        interceptor.setNext(retryInterceptor);

        CommandInvoker invoker=new CommandInvoker();

        retryInterceptor.setNext(invoker);

        CommandExecutor executor=new CommandExecutorImpl(interceptor);
        DemoCommand command=new DemoCommand();
        String rest=executor.execute(command);
        assertNull(rest);
    }
}
