package com.haoxuer.interceptor;

public class DemoCommand implements Command<String> {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String execute() {
        return name;
    }
}
