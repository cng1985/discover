package com.haoxuer.interceptor;

/**
 * Created by ada on 2017/6/19.
 */
public class CommandException extends RuntimeException {
    public CommandException(String message) {
        super(message);
    }
}
