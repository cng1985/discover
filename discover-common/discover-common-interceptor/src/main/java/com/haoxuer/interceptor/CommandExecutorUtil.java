package com.haoxuer.interceptor;

public class CommandExecutorUtil {

    public static CommandExecutor getDefaultExecutor(){
        CommandInterceptor first = new LogInterceptor();
        CommandInterceptor retryInterceptor = new RetryInterceptor();
        first.setNext(retryInterceptor);

        CommandInvoker invoker = new CommandInvoker();
        retryInterceptor.setNext(invoker);
        CommandExecutor commandExecutor = new CommandExecutorImpl( first);
        return commandExecutor;
    }
}
