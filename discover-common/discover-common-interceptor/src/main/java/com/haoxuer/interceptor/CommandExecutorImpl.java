package com.haoxuer.interceptor;

/**
 * Command executor that passes commands to the first interceptor in the chain
 *
 * @author Marcus Klimstra (CGI)
 * @author Joram Barrez
 */
public class CommandExecutorImpl implements CommandExecutor {

    protected CommandInterceptor first;

    public CommandExecutorImpl(CommandInterceptor first) {
        this.first = first;
    }

    public CommandInterceptor getFirst() {
        return first;
    }

    public void setFirst(CommandInterceptor commandInterceptor) {
        this.first = commandInterceptor;
    }


    @Override
    public <T> T execute(Command<T> command) {
        return first.execute(command);
    }
}
