package com.haoxuer.discover.data.entity;

import com.haoxuer.discover.data.enums.StoreState;

import javax.persistence.MappedSuperclass;

/**
 * 带有存储状态的实体.
 *
 * @author ada
 */
@MappedSuperclass
public class BaseStateEntity extends BaseEntity {

  /**
   * 数据存储状态.
   */
  private StoreState storeState;
  
  public StoreState getStoreState() {
    return storeState;
  }
  
  public void setStoreState(StoreState storeState) {
    this.storeState = storeState;
  }
}
