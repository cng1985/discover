package com.haoxuer.discover.cache;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component("sysKeyGenerator")
public class SysKeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object o, Method method, Object... objects) {
        StringBuffer buffer=new StringBuffer();
        if (o!=null){
            buffer.append(o.getClass().getSimpleName());
            buffer.append("-");
        }
        buffer.append(method.getName());
        if (objects!=null){
            for (Object object : objects) {
                buffer.append("-"+object);
            }
        }
        return buffer.toString();
    }
}
