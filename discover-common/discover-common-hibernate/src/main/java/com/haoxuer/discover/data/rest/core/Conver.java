package com.haoxuer.discover.data.rest.core;

/**
 * 数据转换接口
 *
 * @param <R> 结果类
 * @param <S> 需要转换的类
 * @author ada
 */
@Deprecated
public interface Conver<R, S> {

  R conver(S source);
}
