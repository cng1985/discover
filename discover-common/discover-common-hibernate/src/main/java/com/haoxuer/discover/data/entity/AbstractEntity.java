package com.haoxuer.discover.data.entity;

import com.nbsaas.codemake.annotation.NoHandle;
import com.nbsaas.codemake.annotation.NoResponse;
import com.nbsaas.codemake.annotation.NoSimple;
import com.haoxuer.discover.data.listener.EntityListener;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;


@EntityListeners(EntityListener.class)
@MappedSuperclass
public abstract class AbstractEntity extends LongEntity implements Serializable {
  
  
  /**
   * 添加时间
   */
  @NoHandle
  private Date addDate;
  
  /**
   * 最新修改时间
   */
  @NoHandle
  @NoResponse
  @NoSimple
  private Date lastDate;
  
  public AbstractEntity() {
    inittime();
    
  }
  
  
  private void inittime() {
    addDate = new Date();
    lastDate = new Date();
  }
  
  
  public Date getAddDate() {
    return addDate;
  }
  
  public void setAddDate(Date addDate) {
    this.addDate = addDate;
  }
  
  public Date getLastDate() {
    return lastDate;
  }
  
  public void setLastDate(Date lastDate) {
    this.lastDate = lastDate;
  }
  
  
}
