package com.haoxuer.discover.data.listener;

import com.haoxuer.discover.data.entity.AbstractEntity;
import java.util.Date;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Listener - 创建日期、修改日期处理
 */
public class EntityListener {

  /**
   * 保存前处理
   *
   * @param entity 基类
   */
  @PrePersist
  public void prePersist(AbstractEntity entity) {
    entity.setLastDate(new Date());
  }

  /**
   * 更新前处理
   *
   * @param entity 基类
   */
  @PreUpdate
  public void preUpdate(AbstractEntity entity) {
    entity.setLastDate(new Date());
  }

}