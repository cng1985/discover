package com.haoxuer.discover.data.rest.core;

import com.haoxuer.discover.data.rest.domain.AbstractPageVo;
import com.haoxuer.discover.data.core.Pagination;

/**
 * 资源转换服务
 *
 * @param <R>   目标
 * @param <S>   源对象
 * @param <PVo> 目标分页对象
 * @author ada
 */
public abstract class BaseResource<R, S, PVo extends AbstractPageVo<R>> {

  public abstract Conver<R, S> getConver();

  public PVo page(Pagination<S> pager) {
    PVo result = getPageVo();

    ConverUtils.coverpage(result, pager, getConver());

    return result;

  }

  public abstract PVo getPageVo();
}
