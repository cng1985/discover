package com.haoxuer.discover.data.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public DateUtils() {
    }

    public static String format(Date date) {
        return format(date, "yyyy-MM-dd HH:mm");
    }

    public static String format(Date date, String str) {
        String result = "";

        try {
            SimpleDateFormat format = new SimpleDateFormat(str);
            result = format.format(date);
        } catch (Exception var4) {
        }

        return result;
    }

    public static String simple(Date date) {
        return format(date, "yyyy-MM-dd");
    }

    public static String formatChines(Date date) {
        return format(date, "yyyy年MM月dd HH:mm");
    }

    public static Date end(Long date) {
        if (date==null){
            return null;
        }
        return end(date);
    }

    public static Date end(Date date) {
        if (date==null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(11, 23);
        calendar.set(12, 59);
        calendar.set(13, 59);
        return calendar.getTime();
    }

    public static Date begin(Long date) {
        if (date==null){
            return null;
        }
        return begin(new Date(date));
    }

    public static Date begin(Date date) {
        if (date==null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        return calendar.getTime();
    }

    public static Date beginMonth(Date date) {
        if (date==null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(5, 1);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 1);
        return calendar.getTime();
    }

    public static Date endMonth(Date date) {
        if (date==null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(5, 1);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.add(2, 1);
        calendar.add(13, -1);
        return calendar.getTime();
    }
}
