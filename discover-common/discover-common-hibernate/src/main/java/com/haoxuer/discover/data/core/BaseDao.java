package com.haoxuer.discover.data.core;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Pageable;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;

/**
 * 基础数据访问DAO
 *
 * @param <T>
 * @param <ID>
 */
public interface BaseDao<T, ID extends Serializable> {

    /**
     * 根据条件查询数据，返回分页结果
     *
     * @param pageable 分页条件
     * @return
     */
    Page<T> page(Pageable pageable);


    List<T> list(Pageable pageable);

    Page<T> pageData(Pageable pageable);



    /**
     * 分页方法<p></p>
     * 请采用{@link BaseDao#page(Pageable)}
     *
     * @param pageable
     * @return
     */
    @Deprecated
    Pagination<T> findByPage(Pageable pageable);

    List<T> list(Integer first, Integer count, List<Filter> filters, List<Order> orders);

    T add(T t);

    T delete(T t);

    T update(T t);

    T merge(T t);

    T findOne(Finder finder);

    @Deprecated
    Pagination<T> find(Finder finder, int pageNo, int pageSize);

    <DTO> Pagination<DTO> findSql(Finder finder, int pageNo, int pageSize, Class<DTO> otoclass);

    List<T> find(Finder finder);

    @Deprecated
    Pagination<T> findByCriteria(Criteria crit, int pageNo, int pageSize);

    List<T> find(String hql, Object... values);


    List<T> filters(Filter... filters);

    <D> D handle(String func, String field, Filter... filters);

    <D> D handle(String func, String field, List<Filter> filters);

    /**
     * 获取某个条件的对象
     *
     * @param filters
     * @return
     */
    T one(Filter... filters);


    List<T> findByProperty(String property, Object value);

    int countQueryResult(Finder finder);

    Long countQuery(Finder finder);

    Long countQuery(Filter... filters);


    int countQuerySqlResult(Finder finder);

    <DTO> DTO hql(Finder finder);

    /**
     * hibernate 转换成sql
     *
     * @param hql
     * @return
     */
    String transHqlToSql(String hql);

    /**
     * @param sql      sql语句
     * @param otoclass 需要转化的对象
     * @return
     */
    <DTO> List<DTO> listSQL(String sql, Class<DTO> otoclass);

    <DTO> List<DTO> listSQL(String sql, Integer star, Integer max, Class<DTO> otoclass);

    /**
     * @param sql      sql语句
     * @param otoclass 需要转化的对象
     * @return
     */
    <DTO> DTO oneSQL(String sql, Class<DTO> otoclass);

}