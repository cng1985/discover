package com.haoxuer.discover.data.core;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Pageable;

import java.io.Serializable;
import java.util.List;

public interface CriteriaDao<T, ID extends Serializable> extends BaseDao<T, ID> {
  Page<T> findPage(Pageable pageable);
  
  long count(Filter... filters);
  
  List<T> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders);
}
