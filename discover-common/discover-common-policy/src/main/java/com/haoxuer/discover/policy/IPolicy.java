package com.haoxuer.discover.policy;

import com.haoxuer.discover.policy.exceptions.ConfigurationParseException;

public interface IPolicy {

  public Object parseConfiguration(String jsonConfiguration) throws ConfigurationParseException;

}
