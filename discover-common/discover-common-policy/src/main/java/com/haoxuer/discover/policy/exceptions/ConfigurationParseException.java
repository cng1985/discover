package com.haoxuer.discover.policy.exceptions;

public class ConfigurationParseException extends RuntimeException {

  private static final long serialVersionUID = -1265213011525200681L;

  /**
   * Constructor.
   *
   * @param message an error message
   */
  public ConfigurationParseException(String message) {
    super(message);
  }
}
