package com.haoxuer.discover.policy.policies;

import com.google.gson.Gson;
import com.haoxuer.discover.policy.IPolicy;
import com.haoxuer.discover.policy.exceptions.ConfigurationParseException;

public abstract class AbstractPolicy<C> implements IPolicy {

  /**
   * Constructor.
   */
  public AbstractPolicy() {
  }

  @Override
  public C parseConfiguration(String jsonConfiguration) throws ConfigurationParseException {
    try {
      return new Gson().fromJson(jsonConfiguration, getConfigurationClass());
    } catch (Exception e) {
      throw new ConfigurationParseException(e.getMessage());
    }
  }

  /**
   * @return the class to use for JSON configuration deserialization
   */
  protected abstract Class<C> getConfigurationClass();
}
