package com.haoxuer.discover.policy;

import com.haoxuer.discover.policy.config.IPListConfig;
import com.haoxuer.discover.policy.policies.IPBlacklistPolicy;
import org.junit.Assert;
import org.junit.Test;

public class IPBlacklistPolicyTest {

  @Test
  public void config(){
    IPBlacklistPolicy policy=new IPBlacklistPolicy();
    IPListConfig config= policy.parseConfiguration("{}");
    Assert.assertNotNull(config);
    Assert.assertEquals(0,config.getIpList().size());

    config= policy.parseConfiguration("{\"ipList\":[\"127.0.0.1\",\"192.168.0.1\"]}");
    Assert.assertNotNull(config);
    Assert.assertEquals(2,config.getIpList().size());


  }
}
