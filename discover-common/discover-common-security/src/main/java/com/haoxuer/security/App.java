package com.haoxuer.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.security.KeyPair;
import java.util.Calendar;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256); //or RS384, RS512, PS256, PS384, PS512, ES256, ES384, ES512
    Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);
    System.out.println("Hello World!");

    String result = "";
    try {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.YEAR, 1);

      String jws = Jwts.builder().setSubject("Joe").signWith(key).compact();
      System.out.println(jws);
    } catch (Exception e) {
    }
  }
}
