package com.haoxuer.discover.plug.api;

import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.service.PluginConfigService;

import javax.annotation.Resource;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.stereotype.Component;

public abstract class IPlugin implements Comparable<IPlugin> {
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  /**
   * 获取ID.
   *
   * @return ID
   */
  public final String getId() {
    return getClass().getAnnotation(Component.class).value();
  }
  
  /**
   * 获取名称.
   *
   * @return 名称
   */
  public abstract String getName();
  
  /**
   * 获取版本.
   *
   * @return 版本
   */
  public abstract String getVersion();
  
  /**
   * 获取作者.
   *
   * @return 作者
   */
  public abstract String getAuthor();
  
  /**
   * 获取网址.
   *
   * @return 网址
   */
  public abstract String getSiteUrl();


  /**
   * 获取安装URL.
   *
   * @return 安装URL
   */
  public String getInstallUrl() {
    return getBaseUrl() + viewName()+"/install.htm";
  }


  /**
   * 获取卸载URL.
   *
   * @return 卸载URL
   */
  public String getUninstallUrl() {
    return getBaseUrl() + viewName()+"/uninstall.htm";
  }

  /**
   * 获取设置URL.
   *
   * @return 设置URL
   */
  public String getSettingUrl() {
    return getBaseUrl() + viewName()+"/setting.htm";
  }

  /**
   * 插件基础路径
   *
   * @return
   */
  public abstract String getBaseUrl();

  /**
   * 插件简单名字
   *
   * @return
   */
  public abstract String viewName();

  
  /**
   * 获取是否已安装.
   *
   * @return 是否已安装
   */
  public boolean getIsInstalled() {
    return pluginConfigService.pluginIdExists(getId());
  }
  
  /**
   * 获取插件配置.
   *
   * @return 插件配置
   */
  public PluginConfig getPluginConfig() {
    return pluginConfigService.findByPluginId(getId());
  }
  
  /**
   * 获取是否已启用.
   *
   * @return 是否已启用
   */
  public boolean getIsEnabled() {
    PluginConfig pluginConfig = getPluginConfig();
    return pluginConfig != null ? pluginConfig.getIsEnabled() : false;
  }
  
  /**
   * 获取属性值.
   *
   * @param name 属性名称
   * @return 属性值
   */
  public String getAttribute(String name) {
    PluginConfig pluginConfig = getPluginConfig();
    return pluginConfig != null ? pluginConfig.getAttribute(name) : null;
  }
  
  /**
   * 获取排序.
   *
   * @return 排序
   */
  public Integer getOrder() {
    PluginConfig pluginConfig = getPluginConfig();
    return pluginConfig != null ? pluginConfig.getSortNum() : null;
  }
  
  
  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37).append(getId()).build();
  }
  
  @Override
  public boolean equals(Object obj) {
    return new EqualsBuilder().append(obj, this).build();
  }
  
  @Override
  public int compareTo(IPlugin plug) {
    return new CompareToBuilder().append(getOrder(), plug.getOrder()).append(getId(), plug.getId()).build();
  }
  
}
