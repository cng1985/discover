/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.plug.data.plugs.file;

import com.haoxuer.discover.plug.api.StoragePlugin;
import com.haoxuer.discover.plug.data.vo.FileInfo;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

/**
 * Plugin - 本地文件存储
 */
@Component("filePlugin")
public class FilePlugin extends StoragePlugin implements ServletContextAware {
  
  private Logger logger = LoggerFactory.getLogger(getClass());
  
  /**
   * servletContext
   */
  private ServletContext servletContext;
  
  @Override
  public void setServletContext(ServletContext servletContext) {
    this.servletContext = servletContext;
  }
  
  @Override
  public String getName() {
    return "servlet上传插件";
  }
  
  @Override
  public String getVersion() {
    return "1.0";
  }
  


  @Override
  public String viewName() {
    return "file";
  }

  @Override
  public void upload(String path, File file, String contentType) {
    File destFile = new File(servletContext.getRealPath(path));
    try {
      FileUtils.moveFile(file, destFile);
      logger.info("size:{}", destFile.length());
      logger.info("viewName:{}", destFile.getName());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public String getUrl(String path) {
    return path;
  }
  
  @Override
  public List<FileInfo> browser(String path) {
    List<FileInfo> fileInfos = new ArrayList<FileInfo>();
    File directory = new File(servletContext.getRealPath(path));
    if (directory.exists() && directory.isDirectory()) {
      for (File file : directory.listFiles()) {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(file.getName());
        fileInfo.setUrl(path + file.getName());
        fileInfo.setIsDirectory(file.isDirectory());
        fileInfo.setSize(file.length());
        fileInfo.setLastModified(new Date(file.lastModified()));
        fileInfos.add(fileInfo);
      }
    }
    return fileInfos;
  }
  
}