package com.haoxuer.discover.plug.data.service.impl;

import com.haoxuer.discover.plug.api.PushPlugin;
import com.haoxuer.discover.plug.data.service.PluginService;
import com.haoxuer.discover.plug.data.service.PushService;
import com.haoxuer.discover.plug.data.vo.PushBack;

import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("pushServiceImpl")
public class PushServiceImpl implements PushService {

  @Resource(name = "pluginServiceImpl")
  private PluginService pluginService;


  @Override
  public PushBack pushAll(String msg, Map<String, String> keys) {
    PushBack last = null;
    for (PushPlugin storagePlugin : pluginService.getPushPlugins(true)) {
      last = storagePlugin.pushAll(msg, keys);
    }
    return last;
  }

  @Override
  public PushBack pushToSingleDevice(String chanel, String msg, Map<String, String> keys) {
    PushBack last = null;
    for (PushPlugin storagePlugin : pluginService.getPushPlugins(true)) {
      last = storagePlugin.pushToSingleDevice(chanel, msg, keys);
    }
    return last;
  }

  @Override
  public PushBack pushToTag(String tag, String msg, Map<String, String> keys) {
    PushBack last = null;
    for (PushPlugin storagePlugin : pluginService.getPushPlugins(true)) {
      last = storagePlugin.pushToTag(tag, msg, keys);
    }
    return last;
  }

  @Override
  public PushBack pushToChannels(String[] chanels, String msg, Map<String, String> keys) {
    PushBack last = null;
    for (PushPlugin storagePlugin : pluginService.getPushPlugins(true)) {
      last = storagePlugin.pushToChannels(chanels, msg, keys);
    }
    return last;
  }
}
