package com.haoxuer.discover.plug.api;

import com.haoxuer.discover.rest.base.ResponseObject;

import java.util.Map;

public abstract class SendCodePlugin extends IPlugin {
  
  
  /**
   * 发送验证码功能.
   *
   * @param params 参数
   * @return
   */
  public abstract ResponseObject sendCode(Map<String, String> params);

  @Override
  public String getBaseUrl() {
    return "admin/plugin_sendcode/";
  }

  @Override
  public String getAuthor() {
    return "ada.young";
  }


  @Override
  public String getSiteUrl() {
    return "http://www.haoxuer.com";
  }


}
