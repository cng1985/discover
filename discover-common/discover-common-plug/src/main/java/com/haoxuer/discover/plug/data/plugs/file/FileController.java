/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.plug.data.plugs.file;

import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller - 本地文件存储
 */
@Controller("adminPluginFileController")
@RequestMapping("/admin/plugin_storage/file")
public class FileController extends PlugTemplateController {
  
  @Resource(name = "filePlugin")
  private FilePlugin filePlugin;
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return filePlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return filePlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_storage";
  }
  
  @Override
  public String getSettingView() {
    return "file";
  }
}