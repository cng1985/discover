package com.haoxuer.discover.plug.data.plugs.noneproxy;

import com.haoxuer.discover.plug.api.ProxyPlugin;
import com.haoxuer.discover.plug.api.PushPlugin;
import com.haoxuer.discover.plug.data.vo.PushBack;
import com.haoxuer.discover.rest.base.ResponseObject;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component("noneProxyPlugin")
public class NoneProxyPlugin extends ProxyPlugin {

  @Override
  public String getName() {
    return "服务插件";
  }

  @Override
  public String getVersion() {
    return "1.01";
  }


  @Override
  public ResponseObject handle(Map<String, String> params) {
    ResponseObject result=new ResponseObject();
    return result;
  }

  @Override
  public String catalog() {
    return "demo";
  }

  @Override
  public String viewName() {
    return "none";
  }
}
