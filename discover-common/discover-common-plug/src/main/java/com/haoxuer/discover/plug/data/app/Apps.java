package com.haoxuer.discover.plug.data.app;

import com.haoxuer.discover.plug.api.IPlugin;
import java.util.ServiceLoader;

public class Apps {

  public static void main(String[] args) {
    ServiceLoader<IPlugin> serviceLoader = ServiceLoader.load(IPlugin.class);
    for (IPlugin iPlugin : serviceLoader) {
      System.out.println(iPlugin);
    }
  }
}
