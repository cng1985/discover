package com.haoxuer.discover.plug.data.service.impl;

import com.haoxuer.discover.plug.api.ProxyPlugin;

import java.util.Map;

public class HandleProxyThread implements Runnable{

  private ProxyPlugin plugin;

  public HandleProxyThread(ProxyPlugin plugin, Map<String, String> params) {
    this.plugin = plugin;
    this.params = params;
  }

  private Map<String,String> params;


  @Override
  public void run() {
    plugin.handle(params);
  }
}
