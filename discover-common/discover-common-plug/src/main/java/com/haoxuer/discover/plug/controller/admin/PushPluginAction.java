package com.haoxuer.discover.plug.controller.admin;

import com.haoxuer.discover.plug.data.service.PluginService;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller - 存储插件.
 */
@Controller("adminPushPluginController")
@RequestMapping("/admin/plugin_push")
public class PushPluginAction {
  
  @Resource(name = "pluginServiceImpl")
  private PluginService pluginService;
  
  /**
   * 列表.
   */
  @RequiresPermissions("plugin_push")
  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String list(ModelMap model) {
    model.addAttribute("pushPlugins", pluginService.getPushPlugins());
    return "/admin/plugin_push/list";
  }
  
}