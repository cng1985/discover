/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.plug.controller.admin;

import com.haoxuer.discover.plug.data.service.PluginService;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller - 存储插件
 */
@Controller("adminStoragePluginController")
@RequestMapping("/admin/plugin_storage")
public class StoragePluginAction {
  
  @Resource(name = "pluginServiceImpl")
  private PluginService pluginService;
  
  /**
   * 列表
   */
  @RequiresPermissions("plugin_storage")
  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String list(ModelMap model) {
    model.addAttribute("storagePlugins", pluginService.getStoragePlugins());
    return "/admin/plugin_storage/list";
  }
  
  
}