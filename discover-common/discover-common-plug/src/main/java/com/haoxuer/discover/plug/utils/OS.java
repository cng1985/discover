package com.haoxuer.discover.plug.utils;

public class OS {
  private OS() {
  }
  
  public static boolean isWindows() {
    boolean result=false;
   String viewName= System.getProperty("os.viewName");
   if (viewName!=null&&viewName.contains("Windows")){
     result=true;
   }
    String os = System.getProperty("os.name");
   if (os!=null&&os.toLowerCase().indexOf("windows")>-1){
     result=true;
   }
    return result;
  }
  
  public static boolean isOSX() {
    return System.getProperty("os.viewName").contains("Mac");
  }
  
  public static boolean isLinux() {
    return !System.getProperty("os.viewName").contains("Windows") && !System.getProperty("os.viewName").contains("Mac");
  }
  
  public static void main(String[] args) {
    System.out.println(isWindows());
  }
}
