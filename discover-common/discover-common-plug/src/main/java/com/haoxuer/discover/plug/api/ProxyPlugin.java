package com.haoxuer.discover.plug.api;

import com.haoxuer.discover.rest.base.ResponseObject;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.Map;

public abstract class ProxyPlugin extends IPlugin {


  public abstract ResponseObject handle(Map<String, String> params);


  /**
   * 目录
   * @return
   */
  public abstract String  catalog();

  @Override
  public int compareTo(IPlugin plug) {
    return new CompareToBuilder().append(getOrder(), plug.getOrder()).build();
  }

  @Override
  public String getBaseUrl() {
    return "admin/plugin_proxy/";
  }

  @Override
  public String getAuthor() {
    return "ada.young";
  }


  @Override
  public String getSiteUrl() {
    return "http://www.haoxuer.com";
  }

}
