package com.haoxuer.discover.plug.data.dao;


import com.haoxuer.discover.plug.data.entity.SystemPayment;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年08月15日11:13:49.
 */
public interface SystemPaymentDao extends BaseDao<SystemPayment, Long> {

  SystemPayment findById(Long id);

  SystemPayment save(SystemPayment bean);

  SystemPayment updateByUpdater(Updater<SystemPayment> updater);

  SystemPayment deleteById(Long id);
}