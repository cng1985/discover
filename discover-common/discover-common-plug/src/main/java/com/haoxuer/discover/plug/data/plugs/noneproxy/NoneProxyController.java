package com.haoxuer.discover.plug.data.plugs.noneproxy;


import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/admin/plugin_proxy/none")
public class NoneProxyController extends PlugTemplateController {
  
  
  @Resource(name = "noneProxyPlugin")
  private NoneProxyPlugin noneProxyPlugin;
  
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return noneProxyPlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return noneProxyPlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_proxy";
  }
  
  @Override
  public String getSettingView() {
    return "none";
  }
}
