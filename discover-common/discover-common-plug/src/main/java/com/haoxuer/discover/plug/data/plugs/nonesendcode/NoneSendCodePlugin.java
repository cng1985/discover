package com.haoxuer.discover.plug.data.plugs.nonesendcode;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.plug.api.SendCodePlugin;
import java.util.Map;
import org.springframework.stereotype.Component;


@Component("noneSendCodePlugin")
public class NoneSendCodePlugin extends SendCodePlugin {
  @Override
  public ResponseObject sendCode(Map<String, String> params) {
    ResponseObject result = new ResponseObject();
    return result;
  }

  @Override
  public String getName() {
    return "发送短信例子插件";
  }

  @Override
  public String getVersion() {
    return "1.01";
  }


  @Override
  public String viewName() {
    return "none";
  }
}
