package com.haoxuer.discover.plug.data.service.impl;

import com.haoxuer.discover.plug.api.SendCodePlugin;
import com.haoxuer.discover.plug.data.service.CodeService;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.plug.data.service.PluginService;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("codeServiceImpl")
public class CodeServiceImpl implements CodeService {

  @Resource(name = "pluginServiceImpl")
  private PluginService pluginService;


  @Override
  public boolean sendCode(String code, String template, String phone) {
    for (SendCodePlugin plugin : pluginService.getSendCodePlugins(true)) {
      Map<String, String> params = new HashMap<String, String>();
      params.put("code", code);
      params.put("template", template);
      params.put("phone", phone);
      plugin.sendCode(params);

    }
    return true;
  }

  @Override
  public ResponseObject sendCode(Map<String, String> params) {
    ResponseObject result = new ResponseObject();
    for (SendCodePlugin plugin : pluginService.getSendCodePlugins(true)) {
      result = plugin.sendCode(params);
      if (result.getCode() == 0) {
        break;
      }
    }
    return result;
  }
}
