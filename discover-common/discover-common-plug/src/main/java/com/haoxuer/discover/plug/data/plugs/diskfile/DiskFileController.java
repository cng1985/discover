/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.plug.data.plugs.diskfile;

import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller - 磁盘文件存储
 */
@Controller
@RequestMapping("/admin/plugin_storage/diskfile")
public class DiskFileController extends PlugTemplateController {
  
  @Resource(name = "diskFilePlugin")
  private DiskFilePlugin diskFilePlugin;
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  @Override
  public PluginConfig getPluginConfig() {
    return diskFilePlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return diskFilePlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_storage";
  }
  
  @Override
  public String getSettingView() {
    return "diskfile";
  }
}