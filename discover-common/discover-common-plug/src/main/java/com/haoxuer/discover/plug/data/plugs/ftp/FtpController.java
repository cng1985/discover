/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.plug.data.plugs.ftp;

import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import com.haoxuer.discover.plug.api.IPlugin;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller - FTP
 */
@Controller("adminPluginFtpController")
@RequestMapping("/admin/plugin_storage/ftp")
public class FtpController extends PlugTemplateController {
  
  @Resource(name = "ftpPlugin")
  private FtpPlugin ftpPlugin;
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return ftpPlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return ftpPlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_storage";
  }
  
  @Override
  public String getSettingView() {
    return "ftp";
  }
}