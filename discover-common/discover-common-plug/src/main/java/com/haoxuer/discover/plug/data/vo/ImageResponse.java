package com.haoxuer.discover.plug.data.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ImageResponse implements Serializable {

    private String url;

    private String  thumbnail;
}
