package com.haoxuer.discover.plug.data.service;

import com.haoxuer.discover.plug.data.vo.PushBack;
import java.util.Map;

/**
 * 消息推送服务
 */
public interface PushService {
  
  PushBack pushAll(String msg, Map<String, String> keys);
  
  PushBack pushToSingleDevice(String chanel, String msg, Map<String, String> keys);
  
  PushBack pushToTag(String tag, String msg, Map<String, String> keys);
  
  
  PushBack pushToChannels(String[] chanels, String msg, Map<String, String> keys);
}
