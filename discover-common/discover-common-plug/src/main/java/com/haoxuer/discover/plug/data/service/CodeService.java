package com.haoxuer.discover.plug.data.service;

import com.haoxuer.discover.rest.base.ResponseObject;

import java.util.Map;

public interface CodeService {

  /**
   * 发送验证码
   *
   * @param code
   * @param template
   * @param phone
   * @return
   */
  boolean sendCode(String code, String template, String phone);

  /**
   * 发送短信
   *
   * @param params
   * @return
   */
  ResponseObject sendCode(Map<String, String> params);
}
