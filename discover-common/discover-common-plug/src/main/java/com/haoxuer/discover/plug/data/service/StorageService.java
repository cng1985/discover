package com.haoxuer.discover.plug.data.service;

import com.haoxuer.discover.plug.data.vo.FileInfo;

import java.util.List;

import com.haoxuer.discover.plug.data.vo.ImageResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface StorageService {


  /**
   * 文件验证
   *
   * @param fileType      文件类型
   * @param multipartFile 上传文件
   * @return 文件验证是否通过
   */
  boolean isValid(FileInfo.FileType fileType, MultipartFile multipartFile);

  /**
   * 文件上传
   *
   * @param fileType      文件类型
   * @param multipartFile 上传文件
   * @param async         是否异步
   * @return 访问URL
   */
  String upload(FileInfo.FileType fileType, MultipartFile multipartFile, boolean async);

  /**
   * 文件上传(异步)
   *
   * @param fileType      文件类型
   * @param multipartFile 上传文件
   * @return 访问URL
   */
  String upload(FileInfo.FileType fileType, MultipartFile multipartFile);


  ImageResponse uploadImage(HttpServletRequest request, MultipartFile multipartFile);

  /**
   * 文件上传至本地
   *
   * @param fileType      文件类型
   * @param multipartFile 上传文件
   * @return 路径
   */
  String uploadLocal(FileInfo.FileType fileType, MultipartFile multipartFile);

  /**
   * 文件浏览
   *
   * @param path      浏览路径
   * @param fileType  文件类型
   * @param orderType 排序类型
   * @return 文件信息
   */
  List<FileInfo> browser(String path, FileInfo.FileType fileType, FileInfo.OrderType orderType);
}
