/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.plug.controller.admin;

import com.haoxuer.discover.plug.data.service.PluginService;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller - 存储插件
 */
@Controller("sendCodePluginController")
@RequestMapping("/admin/plugin_sendcode")
public class SendCodePluginAction {
  
  @Resource(name = "pluginServiceImpl")
  private PluginService pluginService;
  
  /**
   * 列表
   */
  @RequiresPermissions("plugin_sendcode")
  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String list(ModelMap model) {
    model.addAttribute("sendCodePlugins", pluginService.getSendCodePlugins());
    return "/admin/plugin_sendcode/list";
  }
  
}