package com.haoxuer.discover.plug.data.dao;


import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年08月15日11:13:49.
 */
public interface PluginConfigDao extends BaseDao<PluginConfig, Long> {

  PluginConfig findById(Long id);

  PluginConfig save(PluginConfig bean);

  PluginConfig updateByUpdater(Updater<PluginConfig> updater);

  PluginConfig deleteById(Long id);
}