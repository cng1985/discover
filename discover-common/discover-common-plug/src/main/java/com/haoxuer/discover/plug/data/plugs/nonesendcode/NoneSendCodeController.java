package com.haoxuer.discover.plug.data.plugs.nonesendcode;


import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/plugin_sendcode/nonesendcode")
public class NoneSendCodeController extends PlugTemplateController {
  
  
  @Resource(name = "noneSendCodePlugin")
  private NoneSendCodePlugin noneSendCodePlugin;
  
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return noneSendCodePlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return noneSendCodePlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_sendcode";
  }
  
  @Override
  public String getSettingView() {
    return "nonesendcode";
  }
}
