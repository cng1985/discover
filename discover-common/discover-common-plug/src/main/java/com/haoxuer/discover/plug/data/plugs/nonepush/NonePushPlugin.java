package com.haoxuer.discover.plug.data.plugs.nonepush;

import com.haoxuer.discover.plug.api.PushPlugin;
import com.haoxuer.discover.plug.data.vo.PushBack;
import java.util.Map;
import org.springframework.stereotype.Component;


@Component("nonePushPlugin")
public class NonePushPlugin extends PushPlugin {
  @Override
  public PushBack pushAll(String msg, Map<String, String> keys) {
    return null;
  }

  @Override
  public PushBack pushToSingleDevice(String chanel, String msg, Map<String, String> keys) {
    return null;
  }

  @Override
  public PushBack pushToTag(String tag, String msg, Map<String, String> keys) {
    return null;
  }

  @Override
  public PushBack pushToChannels(String[] chanels, String msg, Map<String, String> keys) {
    return null;
  }

  @Override
  public String getName() {
    return "推送例子插件";
  }

  @Override
  public String getVersion() {
    return "1.01";
  }


  @Override
  public String viewName() {
    return "none";
  }


}
