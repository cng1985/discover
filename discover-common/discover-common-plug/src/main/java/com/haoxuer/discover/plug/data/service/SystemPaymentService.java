package com.haoxuer.discover.plug.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.plug.data.entity.SystemPayment;

import java.util.List;

/**
 * Created by imake on 2017年08月15日11:13:49.
 */
public interface SystemPaymentService {
  
  SystemPayment findById(Long id);
  
  SystemPayment save(SystemPayment bean);
  
  SystemPayment update(SystemPayment bean);
  
  SystemPayment deleteById(Long id);
  
  SystemPayment[] deleteByIds(Long[] ids);
  
  Page<SystemPayment> page(Pageable pageable);
  
  Page<SystemPayment> page(Pageable pageable, Object search);
  
  
  List<SystemPayment> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}