package com.haoxuer.discover.plug.controller.admin;

import com.haoxuer.discover.plug.data.service.PluginService;
import com.haoxuer.discover.plug.data.service.ProxyService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * Controller - 存储插件.
 */
@Controller("proxyPluginAction")
@RequestMapping("/admin/plugin_proxy")
public class ProxyPluginAction {
  
  @Resource(name = "proxyServiceImpl")
  private ProxyService proxyService;
  
  /**
   * 列表.
   */
  @RequiresPermissions("plugin_proxy")
  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String list(ModelMap model) {
    model.addAttribute("list", proxyService.getPlugins());
    return "/admin/plugin_proxy/list";
  }
  
}