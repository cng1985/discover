package com.haoxuer.discover.plug.data.plugs.base;

import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import com.haoxuer.discover.plug.Message;
import com.haoxuer.discover.plug.api.IPlugin;

import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public abstract class PlugTemplateController {
  
  public abstract PluginConfig getPluginConfig();
  
  public abstract IPlugin getPlug();
  
  public abstract PluginConfigService getPluginConfigService();
  
  public abstract String getView();
  
  public abstract String getSettingView();
  
  
  /**
   * 安装
   */
  @RequestMapping(value = "/install")
  public String install(RedirectAttributes redirectAttributes) {
    if (!getPlug().getIsInstalled()) {
      PluginConfig pluginConfig = new PluginConfig();
      pluginConfig.setPluginId(getPlug().getId());
      pluginConfig.setIsEnabled(false);
      getPluginConfigService().save(pluginConfig);
    }
    addFlashMessage(redirectAttributes, SUCCESS_MESSAGE);
    
    return "redirect:" + getView() + "/list.htm";
  }
  
  /**
   * 卸载
   */
  @RequestMapping(value = "/uninstall")
  public String uninstall(RedirectAttributes redirectAttributes) {
    if (getPlug().getIsInstalled()) {
      PluginConfig pluginConfig = getPlug().getPluginConfig();
      getPluginConfigService().deleteById(pluginConfig.getId());
    }
    addFlashMessage(redirectAttributes, SUCCESS_MESSAGE);
    return "redirect:" + getView() + "/list.htm";
  }
  
  
  /**
   * 设置
   */
  @RequestMapping(value = "/setting", method = RequestMethod.GET)
  public String setting(ModelMap model) {
    PluginConfig pluginConfig = getPlug().getPluginConfig();
    model.addAttribute("pluginConfig", pluginConfig);
    return getView() + "/setting_"+getSettingView();
  }
  
  protected void addFlashMessage(RedirectAttributes redirectAttributes, Message message) {
    if (redirectAttributes != null && message != null) {
      redirectAttributes.addFlashAttribute("msg", message);
    }
  }
  
  protected static final Message SUCCESS_MESSAGE = new Message(Message.Type.success, "成功");
  
  /**
   * 更新
   */
  @RequestMapping(value = "/update", method = RequestMethod.POST)
  public String update(Integer order, HttpServletRequest request, RedirectAttributes redirectAttributes) {
    PluginConfig pluginConfig = getPluginConfig(order, request);
    getPluginConfigService().update(pluginConfig);
    addFlashMessage(redirectAttributes, SUCCESS_MESSAGE);
    return "redirect:" + getView() + "/list.htm";
  }
  
  
  protected PluginConfig getPluginConfig(Integer order, HttpServletRequest request) {
    PluginConfig pluginConfig = getPluginConfig();
    pluginConfig.setSortNum(order);
    String enabled = request.getParameter("enabled");
    if ("on".equals(enabled)) {
      pluginConfig.setIsEnabled(true);
    } else {
      pluginConfig.setIsEnabled(false);
    }
    Set set = request.getParameterMap().keySet();
    for (Object param : set) {
      pluginConfig.setAttribute("" + param, request.getParameter("" + param));
    }
    return pluginConfig;
  }
}
