package com.haoxuer.discover.plug.data.service;

import com.haoxuer.discover.plug.api.ProxyPlugin;
import com.haoxuer.discover.rest.base.ResponseObject;

import java.util.List;
import java.util.Map;

public interface ProxyService {

  /**
   * 执行单个代理服务，插件启用排序号最前的代理
   *
   * @param params
   * @return
   */
  ResponseObject handleSingle(String catalog,Map<String, String> params);

  /**
   * 串行执行代理服务
   * @param params
   * @return
   */
  ResponseObject handleSerial(String catalog,Map<String, String> params);


  ResponseObject handleParallel(String catalog,Map<String, String> params);

  List<ProxyPlugin> getPlugins();

}
