/*
 * 
 * 
 * 
 */

package com.haoxuer.discover.plug.api;

import com.haoxuer.discover.plug.data.vo.FileInfo;

import java.io.File;
import java.util.List;

/**
 * Plugin - 存储.
 */
public abstract class StoragePlugin extends IPlugin {
  
  
  /**
   * 文件上传.
   *
   * @param path        上传路径
   * @param file        上传文件
   * @param contentType 文件类型
   */
  public abstract void upload(String path, File file, String contentType);
  
  /**
   * 获取访问URL.
   *
   * @param path 上传路径
   * @return 访问URL
   */
  public abstract String getUrl(String path);
  
  /**
   * 文件浏览.
   *
   * @param path 浏览路径
   * @return 文件信息
   */
  public abstract List<FileInfo> browser(String path);


  @Override
  public String getBaseUrl() {
    return "admin/plugin_storage/";
  }

  @Override
  public String getAuthor() {
    return "ada.young";
  }


  @Override
  public String getSiteUrl() {
    return "http://www.haoxuer.com";
  }
  
  
}