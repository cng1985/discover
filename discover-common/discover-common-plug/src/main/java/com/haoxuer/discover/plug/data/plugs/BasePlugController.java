package com.haoxuer.discover.plug.data.plugs;

import com.haoxuer.discover.plug.Message;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class BasePlugController {
  
  protected static final Message SUCCESS_MESSAGE = new Message(Message.Type.success, "成功");
  
  protected void addFlashMessage(RedirectAttributes redirectAttributes, Message message) {
    if (redirectAttributes != null && message != null) {
      redirectAttributes.addFlashAttribute("msg", message);
    }
  }
  
}
