package com.haoxuer.discover.plug.data.plugs.nonepush;


import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/plugin_push/nonepush")
public class NonePushController extends PlugTemplateController {
  
  
  @Resource(name = "nonePushPlugin")
  private NonePushPlugin nonePushPlugin;
  
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return nonePushPlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return nonePushPlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_push";
  }
  
  @Override
  public String getSettingView() {
    return "nonepush";
  }
}
