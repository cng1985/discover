package com.haoxuer.discover.plug.api;

public abstract class SendEmailPlugin extends IPlugin {
  
  public abstract boolean send(String to, String subject, String textMessage, String htmlMessage);

  @Override
  public String getBaseUrl() {
    return "admin/plugin_sendemail/";
  }

  @Override
  public String getAuthor() {
    return "ada.young";
  }


  @Override
  public String getSiteUrl() {
    return "http://www.haoxuer.com";
  }
  
}
