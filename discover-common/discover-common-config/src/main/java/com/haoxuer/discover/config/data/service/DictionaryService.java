package com.haoxuer.discover.config.data.service;

import com.haoxuer.discover.config.data.entity.Dictionary;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2019年03月11日11:40:11.
 */
public interface DictionaryService {

  Dictionary findById(Long id);

  Dictionary save(Dictionary bean);

  Dictionary update(Dictionary bean);

  Dictionary deleteById(Long id);

  Dictionary[] deleteByIds(Long[] ids);

  Page<Dictionary> page(Pageable pageable);

  Page<Dictionary> page(Pageable pageable, Object search);


  List<Dictionary> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}