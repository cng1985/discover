package com.haoxuer.discover.config.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.config.data.dao.DictionaryItemDao;
import com.haoxuer.discover.config.data.entity.DictionaryItem;

/**
* Created by imake on 2019年03月11日11:39:47.
*/
@Repository

public class DictionaryItemDaoImpl extends CriteriaDaoImpl<DictionaryItem, Long> implements DictionaryItemDao {

	@Override
	public DictionaryItem findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public DictionaryItem save(DictionaryItem bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public DictionaryItem deleteById(Long id) {
		DictionaryItem entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<DictionaryItem> getEntityClass() {
		return DictionaryItem.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}