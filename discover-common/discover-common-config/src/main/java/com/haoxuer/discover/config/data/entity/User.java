package com.haoxuer.discover.config.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "user_info")
public class User extends AbstractEntity {

  /**
   * 用户头像
   */
  private String avatar;


  /**
   * 手机号码
   */
  @Column(length = 15)
  private String phone;

  /**
   * 用户登录次数
   */
  private Integer loginSize = 0;

  /**
   * 用户真实姓名
   */
  @Column(length = 20)
  private String name;

  /**
   * 性别
   */
  @Column(length = 2)
  private String sex;



  public  static User fromId(Long id){
   User result=new User();
   result.setId(id);
   return result;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Integer getLoginSize() {
    return loginSize;
  }

  public void setLoginSize(Integer loginSize) {
    this.loginSize = loginSize;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }
}
