package com.haoxuer.discover.config.api.apis;


import com.haoxuer.discover.config.api.domain.list.DictionaryItemList;
import com.haoxuer.discover.config.api.domain.page.DictionaryItemPage;
import com.haoxuer.discover.config.api.domain.request.*;
import com.haoxuer.discover.config.api.domain.response.DictionaryItemResponse;

public interface DictionaryItemApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    DictionaryItemResponse create(DictionaryItemDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    DictionaryItemResponse update(DictionaryItemDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    DictionaryItemResponse delete(DictionaryItemDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     DictionaryItemResponse view(DictionaryItemDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    DictionaryItemList list(DictionaryItemSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    DictionaryItemPage search(DictionaryItemSearchRequest request);

}