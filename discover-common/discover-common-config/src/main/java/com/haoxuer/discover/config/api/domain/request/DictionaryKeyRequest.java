package com.haoxuer.discover.config.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class DictionaryKeyRequest extends RequestUserTokenObject {

  private String key;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
