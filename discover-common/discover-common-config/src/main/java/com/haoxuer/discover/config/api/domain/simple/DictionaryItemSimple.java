package com.haoxuer.discover.config.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by BigWorld on 2021年05月22日11:11:56.
*/
@Data
public class DictionaryItemSimple implements Serializable {

    private Long id;

     private Long dictionary;
     private int sortNum;
     private String name;
     private StoreState state;

     private String stateName;

}
