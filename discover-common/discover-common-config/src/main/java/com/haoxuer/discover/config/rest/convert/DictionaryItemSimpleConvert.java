package com.haoxuer.discover.config.rest.convert;

import com.haoxuer.discover.config.api.domain.simple.DictionaryItemSimple;
import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.data.rest.core.Conver;
public class DictionaryItemSimpleConvert implements Conver<DictionaryItemSimple, DictionaryItem> {


    @Override
    public DictionaryItemSimple conver(DictionaryItem source) {
        DictionaryItemSimple result = new DictionaryItemSimple();

            result.setId(source.getId());
            if(source.getDictionary()!=null){
               result.setDictionary(source.getDictionary().getId());
            }
             result.setSortNum(source.getSortNum());
             result.setName(source.getName());
             result.setState(source.getState());

             result.setStateName(source.getState()+"");
        return result;
    }
}
