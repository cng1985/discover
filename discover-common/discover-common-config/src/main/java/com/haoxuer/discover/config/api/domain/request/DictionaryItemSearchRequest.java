package com.haoxuer.discover.config.api.domain.request;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;


/**
*
* Created by imake on 2021年05月22日11:11:56.
*/

@Data
public class DictionaryItemSearchRequest  {

    //字典
     @Search(name = "dictionary.id",operator = Filter.Operator.eq)
     private Long dictionary;

    //字典
     @Search(name = "dictionary.key",operator = Filter.Operator.like)
     private String key;




    private String sortField;


    private String sortMethod;
}