package com.haoxuer.discover.config.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.config.data.dao.ConfigOptionDao;
import com.haoxuer.discover.config.data.entity.ConfigOption;
import com.haoxuer.discover.config.data.service.ConfigOptionService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月17日10:47:24.
*/


@Scope("prototype")
@Service
@Transactional
public class ConfigOptionServiceImpl implements ConfigOptionService {

	private ConfigOptionDao dao;


	@Override
	@Transactional(readOnly = true)
	public ConfigOption findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public ConfigOption save(ConfigOption bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public ConfigOption update(ConfigOption bean) {
		Updater<ConfigOption> updater = new Updater<ConfigOption>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public ConfigOption deleteById(Long id) {
		ConfigOption bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public ConfigOption[] deleteByIds(Long[] ids) {
		ConfigOption[] beans = new ConfigOption[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ConfigOptionDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<ConfigOption> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<ConfigOption> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<ConfigOption> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}