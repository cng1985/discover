package com.haoxuer.discover.config.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.config.data.entity.Dictionary;

/**
* Created by imake on 2019年03月11日11:40:11.
*/
public interface DictionaryDao extends BaseDao<Dictionary,Long>{

	 Dictionary findById(Long id);

	 Dictionary save(Dictionary bean);

	 Dictionary updateByUpdater(Updater<Dictionary> updater);

	 Dictionary deleteById(Long id);
}