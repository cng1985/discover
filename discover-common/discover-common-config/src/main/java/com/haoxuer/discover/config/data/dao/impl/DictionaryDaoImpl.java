package com.haoxuer.discover.config.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.config.data.dao.DictionaryDao;
import com.haoxuer.discover.config.data.entity.Dictionary;

/**
* Created by imake on 2019年03月11日11:40:11.
*/
@Repository

public class DictionaryDaoImpl extends CriteriaDaoImpl<Dictionary, Long> implements DictionaryDao {

	@Override
	public Dictionary findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Dictionary save(Dictionary bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Dictionary deleteById(Long id) {
		Dictionary entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Dictionary> getEntityClass() {
		return Dictionary.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}