package com.haoxuer.discover.config.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.InputType;
import com.haoxuer.discover.data.entity.LongEntity;
import com.haoxuer.discover.data.enums.StoreState;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import com.nbsaas.codemake.annotation.SearchBean;
import com.nbsaas.codemake.annotation.SearchItem;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 数据字典子项
 */
@SearchBean(items = {
        @SearchItem(label = "字典", name = "dictionaryKey", key = "dictionary.key", operator = "eq", classType = "Long")
})
@Entity
@Table(name = "system_dictionary_item")
public class DictionaryItem extends LongEntity implements Comparable {

    @FormField(title = "开始时间", sortNum = "1", grid = true)
    private String name;

    @SearchItem(label = "字典", name = "dictionary", key = "dictionary.id", operator = "eq", classType = "Long")
    @FieldConvert
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    private Dictionary dictionary;

    @FormField(title = "排序号", sortNum = "3", grid = true)
    private int sortNum;

    private StoreState state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    public StoreState getState() {
        return state;
    }

    public void setState(StoreState state) {
        this.state = state;
    }

    @Override
    public int compareTo(Object o) {
        DictionaryItem item = (DictionaryItem) o;
        return compare(this.sortNum, item.sortNum);
    }

    public static int compare(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
}
