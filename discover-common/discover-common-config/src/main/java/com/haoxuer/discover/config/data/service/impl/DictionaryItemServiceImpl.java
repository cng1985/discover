package com.haoxuer.discover.config.data.service.impl;

import com.haoxuer.discover.config.data.dao.DictionaryItemDao;
import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.config.data.service.DictionaryItemService;
import com.haoxuer.discover.config.rest.conver.StringDictionaryConver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by imake on 2019年03月11日15:34:11.
 */


@Scope("prototype")
@Service
@Transactional
public class DictionaryItemServiceImpl implements DictionaryItemService {

  private DictionaryItemDao dao;


  @Override
  @Transactional(readOnly = true)
  public DictionaryItem findById(Long id) {
    return dao.findById(id);
  }


  @Override
  @Transactional
  public DictionaryItem save(DictionaryItem bean) {
    bean.setState(StoreState.normal);
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public DictionaryItem update(DictionaryItem bean) {
    Updater<DictionaryItem> updater = new Updater<DictionaryItem>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public DictionaryItem deleteById(Long id) {
    DictionaryItem bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public DictionaryItem[] deleteByIds(Long[] ids) {
    DictionaryItem[] beans = new DictionaryItem[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(DictionaryItemDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<DictionaryItem> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<DictionaryItem> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<String> key(String key) {
    List<String> result = new ArrayList<>();
    List<DictionaryItem> items = dao.filters(Filter.eq("dictionary.key", key));
    if (items != null) {
      Collections.sort(items);
      result = ConverResourceUtils.converCollect(items, new StringDictionaryConver());
    }
    return result;
  }

  @Override
  public List<String> forId(Long id) {
    List<String> result = new ArrayList<>();
    List<DictionaryItem> items = dao.filters(Filter.eq("dictionary.id", id));
    if (items != null) {
      Collections.sort(items);
      result = ConverResourceUtils.converCollect(items, new StringDictionaryConver());
    }
    return result;
  }

  @Override
  public List<DictionaryItem> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}