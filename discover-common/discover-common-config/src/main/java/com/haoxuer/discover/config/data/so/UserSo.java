package com.haoxuer.discover.config.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;

import java.io.Serializable;

/**
* Created by imake on 2018年12月17日21:25:13.
*/
public class UserSo implements Serializable {

    @Search(name = "name",operator = Filter.Operator.like)
    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
