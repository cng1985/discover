package com.haoxuer.discover.config.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.config.data.entity.DictionaryItem;

/**
* Created by imake on 2019年03月11日11:39:47.
*/
public interface DictionaryItemDao extends BaseDao<DictionaryItem,Long>{

	 DictionaryItem findById(Long id);

	 DictionaryItem save(DictionaryItem bean);

	 DictionaryItem updateByUpdater(Updater<DictionaryItem> updater);

	 DictionaryItem deleteById(Long id);
}