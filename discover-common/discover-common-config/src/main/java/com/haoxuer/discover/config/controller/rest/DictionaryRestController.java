package com.haoxuer.discover.config.controller.rest;

import com.haoxuer.discover.config.api.domain.request.DictionaryKeyRequest;
import com.haoxuer.discover.config.api.domain.request.FindByIdRequest;
import com.haoxuer.discover.config.api.domain.response.DictionaryResponse;
import com.haoxuer.discover.config.api.handle.DictionaryApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("/rest")
@RestController
public class DictionaryRestController {

  @RequestMapping("/dictionary/key")
  public DictionaryResponse key(DictionaryKeyRequest request) {
    return api.key(request);
  }

  @RequestMapping("/dictionary/findbyid")
  public DictionaryResponse findById(FindByIdRequest request) {
    return api.findById(request);
  }

  @Autowired
  private DictionaryApi api;
}
