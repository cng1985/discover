package com.haoxuer.discover.config.utils;

import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.rest.base.ResponseList;
import com.haoxuer.discover.rest.base.ResponsePage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ada on 2017/5/23.
 */
public class ConverResourceUtils {
  
  public ConverResourceUtils() {
  }
  
  
  private static <R, S> void conver(ResponsePage<R> result, Page<S> pager) {
    result.setNo(pager.getPageNumber());
    result.setSize(pager.getPageSize());
    result.setTotal((int) pager.getTotal());
    result.setTotalPage(pager.getTotalPages());
  }

  private static <R, S> void conver(ResponsePage<R> result, Pagination<S> pager) {
    result.setNo(pager.getPageNo());
    result.setSize(pager.getPageSize());
    result.setTotal((int) pager.getTotalCount());
    result.setTotalPage(pager.getTotalPage());
  }
  
  public static <R, S> ResponsePage<R> converPage(ResponsePage<R> result, Page<S> pager, Conver<R, S> conver) {
    conver(result, pager);
    List<S> cs = pager.getContent();
    List<R> vos=new ArrayList<>();
    if (cs!=null){
      vos =converCollect(cs,conver);
    }
    result.setList(vos);
    return result;
  }
  
  public static <R, S> ResponsePage<R> converPage(ResponsePage<R> result, Pagination<S> pager, Conver<R, S> conver) {
    conver(result, pager);
    List<R> vos = new ArrayList();
    List<S> cs = pager.getList();
    if (cs!=null){
      vos =converCollect(cs,conver);
    }
    result.setList(vos);
    return result;
  }
  
  public static <R, S> ResponseList<R> converList(ResponseList<R> result, Page<S> pager, Conver<R, S> conver) {
    List<R> vos = new ArrayList();
    List<S> cs = pager.getContent();
    if (cs!=null){
      vos =converCollect(cs,conver);
    }
    result.setList(vos);
    return result;
  }
  public static <R, S> ResponseList<R> converList(ResponseList<R> result, List<S> cs, Conver<R, S> conver) {
    List<R> vos = new ArrayList();
    if (cs!=null){
      vos =converCollect(cs,conver);
    }
    result.setList(vos);
    return result;
  }

  public static <R, S> List<R> converList(List<S> source, Conver<R, S> conver) {
    return converCollect(source,conver);
  }
  
  public static <R, S> List<R> converCollect(Collection<S> source, Conver<R, S> conver) {
    List<R> vos = new ArrayList();
    if (source != null) {
      for (S item : source) {
        vos.add(conver.conver(item));
      }
    }
    return vos;
  }
}
