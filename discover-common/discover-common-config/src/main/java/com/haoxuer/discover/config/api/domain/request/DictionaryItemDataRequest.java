package com.haoxuer.discover.config.api.domain.request;


import com.haoxuer.discover.data.enums.StoreState;
import lombok.Data;

/**
*
* Created by imake on 2021年05月22日11:11:56.
*/

@Data
public class DictionaryItemDataRequest  {

    private Long id;

     private Long dictionary;

     private int sortNum;

     private String name;

     private StoreState state;


}