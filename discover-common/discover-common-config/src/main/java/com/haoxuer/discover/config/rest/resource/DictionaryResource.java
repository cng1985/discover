package com.haoxuer.discover.config.rest.resource;

import com.haoxuer.discover.config.api.domain.request.DictionaryKeyRequest;
import com.haoxuer.discover.config.api.domain.request.FindByIdRequest;
import com.haoxuer.discover.config.api.domain.response.DictionaryResponse;
import com.haoxuer.discover.config.api.handle.DictionaryApi;
import com.haoxuer.discover.config.data.dao.DictionaryDao;
import com.haoxuer.discover.config.data.entity.Dictionary;
import com.haoxuer.discover.config.rest.conver.DictionaryResponseConver;
import com.haoxuer.discover.data.page.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class DictionaryResource implements DictionaryApi {

  @Autowired
  private DictionaryDao dictionaryDao;

  @Override
  public DictionaryResponse key(DictionaryKeyRequest request) {
    DictionaryResponse result = new DictionaryResponse();

    Dictionary dictionary = dictionaryDao.one(Filter.eq("key", request.getKey()));
    if (dictionary == null) {
      result.setCode(501);
      result.setMsg("无效key");
      return result;
    }
    result=new DictionaryResponseConver().conver(dictionary);
    return result;
  }

  @Override
  public DictionaryResponse findById(FindByIdRequest request) {
    DictionaryResponse result = new DictionaryResponse();
    Dictionary dictionary = dictionaryDao.findById(request.getId());
    if (dictionary == null) {
      result.setCode(501);
      result.setMsg("无效id");
      return result;
    }
    result=new DictionaryResponseConver().conver(dictionary);
    return result;
  }
}
