package com.haoxuer.discover.config.data.service;

import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2019年03月11日15:34:11.
 */
public interface DictionaryItemService {

  DictionaryItem findById(Long id);

  DictionaryItem save(DictionaryItem bean);

  DictionaryItem update(DictionaryItem bean);

  DictionaryItem deleteById(Long id);

  DictionaryItem[] deleteByIds(Long[] ids);

  Page<DictionaryItem> page(Pageable pageable);

  Page<DictionaryItem> page(Pageable pageable, Object search);

  List<String> key(String key);

  List<String> forId(Long id);


  List<DictionaryItem> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}