package com.haoxuer.discover.config.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.config.data.entity.User;

/**
* Created by imake on 2018年12月17日21:25:13.
*/
public interface UserDao extends BaseDao<User,Long>{

	 User findById(Long id);

	 User save(User bean);

	 User updateByUpdater(Updater<User> updater);

	 User deleteById(Long id);
}