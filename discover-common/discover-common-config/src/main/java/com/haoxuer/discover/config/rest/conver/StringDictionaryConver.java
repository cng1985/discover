package com.haoxuer.discover.config.rest.conver;

import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.data.rest.core.Conver;

public class StringDictionaryConver implements Conver<String, DictionaryItem> {
  @Override
  public String conver(DictionaryItem source) {
    return source.getName();
  }
}
