package com.haoxuer.discover.config.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;

import java.io.Serializable;

/**
* Created by imake on 2019年03月11日11:40:11.
*/
public class DictionarySo implements Serializable {

    @Search(name = "name",operator = Filter.Operator.like)
    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
