package com.haoxuer.discover.config.rest.conver;

import com.haoxuer.discover.config.api.domain.simple.UserSimple;
import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.rest.core.Conver;
import com.vdurmont.emoji.EmojiParser;

public class UserSimpleConver implements Conver<UserSimple, User> {
  @Override
  public UserSimple conver(User source) {
    UserSimple result=new UserSimple();
    result.setId(source.getId());
    result.setAvatar(source.getAvatar());
    result.setLoginSize(source.getLoginSize());
    if (source.getName()!=null){
      String note = EmojiParser.parseToUnicode(source.getName());
      result.setName(note);
    }else{
      result.setName("无名氏");
    }
    result.setPhone(source.getPhone());
    return result;
  }
}
