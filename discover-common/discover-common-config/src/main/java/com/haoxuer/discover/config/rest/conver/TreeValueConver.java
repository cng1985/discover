package com.haoxuer.discover.config.rest.conver;

import com.haoxuer.discover.config.api.domain.simple.TreeSimple;
import com.haoxuer.discover.data.entity.CatalogEntity;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.rest.simple.TreeValue;

public class TreeValueConver implements Conver<TreeValue, CatalogEntity> {
    @Override
    public TreeValue conver(CatalogEntity source) {
        TreeValue result=new TreeValue();
        result.setValue(source.getId()+"");
        result.setLabel(source.getName());
        return result;
    }
}
