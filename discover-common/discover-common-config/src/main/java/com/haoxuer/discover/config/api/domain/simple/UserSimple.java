package com.haoxuer.discover.config.api.domain.simple;

import java.io.Serializable;

public class UserSimple implements Serializable {

  private Long id;

  /**
   * 用户头像
   */
  private String avatar;


  /**
   * 手机号码
   */
  private String phone;

  /**
   * 用户登录次数
   */
  private Integer loginSize = 0;

  /**
   * 用户真实姓名
   */
  private String name;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Integer getLoginSize() {
    return loginSize;
  }

  public void setLoginSize(Integer loginSize) {
    this.loginSize = loginSize;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
