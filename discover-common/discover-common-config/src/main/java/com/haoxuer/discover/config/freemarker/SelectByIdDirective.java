package com.haoxuer.discover.config.freemarker;

import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.config.data.service.DictionaryItemService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SelectByIdDirective extends ListDirective<String> {

  @Autowired
  DictionaryItemService service;

  @Override
  public List<String> list() {
    List<String> list = service.forId(getLong("id", 1L));
    return list;
  }
}
