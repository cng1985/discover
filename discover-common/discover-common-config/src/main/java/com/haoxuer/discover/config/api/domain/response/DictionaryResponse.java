package com.haoxuer.discover.config.api.domain.response;

import com.haoxuer.discover.rest.base.ResponseObject;

import java.util.ArrayList;
import java.util.List;

public class DictionaryResponse extends ResponseObject {

  private String name;

  private List<String> items=new ArrayList<>();

  private Integer version;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getItems() {
    return items;
  }

  public void setItems(List<String> items) {
    this.items = items;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }
}
