package com.haoxuer.discover.config.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by imake on 2021年05月22日11:11:56.
*/

@Data
public class DictionaryItemResponse extends ResponseObject {

    private Long id;

     private Long dictionary;

     private int sortNum;

     private String name;

     private StoreState state;


     private String stateName;
}