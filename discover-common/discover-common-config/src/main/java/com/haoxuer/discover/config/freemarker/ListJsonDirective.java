package com.haoxuer.discover.config.freemarker;

import com.google.gson.Gson;
import com.haoxuer.discover.common.freemarker.TextDirective;
import freemarker.core.Environment;

import java.io.IOException;
import java.util.List;

public abstract class ListJsonDirective<T> extends TextDirective {

    public abstract List<T> getList();

    @Override
    public void handle(Environment env) throws IOException {
        List<T> list = getList();
        if (list != null && list.size() > 0) {
            env.getOut().append(new Gson().toJson(list));
        } else {
            env.getOut().append("[]");
        }
    }
}
