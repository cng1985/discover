package com.haoxuer.discover.config.utils;


import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.rest.core.Converter;
import com.haoxuer.discover.rest.response.ListResponse;
import com.haoxuer.discover.rest.response.PageResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ada on 2017/5/23.
 */
public class ConverterResourceUtils {
  
  public ConverterResourceUtils() {
  }
  
  

  private static <R, S> void convert(PageResponse<R> result, Page<S> pager) {
    result.setNo(pager.getNo());
    result.setSize(pager.getPageSize());
    result.setTotal(pager.getTotal());
    result.setTotalPage(pager.getTotalPages());
  }
  

  public static <R, S> PageResponse<R> convertPage(PageResponse<R> result, Page<S> pager, Converter<R, S> convert) {
    convert(result, pager);
    List<R> vos = new ArrayList<>();
    List<S> cs = pager.getContent();
    if (cs!=null){
      vos = convertCollect(cs,convert);
    }
    result.setData(vos);
    return result;
  }
  
  public static <R, S> ListResponse<R> convertList(ListResponse<R> result, Page<S> pager, Converter<R, S> convert) {
    List<R> vos = new ArrayList<>();
    List<S> cs = pager.getContent();
    if (cs!=null){
      vos = convertCollect(cs,convert);
    }
    result.setData(vos);
    return result;
  }
  public static <R, S> ListResponse<R> convertList(ListResponse<R> result, List<S> cs, Converter<R, S> convert) {
    List<R> vos = new ArrayList<>();
    if (cs!=null){
      vos = convertCollect(cs,convert);
    }
    result.setData(vos);
    return result;
  }

  public static <R, S> List<R> convertList(List<S> source, Converter<R, S> convert) {
    return convertCollect(source,convert);
  }
  
  public static <R, S> List<R> convertCollect(Collection<S> source, Converter<R, S> convert) {
    List<R> vos = new ArrayList();
    if (source != null) {
      for (S item : source) {
        vos.add(convert.convert(item));
      }
    }
    return vos;
  }
}
