package com.haoxuer.discover.config.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.config.data.dao.ConfigOptionDao;
import com.haoxuer.discover.config.data.entity.ConfigOption;

import java.util.Date;

/**
 * Created by imake on 2018年12月17日10:47:24.
 */
@Repository

public class ConfigOptionDaoImpl extends CriteriaDaoImpl<ConfigOption, Long> implements ConfigOptionDao {

  @Override
  public ConfigOption findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public ConfigOption save(ConfigOption bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public ConfigOption deleteById(Long id) {
    ConfigOption entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public String key(String key) {
    ConfigOption option = one(Filter.eq("key", key));
    if (option != null) {
      return option.getValue();
    }
    return null;
  }

  @Override
  public void put(String key, String value) {
    ConfigOption option = one(Filter.eq("key", key));
    if (option == null) {
      option = new ConfigOption();
      option.setKey(key);
      save(option);
    }
    option.setLastDate(new Date());
    option.setValue(value);
  }

  @Override
  protected Class<ConfigOption> getEntityClass() {
    return ConfigOption.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}