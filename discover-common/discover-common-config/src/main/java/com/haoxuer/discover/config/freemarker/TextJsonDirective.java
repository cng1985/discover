package com.haoxuer.discover.config.freemarker;

import com.google.gson.Gson;
import com.haoxuer.discover.common.freemarker.TextDirective;
import com.haoxuer.discover.rest.base.ResponseList;
import com.haoxuer.discover.rest.simple.TreeValue;
import freemarker.core.Environment;

import java.io.IOException;

public abstract class TextJsonDirective extends TextDirective {

    public abstract ResponseList<TreeValue> getResponseList();

    @Override
    public void handle(Environment env) throws IOException {
        ResponseList<TreeValue> list = getResponseList();
        if (list != null && list.getList() != null && list.getList().size() > 0) {
            env.getOut().append(new Gson().toJson(list.getList()));
        } else {
            env.getOut().append("[]");
        }
    }
}
