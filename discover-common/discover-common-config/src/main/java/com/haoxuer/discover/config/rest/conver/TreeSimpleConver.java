package com.haoxuer.discover.config.rest.conver;

import com.haoxuer.discover.config.api.domain.simple.TreeSimple;
import com.haoxuer.discover.data.entity.CatalogEntity;
import com.haoxuer.discover.data.rest.core.Conver;

public class TreeSimpleConver implements Conver<TreeSimple, CatalogEntity> {
    @Override
    public TreeSimple conver(CatalogEntity source) {
        TreeSimple result=new TreeSimple();
        result.setId(source.getId());
        result.setLabel(source.getName());
        return result;
    }
}
