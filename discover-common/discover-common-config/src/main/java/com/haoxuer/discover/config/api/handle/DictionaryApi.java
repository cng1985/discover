package com.haoxuer.discover.config.api.handle;

import com.haoxuer.discover.config.api.domain.request.DictionaryKeyRequest;
import com.haoxuer.discover.config.api.domain.request.FindByIdRequest;
import com.haoxuer.discover.config.api.domain.response.DictionaryResponse;

public interface DictionaryApi {

  /**
   * 根据key查找
   * @param request
   * @return
   */
  DictionaryResponse key(DictionaryKeyRequest request);

  /**
   * 根据id查找字典
   *
   * @param request
   * @return
   */
  DictionaryResponse findById(FindByIdRequest request);

}
