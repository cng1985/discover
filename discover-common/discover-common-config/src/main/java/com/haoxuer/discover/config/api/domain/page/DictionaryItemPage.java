package com.haoxuer.discover.config.api.domain.page;


import com.haoxuer.discover.config.api.domain.simple.DictionaryItemSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月22日11:11:56.
*/

@Data
public class DictionaryItemPage  extends ResponsePage<DictionaryItemSimple> {

}