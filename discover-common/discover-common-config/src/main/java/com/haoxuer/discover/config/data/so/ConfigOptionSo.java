package com.haoxuer.discover.config.data.so;

import java.io.Serializable;

/**
* Created by imake on 2018年12月17日10:47:24.
*/
public class ConfigOptionSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
