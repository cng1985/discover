package com.haoxuer.discover.config.controller.admin;

import com.haoxuer.discover.config.data.entity.Dictionary;
import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.config.data.service.DictionaryItemService;
import com.haoxuer.discover.config.data.service.DictionaryService;
import com.haoxuer.discover.config.data.so.DictionarySo;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collections;

/**
 * Created by imake on 2019年03月11日11:40:11.
 */


@Scope("prototype")
@Controller
public class DictionaryAction {

  public static final String MODEL = "model";

  public static final String REDIRECT_LIST_HTML = "redirect:/admin/dictionary/view_list.htm";

  private static final Logger log = LoggerFactory.getLogger(DictionaryAction.class);

  @Autowired
  private DictionaryService manager;

  @Autowired
  private DictionaryItemService itemService;


  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/view_list")
  public String list(Pageable pageable, DictionarySo so, ModelMap model) {

    if (pageable == null) {
      pageable = new Pageable();
    }
    if (pageable.getOrders().isEmpty()) {
      pageable.getOrders().add(Order.desc("id"));
    }
    pageable.getFilters().addAll(FilterUtils.getFilters(so));
    pageable.getFilters().add(Filter.eq("storeState", StoreState.normal));
    Page<Dictionary> pagination = manager.page(pageable);
    model.addAttribute("list", pagination.getContent());
    model.addAttribute("page", pagination);
    model.addAttribute("so", so);
    return "/admin/dictionary/list";
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/view_add")
  public String add(ModelMap model) {
    return "/admin/dictionary/add";
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/view_edit")
  public String edit(Pageable pageable, Long id, ModelMap model) {
    model.addAttribute(MODEL, manager.findById(id));
    model.addAttribute("page", pageable);
    return "/admin/dictionary/edit";
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/view_config")
  public String view(Long id, ModelMap model) {
    Dictionary dictionary = manager.findById(id);
    model.addAttribute(MODEL, dictionary);
    if (dictionary != null) {
      Collections.sort(dictionary.getItems());
      model.addAttribute("items", dictionary.getItems());
    }
    return "/admin/dictionary/config";
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/model_save_item")
  public String saveItem(DictionaryItem bean, RedirectAttributes attributes) {

    String view = "redirect:/admin/dictionary/view_config.htm";
    try {
      attributes.addAttribute("id", bean.getDictionary().getId());
      itemService.save(bean);
      log.info("save object id={}", bean.getId());
    } catch (Exception e) {
      e.printStackTrace();
      log.error("保存失败", e);
    }
    return view;
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/model_delete_item")
  public String deleteItem(Long id, RedirectAttributes attributes) {

    String view = "redirect:/admin/dictionary/view_config.htm";
    try {
      DictionaryItem item = itemService.findById(id);
      itemService.deleteById(id);
      attributes.addAttribute("id", item.getDictionary().getId());
    } catch (Exception e) {
      log.error("删除失败", e);
    }
    return view;
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/model_update_item")
  public String updateItem(DictionaryItem bean, RedirectAttributes attributes) {

    String view = "redirect:/admin/dictionary/view_config.htm";
    try {
      itemService.update(bean);
      log.info("save object id={}", bean.getId());
      attributes.addAttribute("id", bean.getDictionary().getId());
    } catch (Exception e) {
      log.error("保存失败", e);
    }
    return view;
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/model_save")
  public String save(Dictionary bean, ModelMap model) {

    String view = REDIRECT_LIST_HTML;
    try {
      manager.save(bean);
      log.info("save object id={}", bean.getId());
    } catch (Exception e) {
      log.error("保存失败", e);
      model.addAttribute("erro", e.getMessage());
      view = "/admin/dictionary/add";
    }
    return view;
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/model_update")
  public String update(Pageable pageable, Dictionary bean, RedirectAttributes redirectAttributes, ModelMap model) {

    String view = REDIRECT_LIST_HTML;
    try {
      manager.update(bean);
      initRedirectData(pageable, redirectAttributes);
    } catch (Exception e) {
      log.error("更新失败", e);
      model.addAttribute("erro", e.getMessage());
      model.addAttribute(MODEL, bean);
      model.addAttribute("page", pageable);
      view = "/admin/dictionary/edit";
    }
    return view;
  }

  @RequiresPermissions("dictionary")
  @RequestMapping("/admin/dictionary/model_delete")
  public String delete(Pageable pageable, Long id, RedirectAttributes redirectAttributes) {

    String view = REDIRECT_LIST_HTML;
    try {
      initRedirectData(pageable, redirectAttributes);
      manager.deleteById(id);
    } catch (DataIntegrityViolationException e) {
      log.error("删除失败", e);
      redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
    }

    return view;
  }

  private void initRedirectData(Pageable pageable, RedirectAttributes redirectAttributes) {
    redirectAttributes.addAttribute("pageNumber", pageable.getPageNumber());
  }
}