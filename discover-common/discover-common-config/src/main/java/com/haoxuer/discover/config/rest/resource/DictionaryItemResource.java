package com.haoxuer.discover.config.rest.resource;

import com.haoxuer.discover.config.api.apis.DictionaryItemApi;
import com.haoxuer.discover.config.api.domain.list.DictionaryItemList;
import com.haoxuer.discover.config.api.domain.page.DictionaryItemPage;
import com.haoxuer.discover.config.api.domain.request.*;
import com.haoxuer.discover.config.api.domain.response.DictionaryItemResponse;
import com.haoxuer.discover.config.data.dao.DictionaryItemDao;
import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.config.rest.convert.DictionaryItemResponseConvert;
import com.haoxuer.discover.config.rest.convert.DictionaryItemSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.config.data.dao.DictionaryDao;
import java.util.ArrayList;
import java.util.List;



@Transactional
@Component
public class DictionaryItemResource implements DictionaryItemApi {

    @Autowired
    private DictionaryItemDao dataDao;

    @Autowired
    private DictionaryDao dictionaryDao;

    @Override
    public DictionaryItemResponse create(DictionaryItemDataRequest request) {
        DictionaryItemResponse result = new DictionaryItemResponse();

        DictionaryItem bean = new DictionaryItem();
        handleData(request, bean);
        dataDao.save(bean);
        result = new DictionaryItemResponseConvert().conver(bean);
        return result;
    }

    @Override
    public DictionaryItemResponse update(DictionaryItemDataRequest request) {
        DictionaryItemResponse result = new DictionaryItemResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        DictionaryItem bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new DictionaryItemResponseConvert().conver(bean);
        return result;
    }

    private void handleData(DictionaryItemDataRequest request, DictionaryItem bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getDictionary()!=null){
               bean.setDictionary(dictionaryDao.findById(request.getDictionary()));
            }

    }

    @Override
    public DictionaryItemResponse delete(DictionaryItemDataRequest request) {
        DictionaryItemResponse result = new DictionaryItemResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public DictionaryItemResponse view(DictionaryItemDataRequest request) {
        DictionaryItemResponse result=new DictionaryItemResponse();
        DictionaryItem bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new DictionaryItemResponseConvert().conver(bean);
        return result;
    }
    @Override
    public DictionaryItemList list(DictionaryItemSearchRequest request) {
        DictionaryItemList result = new DictionaryItemList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<DictionaryItem> organizations = dataDao.list(0, 100, filters, orders);
        DictionaryItemSimpleConvert convert=new DictionaryItemSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public DictionaryItemPage search(DictionaryItemSearchRequest request) {
        DictionaryItemPage result=new DictionaryItemPage();
        Pageable pageable = new Pageable();
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<DictionaryItem> page=dataDao.page(pageable);
        DictionaryItemSimpleConvert convert=new DictionaryItemSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
