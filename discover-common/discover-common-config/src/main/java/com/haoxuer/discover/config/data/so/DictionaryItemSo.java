package com.haoxuer.discover.config.data.so;

import java.io.Serializable;

/**
* Created by imake on 2019年03月11日15:34:11.
*/
public class DictionaryItemSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
