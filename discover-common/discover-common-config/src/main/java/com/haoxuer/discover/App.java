package com.haoxuer.discover;

import com.haoxuer.discover.config.data.entity.ConfigOption;
import com.haoxuer.discover.config.data.entity.Dictionary;
import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.elementuiForm.ElementUIFormDir;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        CodeMake make = new CodeMake(ElementUIFormDir.class, TemplateHibernateDir.class);
        File view = new File("E:\\workspace\\iowl\\iowl_web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(DictionaryItem.class);

        make.setDao(false);
        make.setService(false);
        make.setView(false);
        make.setAction(false);
        make.setRest(true);
        make.setApi(true);
        make.makes(cs);
        System.out.println("ok");
    }
}
