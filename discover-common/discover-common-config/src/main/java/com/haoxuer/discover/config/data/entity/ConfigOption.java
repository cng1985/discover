package com.haoxuer.discover.config.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "system_config_option")
public class ConfigOption extends AbstractEntity {

  @Column(name = "config_key", unique = true, length = 50)
  private String key;

  @Column(name = "config_value")
  private String value;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
