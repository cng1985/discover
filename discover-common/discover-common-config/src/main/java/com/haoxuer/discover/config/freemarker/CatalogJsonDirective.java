package com.haoxuer.discover.config.freemarker;

import com.google.gson.Gson;
import com.haoxuer.discover.common.utils.DirectiveUtils;
import com.haoxuer.discover.config.api.domain.simple.TreeSimple;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CatalogJsonDirective implements TemplateDirectiveModel {

    public Integer getInt(String name) {
        Integer size = null;

        try {
            size = DirectiveUtils.getInt(name, this.map);
        } catch (Exception var4) {
        }

        return size;
    }

    public Integer getInt(String name, Integer num) {
        Integer size = null;

        try {
            size = DirectiveUtils.getInt(name, this.map);
            if (size == null) {
                size = num;
            }
        } catch (Exception var5) {
        }

        return size;
    }

    public Long getLong(String name, Long num) {
        Long size = null;

        try {
            size = DirectiveUtils.getLong(name, this.map);
            if (size == null) {
                size = num;
            }
        } catch (Exception var5) {
        }

        return size;
    }

    public String getString(String name, String defaul) {
        String result = null;

        try {
            result = DirectiveUtils.getString(name, this.map);
        } catch (Exception var5) {
        }

        if (defaul == null) {
            defaul = "";
        }

        if (result == null) {
            result = defaul;
        }

        return result;
    }

    private Map map;


    public List<Filter> filters() {

        List<Filter> filter = new ArrayList<Filter>();
        Integer id = getInt("id");
        if (id == null) {
            id = 1;
        }
        filter.add(Filter.eq("parent.id", id));
        return filter;
    }

    public List<Order> orders() {
        List<Order> orders = new ArrayList<Order>();
        orders.add(Order.asc("code"));
        return orders;
    }

    public abstract List<TreeSimple> getList();

    @Override
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        this.map = params;
        Map<String, TemplateModel> paramWrap = new HashMap(params);
        Map<String, TemplateModel> origMap = DirectiveUtils.addParamsToVariable(env, paramWrap);
        if (env != null) {

            List<TreeSimple> list = getList();
            if (list != null && list.size() > 0) {
                env.getOut().append(new Gson().toJson(list));
            } else {
                env.getOut().append("[]");
            }
        }
        DirectiveUtils.removeParamsFromVariable(env, paramWrap, origMap);
    }
}
