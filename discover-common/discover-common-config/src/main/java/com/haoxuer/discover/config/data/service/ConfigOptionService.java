package com.haoxuer.discover.config.data.service;

import com.haoxuer.discover.config.data.entity.ConfigOption;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月17日10:47:24.
*/
public interface ConfigOptionService {

	ConfigOption findById(Long id);

	ConfigOption save(ConfigOption bean);

	ConfigOption update(ConfigOption bean);

	ConfigOption deleteById(Long id);
	
	ConfigOption[] deleteByIds(Long[] ids);
	
	Page<ConfigOption> page(Pageable pageable);
	
	Page<ConfigOption> page(Pageable pageable, Object search);


	List<ConfigOption> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}