package com.haoxuer.discover.config.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.config.data.dao.UserDao;
import com.haoxuer.discover.config.data.entity.User;

/**
* Created by imake on 2018年12月17日21:25:13.
*/
@Repository

public class UserDaoImpl extends CriteriaDaoImpl<User, Long> implements UserDao {

	@Override
	public User findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public User save(User bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public User deleteById(Long id) {
		User entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<User> getEntityClass() {
		return User.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}