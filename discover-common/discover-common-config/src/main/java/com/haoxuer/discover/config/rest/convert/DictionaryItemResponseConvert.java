package com.haoxuer.discover.config.rest.convert;

import com.haoxuer.discover.config.api.domain.response.DictionaryItemResponse;
import com.haoxuer.discover.config.data.entity.DictionaryItem;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class DictionaryItemResponseConvert implements Conver<DictionaryItemResponse, DictionaryItem> {
    @Override
    public DictionaryItemResponse conver(DictionaryItem source) {
        DictionaryItemResponse result = new DictionaryItemResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getDictionary()!=null){
           result.setDictionary(source.getDictionary().getId());
        }

         result.setStateName(source.getState()+"");

        return result;
    }
}
