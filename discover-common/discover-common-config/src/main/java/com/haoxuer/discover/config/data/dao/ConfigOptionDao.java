package com.haoxuer.discover.config.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.config.data.entity.ConfigOption;

/**
* Created by imake on 2018年12月17日10:47:24.
*/
public interface ConfigOptionDao extends BaseDao<ConfigOption,Long>{

	 ConfigOption findById(Long id);

	 ConfigOption save(ConfigOption bean);

	 ConfigOption updateByUpdater(Updater<ConfigOption> updater);

	 ConfigOption deleteById(Long id);

	/**
	 * 获取某个配置项
	 * @param key
	 * @return
	 */
	 String key(String key);


	 void put(String key,String value);
}