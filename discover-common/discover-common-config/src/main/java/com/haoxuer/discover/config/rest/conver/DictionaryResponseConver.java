package com.haoxuer.discover.config.rest.conver;

import com.haoxuer.discover.config.api.domain.response.DictionaryResponse;
import com.haoxuer.discover.config.data.entity.Dictionary;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.rest.core.Conver;

import java.util.Collections;

public class DictionaryResponseConver implements Conver<DictionaryResponse, Dictionary> {
  @Override
  public DictionaryResponse conver(Dictionary source) {
    DictionaryResponse result=new DictionaryResponse();
    result.setName(source.getName());
    result.setVersion(source.getVersion());
    if (source.getItems()!=null){
      Collections.sort(source.getItems());
      result.setItems(ConverResourceUtils.converCollect(source.getItems(),new StringDictionaryConver()));
    }

    return result;
  }
}
