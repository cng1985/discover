package com.haoxuer.discover.config.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.InputType;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.enums.StoreState;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "system_dictionary")
public class Dictionary extends AbstractEntity {

  @FormField(title = "字典名称", sortNum = "1", grid = true, col = 12, type = InputType.text)
  private String name;

  @FormField(title = "字典描述", sortNum = "4", grid = true, col = 12, type = InputType.text)
  private String note;

  @FormField(title = "字典key", sortNum = "2", grid = true, col = 12, type = InputType.text)
  @Column(name = "data_key",length = 30,unique = true)
  private String key;

  @FormField(title = "字典版本", sortNum = "3", grid = true, col = 12, type = InputType.text)
  private Integer version;

  /**
   * 存储状态
   */
  private StoreState storeState;

  @OneToMany(fetch = FetchType.LAZY,mappedBy = "dictionary")
  private List<DictionaryItem> items=null;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public List<DictionaryItem> getItems() {
    return items;
  }

  public void setItems(List<DictionaryItem> items) {
    this.items = items;
  }

  public StoreState getStoreState() {
    return storeState;
  }

  public void setStoreState(StoreState storeState) {
    this.storeState = storeState;
  }
}
