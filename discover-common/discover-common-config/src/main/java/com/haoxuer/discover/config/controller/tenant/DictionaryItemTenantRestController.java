package com.haoxuer.discover.config.controller.tenant;

import com.haoxuer.discover.config.api.apis.DictionaryItemApi;
import com.haoxuer.discover.config.api.domain.list.DictionaryItemList;
import com.haoxuer.discover.config.api.domain.page.DictionaryItemPage;
import com.haoxuer.discover.config.api.domain.request.*;
import com.haoxuer.discover.config.api.domain.response.DictionaryItemResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/dictionaryitem")
@RestController
public class DictionaryItemTenantRestController  {


	@RequiresPermissions("dictionaryitem")
    @RequiresUser
    @RequestMapping("create")
    public DictionaryItemResponse create(DictionaryItemDataRequest request) {
        return api.create(request);
    }

	@RequiresPermissions("dictionaryitem")
    @RequiresUser
    @RequestMapping("update")
    public DictionaryItemResponse update(DictionaryItemDataRequest request) {
        return api.update(request);
    }

	@RequiresPermissions("dictionaryitem")
    @RequiresUser
    @RequestMapping("delete")
    public DictionaryItemResponse delete(DictionaryItemDataRequest request) {
        DictionaryItemResponse result = new DictionaryItemResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("dictionaryitem")
    @RequiresUser
    @RequestMapping("view")
    public DictionaryItemResponse view(DictionaryItemDataRequest request) {
       return api.view(request);
   }

    @RequiresUser
    @RequestMapping("list")
    public DictionaryItemList list(DictionaryItemSearchRequest request) {
        return api.list(request);
    }

    @RequiresUser
    @RequestMapping("search")
    public DictionaryItemPage search(DictionaryItemSearchRequest request) {
        return api.search(request);
    }

    @Autowired
    private DictionaryItemApi api;

}
