package com.haoxuer.discover.config.api.domain.simple;

import java.io.Serializable;
import java.util.List;

public class TreeSimple implements Serializable {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public List<TreeSimple> getChildren() {
        return children;
    }

    public void setChildren(List<TreeSimple> children) {
        this.children = children;
    }

    private Integer id;

    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    private List<TreeSimple> children;
}
