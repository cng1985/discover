package com.haoxuer.discover.user;

import com.haoxuer.discover.rest.simple.TreeItem;
import com.haoxuer.discover.rest.simple.TreeLabel;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );

        TreeLabel item=new TreeLabel();
        List<TreeItem> items=new ArrayList<>();
        items.add(new TreeLabel());
        items.add(new TreeLabel());
        item.setChildren(items);
        assertEquals(2,item.getChildren().size());
    }
}
