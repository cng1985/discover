package com.haoxuer.discover.rest.response;

import java.util.HashMap;

public class ResponseMap extends HashMap<String, Object> {


    public ResponseMap(){
        put("code",0);
        put("msg","成功");
    }
    public void setMsg(String msg) {
        put("msg",msg);
    }
    public void setCode(int code) {
        put("code",code);
    }
}
