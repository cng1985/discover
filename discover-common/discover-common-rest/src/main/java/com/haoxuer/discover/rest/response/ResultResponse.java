package com.haoxuer.discover.rest.response;

import com.haoxuer.discover.rest.enums.ResponseType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 接口响应结果
 *
 * @author ada
 */

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode()
public class ResultResponse<T> implements Serializable {


    /**
     * 是否成功
     */
    private boolean success = true;

    /**
     * 状态码
     */
    private int code = 200;

    /**
     * 状态消息
     */
    private String msg = "success";

    protected ResponseType responseType;

    private T data;


    public ResultResponse() {
        responseType = ResponseType.object;
    }
}
