package com.haoxuer.discover.rest.handler;

import com.haoxuer.discover.rest.base.RequestObject;

/**
 * Created by ada on 2017/5/16.
 */
public interface SignHandler {
    String sign(RequestObject object);
}
