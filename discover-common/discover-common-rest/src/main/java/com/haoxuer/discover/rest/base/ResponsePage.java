package com.haoxuer.discover.rest.base;

/**
 * 返回分页对象
 * Created by ada on 2017/5/16.
 */

@Deprecated
public class ResponsePage<T> extends ResponseList<T> {
    /**
     * 当前页码
     */
    private int no;

    /**
     * 分页大小
     */
    private int size;

    /**
     * 数据合计
     */
    private int total;

    /**
     * 总共有多少页
     */
    private int totalPage;

    public int getSplitSize() {
        return splitSize;
    }

    public void setSplitSize(int splitSize) {
        this.splitSize = splitSize;
    }

    private int splitSize = 3;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * 第一条数据位置
     *
     */
    public int getFirstNo() {
        int result = 1;
        result = getNo() - splitSize;

        int t = getEndNo() - result;
        int num = splitSize * 2;
        if (t < num) {
            result = result - (num - t);
        }
        if (result <= 0) {
            result = 1;
        }
        return result;
    }


    /**
     * 第一条数据位置
     *
     */
    public int getEndNo() {
        int result = 1;
        result = getNo() + splitSize;

        if (getNo() <= splitSize) {
            result = result + splitSize - getNo();
            result++;
        } else {
        }

        if (result > getTotalPage()) {
            result = getTotalPage();
        }
        return result;
    }
}
