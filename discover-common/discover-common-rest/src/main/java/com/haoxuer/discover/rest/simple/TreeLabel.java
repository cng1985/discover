package com.haoxuer.discover.rest.simple;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TreeLabel implements TreeItem {

    private Integer id;

    private String value;

    private String label;

    private List<TreeItem> children;
}
