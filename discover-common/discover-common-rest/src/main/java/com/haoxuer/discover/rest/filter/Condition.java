package com.haoxuer.discover.rest.filter;

/**
 * @author ada
 */
public enum Condition {
    AND,
    OR;
    private Condition() {
    }
}