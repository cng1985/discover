package com.haoxuer.discover.rest.request;

import java.io.Serializable;

public interface RequestId {

     Serializable getId();
}
