package com.haoxuer.discover.rest.base;

import lombok.Data;
import lombok.ToString;

/**
 * Created by ada on 2017/5/16.
 */

@ToString(callSuper = true)
@Data
public class RequestTokenObject extends RequestObjectBase {

    private String token;

}
