package com.haoxuer.discover.rest.entity;

import com.haoxuer.discover.rest.base.ResponseList;

/**
 * 简单返回名字+id的集合
 * 
 * @author 73552
 *
 */
public class NameIntPage extends ResponseList<NameIntSimple> {

}
