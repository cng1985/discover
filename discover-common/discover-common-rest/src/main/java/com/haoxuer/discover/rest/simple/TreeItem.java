package com.haoxuer.discover.rest.simple;

import java.util.List;

public interface TreeItem {

     Integer getId();

     String getValue();

     String getLabel();

     List<TreeItem> getChildren();
}
