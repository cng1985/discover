package com.haoxuer.discover.rest.request;

import com.haoxuer.discover.rest.base.RequestTokenObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author ada
 */

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PageRequest extends RequestTokenObject {

    public Integer getPage() {
        if (page == null) {
            page = 1;
        }
        return page;
    }

    /**
     * 页码
     */
    private Integer page;

    private Integer no;
    private Integer size;

    /**
     * 分页数量大小
     */
    private Integer rows;

    private Integer pageSize;

    private String sortField;

    private String sortMethod;

    public Integer getNo() {
        if (no == null) {
            no = 1;
        }
        return no;
    }

    public Integer getSize() {
        if (size == null) {
            size = 10;
        }
        return size;
    }

    public Integer getRows() {
        if (rows == null) {
            rows = 10;
        }
        return rows;
    }

    public Integer getPageSize() {
        if (pageSize == null) {
            pageSize = 10;
        }
        return pageSize;
    }
}
