package com.haoxuer.discover.rest.request;

import com.haoxuer.discover.rest.base.RequestTokenObject;

/**
 * Created by ada on 2017/5/16.
 */
public class RequestKey extends RequestTokenObject {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
