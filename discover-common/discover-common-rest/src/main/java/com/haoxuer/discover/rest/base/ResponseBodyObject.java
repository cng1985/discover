package com.haoxuer.discover.rest.base;

public class ResponseBodyObject extends ResponseObject {


    private Object body;

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public ResponseBodyObject(Object body) {
        this.body = body;
    }

    public ResponseBodyObject() {
        this.body = new Object();
    }

    public static ResponseBodyObject body(Object object) {
        ResponseBodyObject result = new ResponseBodyObject();
        if (object != null) {
            result.setBody(object);
        }
        return result;
    }
}
