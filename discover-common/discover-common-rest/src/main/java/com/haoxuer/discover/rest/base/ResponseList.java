package com.haoxuer.discover.rest.base;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ada on 2017/5/16.
 */
@Deprecated
public class ResponseList<T> extends ResponseObject {

    private List<T> list=new ArrayList<>();

    public List<T> getList() {
        if (list==null){
            list=new ArrayList<>();
        }
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
