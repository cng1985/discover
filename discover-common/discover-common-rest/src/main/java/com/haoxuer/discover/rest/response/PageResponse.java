package com.haoxuer.discover.rest.response;

import com.haoxuer.discover.rest.enums.ResponseType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author ada
 */

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class PageResponse<T> extends ListResponse<T> {
    /**
     * 页码
     */
    private Integer no;

    /**
     * 分页大小
     */
    private Integer size;

    /**
     * 总数量
     */
    private Long total;

    private Integer totalPage;

    public PageResponse() {
        this.responseType = ResponseType.page;
    }

}
