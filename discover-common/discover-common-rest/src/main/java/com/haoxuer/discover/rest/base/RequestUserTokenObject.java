package com.haoxuer.discover.rest.base;

import lombok.Data;
import lombok.ToString;

/**
 * Created by ada on 2017/7/22.
 */

@ToString(callSuper = true)
@Data
public class RequestUserTokenObject extends RequestTokenObject {

    private String userToken;
}
