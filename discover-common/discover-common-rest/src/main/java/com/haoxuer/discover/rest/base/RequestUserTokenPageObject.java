package com.haoxuer.discover.rest.base;

import lombok.Data;
import lombok.ToString;

/**
 * Created by ada on 2017/7/22.
 */

@ToString(callSuper = true)
@Data
public class RequestUserTokenPageObject extends RequestUserTokenObject {

    private Integer no;

    protected Integer size;

    private String sortField;

    private String sortMethod;

    public Integer getNo() {
        if (no == null) {
            return 1;
        }
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Integer getSize() {
        if (size == null) {
            return 10;
        }
        if (size > 500) {
            return 500;
        }
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "RequestUserTokenPageObject{" +
                "no=" + no +
                ", size=" + size +
                '}';
    }
}
