package com.haoxuer.discover.rest.response;

import com.haoxuer.discover.rest.enums.ResponseType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode()
public class ListResponse<T> extends ResultResponse<List<T>> {

    public ListResponse() {
        this.responseType = ResponseType.list;
    }

}
