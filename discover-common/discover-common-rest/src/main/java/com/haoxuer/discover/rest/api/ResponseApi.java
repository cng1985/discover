package com.haoxuer.discover.rest.api;


import com.haoxuer.discover.rest.request.PageRequest;
import com.haoxuer.discover.rest.request.RequestId;
import com.haoxuer.discover.rest.response.ListResponse;
import com.haoxuer.discover.rest.response.PageResponse;
import com.haoxuer.discover.rest.response.ResultResponse;

/**
 * 响应接口
 *
 * @param <Response> 详情对象
 * @param <Simple> 列表对象
 * @param <Form> 表单对象
 * @param <Request> 搜索对象
 */
public interface ResponseApi<Response, Simple, Form extends RequestId, Request extends PageRequest> {

    /**
     * 分页查询
     *
     * @param request
     * @return 分页数据信息
     */
    PageResponse<Simple> search(Request request);

    /**
     * 根据条件查询集合，不分页
     *
     * @param request
     * @return 数据集合数据
     */
    ListResponse<Simple> list(Request request);

    /**
     * 创建
     *
     * @param request
     * @return 数据详情
     */
    ResultResponse<Response> create(Form request);

    /**
     * 更新
     *
     * @param request
     * @return 数据详情
     */
    ResultResponse<Response> update(Form request);

    /**
     * 删除
     *
     * @param request
     * @return 删除状态
     */
    ResultResponse<?> delete(Form request);

    /**
     * 根据ID查询详情
     *
     * @param request
     * @return 数据详情
     */
    ResultResponse<Response> view(Form request);

}
