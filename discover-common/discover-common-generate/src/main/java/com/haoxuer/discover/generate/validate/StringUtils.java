package com.haoxuer.discover.generate.validate;

public class StringUtils {

    public static String phone(String phone){
        if (phone!=null){
            return phone.replaceAll("(\\d{3})\\d{4}(\\d{4})","$1****$2");
        }else{
            return "";
        }
    }
}
