package com.haoxuer.discover.generate.utils;


public class BankCardBins {
  private static String[][] initArr = {
      {"620302", "中国工商银行"},
      {"620402", "中国工商银行"},
      {"620403", "中国工商银行"},
      {"620404", "中国工商银行"},
      {"620406", "中国工商银行"},
      {"620407", "中国工商银行"},
      {"620409", "中国工商银行"},
      {"620410", "中国工商银行"},
      {"620411", "中国工商银行"},
      {"620412", "中国工商银行"},
      {"620502", "中国工商银行"},
      {"620503", "中国工商银行"},
      {"620405", "中国工商银行"},
      {"620408", "中国工商银行"},
      {"620512", "中国工商银行"},
      {"620602", "中国工商银行"},
      {"620604", "中国工商银行"},
      {"620607", "中国工商银行"},
      {"620611", "中国工商银行"},
      {"620612", "中国工商银行"},
      {"620704", "中国工商银行"},
      {"620706", "中国工商银行"},
      {"620707", "中国工商银行"},
      {"620708", "中国工商银行"},
      {"620709", "中国工商银行"},
      {"620710", "中国工商银行"},
      {"620609", "中国工商银行"},
      {"620712", "中国工商银行"},
      {"620713", "中国工商银行"},
      {"620714", "中国工商银行"},
      {"620802", "中国工商银行"},
      {"620711", "中国工商银行"},
      {"620904", "中国工商银行"},
      {"620905", "中国工商银行"},
      {"621001", "中国工商银行"},
      {"620902", "中国工商银行"},
      {"621103", "中国工商银行"},
      {"621105", "中国工商银行"},
      {"621106", "中国工商银行"},
      {"621107", "中国工商银行"},
      {"621102", "中国工商银行"},
      {"621203", "中国工商银行"},
      {"621204", "中国工商银行"},
      {"621205", "中国工商银行"},
      {"621206", "中国工商银行"},
      {"621207", "中国工商银行"},
      {"621208", "中国工商银行"},
      {"621209", "中国工商银行"},
      {"621210", "中国工商银行"},
      {"621302", "中国工商银行"},
      {"621303", "中国工商银行"},
      {"621202", "中国工商银行"},
      {"621305", "中国工商银行"},
      {"621306", "中国工商银行"},
      {"621307", "中国工商银行"},
      {"621309", "中国工商银行"},
      {"621311", "中国工商银行"},
      {"621313", "中国工商银行"},
      {"621211", "中国工商银行"},
      {"621315", "中国工商银行"},
      {"621404", "中国工商银行"},
      {"621405", "中国工商银行"},
      {"621406", "中国工商银行"},
      {"621407", "中国工商银行"},
      {"621408", "中国工商银行"},
      {"621409", "中国工商银行"},
      {"621410", "中国工商银行"},
      {"621502", "中国工商银行"},
      {"621317", "中国工商银行"},
      {"621511", "中国工商银行"},
      {"621602", "中国工商银行"},
      {"621603", "中国工商银行"},
      {"621604", "中国工商银行"},
      {"621605", "中国工商银行"},
      {"621608", "中国工商银行"},
      {"621609", "中国工商银行"},
      {"621610", "中国工商银行"},
      {"621611", "中国工商银行"},
      {"621612", "中国工商银行"},
      {"621613", "中国工商银行"},
      {"621614", "中国工商银行"},
      {"621615", "中国工商银行"},
      {"621616", "中国工商银行"},
      {"621617", "中国工商银行"},
      {"621607", "中国工商银行"},
      {"621606", "中国工商银行"},
      {"621804", "中国工商银行"},
      {"621807", "中国工商银行"},
      {"621813", "中国工商银行"},
      {"621814", "中国工商银行"},
      {"621817", "中国工商银行"},
      {"621901", "中国工商银行"},
      {"621904", "中国工商银行"},
      {"621905", "中国工商银行"},
      {"621906", "中国工商银行"},
      {"621907", "中国工商银行"},
      {"621908", "中国工商银行"},
      {"621909", "中国工商银行"},
      {"621910", "中国工商银行"},
      {"621911", "中国工商银行"},
      {"621912", "中国工商银行"},
      {"621913", "中国工商银行"},
      {"621915", "中国工商银行"},
      {"622002", "中国工商银行"},
      {"621903", "中国工商银行"},
      {"622004", "中国工商银行"},
      {"622005", "中国工商银行"},
      {"622006", "中国工商银行"},
      {"622007", "中国工商银行"},
      {"622008", "中国工商银行"},
      {"622010", "中国工商银行"},
      {"622011", "中国工商银行"},
      {"622012", "中国工商银行"},
      {"621914", "中国工商银行"},
      {"622015", "中国工商银行"},
      {"622016", "中国工商银行"},
      {"622003", "中国工商银行"},
      {"622018", "中国工商银行"},
      {"622019", "中国工商银行"},
      {"622020", "中国工商银行"},
      {"622102", "中国工商银行"},
      {"622103", "中国工商银行"},
      {"620200", "中国工商银行"},
      {"622013", "中国工商银行"},
      {"622111", "中国工商银行"},
      {"622114", "中国工商银行"},
      {"622017", "中国工商银行"},
      {"622110", "中国工商银行"},
      {"622303", "中国工商银行"},
      {"622304", "中国工商银行"},
      {"622305", "中国工商银行"},
      {"622306", "中国工商银行"},
      {"622307", "中国工商银行"},
      {"622308", "中国工商银行"},
      {"622309", "中国工商银行"},
      {"622314", "中国工商银行"},
      {"622315", "中国工商银行"},
      {"622317", "中国工商银行"},
      {"622302", "中国工商银行"},
      {"622402", "中国工商银行"},
      {"622403", "中国工商银行"},
      {"622404", "中国工商银行"},
      {"622313", "中国工商银行"},
      {"622504", "中国工商银行"},
      {"622505", "中国工商银行"},
      {"622509", "中国工商银行"},
      {"622513", "中国工商银行"},
      {"622517", "中国工商银行"},
      {"622502", "中国工商银行"},
      {"622604", "中国工商银行"},
      {"622605", "中国工商银行"},
      {"622606", "中国工商银行"},
      {"622510", "中国工商银行"},
      {"622703", "中国工商银行"},
      {"622715", "中国工商银行"},
      {"622806", "中国工商银行"},
      {"622902", "中国工商银行"},
      {"622903", "中国工商银行"},
      {"622706", "中国工商银行"},
      {"621304", "中国工商银行"},
      {"621402", "中国工商银行"},
      {"623008", "中国工商银行"},
      {"623011", "中国工商银行"},
      {"623012", "中国工商银行"},
      {"622904", "中国工商银行"},
      {"623015", "中国工商银行"},
      {"623100", "中国工商银行"},
      {"623202", "中国工商银行"},
      {"623301", "中国工商银行"},
      {"623400", "中国工商银行"},
      {"623500", "中国工商银行"},
      {"623602", "中国工商银行"},
      {"623803", "中国工商银行"},
      {"623901", "中国工商银行"},
      {"623014", "中国工商银行"},
      {"624100", "中国工商银行"},
      {"624200", "中国工商银行"},
      {"624301", "中国工商银行"},
      {"624402", "中国工商银行"},
      {"623700", "中国工商银行"},
      {"624000", "中国工商银行"},
      {"622104", "中国工商银行"},
      {"622105", "中国工商银行"},
      {"623002", "中国工商银行"},
      {"623006", "中国工商银行"},
      {"623062", "中国工商银行"},
      {"621376", "中国工商银行"},
      {"621423", "中国工商银行"},
      {"621428", "中国工商银行"},
      {"621434", "中国工商银行"},
      {"621761", "中国工商银行"},
      {"621749", "中国工商银行"},
      {"621300", "中国工商银行"},
      {"621378", "中国工商银行"},
      {"621379", "中国工商银行"},
      {"621720", "中国工商银行"},
      {"621762", "中国工商银行"},
      {"621240", "中国工商银行"},
      {"621724", "中国工商银行"},
      {"621371", "中国工商银行"},
      {"621414", "中国工商银行"},
      {"621375", "中国工商银行"},
      {"621734", "中国工商银行"},
      {"621433", "中国工商银行"},
      {"621370", "中国工商银行"},
      {"621733", "中国工商银行"},
      {"621732", "中国工商银行"},
      {"621764", "中国工商银行"},
      {"621372", "中国工商银行"},
      {"621765", "中国工商银行"},
      {"621369", "中国工商银行"},
      {"621750", "中国工商银行"},
      {"621377", "中国工商银行"},
      {"621367", "中国工商银行"},
      {"621374", "中国工商银行"},
      {"621781", "中国工商银行"},
      {"625929", "中国工商银行"},
      {"625927", "中国工商银行"},
      {"625930", "中国工商银行"},
      {"625114", "中国工商银行"},
      {"625021", "中国工商银行"},
      {"625022", "中国工商银行"},
      {"622159", "中国工商银行"},
      {"625932", "中国工商银行"},
      {"625931", "中国工商银行"},
      {"625113", "中国工商银行"},
      {"625914", "中国工商银行"},
      {"625928", "中国工商银行"},
      {"625986", "中国工商银行"},
      {"625925", "中国工商银行"},
      {"625921", "中国工商银行"},
      {"625926", "中国工商银行"},
      {"625917", "中国工商银行"},
      {"622158", "中国工商银行"},
      {"625922", "中国工商银行"},
      {"625933", "中国工商银行"},
      {"625920", "中国工商银行"},
      {"625924", "中国工商银行"},
      {"620054", "中国工商银行"},
      {"620142", "中国工商银行"},
      {"620114", "中国工商银行"},
      {"620146", "中国工商银行"},
      {"620143", "中国工商银行"},
      {"620149", "中国工商银行"},
      {"620187", "中国工商银行"},
      {"620124", "中国工商银行"},
      {"620183", "中国工商银行"},
      {"620094", "中国工商银行"},
      {"620186", "中国工商银行"},
      {"620046", "中国工商银行"},
      {"620148", "中国工商银行"},
      {"620185", "中国工商银行"},
      {"427018", "中国工商银行"},
      {"427020", "中国工商银行"},
      {"427029", "中国工商银行"},
      {"427030", "中国工商银行"},
      {"427039", "中国工商银行"},
      {"438125", "中国工商银行"},
      {"427062", "中国工商银行"},
      {"427064", "中国工商银行"},
      {"622202", "中国工商银行"},
      {"622203", "中国工商银行"},
      {"622926", "中国工商银行"},
      {"622927", "中国工商银行"},
      {"622928", "中国工商银行"},
      {"622210", "中国工商银行"},
      {"622211", "中国工商银行"},
      {"622212", "中国工商银行"},
      {"622213", "中国工商银行"},
      {"622214", "中国工商银行"},
      {"526836", "中国工商银行"},
      {"513685", "中国工商银行"},
      {"543098", "中国工商银行"},
      {"458441", "中国工商银行"},
      {"402791", "中国工商银行"},
      {"427028", "中国工商银行"},
      {"427038", "中国工商银行"},
      {"548259", "中国工商银行"},
      {"622200", "中国工商银行"},
      {"621722", "中国工商银行"},
      {"621723", "中国工商银行"},
      {"621721", "中国工商银行"},
      {"356879", "中国工商银行"},
      {"356880", "中国工商银行"},
      {"356881", "中国工商银行"},
      {"356882", "中国工商银行"},
      {"62451804", "中国工商银行"},
      {"62451810", "中国工商银行"},
      {"62451811", "中国工商银行"},
      {"62458071", "中国工商银行"},
      {"451804", "中国工商银行"},
      {"451810", "中国工商银行"},
      {"451811", "中国工商银行"},
      {"458071", "中国工商银行"},
      {"622231", "中国工商银行"},
      {"622232", "中国工商银行"},
      {"622233", "中国工商银行"},
      {"622234", "中国工商银行"},
      {"622929", "中国工商银行"},
      {"622930", "中国工商银行"},
      {"622931", "中国工商银行"},
      {"528856", "中国工商银行"},
      {"524374", "中国工商银行"},
      {"550213", "中国工商银行"},
      {"622237", "中国工商银行"},
      {"622239", "中国工商银行"},
      {"622238", "中国工商银行"},
      {"622223", "中国工商银行"},
      {"622229", "中国工商银行"},
      {"622224", "中国工商银行"},
      {"489734", "中国工商银行"},
      {"489735", "中国工商银行"},
      {"489736", "中国工商银行"},
      {"530970", "中国工商银行"},
      {"530990", "中国工商银行"},
      {"558360", "中国工商银行"},
      {"370249", "中国工商银行"},
      {"370246", "中国工商银行"},
      {"370248", "中国工商银行"},
      {"625330", "中国工商银行"},
      {"625331", "中国工商银行"},
      {"625332", "中国工商银行"},
      {"621558", "中国工商银行"},
      {"621559", "中国工商银行"},
      {"625916", "中国工商银行"},
      {"625915", "中国工商银行"},
      {"427010", "中国工商银行"},
      {"427019", "中国工商银行"},
      {"374738", "中国工商银行"},
      {"374739", "中国工商银行"},
      {"544210", "中国工商银行"},
      {"548943", "中国工商银行"},
      {"621225", "中国工商银行"},
      {"621226", "中国工商银行"},
      {"6245806", "中国工商银行"},
      {"6253098", "中国工商银行"},
      {"45806", "中国工商银行"},
      {"53098", "中国工商银行"},
      {"625939", "中国工商银行"},
      {"625987", "中国工商银行"},
      {"622944", "中国工商银行"},
      {"622949", "中国工商银行"},
      {"625900", "中国工商银行"},
      {"622889", "中国工商银行"},
      {"625019", "中国工商银行"},
      {"628286", "中国工商银行"},
      {"622235", "中国工商银行"},
      {"622230", "中国工商银行"},
      {"622245", "中国工商银行"},
      {"622240", "中国工商银行"},
      {"621730", "中国工商银行"},
      {"621288", "中国工商银行"},
      {"621731", "中国工商银行"},
      {"620030", "中国工商银行"},
      {"621763", "中国工商银行"},
      {"625934", "中国工商银行"},
      {"524091", "中国工商银行"},
      {"525498", "中国工商银行"},
      {"622236", "中国工商银行"},
      {"625018", "中国工商银行"},
      {"622208", "中国工商银行"},
      {"621227", "中国工商银行"},
      {"438126", "中国工商银行"},
      {"622206", "中国工商银行"},
      {"9558", "中国工商银行"},
      {"900010", "中国工商银行"},
      {"900000", "中国工商银行"},
      {"524047", "中国工商银行"},
      {"510529", "中国工商银行"},
      {"370267", "中国工商银行"},
      {"370247", "中国工商银行"},
      {"625017", "中国工商银行"},
      {"621670", "中国工商银行"},
      {"621618", "中国工商银行"},
      {"620058", "中国工商银行"},
      {"622171", "中国工商银行"},
      {"620516", "中国工商银行"},
      {"620086", "中国工商银行"},
      {"628288", "中国工商银行"},
      {"621281", "中国工商银行"},
      {"622246", "中国工商银行"},
      {"622215", "中国工商银行"},
      {"622225", "中国工商银行"},
      {"622220", "中国工商银行"},
      {"621294", "中国银行"},
      {"621293", "中国银行"},
      {"621342", "中国银行"},
      {"621343", "中国银行"},
      {"621394", "中国银行"},
      {"621364", "中国银行"},
      {"621648", "中国银行"},
      {"621248", "中国银行"},
      {"621249", "中国银行"},
      {"621638", "中国银行"},
      {"621334", "中国银行"},
      {"621395", "中国银行"},
      {"524865", "中国银行"},
      {"525745", "中国银行"},
      {"525746", "中国银行"},
      {"524864", "中国银行"},
      {"622759", "中国银行"},
      {"622761", "中国银行"},
      {"622762", "中国银行"},
      {"622763", "中国银行"},
      {"622752", "中国银行"},
      {"622753", "中国银行"},
      {"622755", "中国银行"},
      {"622757", "中国银行"},
      {"622758", "中国银行"},
      {"622756", "中国银行"},
      {"622754", "中国银行"},
      {"512315", "中国银行"},
      {"512316", "中国银行"},
      {"512411", "中国银行"},
      {"512412", "中国银行"},
      {"514957", "中国银行"},
      {"558868", "中国银行"},
      {"514958", "中国银行"},
      {"518378", "中国银行"},
      {"518379", "中国银行"},
      {"518474", "中国银行"},
      {"518475", "中国银行"},
      {"518476", "中国银行"},
      {"547766", "中国银行"},
      {"620026", "中国银行"},
      {"620025", "中国银行"},
      {"620531", "中国银行"},
      {"620019", "中国银行"},
      {"620035", "中国银行"},
      {"409672", "中国银行"},
      {"552742", "中国银行"},
      {"553131", "中国银行"},
      {"622771", "中国银行"},
      {"622770", "中国银行"},
      {"622772", "中国银行"},
      {"409668", "中国银行"},
      {"409669", "中国银行"},
      {"409667", "中国银行"},
      {"625141", "中国银行"},
      {"625143", "中国银行"},
      {"620211", "中国银行"},
      {"620210", "中国银行"},
      {"621661", "中国银行"},
      {"409666", "中国银行"},
      {"621787", "中国银行"},
      {"621785", "中国银行"},
      {"622751", "中国银行"},
      {"622750", "中国银行"},
      {"621756", "中国银行"},
      {"621757", "中国银行"},
      {"621330", "中国银行"},
      {"621331", "中国银行"},
      {"621758", "中国银行"},
      {"621759", "中国银行"},
      {"621332", "中国银行"},
      {"621333", "中国银行"},
      {"621663", "中国银行"},
      {"409665", "中国银行"},
      {"456351", "中国银行"},
      {"601382", "中国银行"},
      {"620202", "中国银行"},
      {"620203", "中国银行"},
      {"625908", "中国银行"},
      {"625907", "中国银行"},
      {"625909", "中国银行"},
      {"625906", "中国银行"},
      {"625910", "中国银行"},
      {"625905", "中国银行"},
      {"625040", "中国银行"},
      {"625042", "中国银行"},
      {"622789", "中国银行"},
      {"622788", "中国银行"},
      {"622480", "中国银行"},
      {"621212", "中国银行"},
      {"620514", "中国银行"},
      {"409670", "中国银行"},
      {"621662", "中国银行"},
      {"621297", "中国银行"},
      {"621741", "中国银行"},
      {"623040", "中国银行"},
      {"621788", "中国银行"},
      {"621786", "中国银行"},
      {"621790", "中国银行"},
      {"621789", "中国银行"},
      {"621215", "中国银行"},
      {"621725", "中国银行"},
      {"621666", "中国银行"},
      {"621668", "中国银行"},
      {"621667", "中国银行"},
      {"621669", "中国银行"},
      {"621660", "中国银行"},
      {"621672", "中国银行"},
      {"622346", "中国银行"},
      {"625055", "中国银行"},
      {"558869", "中国银行"},
      {"621231", "中国银行"},
      {"621620", "中国银行"},
      {"622347", "中国银行"},
      {"622479", "中国银行"},
      {"621256", "中国银行"},
      {"622274", "中国银行"},
      {"628388", "中国银行"},
      {"620061", "中国银行"},
      {"622760", "中国银行"},
      {"621041", "中国银行"},
      {"622273", "中国银行"},
      {"377677", "中国银行"},
      {"621665", "中国银行"},
      {"622765", "中国银行"},
      {"622764", "中国银行"},
      {"621568", "中国银行"},
      {"621569", "中国银行"},
      {"620513", "中国银行"},
      {"620040", "中国银行"},
      {"625140", "中国银行"},
      {"628313", "中国银行"},
      {"628312", "中国银行"},
      {"623208", "中国银行"},
      {"356833", "中国银行"},
      {"356835", "中国银行"},
      {"438088", "中国银行"},
      {"622348", "中国银行"},
      {"625333", "中国银行"},
      {"518377", "中国银行"},
      {"409671", "中国银行"},
      {"625145", "中国银行"},
      {"621283", "中国银行"},
      {"5453242", "中国建设银行"},
      {"5491031", "中国建设银行"},
      {"553242", "中国建设银行"},
      {"5544033", "中国建设银行"},
      {"524094", "中国建设银行"},
      {"526410", "中国建设银行"},
      {"53243", "中国建设银行"},
      {"436738", "中国建设银行"},
      {"558895", "中国建设银行"},
      {"552801", "中国建设银行"},
      {"622675", "中国建设银行"},
      {"622676", "中国建设银行"},
      {"622677", "中国建设银行"},
      {"621082", "中国建设银行"},
      {"621083", "中国建设银行"},
      {"434062", "中国建设银行"},
      {"552245", "中国建设银行"},
      {"621466", "中国建设银行"},
      {"622966", "中国建设银行"},
      {"621499", "中国建设银行"},
      {"622988", "中国建设银行"},
      {"628316", "中国建设银行"},
      {"628317", "中国建设银行"},
      {"625363", "中国建设银行"},
      {"625362", "中国建设银行"},
      {"53242", "中国建设银行"},
      {"489592", "中国建设银行"},
      {"491031", "中国建设银行"},
      {"453242", "中国建设银行"},
      {"621488", "中国建设银行"},
      {"621598", "中国建设银行"},
      {"621487", "中国建设银行"},
      {"621081", "中国建设银行"},
      {"589970", "中国建设银行"},
      {"621084", "中国建设银行"},
      {"434061", "中国建设银行"},
      {"421349", "中国建设银行"},
      {"625966", "中国建设银行"},
      {"625965", "中国建设银行"},
      {"625964", "中国建设银行"},
      {"356899", "中国建设银行"},
      {"356896", "中国建设银行"},
      {"356895", "中国建设银行"},
      {"436742", "中国建设银行"},
      {"622700", "中国建设银行"},
      {"436718", "中国建设银行"},
      {"531693", "中国建设银行"},
      {"532458", "中国建设银行"},
      {"436748", "中国建设银行"},
      {"532450", "中国建设银行"},
      {"436745", "中国建设银行"},
      {"436728", "中国建设银行"},
      {"622708", "中国建设银行"},
      {"622168", "中国建设银行"},
      {"622166", "中国建设银行"},
      {"621700", "中国建设银行"},
      {"557080", "中国建设银行"},
      {"544887", "中国建设银行"},
      {"559051", "中国建设银行"},
      {"628366", "中国建设银行"},
      {"628266", "中国建设银行"},
      {"622725", "中国建设银行"},
      {"622728", "中国建设银行"},
      {"622382", "中国建设银行"},
      {"622381", "中国建设银行"},
      {"621467", "中国建设银行"},
      {"621621", "中国建设银行"},
      {"620060", "中国建设银行"},
      {"622280", "中国建设银行"},
      {"621080", "中国建设银行"},
      {"620107", "中国建设银行"},
      {"621284", "中国建设银行"},
      {"544033", "中国建设银行"},
      {"622707", "中国建设银行"},
      {"625956", "中国建设银行"},
      {"625955", "中国建设银行"},
      {"84301", "浦发银行"},
      {"84336", "浦发银行"},
      {"622500", "浦发银行"},
      {"84373", "浦发银行"},
      {"84385", "浦发银行"},
      {"84390", "浦发银行"},
      {"87000", "浦发银行"},
      {"87010", "浦发银行"},
      {"87030", "浦发银行"},
      {"87040", "浦发银行"},
      {"84380", "浦发银行"},
      {"984301", "浦发银行"},
      {"984303", "浦发银行"},
      {"84361", "浦发银行"},
      {"87050", "浦发银行"},
      {"84342", "浦发银行"},
      {"625957", "浦发银行"},
      {"625958", "浦发银行"},
      {"625970", "浦发银行"},
      {"622522", "浦发银行"},
      {"622523", "浦发银行"},
      {"622521", "浦发银行"},
      {"622516", "浦发银行"},
      {"622518", "浦发银行"},
      {"622176", "浦发银行"},
      {"622177", "浦发银行"},
      {"498451", "浦发银行"},
      {"620530", "浦发银行"},
      {"622519", "浦发银行"},
      {"621791", "浦发银行"},
      {"622517", "浦发银行"},
      {"622520", "浦发银行"},
      {"628222", "浦发银行"},
      {"628221", "浦发银行"},
      {"356852", "浦发银行"},
      {"356851", "浦发银行"},
      {"356850", "浦发银行"},
      {"622276", "浦发银行"},
      {"377187", "浦发银行"},
      {"515672", "浦发银行"},
      {"517650", "浦发银行"},
      {"525998", "浦发银行"},
      {"456418", "浦发银行"},
      {"622228", "浦发银行"},
      {"622277", "浦发银行"},
      {"621795", "浦发银行"},
      {"621793", "浦发银行"},
      {"621352", "浦发银行"},
      {"621792", "浦发银行"},
      {"621390", "浦发银行"},
      {"621796", "浦发银行"},
      {"404739", "浦发银行"},
      {"404738", "浦发银行"},
      {"621351", "浦发银行"},
      {"625971", "浦发银行"},
      {"625993", "浦发银行"},
      {"356885", "招商银行"},
      {"356886", "招商银行"},
      {"356887", "招商银行"},
      {"356888", "招商银行"},
      {"356890", "招商银行"},
      {"439188", "招商银行"},
      {"479228", "招商银行"},
      {"479229", "招商银行"},
      {"356889", "招商银行"},
      {"552534", "招商银行"},
      {"552587", "招商银行"},
      {"622575", "招商银行"},
      {"622576", "招商银行"},
      {"622577", "招商银行"},
      {"622578", "招商银行"},
      {"622579", "招商银行"},
      {"622581", "招商银行"},
      {"622582", "招商银行"},
      {"545620", "招商银行"},
      {"545621", "招商银行"},
      {"545947", "招商银行"},
      {"545948", "招商银行"},
      {"545619", "招商银行"},
      {"545623", "招商银行"},
      {"410062", "招商银行"},
      {"468203", "招商银行"},
      {"512425", "招商银行"},
      {"524011", "招商银行"},
      {"622580", "招商银行"},
      {"622588", "招商银行"},
      {"95555", "招商银行"},
      {"439225", "招商银行"},
      {"439226", "招商银行"},
      {"625802", "招商银行"},
      {"625803", "招商银行"},
      {"690755", "招商银行"},
      {"690755", "招商银行"},
      {"518718", "招商银行"},
      {"518710", "招商银行"},
      {"439227", "招商银行"},
      {"620520", "招商银行"},
      {"622598", "招商银行"},
      {"622609", "招商银行"},
      {"621286", "招商银行"},
      {"402658", "招商银行"},
      {"370286", "招商银行"},
      {"370285", "招商银行"},
      {"370289", "招商银行"},
      {"370287", "招商银行"},
      {"521302", "招商银行"},
      {"621299", "招商银行"},
      {"621485", "招商银行"},
      {"621483", "招商银行"},
      {"628262", "招商银行"},
      {"628362", "招商银行"},
      {"621486", "招商银行"},
      {"518212", "中信银行"},
      {"556617", "中信银行"},
      {"558916", "中信银行"},
      {"433666", "中信银行"},
      {"520108", "中信银行"},
      {"403393", "中信银行"},
      {"514906", "中信银行"},
      {"433669", "中信银行"},
      {"433668", "中信银行"},
      {"433667", "中信银行"},
      {"404157", "中信银行"},
      {"404174", "中信银行"},
      {"404173", "中信银行"},
      {"404172", "中信银行"},
      {"400360", "中信银行"},
      {"403391", "中信银行"},
      {"403392", "中信银行"},
      {"404158", "中信银行"},
      {"404159", "中信银行"},
      {"404171", "中信银行"},
      {"621773", "中信银行"},
      {"621767", "中信银行"},
      {"621768", "中信银行"},
      {"621770", "中信银行"},
      {"621772", "中信银行"},
      {"622689", "中信银行"},
      {"622688", "中信银行"},
      {"622680", "中信银行"},
      {"622679", "中信银行"},
      {"622678", "中信银行"},
      {"356392", "中信银行"},
      {"356391", "中信银行"},
      {"356390", "中信银行"},
      {"376968", "中信银行"},
      {"376969", "中信银行"},
      {"376966", "中信银行"},
      {"433670", "中信银行"},
      {"433671", "中信银行"},
      {"433680", "中信银行"},
      {"968807", "中信银行"},
      {"968808", "中信银行"},
      {"968809", "中信银行"},
      {"628208", "中信银行"},
      {"628209", "中信银行"},
      {"628206", "中信银行"},
      {"442729", "中信银行"},
      {"442730", "中信银行"},
      {"622690", "中信银行"},
      {"622691", "中信银行"},
      {"622998", "中信银行"},
      {"622999", "中信银行"},
      {"621771", "中信银行"},
      {"620082", "中信银行"},
      {"622692", "中信银行"},
      {"622698", "中信银行"},
      {"622696", "中信银行"},
      {"622663", "光大银行"},
      {"622664", "光大银行"},
      {"622665", "光大银行"},
      {"622666", "光大银行"},
      {"622667", "光大银行"},
      {"622669", "光大银行"},
      {"622670", "光大银行"},
      {"622671", "光大银行"},
      {"622672", "光大银行"},
      {"622668", "光大银行"},
      {"622661", "光大银行"},
      {"622674", "光大银行"},
      {"90030", "光大银行"},
      {"622673", "光大银行"},
      {"622660", "光大银行"},
      {"622662", "光大银行"},
      {"356837", "光大银行"},
      {"356838", "光大银行"},
      {"356839", "光大银行"},
      {"356840", "光大银行"},
      {"486497", "光大银行"},
      {"425862", "光大银行"},
      {"625978", "光大银行"},
      {"625980", "光大银行"},
      {"625979", "光大银行"},
      {"625981", "光大银行"},
      {"625977", "光大银行"},
      {"625976", "光大银行"},
      {"625975", "光大银行"},
      {"628201", "光大银行"},
      {"628202", "光大银行"},
      {"406254", "光大银行"},
      {"406252", "光大银行"},
      {"622655", "光大银行"},
      {"620518", "光大银行"},
      {"621489", "光大银行"},
      {"621492", "光大银行"},
      {"303", "光大银行"},
      {"481699", "光大银行"},
      {"524090", "光大银行"},
      {"543159", "光大银行"},
      {"620085", "光大银行"},
      {"622161", "光大银行"},
      {"622570", "光大银行"},
      {"622650", "光大银行"},
      {"622658", "光大银行"},
      {"622685", "光大银行"},
      {"622659", "光大银行"},
      {"622687", "光大银行"},
      {"622657", "光大银行"},
      {"407405", "民生银行"},
      {"421869", "民生银行"},
      {"421870", "民生银行"},
      {"421871", "民生银行"},
      {"512466", "民生银行"},
      {"528948", "民生银行"},
      {"552288", "民生银行"},
      {"517636", "民生银行"},
      {"556610", "民生银行"},
      {"545392", "民生银行"},
      {"545393", "民生银行"},
      {"545431", "民生银行"},
      {"545447", "民生银行"},
      {"622617", "民生银行"},
      {"622622", "民生银行"},
      {"622615", "民生银行"},
      {"622619", "民生银行"},
      {"472067", "民生银行"},
      {"421393", "民生银行"},
      {"472068", "民生银行"},
      {"622600", "民生银行"},
      {"622601", "民生银行"},
      {"628258", "民生银行"},
      {"464580", "民生银行"},
      {"356858", "民生银行"},
      {"356857", "民生银行"},
      {"356856", "民生银行"},
      {"356859", "民生银行"},
      {"464581", "民生银行"},
      {"427571", "民生银行"},
      {"427570", "民生银行"},
      {"421865", "民生银行"},
      {"415599", "民生银行"},
      {"553161", "民生银行"},
      {"545217", "民生银行"},
      {"523952", "民生银行"},
      {"622602", "民生银行"},
      {"622621", "民生银行"},
      {"622616", "民生银行"},
      {"622603", "民生银行"},
      {"377155", "民生银行"},
      {"377153", "民生银行"},
      {"377152", "民生银行"},
      {"377158", "民生银行"},
      {"622620", "民生银行"},
      {"622623", "民生银行"},
      {"625913", "民生银行"},
      {"625912", "民生银行"},
      {"625911", "民生银行"},
      {"622618", "民生银行"},
      {"428911", "广东发展银行"},
      {"436768", "广东发展银行"},
      {"436769", "广东发展银行"},
      {"436770", "广东发展银行"},
      {"491032", "广东发展银行"},
      {"491033", "广东发展银行"},
      {"491034", "广东发展银行"},
      {"491035", "广东发展银行"},
      {"491036", "广东发展银行"},
      {"491037", "广东发展银行"},
      {"491038", "广东发展银行"},
      {"436771", "广东发展银行"},
      {"518364", "广东发展银行"},
      {"541709", "广东发展银行"},
      {"541710", "广东发展银行"},
      {"548844", "广东发展银行"},
      {"493427", "广东发展银行"},
      {"520152", "广东发展银行"},
      {"520382", "广东发展银行"},
      {"552794", "广东发展银行"},
      {"528931", "广东发展银行"},
      {"685800", "广东发展银行"},
      {"558894", "广东发展银行"},
      {"406365", "广东发展银行"},
      {"487013", "广东发展银行"},
      {"406366", "广东发展银行"},
      {"6858000", "广东发展银行"},
      {"6858001", "广东发展银行"},
      {"6858009", "广东发展银行"},
      {"625072", "广东发展银行"},
      {"625809", "广东发展银行"},
      {"625071", "广东发展银行"},
      {"625808", "广东发展银行"},
      {"9111", "广东发展银行"},
      {"622568", "广东发展银行"},
      {"622558", "广东发展银行"},
      {"622555", "广东发展银行"},
      {"622556", "广东发展银行"},
      {"622557", "广东发展银行"},
      {"622559", "广东发展银行"},
      {"622560", "广东发展银行"},
      {"621462", "广东发展银行"},
      {"625810", "广东发展银行"},
      {"628260", "广东发展银行"},
      {"628259", "广东发展银行"},
      {"625807", "广东发展银行"},
      {"625806", "广东发展银行"},
      {"625805", "广东发展银行"},
      {"621222", "华夏银行"},
      {"623022", "华夏银行"},
      {"622638", "华夏银行"},
      {"625969", "华夏银行"},
      {"625968", "华夏银行"},
      {"625967", "华夏银行"},
      {"622630", "华夏银行"},
      {"999999", "华夏银行"},
      {"539867", "华夏银行"},
      {"539868", "华夏银行"},
      {"528709", "华夏银行"},
      {"528708", "华夏银行"},
      {"628318", "华夏银行"},
      {"623021", "华夏银行"},
      {"622632", "华夏银行"},
      {"623020", "华夏银行"},
      {"523959", "华夏银行"},
      {"622633", "华夏银行"},
      {"622637", "华夏银行"},
      {"622636", "华夏银行"},
      {"622631", "华夏银行"},
      {"623023", "华夏银行"},
      {"62215049", "邮储银行"},
      {"62215050", "邮储银行"},
      {"62215051", "邮储银行"},
      {"62218850", "邮储银行"},
      {"62218851", "邮储银行"},
      {"62218849", "邮储银行"},
      {"622150", "邮储银行"},
      {"622151", "邮储银行"},
      {"622188", "邮储银行"},
      {"622199", "邮储银行"},
      {"621096", "邮储银行"},
      {"621098", "邮储银行"},
      {"622810", "邮储银行"},
      {"622811", "邮储银行"},
      {"621797", "邮储银行"},
      {"621799", "邮储银行"},
      {"621798", "邮储银行"},
      {"620529", "邮储银行"},
      {"621599", "邮储银行"},
      {"955100", "邮储银行"},
      {"621095", "邮储银行"},
      {"622181", "邮储银行"},
      {"621674", "邮储银行"},
      {"625919", "邮储银行"},
      {"621622", "邮储银行"},
      {"622812", "邮储银行"},
      {"628310", "邮储银行"},
      {"620062", "邮储银行"},
      {"623219", "邮储银行"},
      {"623218", "邮储银行"},
      {"621285", "邮储银行"},
      {"95595", "中国农业银行"},
      {"95596", "中国农业银行"},
      {"95597", "中国农业银行"},
      {"95598", "中国农业银行"},
      {"95599", "中国农业银行"},
      {"622840", "中国农业银行"},
      {"622844", "中国农业银行"},
      {"622847", "中国农业银行"},
      {"622848", "中国农业银行"},
      {"622845", "中国农业银行"},
      {"622826", "中国农业银行"},
      {"622827", "中国农业银行"},
      {"622841", "中国农业银行"},
      {"103", "中国农业银行"},
      {"622824", "中国农业银行"},
      {"622825", "中国农业银行"},
      {"622823", "中国农业银行"},
      {"622846", "中国农业银行"},
      {"622843", "中国农业银行"},
      {"622849", "中国农业银行"},
      {"622821", "中国农业银行"},
      {"622822", "中国农业银行"},
      {"621671", "中国农业银行"},
      {"620501", "中国农业银行"},
      {"622828", "中国农业银行"},
      {"621619", "中国农业银行"},
      {"620059", "中国农业银行"},
      {"623018", "中国农业银行"},
      {"623206", "中国农业银行"},
      {"621282", "中国农业银行"},
      {"621336", "中国农业银行"},
      {"549633", "兴业银行"},
      {"552398", "兴业银行"},
      {"548738", "兴业银行"},
      {"625087", "兴业银行"},
      {"625086", "兴业银行"},
      {"625085", "兴业银行"},
      {"625082", "兴业银行"},
      {"625083", "兴业银行"},
      {"625084", "兴业银行"},
      {"451290", "兴业银行"},
      {"451289", "兴业银行"},
      {"524070", "兴业银行"},
      {"523036", "兴业银行"},
      {"622902", "兴业银行"},
      {"622901", "兴业银行"},
      {"461982", "兴业银行"},
      {"486494", "兴业银行"},
      {"486493", "兴业银行"},
      {"486861", "兴业银行"},
      {"528057", "兴业银行"},
      {"527414", "兴业银行"},
      {"90592", "兴业银行"},
      {"622909", "兴业银行"},
      {"966666", "兴业银行"},
      {"625962", "兴业银行"},
      {"625961", "兴业银行"},
      {"625960", "兴业银行"},
      {"625963", "兴业银行"},
      {"438589", "兴业银行"},
      {"438588", "兴业银行"},
      {"622908", "兴业银行"},
      {"622922", "兴业银行"},
      {"628212", "兴业银行"},
      {"526855", "平安银行"},
      {"622156", "平安银行"},
      {"622155", "平安银行"},
      {"528020", "平安银行"},
      {"622157", "平安银行"},
      {"531659", "平安银行"},
      {"412962", "平安银行"},
      {"412963", "平安银行"},
      {"998802", "平安银行"},
      {"998801", "平安银行"},
      {"415753", "平安银行"},
      {"415752", "平安银行"},
      {"622989", "平安银行"},
      {"622986", "平安银行"},
      {"435745", "平安银行"},
      {"435744", "平安银行"},
      {"356869", "平安银行"},
      {"356868", "平安银行"},
      {"483536", "平安银行"},
      {"998800", "平安银行"},
      {"622538", "平安银行"},
      {"620010", "平安银行"},
      {"622536", "平安银行"},
      {"622539", "平安银行"},
      {"622535", "平安银行"},
      {"622983", "平安银行"},
      {"623058", "平安银行"},
      {"625361", "平安银行"},
      {"625360", "平安银行"},
      {"622525", "平安银行"},
      {"622526", "平安银行"},
      {"622298", "平安银行"},
      {"602907", "平安银行"},
      {"621626", "平安银行"},
      {"627068", "平安银行"},
      {"627067", "平安银行"},
      {"627066", "平安银行"},
      {"627069", "平安银行"},
      {"458123", "中国交通银行"},
      {"458124", "中国交通银行"},
      {"520169", "中国交通银行"},
      {"552853", "中国交通银行"},
      {"521899", "中国交通银行"},
      {"955593", "中国交通银行"},
      {"955592", "中国交通银行"},
      {"955591", "中国交通银行"},
      {"955590", "中国交通银行"},
      {"622258", "中国交通银行"},
      {"622259", "中国交通银行"},
      {"622261", "中国交通银行"},
      {"622260", "中国交通银行"},
      {"622250", "中国交通银行"},
      {"622251", "中国交通银行"},
      {"622253", "中国交通银行"},
      {"622252", "中国交通银行"},
      {"6653783", "中国交通银行"},
      {"49104", "中国交通银行"},
      {"53783", "中国交通银行"},
      {"6649104", "中国交通银行"},
      {"622254", "中国交通银行"},
      {"622255", "中国交通银行"},
      {"622256", "中国交通银行"},
      {"622257", "中国交通银行"},
      {"625029", "中国交通银行"},
      {"625028", "中国交通银行"},
      {"434910", "中国交通银行"},
      {"522964", "中国交通银行"},
      {"601428", "中国交通银行"},
      {"66601428", "中国交通银行"},
      {"622656", "中国交通银行"},
      {"620013", "中国交通银行"},
      {"620021", "中国交通银行"},
      {"621069", "中国交通银行"},
      {"628218", "中国交通银行"},
      {"628216", "中国交通银行"},
      {"622262", "中国交通银行"},
      {"620521", "中国交通银行"},
      {"621436", "中国交通银行"},
      {"622284", "中国交通银行"},
      {"66405512", "中国交通银行"},
      {"405512", "中国交通银行"},
      {"621335", "中国交通银行"},
      {"621002", "中国交通银行"}
  };
  
  
  // 查找方法
  public static String search(String des) {
    if (des == null) {
      return null;
    }
    int low = 0;
    int high = initArr.length;
    while (low < high) {
      
      if (des.startsWith(initArr[low][0])) {
        return initArr[low][1];
      }
      low++;
    }
    return null;
  }
  
  public static void main(String[] args) {
    
    long start = System.currentTimeMillis();
    String card = "6217996900005600003";
    String bin = card.substring(0, 6);
    System.out.println("bin:" + bin);
    System.out.println(search(bin));
    System.out.println(System.currentTimeMillis() - start);
    
  }
}
