package com.haoxuer;

import com.haoxuer.eventdispatcher.Event;
import com.haoxuer.eventdispatcher.EventDispatcher;
import com.haoxuer.eventdispatcher.EventListener;
import com.haoxuer.eventdispatcher.impl.EventDispatcherImpl;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
        EventDispatcher dispatcher=new EventDispatcherImpl();
        dispatcher.addEventListener(new EventListener() {
            @Override
            public void onEvent(Event event) {
                System.out.println(event.getType());
            }

            @Override
            public boolean isFailOnException() {
                return false;
            }
        });
        dispatcher.dispatchEvent(new ArticleAddEvent());

    }
}
