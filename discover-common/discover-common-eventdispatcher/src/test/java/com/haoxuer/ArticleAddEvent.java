package com.haoxuer;

import com.haoxuer.eventdispatcher.Event;

public class ArticleAddEvent implements Event {
    @Override
    public String getType() {
        return "articleadd";
    }
}
