package com.haoxuer.discover.oauth.impl;

import com.google.gson.Gson;
import com.haoxuer.discover.oauth.api.OauthHandler;
import com.haoxuer.discover.oauth.domain.GitHubUser;
import com.haoxuer.discover.oauth.domain.OauthResponse;
import com.haoxuer.discover.oauth.domain.TokenResponse;
import jodd.http.HttpRequest;


public class GitHubHander implements OauthHandler {
  @Override
  public void setKey(String key) {

  }

  @Override
  public void setSecret(String secret) {

  }

  @Override
  public OauthResponse login(String accessToken, String openid) {
    GitHubUser result = null;

    Gson gson = new Gson();

    HttpRequest request = HttpRequest.get("https://api.github.com/user");

    request.query("access_token", accessToken);
    request.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
    String infojson;
    try {
      infojson = request.send().body();
      result = gson.fromJson(infojson, GitHubUser.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  @Override
  public TokenResponse getToken(String code) {
    return null;
  }
}
