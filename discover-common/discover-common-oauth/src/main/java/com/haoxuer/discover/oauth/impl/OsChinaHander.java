package com.haoxuer.discover.oauth.impl;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.haoxuer.discover.oauth.api.OauthHandler;
import com.haoxuer.discover.oauth.domain.OauthResponse;
import com.haoxuer.discover.oauth.domain.OsChinaUser;
import com.haoxuer.discover.oauth.domain.TokenResponse;
import jodd.http.HttpRequest;

public class OsChinaHander implements OauthHandler {
  private String client_id;
  
  private String client_secret;
  private String redirect_uri;
  
  public String getRedirect_uri() {
    return redirect_uri;
  }
  
  public void setRedirect_uri(String redirect_uri) {
    this.redirect_uri = redirect_uri;
  }
  
  @Override
  public void setKey(String key) {
    this.client_id = key;
  }
  
  @Override
  public void setSecret(String secret) {
    this.client_secret = secret;
  }
  
  @Override
  public OauthResponse login(String accessToken, String openid) {
    OsChinaUser result = null;
    HttpRequest request = HttpRequest.get("http://www.oschina.net/action/openapi/user");

    request.query("access_token", accessToken);
    request.query("dataType", "json");
    request.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
    String infojson = null;
    try {
      infojson = request.send().body();
      Gson gson = new Gson();
      result = gson.fromJson(infojson, OsChinaUser.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }
  
  @Override
  public TokenResponse getToken(String code) {
    TokenResponse response = new TokenResponse();

    HttpRequest request = HttpRequest.get("http://www.oschina.net/action/openapi/token");
    //con.method(Connection.Method.POST);
    request.query("client_id", client_id);
    request.query("client_secret", client_secret);
    request.query("grant_type", "authorization_code");
    request.query("redirect_uri", redirect_uri);
    request.query("code", code);
    request.query("dataType", "json");
    request.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
    try {
      String body = request.send().body();
      JsonParser parser = new JsonParser();
      JsonElement element = parser.parse(body);
      response.setAccessToken(ElementUtils.getString(element, "access_token"));
      response.setRefreshToken(ElementUtils.getString(element, "refresh_token"));
      response.setTokenType(ElementUtils.getString(element, "token_type"));
      response.setExpiresIn(ElementUtils.getInt(element, "expires_in"));
      response.setOpenId(ElementUtils.getString(element, "uid"));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return response;
  }
}
