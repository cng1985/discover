package com.haoxuer.discover.oauth.impl;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.haoxuer.discover.oauth.api.OauthHandler;
import com.haoxuer.discover.oauth.domain.OauthResponse;
import com.haoxuer.discover.oauth.domain.TokenResponse;
import com.haoxuer.discover.oauth.domain.WeiboUser;
import jodd.http.HttpRequest;

public class WeiBoHander implements OauthHandler {
  @Override
  public void setKey(String key) {

  }

  @Override
  public void setSecret(String secret) {

  }

  @Override
  public OauthResponse login(String accessToken, String openid) {
    OauthResponse result = null;

    try {
      HttpRequest request = HttpRequest.get("https://api.weibo.com/2/account/get_uid.json");
      request.query("access_token", accessToken);
      request.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
      String backjson = null;
      backjson = request.send().body();
      JsonParser parser = new JsonParser();
      JsonElement e = parser.parse(backjson);
      String uid = e.getAsJsonObject().get("uid").getAsString();

      HttpRequest inforequest = HttpRequest.get("https://api.weibo.com/2/users/show.json");
      inforequest.query("access_token", accessToken);
      inforequest.query("uid", uid);
      inforequest.header("User-Agent",
          "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
      String uifos = inforequest.send().body();
      Gson gson = new Gson();
      result = gson.fromJson(uifos, WeiboUser.class);
    } catch (Exception e) {
      e.printStackTrace();
    }


    return result;
  }

  @Override
  public TokenResponse getToken(String code) {
    return null;
  }
}
