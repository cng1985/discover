package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.response.UserAccessLogResponse;
import com.haoxuer.discover.user.data.entity.UserAccessLog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class UserAccessLogResponseConvert implements Conver<UserAccessLogResponse, UserAccessLog> {
    @Override
    public UserAccessLogResponse conver(UserAccessLog source) {
        UserAccessLogResponse result = new UserAccessLogResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getCreator()!=null){
            result.setCreatorName(source.getCreator().getName());
         }


        return result;
    }
}
