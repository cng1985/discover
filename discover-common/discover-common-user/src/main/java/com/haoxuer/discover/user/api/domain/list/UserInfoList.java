package com.haoxuer.discover.user.api.domain.list;


import com.haoxuer.discover.user.api.domain.simple.UserInfoSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年04月28日11:36:16.
*/

@Data
public class UserInfoList  extends ResponseList<UserInfoSimple> {

}