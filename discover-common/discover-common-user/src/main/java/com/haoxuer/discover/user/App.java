package com.haoxuer.discover.user;

import com.haoxuer.discover.user.data.entity.*;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernateSimple.TemplateHibernateSimpleDir;
import com.nbsaas.codemake.templates.elementui.ElementUIDir;

import java.io.File;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    CodeMake make = getCodeMake();

    make.makes(UserAccessLog.class);
    System.out.println("ok");
  }

  private static CodeMake getCodeMake() {
    CodeMake make = new CodeMake(ElementUIDir.class, TemplateHibernateSimpleDir.class);
    File view = new File("E:\\workspace\\iowl\\iowl_web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
    make.setView(view);


    make.setDao(true);
    make.setService(false);
    make.setView(false);
    make.setAction(true);
    make.setRest(true);
    make.setApi(true);
    make.put("restDomain",true);
    return make;
  }
}
