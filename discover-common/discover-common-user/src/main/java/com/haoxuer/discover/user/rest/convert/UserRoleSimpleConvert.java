package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.simple.UserRoleSimple;
import com.haoxuer.discover.user.data.entity.UserRole;
import com.haoxuer.discover.data.rest.core.Conver;
public class UserRoleSimpleConvert implements Conver<UserRoleSimple, UserRole> {


    @Override
    public UserRoleSimple conver(UserRole source) {
        UserRoleSimple result = new UserRoleSimple();

            result.setId(source.getId());
             result.setAlias(source.getAlias());
            if(source.getCatalog()!=null){
               result.setCatalog(source.getCatalog().getId());
            }
             if(source.getCatalog()!=null){
                result.setCatalogName(source.getCatalog().getName());
             }
             result.setLastDate(source.getLastDate());
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());
             result.setDescription(source.getDescription());
             result.setType(source.getType());

             result.setTypeName(source.getType()+"");
        return result;
    }
}
