package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.StructureList;
import com.haoxuer.discover.user.api.domain.page.StructurePage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.StructureResponse;

public interface StructureApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    StructureResponse create(StructureDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    StructureResponse update(StructureDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    StructureResponse delete(StructureDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     StructureResponse view(StructureDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    StructureList list(StructureSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    StructurePage search(StructureSearchRequest request);

}