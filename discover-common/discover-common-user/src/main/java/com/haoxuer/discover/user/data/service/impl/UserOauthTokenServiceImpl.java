package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.user.data.response.UserLoginAuthResponse;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import com.haoxuer.discover.user.data.dao.UserOauthTokenDao;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.entity.UserOauthToken;
import com.haoxuer.discover.user.data.service.UserOauthTokenService;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2017年07月21日16:04:29.
 */
@Service
@Transactional
public class UserOauthTokenServiceImpl implements UserOauthTokenService {

  private UserOauthTokenDao dao;

  @Autowired
  private UserInfoDao userInfoDao;


  @Override
  public UserOauthToken findByUid(String uid) {


    Finder finder = Finder.create();
    finder.append("from UserOauthToken u where u.uid=:uid");
    finder.setParam("uid", uid);
    return dao.findOne(finder);
  }

  @Override
  public UserOauthToken login(OauthResponse response) {

    UserOauthToken token = dao.loginAuth(response);
    if (token.getUser() == null || token.getUser().getId() == null) {
      UserInfo userInfo = new UserInfo();
      userInfo.setName(response.getName());
      userInfo.setAvatar(response.getAvatar());
      userInfoDao.save(userInfo);
      token.setUser(userInfo);
    }

    return token;
  }

  @Override
  public UserLoginAuthResponse loginAuth(OauthResponse response) {
    UserLoginAuthResponse result = new UserLoginAuthResponse();
    UserOauthToken token = dao.loginAuth(response);
    result.setId(token.getId());
    if (token.getUser() != null && token.getUser().getId() != null) {
      result.setUser(token.getUser().getId());
    }
    return result;
  }

  @Override
  @Transactional(readOnly = true)
  public UserOauthToken findById(Long id) {
    return dao.findById(id);
  }

  @Override
  public UserOauthToken bind(Long id, Long user) {
    UserOauthToken token = dao.findById(id);
    if (token != null) {
      token.setUser(UserInfo.fromId(user));
    }
    return token;
  }


  @Override
  @Transactional
  public UserOauthToken save(UserOauthToken bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public UserOauthToken update(UserOauthToken bean) {
    Updater<UserOauthToken> updater = new Updater<UserOauthToken>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public UserOauthToken deleteById(Long id) {
    UserOauthToken bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public UserOauthToken[] deleteByIds(Long[] ids) {
    UserOauthToken[] beans = new UserOauthToken[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(UserOauthTokenDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<UserOauthToken> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<UserOauthToken> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<UserOauthToken> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}