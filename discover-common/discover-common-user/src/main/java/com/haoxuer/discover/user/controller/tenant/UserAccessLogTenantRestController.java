package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.UserAccessLogApi;
import com.haoxuer.discover.user.api.domain.list.UserAccessLogList;
import com.haoxuer.discover.user.api.domain.page.UserAccessLogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserAccessLogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/useraccesslog")
@RestController
public class UserAccessLogTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("useraccesslog")
    @RequiresUser
    @RequestMapping("create")
    public UserAccessLogResponse create(UserAccessLogDataRequest request) {
        init(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("useraccesslog")
    @RequiresUser
    @RequestMapping("update")
    public UserAccessLogResponse update(UserAccessLogDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("useraccesslog")
    @RequiresUser
    @RequestMapping("delete")
    public UserAccessLogResponse delete(UserAccessLogDataRequest request) {
        init(request);
        UserAccessLogResponse result = new UserAccessLogResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("useraccesslog")
    @RequiresUser
    @RequestMapping("view")
    public UserAccessLogResponse view(UserAccessLogDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("useraccesslog")
    @RequiresUser
    @RequestMapping("list")
    public UserAccessLogList list(UserAccessLogSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("useraccesslog")
    @RequiresUser
    @RequestMapping("search")
    public UserAccessLogPage search(UserAccessLogSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private UserAccessLogApi api;

}
