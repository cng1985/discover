package com.haoxuer.discover.user.data.entity;

import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.InputType;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.enums.State;
import com.haoxuer.discover.user.data.enums.DataScope;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.*;

/**
 * Created by ada on 2017/7/21.
 */

@MappedSuperclass
public abstract class AbstractUser extends AbstractEntity {
  
  
  /**
   * 属性
   */
  @ElementCollection(fetch = FetchType.LAZY)
  @CollectionTable(name = "user_info_attribute", joinColumns = {@JoinColumn(name = "user_id")})
  @MapKeyColumn(name = "name", length = 36)
  @Column(name = "attr", length = 100)
  private Map<String, String> attributes = new HashMap<String, String>();
  
  /**
   * 用户头像
   */
  private String avatar;
  
  /**
   * 用户类型
   */
  private Integer catalog;
  
  
  /**
   * 手机号码
   */
  @Column(length = 15)
  private String phone;
  
  /**
   * 用户登录次数
   */
  private Integer loginSize = 0;
  
  /**
   * 用户真实姓名
   */
  @Column(length = 20)
  private String name;

  @FieldName
  @FieldConvert(classType = "Integer")
  @ManyToOne(fetch = FetchType.LAZY)
  private Structure structure;

  @FormField(title = "数据范围", sortNum = "1",type = InputType.el_radio_group,grid = true)
  private DataScope dataScope;


  @Column(length = 100)
  private String note;
  
  /**
   * 用户状态
   */
  @Enumerated()
  private State state;
  
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "user_label_links", joinColumns = {@JoinColumn(name = "user_id")})
  private Set<UserLabel> labels = new HashSet<UserLabel>();

  public Map<String, String> getAttributes() {
    return attributes;
  }

  public void setAttributes(Map<String, String> attributes) {
    this.attributes = attributes;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public Integer getCatalog() {
    return catalog;
  }

  public void setCatalog(Integer catalog) {
    this.catalog = catalog;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Integer getLoginSize() {
    return loginSize;
  }

  public void setLoginSize(Integer loginSize) {
    this.loginSize = loginSize;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public Set<UserLabel> getLabels() {
    return labels;
  }

  public void setLabels(Set<UserLabel> labels) {
    this.labels = labels;
  }

  public Structure getStructure() {
    return structure;
  }

  public void setStructure(Structure structure) {
    this.structure = structure;
  }

  public DataScope getDataScope() {
    return dataScope;
  }

  public void setDataScope(DataScope dataScope) {
    this.dataScope = dataScope;
  }
}
