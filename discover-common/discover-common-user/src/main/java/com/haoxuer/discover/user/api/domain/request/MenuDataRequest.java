package com.haoxuer.discover.user.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;

import java.util.Date;

/**
 * Created by imake on 2021年01月11日23:03:48.
 */

@Data
public class MenuDataRequest extends BaseRequest {

    private Integer id;

    private String code;

    private Integer catalog;

    private String icon;

    private String permission;

    private Date addDate;

    private String path;

    private Integer levelInfo;

    private Integer sortNum;

    private String ids;

    private Integer lft;

    private Date lastDate;

    private String name;

    private Long nums;

    private Integer rgt;

    private Integer menuType;

    private Integer parent;

    private String  router;

}