package com.haoxuer.discover.user.rest.vo;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.data.entity.UserInfo;

public class UserBackResponse extends ResponseObject {

  private UserInfo user;

  public UserInfo getUser() {
    return user;
  }

  public void setUser(UserInfo user) {
    this.user = user;
  }
}
