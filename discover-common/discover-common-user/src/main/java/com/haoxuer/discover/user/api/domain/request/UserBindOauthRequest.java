package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class UserBindOauthRequest extends RequestUserTokenObject {

  /**
   * 第三方登陆类型
   */
  private String type;

  private String code;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
