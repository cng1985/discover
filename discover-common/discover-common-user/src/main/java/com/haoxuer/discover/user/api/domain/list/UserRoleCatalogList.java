package com.haoxuer.discover.user.api.domain.list;


import com.haoxuer.discover.user.api.domain.simple.UserRoleCatalogSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年05月16日16:35:38.
*/

@Data
public class UserRoleCatalogList  extends ResponseList<UserRoleCatalogSimple> {

}