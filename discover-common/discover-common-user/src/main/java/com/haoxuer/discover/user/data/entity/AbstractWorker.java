package com.haoxuer.discover.user.data.entity;


import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "user_info")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "userType", discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue("1")
public class AbstractWorker extends AbstractEntity {

}
