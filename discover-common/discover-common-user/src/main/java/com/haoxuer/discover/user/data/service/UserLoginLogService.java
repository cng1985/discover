package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.entity.UserLoginLog;
import com.haoxuer.discover.user.data.enums.LoginState;
import org.apache.shiro.authc.UsernamePasswordToken;

import java.util.List;

/**
 * Created by imake on 2017年07月21日15:55:37.
 */
public interface UserLoginLogService {

    UserLoginLog findById(Long id);

    UserLoginLog save(UserLoginLog bean);

    UserLoginLog update(UserLoginLog bean);

    UserLoginLog deleteById(Long id);

    UserLoginLog[] deleteByIds(Long[] ids);


    Page<UserLoginLog> page(Pageable pageable);

    Page<UserLoginLog> page(Pageable pageable, Object search);


    List<UserLoginLog> list(int first, Integer size, List<Filter> filters, List<Order> orders);

    UserLoginLog save(UsernamePasswordToken token, LoginState loginState);
}