package com.haoxuer.discover.user.controller.rest;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.api.apis.UserHandler;
import com.haoxuer.discover.user.api.apis.UserInfoApi;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserInfoResponse;
import com.haoxuer.discover.user.api.domain.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ada on 2017/6/29.
 */


@RestController
@RequestMapping(value = "rest")
public class UserRestController {
  
  
  @RequestMapping(value = "/user/sendcode")
  public ResponseObject sendCode(SendCodeRequest request) {
    return userHandler.sendCode(request);
  }
  
  @RequestMapping(value = "/user/loginbycode")
  public UserResponse loginByCode(UserLoginCodeRequest request) {
    return userHandler.loginByCode(request);
  }
  
  
  @RequestMapping(value = "/user/login")
  public UserResponse login(UserLoginRequest request) {
    return userHandler.login(request);
  }
  
  @RequestMapping(value = "/user/loginoauth")
  public UserResponse loginOauth(UserLoginOatuthRequest request) {
    return userHandler.loginOauth(request);
  }
  
  @RequestMapping(value = "/user/registeroauth")
  public UserResponse registerOauth(UserRegisterOatuthRequest request) {
    return userHandler.registerOauth(request);
  }
  
  @RequestMapping(value = "/user/registerbycode")
  public UserResponse registerByCode(UserRegisterCodeRequest request) {
    return userHandler.registerByCode(request);
  }
  
  
  @RequestMapping(value = "/user/resetpassword")
  public UserResponse resetPassword(UserResetPasswordRequest request) {
    return userHandler.resetPassword(request);
  }
  
  
  @RequestMapping(value = "/user/changephone")
  public ResponseObject changePhone(UserChangePhoneRequest request) {
    return userHandler.changePhone(request);
  }
  
  @RequestMapping(value = "/user/changepassword")
  public ResponseObject changePassword(UserChangePasswordRequest request) {
    return userHandler.changePassword(request);
  }
  
  
  @RequestMapping(value = "/user/update")
  public ResponseObject update(UserUpdateRequest request) {
    return userHandler.update(request);
  }
  
  
  @RequestMapping(value = "/user/checkphonecode")
  public ResponseObject checkPhoneCode(CheckPhoneCodeRequest request) {
    return userHandler.checkPhoneCode(request);
  }
  
  
  @RequestMapping(value = "/user/loginoauthok")
  public UserResponse loginOauthOk(UserLoginOatuthRequest request) {
    return userHandler.loginOauthOk(request);
  }

  @RequestMapping(value = "/user/bindoauth")
  public UserResponse bindOauth(UserBindOauthRequest request) {
    return userHandler.bindOauth(request);
  }

  @RequestMapping(value = "/user/bindByCode")
  public UserResponse bindByCode(UserLoginCodeRequest request) {
    return userHandler.bindByCode(request);
  }

  @RequestMapping(value = "/user/bindPhone")
  public UserResponse bindPhone(UserBindPhoneRequest request) {
    return userHandler.bindPhone(request);
  }

  @RequestMapping(value = "/user/findOpenId")
  public UserResponse findOpenId(UserLoginOatuthRequest request) {
    return userHandler.findOpenId(request);
  }

  @Autowired
  UserHandler userHandler;


  @RequestMapping(value = "/user/loginBySubject")
  public UserInfoResponse login(UserInfoDataRequest request) {
    return api.login(request);
  }

  @Autowired
  private UserInfoApi api;
  
}
