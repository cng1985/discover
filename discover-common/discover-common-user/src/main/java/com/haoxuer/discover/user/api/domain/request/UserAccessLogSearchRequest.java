package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年12月04日16:19:12.
*/

@Data
public class UserAccessLogSearchRequest extends BasePageRequest {

    //用户昵称
     @Search(name = "creator.name",operator = Filter.Operator.like)
     private String creatorName;


    @Search(name = "storeState",operator = Filter.Operator.eq)
    private StoreState storeState=StoreState.normal;

    private String sortField;


    private String sortMethod;
}