package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.UserRoleCatalogApi;
import com.haoxuer.discover.user.api.domain.list.UserRoleCatalogList;
import com.haoxuer.discover.user.api.domain.page.UserRoleCatalogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserRoleCatalogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/userrolecatalog")
@RestController
public class UserRoleCatalogTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("userrolecatalog")
    @RequiresUser
    @RequestMapping("create")
    public UserRoleCatalogResponse create(UserRoleCatalogDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("userrolecatalog")
    @RequiresUser
    @RequestMapping("update")
    public UserRoleCatalogResponse update(UserRoleCatalogDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("userrolecatalog")
    @RequiresUser
    @RequestMapping("delete")
    public UserRoleCatalogResponse delete(UserRoleCatalogDataRequest request) {
        init(request);
        UserRoleCatalogResponse result = new UserRoleCatalogResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("userrolecatalog")
    @RequiresUser
    @RequestMapping("view")
    public UserRoleCatalogResponse view(UserRoleCatalogDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("userrolecatalog")
    @RequiresUser
    @RequestMapping("list")
    public UserRoleCatalogList list(UserRoleCatalogSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("userrolecatalog")
    @RequiresUser
    @RequestMapping("search")
    public UserRoleCatalogPage search(UserRoleCatalogSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private UserRoleCatalogApi api;

}
