package com.haoxuer.discover.user.api.domain.list;


import com.haoxuer.discover.user.api.domain.simple.StructureSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年05月21日22:44:58.
*/

@Data
public class StructureList  extends ResponseList<StructureSimple> {

}