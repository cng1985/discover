package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.user.data.dao.UserLabelDao;
import com.haoxuer.discover.user.data.entity.UserLabel;
import com.haoxuer.discover.user.data.service.UserLabelService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2017年12月06日15:59:13.
 */


@Scope("prototype")
@Service
@Transactional
public class UserLabelServiceImpl implements UserLabelService {
  
  private UserLabelDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public UserLabel findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public UserLabel save(UserLabel bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public UserLabel update(UserLabel bean) {
    Updater<UserLabel> updater = new Updater<UserLabel>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public UserLabel deleteById(Long id) {
    UserLabel bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public UserLabel[] deleteByIds(Long[] ids) {
    UserLabel[] beans = new UserLabel[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(UserLabelDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<UserLabel> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<UserLabel> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<UserLabel> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}