package com.haoxuer.discover.user.oauth.domain;

public class OsChinaUser implements OauthResponse {
  @Override
  public String getOpenid() {
    return id;
  }
  
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public String getAvatar() {
    return avatar;
  }
  
  @Override
  public String type() {
    return "oschina";
  }
  
  private String id;
  
  /**
   * 用户email
   */
  private String email;
  
  
  /**
   * 用户名
   */
  private String name;
  
  
  /**
   * 性别
   */
  private String gender;
  
  
  /**
   * 头像
   */
  private String avatar;
  /**
   * 地点
   */
  private String location;
  /**
   * 主页
   */
  private String url;
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getEmail() {
    return email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getGender() {
    return gender;
  }
  
  public void setGender(String gender) {
    this.gender = gender;
  }
  
  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }
  
  public String getLocation() {
    return location;
  }
  
  public void setLocation(String location) {
    this.location = location;
  }
  
  public String getUrl() {
    return url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  @Override
  public String toString() {
    return "OsChinaUser{" +
        "id='" + id + '\'' +
        ", email='" + email + '\'' +
        ", name='" + name + '\'' +
        ", gender='" + gender + '\'' +
        ", avatar='" + avatar + '\'' +
        ", location='" + location + '\'' +
        ", url='" + url + '\'' +
        '}';
  }
}
