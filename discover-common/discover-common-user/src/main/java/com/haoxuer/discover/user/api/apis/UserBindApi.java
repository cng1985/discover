package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.UserBindList;
import com.haoxuer.discover.user.api.domain.page.UserBindPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserBindResponse;

public interface UserBindApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    UserBindResponse create(UserBindDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    UserBindResponse update(UserBindDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    UserBindResponse delete(UserBindDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     UserBindResponse view(UserBindDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    UserBindList list(UserBindSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    UserBindPage search(UserBindSearchRequest request);

}