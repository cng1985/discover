package com.haoxuer.discover.user.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;

/**
* Created by imake on 2020年07月27日11:36:00.
*/
@Data
public class UserRoleSo implements Serializable {



    private String sortField;

    private String sortMethod;

}
