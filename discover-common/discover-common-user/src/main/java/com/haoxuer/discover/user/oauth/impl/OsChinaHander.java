package com.haoxuer.discover.user.oauth.impl;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.haoxuer.discover.user.oauth.api.OauthHandler;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.user.oauth.domain.OsChinaUser;
import com.haoxuer.discover.user.oauth.domain.TokenResponse;
import com.haoxuer.utils.http.Connection;
import com.haoxuer.utils.http.HttpConnection;
import java.io.IOException;

public class OsChinaHander implements OauthHandler {
  private String client_id;
  
  private String client_secret;
  private String redirect_uri;
  
  public String getRedirect_uri() {
    return redirect_uri;
  }
  
  public void setRedirect_uri(String redirect_uri) {
    this.redirect_uri = redirect_uri;
  }
  
  @Override
  public void setKey(String key) {
    this.client_id = key;
  }
  
  @Override
  public void setSecret(String secret) {
    this.client_secret = secret;
  }
  
  @Override
  public OauthResponse login(String access_token, String openid) {
    OsChinaUser result = null;
    Connection info = HttpConnection.connect("http://www.oschina.net/action/openapi/user");
    
    info.data("access_token", access_token);
    info.data("dataType", "json");
    info.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
    String infojson = null;
    try {
      infojson = info.execute().body();
      Gson gson = new Gson();
      result = gson.fromJson(infojson, OsChinaUser.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }
  
  @Override
  public TokenResponse getToken(String code) {
    TokenResponse response = new TokenResponse();
    
    Connection con = HttpConnection.connect("http://www.oschina.net/action/openapi/token");
    //con.method(Connection.Method.POST);
    con.data("client_id", client_id);
    con.data("client_secret", client_secret);
    con.data("grant_type", "authorization_code");
    con.data("redirect_uri", redirect_uri);
    con.data("code", code);
    con.data("dataType", "json");
    con.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
    try {
      String body = con.execute().body();
      JsonParser parser = new JsonParser();
      JsonElement element = parser.parse(body);
      response.setAccessToken(ElementUtils.getString(element, "access_token"));
      response.setRefreshToken(ElementUtils.getString(element, "refresh_token"));
      response.setTokenType(ElementUtils.getString(element, "token_type"));
      response.setExpiresIn(ElementUtils.getInt(element, "expires_in"));
      response.setOpenId(ElementUtils.getString(element, "uid"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return response;
  }
}
