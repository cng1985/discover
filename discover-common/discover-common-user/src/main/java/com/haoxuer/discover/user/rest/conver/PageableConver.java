package com.haoxuer.discover.user.rest.conver;

import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.rest.base.RequestUserTokenPageObject;

@Deprecated
public class PageableConver implements Conver<Pageable, RequestUserTokenPageObject> {
    @Override
    public Pageable conver(RequestUserTokenPageObject source) {
        return new PageableConverter().apply(source);
    }
}
