package com.haoxuer.discover.user.oauth.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.haoxuer.discover.user.oauth.api.OauthHandler;
import com.haoxuer.discover.user.oauth.domain.TokenResponse;
import com.haoxuer.discover.user.oauth.domain.UserWeiXin;
import com.haoxuer.utils.http.Connection;
import com.haoxuer.utils.http.HttpConnection;
import java.io.IOException;

/**
 * Created by ada on 2017/6/29.
 */
public class WeiXinOauthHandler implements OauthHandler {
  /**
   * @param element
   * @return
   */
  private String getString(JsonElement element, String key) {
    
    String result = "";
    try {
      result = element.getAsJsonObject().get(key).getAsString();
    } catch (Exception e2) {
    }
    
    return result;
  }
  
  private Integer getInt(JsonElement element, String key) {
    
    Integer result = 0;
    try {
      result = element.getAsJsonObject().get(key).getAsInt();
    } catch (Exception e2) {
    }
    return result;
  }
  
  private String oauth_consumer_key;
  
  private String secret;
  
  @Override
  public void setKey(String oauth_consumer_key) {
    this.oauth_consumer_key = oauth_consumer_key;
  }
  
  @Override
  public void setSecret(String secret) {
    this.secret = secret;
  }
  
  public UserWeiXin login(String access_token, String openid) {
    UserWeiXin weixin = null;
    try {
      Connection con = HttpConnection.connect("https://api.weixin.qq.com/sns/userinfo");
      con.data("access_token", access_token);
      con.data("openid", openid);
      String body;
      body = con.execute().body();
      
      weixin = new UserWeiXin();
      JsonParser parser = new JsonParser();
      JsonElement element = parser.parse(body);
      String nickname = getString(element, "nickname");
      weixin.setNickName(nickname);
      String headimgurl = getString(element, "headimgurl");
      weixin.setHeadimgurl(headimgurl);
      String city = getString(element, "city");
      weixin.setCity(city);
      Integer sexid = getInt(element, "sex");
      String sex = "男";
      if (sexid == null || sexid == 0) {
        sex = "女";
      }
      weixin.setSex(sex);
      String province = getString(element, "province");
      weixin.setProvince(province);
      String country = getString(element, "country");
      weixin.setCountry(country);
      weixin.setUnionid(getString(element, "unionid"));
      weixin.setAccessToken(access_token);
      weixin.setOpenid(openid);
      
    } catch (Exception e) {
      e.printStackTrace();
    }
    return weixin;
    
  }
  
  @Override
  public TokenResponse getToken(String code) {
    TokenResponse result = new TokenResponse();
    Connection con = HttpConnection.connect("https://api.weixin.qq.com/sns/oauth2/access_token");
    con.data("appid", oauth_consumer_key);
    con.data("secret", secret);
    con.data("code", code);
    con.data("grant_type", "authorization_code");
    String body;
    try {
      body = con.execute().body();
      JsonParser parser = new JsonParser();
      JsonElement element = parser.parse(body);
      result.setAccessToken(ElementUtils.getString(element, "access_token"));
      result.setRefreshToken(ElementUtils.getString(element, "refresh_token"));
      result.setTokenType("weixin");
      result.setExpiresIn(ElementUtils.getInt(element, "expires_in"));
      result.setOpenId(ElementUtils.getString(element, "openid"));
      result.setUnionid(ElementUtils.getString(element, "unionid"));
    } catch (IOException e) {
      e.printStackTrace();
    }

    return result;
  }
}
