package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.user.data.entity.UserRole;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2020年07月27日11:36:00.
*/
public interface UserRoleService {

	UserRole findById(Long id);

	UserRole save(UserRole bean);

	UserRole update(UserRole bean);

	UserRole deleteById(Long id);
	
	UserRole[] deleteByIds(Long[] ids);
	
	Page<UserRole> page(Pageable pageable);
	
	Page<UserRole> page(Pageable pageable, Object search);


	List<UserRole> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}