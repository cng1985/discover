package com.haoxuer.discover.user.controller.admin;

import com.haoxuer.discover.controller.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
/**
*
* Created by imake on 2021年05月16日16:35:38.
*/

@Scope("prototype")
@Controller
public class UserRoleCatalogAction extends BaseAction {


	@RequiresPermissions("userrolecatalog")
	@RequestMapping("/admin/userrolecatalog/view_list")
	public String list() {
		return getView("userrolecatalog/list");
	}

}