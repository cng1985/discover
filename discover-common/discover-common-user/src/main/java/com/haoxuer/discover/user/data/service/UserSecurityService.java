package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.user.data.entity.UserSecurity;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.enums.SecurityType;

import java.util.List;

/**
* Created by imake on 2018年11月28日09:53:01.
*/
public interface UserSecurityService {

	UserSecurity findById(Long id);

	UserSecurity save(UserSecurity bean);

	UserSecurity update(UserSecurity bean);

	UserSecurity deleteById(Long id);
	
	UserSecurity[] deleteByIds(Long[] ids);
	
	Page<UserSecurity> page(Pageable pageable);
	
	Page<UserSecurity> page(Pageable pageable, Object search);


	List<UserSecurity> list(int first, Integer size, List<Filter> filters, List<Order> orders);


	/**
	 * 查找用户安全信息
	 *
	 * @param user
	 * @param type
	 * @return
	 */
	UserSecurity findByUser(Long user, SecurityType type);

}