package com.haoxuer.discover.user.controller.rest;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;
import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import com.haoxuer.discover.user.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseRestController {

    @Autowired
    UserTokenService tokenService;

    protected void init(BaseRequest request) {
        Long user = tokenService.user(request.getUserToken());
        request.setUser(user);
    }

    protected void init(BasePageRequest request) {
        Long user = tokenService.user(request.getUserToken());
        request.setUser(user);

    }

}
