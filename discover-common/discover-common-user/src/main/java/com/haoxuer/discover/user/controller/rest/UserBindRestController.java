package com.haoxuer.discover.user.controller.rest;

import com.haoxuer.discover.user.api.apis.UserBindApi;
import com.haoxuer.discover.user.api.domain.list.UserBindList;
import com.haoxuer.discover.user.api.domain.page.UserBindPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserBindResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/userbind")
@RestController
public class UserBindRestController extends BaseRestController {


    @RequestMapping("create")
    public UserBindResponse create(UserBindDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public UserBindResponse update(UserBindDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public UserBindResponse delete(UserBindDataRequest request) {
        init(request);
        UserBindResponse result = new UserBindResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public UserBindResponse view(UserBindDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public UserBindList list(UserBindSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public UserBindPage search(UserBindSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private UserBindApi api;

}
