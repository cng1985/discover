package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.entity.UserOauthToken;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.user.data.dao.UserOauthTokenDao;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by imake on 2017年07月21日16:04:29.
 */
@Repository

public class UserOauthTokenDaoImpl extends CriteriaDaoImpl<UserOauthToken, Long> implements UserOauthTokenDao {

  @Override
  public UserOauthToken findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserOauthToken findByUser(Long user, String type) {
    Finder finder = Finder.create();
    finder.append("from UserOauthToken u where u.user.id=:user and u.token_type=:token_type");
    finder.setParam("user", user);
    finder.setParam("token_type", type);
    return findOne(finder);
  }

  @Override
  public List<UserOauthToken> tokens(Long user) {
    List<Filter> filters=new ArrayList<>();
    filters.add(Filter.eq("user.id",user));
    return list(0,20,filters,null);
  }

  @Override
  public UserOauthToken findByOpenId(String openid, String type) {
    Finder finder = Finder.create();
    finder.append("from UserOauthToken u where u.uid=:uid and u.token_type=:token_type");
    finder.setParam("uid", openid);
    finder.setParam("token_type", type);
    return findOne(finder);
  }

  @Override
  public UserOauthToken save(UserOauthToken bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserOauthToken deleteById(Long id) {
    UserOauthToken entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public UserOauthToken loginAuth(OauthResponse response) {
    Finder finder = Finder.create();
    finder.append("from UserOauthToken u where u.uid=:uid");
    finder.append(" and u.token_type=:token_type");
    finder.setParam("uid", response.getOpenid());
    finder.setParam("token_type", response.type());
    UserOauthToken token = findOne(finder);
    if (token == null) {
      token = new UserOauthToken();
      token.setUid(response.getOpenid());
      token.setToken_type(response.type());
      save(token);
    }
    token.setLastDate(new Date());
    Integer num = token.getLoginSize();
    if (num == null) {
      num = 1;
    }
    num++;
    token.setLoginSize(num);
    return token;
  }

  @Override
  protected Class<UserOauthToken> getEntityClass() {
    return UserOauthToken.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}