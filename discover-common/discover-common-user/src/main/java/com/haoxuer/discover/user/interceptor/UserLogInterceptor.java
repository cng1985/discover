package com.haoxuer.discover.user.interceptor;

import com.haoxuer.discover.user.api.apis.UserAccessLogApi;
import com.haoxuer.discover.user.api.domain.request.UserAccessLogDataRequest;
import com.haoxuer.discover.user.data.entity.UserAccessLog;
import com.haoxuer.discover.user.shiro.realm.ShiroUser;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserLogInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserAccessLogApi logApi;

    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Real-IP");
        if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
            return ip;
        }
        ip = request.getHeader("X-Forwarded-For");
        if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
            // 多次反向代理后会有多个IP值，第一个为真实IP。
            int index = ip.indexOf(',');
            if (index != -1) {
                return ip.substring(0, index);
            } else {
                return ip;
            }
        } else {
            return request.getRemoteAddr();
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        request.setAttribute("time", System.currentTimeMillis());
        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        try {
            handleLog(request);
        } catch (Exception e) {

        }

    }

    private void handleLog(HttpServletRequest request) {
        ShiroUser user = UserUtil.getCurrentShiroUser();
        if (user != null) {
            UserAccessLogDataRequest data = new UserAccessLogDataRequest();
            data.setCreator(user.getId());
            data.setIp(getIpAddr(request));
            Long time = (Long) request.getAttribute("time");
            if (time != null) {
                time = System.currentTimeMillis() - time;
                data.setConsumeTime(time);
            }
            data.setUrl(request.getRequestURI());
            logApi.create(data);
        }

    }
}
