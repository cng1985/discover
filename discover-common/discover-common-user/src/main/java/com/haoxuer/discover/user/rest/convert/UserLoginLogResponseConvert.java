package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.response.UserLoginLogResponse;
import com.haoxuer.discover.user.data.entity.UserLoginLog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class UserLoginLogResponseConvert implements Conver<UserLoginLogResponse, UserLoginLog> {
    @Override
    public UserLoginLogResponse conver(UserLoginLog source) {
        UserLoginLogResponse result = new UserLoginLogResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getUser()!=null){
           result.setUser(source.getUser().getId());
        }
         if(source.getUser()!=null){
            result.setUserName(source.getUser().getName());
         }

         result.setStateName(source.getState()+"");

        return result;
    }
}
