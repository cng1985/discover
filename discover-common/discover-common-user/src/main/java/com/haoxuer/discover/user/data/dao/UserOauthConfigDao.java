package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.UserOauthConfig;
import com.haoxuer.discover.user.oauth.api.OauthHandler;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年07月20日17:47:47.
 */
public interface UserOauthConfigDao extends BaseDao<UserOauthConfig, Long> {
  
  UserOauthConfig findById(Long id);
  
  UserOauthConfig save(UserOauthConfig bean);
  
  UserOauthConfig updateByUpdater(Updater<UserOauthConfig> updater);
  
  UserOauthConfig deleteById(Long id);
  
  OauthHandler id(String model);
  
  void clear();
  
}