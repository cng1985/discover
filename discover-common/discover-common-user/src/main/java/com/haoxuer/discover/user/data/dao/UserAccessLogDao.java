package com.haoxuer.discover.user.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.user.data.entity.UserAccessLog;

/**
* Created by imake on 2021年12月04日16:19:12.
*/
public interface UserAccessLogDao extends BaseDao<UserAccessLog,Long>{

	 UserAccessLog findById(Long id);

	 UserAccessLog save(UserAccessLog bean);

	 UserAccessLog updateByUpdater(Updater<UserAccessLog> updater);

	 UserAccessLog deleteById(Long id);
}