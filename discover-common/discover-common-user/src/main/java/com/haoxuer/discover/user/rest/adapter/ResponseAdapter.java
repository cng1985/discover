package com.haoxuer.discover.user.rest.adapter;

import com.haoxuer.discover.filter.common.HandlerResponse;
import com.haoxuer.discover.rest.base.ResponseObject;

public class ResponseAdapter implements HandlerResponse {

  public ResponseAdapter(ResponseObject object) {
    this.object = object;
  }

  private ResponseObject object;


  @Override
  public String getMsg() {
    return object.getMsg();
  }

  @Override
  public void setMsg(String msg) {
    object.setMsg(msg);
  }

  @Override
  public int getCode() {
    return object.getCode();
  }

  @Override
  public void setCode(int code) {
    object.setCode(code);
  }
}
