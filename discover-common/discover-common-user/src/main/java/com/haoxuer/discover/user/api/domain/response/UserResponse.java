package com.haoxuer.discover.user.api.domain.response;

import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;

/**
 * Created by ada on 2017/6/29.
 */
@Data
public class UserResponse extends ResponseObject {

    private Long id;

    /**
     * 用户令牌
     */
    private String userToken;

    /**
     * 用户昵称
     */
    private String name;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 有没有手机号码
     */
    private Boolean havePhone;

    private String openId;

}
