package com.haoxuer.discover.user.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.user.data.entity.UserLoginLog;

/**
* Created by imake on 2021年05月22日10:49:09.
*/
public interface UserLoginLogDao extends BaseDao<UserLoginLog,Long>{

	 UserLoginLog findById(Long id);

	 UserLoginLog save(UserLoginLog bean);

	 UserLoginLog updateByUpdater(Updater<UserLoginLog> updater);

	 UserLoginLog deleteById(Long id);
}