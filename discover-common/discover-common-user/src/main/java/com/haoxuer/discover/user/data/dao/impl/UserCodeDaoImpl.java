package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.user.data.entity.UserCode;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.user.data.dao.UserCodeDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年02月23日16:25:55.
 */
@Repository

public class UserCodeDaoImpl extends CriteriaDaoImpl<UserCode, Long> implements UserCodeDao {

  @Override
  public UserCode findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserCode save(UserCode bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserCode deleteById(Long id) {
    UserCode entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public UserCode findByCode(String code) {
    Finder finder = Finder.create();
    finder.append("from UserCode u where u.code=:code");
    finder.setParam("code", code);
    return findOne(finder);
  }

  @Override
  protected Class<UserCode> getEntityClass() {
    return UserCode.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}