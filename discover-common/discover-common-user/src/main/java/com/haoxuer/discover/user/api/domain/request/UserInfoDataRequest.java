package com.haoxuer.discover.user.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.data.enums.State;
import com.haoxuer.discover.data.enums.StoreState;

import java.util.Date;

/**
 * Created by imake on 2021年04月28日11:36:16.
 */

@Data
public class UserInfoDataRequest extends BaseRequest {

    private Long id;

    private StoreState storeState;

    private String phone;

    private Integer catalog;

    private String name;

    private String avatar;

    private State state;

    private Integer loginSize;

    private String note;

    private String username;

    private String password;

}