package com.haoxuer.discover.user.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年05月22日10:49:09.
*/

@Scope("prototype")
@Controller
public class UserLoginLogAction extends BaseAction{


	@RequiresPermissions("userloginlog")
	@RequestMapping("/admin/userloginlog/view_list")
	public String list() {
		return getView("userloginlog/list");
	}

}