package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.user.api.domain.simple.MenuSimple;
import com.haoxuer.discover.user.data.entity.Menu;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;


@Data
public class MenuBasicSimpleConvert implements Conver<MenuSimple, Menu> {

    private int fetch;

    @Override
    public MenuSimple conver(Menu source) {
        MenuSimple result = new MenuSimple();

        result.setId(source.getId());
        result.setLabel(source.getName());
        result.setValue("" + source.getId());
        result.setName(source.getName());
        result.setIcon(source.getIcon());
        result.setPath(source.getPath());
        result.setPermission(source.getPermission());
        result.setCatalog(source.getCatalog());
        result.setIds(source.getIds());
        if (fetch != 0 && source.getChildrens() != null && source.getChildrens().size() > 0) {
            List<MenuSimple> menus =   ConverResourceUtils.converList(source.getChildrens(), this);
            List<MenuSimple> simples = menus.stream().filter(item -> item.getCatalog() == 0).collect(Collectors.toList());
            result.setChildren(simples);
        }
        return result;
    }
}
