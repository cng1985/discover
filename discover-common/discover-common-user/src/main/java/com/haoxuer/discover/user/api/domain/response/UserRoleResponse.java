package com.haoxuer.discover.user.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.user.data.enums.RoleType;

/**
 * Created by imake on 2021年05月16日16:09:08.
 */

@Data
public class UserRoleResponse extends ResponseObject {

    private Long id;

    private String alias;

    private Integer catalog;

    private String catalogName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date lastDate;

    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;

    private String description;

    private RoleType type;


    private String typeName;

    private List<String> authorities;
}