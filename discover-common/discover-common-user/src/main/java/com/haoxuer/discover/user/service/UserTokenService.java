package com.haoxuer.discover.user.service;

public interface UserTokenService {

  /**
   * 把用户id转化为json web token
   *
   * @param id
   * @return
   */
  String token(Long id);


  Long user(String token);
}
