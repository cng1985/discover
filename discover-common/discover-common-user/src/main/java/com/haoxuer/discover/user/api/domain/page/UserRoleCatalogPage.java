package com.haoxuer.discover.user.api.domain.page;


import com.haoxuer.discover.user.api.domain.simple.UserRoleCatalogSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月16日16:35:38.
*/

@Data
public class UserRoleCatalogPage  extends ResponsePage<UserRoleCatalogSimple> {

}