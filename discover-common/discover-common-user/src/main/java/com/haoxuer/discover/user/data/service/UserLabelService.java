package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.user.data.entity.UserLabel;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2017年12月06日15:59:13.
 */
public interface UserLabelService {
  
  UserLabel findById(Long id);
  
  UserLabel save(UserLabel bean);
  
  UserLabel update(UserLabel bean);
  
  UserLabel deleteById(Long id);
  
  UserLabel[] deleteByIds(Long[] ids);
  
  Page<UserLabel> page(Pageable pageable);
  
  Page<UserLabel> page(Pageable pageable, Object search);
  
  
  List<UserLabel> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}