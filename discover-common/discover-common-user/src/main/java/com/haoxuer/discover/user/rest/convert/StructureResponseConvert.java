package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.response.StructureResponse;
import com.haoxuer.discover.user.data.entity.Structure;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class StructureResponseConvert implements Conver<StructureResponse, Structure> {
    @Override
    public StructureResponse conver(Structure source) {
        StructureResponse result = new StructureResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getParent()!=null){
           result.setParent(source.getParent().getId());
        }
         if(source.getParent()!=null){
            result.setParentName(source.getParent().getName());
         }


        return result;
    }
}
