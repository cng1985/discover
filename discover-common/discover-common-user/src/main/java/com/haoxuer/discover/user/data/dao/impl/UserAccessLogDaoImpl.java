package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.enums.StoreState;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.user.data.dao.UserAccessLogDao;
import com.haoxuer.discover.user.data.entity.UserAccessLog;

/**
* Created by imake on 2021年12月04日16:19:12.
*/
@Repository

public class UserAccessLogDaoImpl extends CriteriaDaoImpl<UserAccessLog, Long> implements UserAccessLogDao {

	@Override
	public UserAccessLog findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public UserAccessLog save(UserAccessLog bean) {

		bean.setStoreState(StoreState.normal);
        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public UserAccessLog deleteById(Long id) {
		UserAccessLog entity = super.get(id);
		if (entity != null) {
			entity.setStoreState(StoreState.recycle);
		}
		return entity;
	}
	
	@Override
	protected Class<UserAccessLog> getEntityClass() {
		return UserAccessLog.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}