package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.user.data.service.UserCodeService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.user.data.dao.UserCodeDao;
import com.haoxuer.discover.user.data.entity.UserCode;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年02月23日16:25:55.
 */


@Scope("prototype")
@Service
@Transactional
public class UserCodeServiceImpl implements UserCodeService {
  
  private UserCodeDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public UserCode findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public UserCode save(UserCode bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public UserCode update(UserCode bean) {
    Updater<UserCode> updater = new Updater<UserCode>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public UserCode deleteById(Long id) {
    UserCode bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public UserCode[] deleteByIds(Long[] ids) {
    UserCode[] beans = new UserCode[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(UserCodeDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<UserCode> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<UserCode> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<UserCode> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}