package com.haoxuer.discover.user.api.domain.list;


import com.haoxuer.discover.user.api.domain.simple.UserBindSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年05月23日09:43:09.
*/

@Data
public class UserBindList  extends ResponseList<UserBindSimple> {

}