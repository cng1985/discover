package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.user.data.dao.UserFeedBackDao;
import com.haoxuer.discover.user.data.entity.UserFeedBack;
import com.haoxuer.discover.user.data.service.UserFeedBackService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2017年11月14日18:38:15.
 */


@Scope("prototype")
@Service
@Transactional
public class UserFeedBackServiceImpl implements UserFeedBackService {
  
  private UserFeedBackDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public UserFeedBack findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public UserFeedBack save(UserFeedBack bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public UserFeedBack update(UserFeedBack bean) {
    Updater<UserFeedBack> updater = new Updater<UserFeedBack>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public UserFeedBack deleteById(Long id) {
    UserFeedBack bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public UserFeedBack[] deleteByIds(Long[] ids) {
    UserFeedBack[] beans = new UserFeedBack[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(UserFeedBackDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<UserFeedBack> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<UserFeedBack> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<UserFeedBack> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}