package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.user.data.dao.UserBindDao;
import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.enums.LoginState;
import com.haoxuer.discover.user.data.service.UserLoginLogService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.user.data.dao.UserLoginLogDao;
import com.haoxuer.discover.user.data.entity.UserLoginLog;

import java.util.List;

import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2017年07月21日15:55:37.
 */
@Service
@Transactional
public class UserLoginLogServiceImpl implements UserLoginLogService {

    private UserLoginLogDao dao;

    @Autowired
    private UserBindDao bindDao;


    @Override
    @Transactional(readOnly = true)
    public UserLoginLog findById(Long id) {
        return dao.findById(id);
    }


    @Override
    @Transactional
    public UserLoginLog save(UserLoginLog bean) {
        dao.save(bean);
        return bean;
    }

    @Override
    @Transactional
    public UserLoginLog update(UserLoginLog bean) {
        Updater<UserLoginLog> updater = new Updater<UserLoginLog>(bean);
        return dao.updateByUpdater(updater);
    }

    @Override
    @Transactional
    public UserLoginLog deleteById(Long id) {
        UserLoginLog bean = dao.findById(id);
        dao.deleteById(id);
        return bean;
    }

    @Override
    @Transactional
    public UserLoginLog[] deleteByIds(Long[] ids) {
        UserLoginLog[] beans = new UserLoginLog[ids.length];
        for (int i = 0, len = ids.length; i < len; i++) {
            beans[i] = deleteById(ids[i]);
        }
        return beans;
    }


    @Autowired
    public void setDao(UserLoginLogDao dao) {
        this.dao = dao;
    }

    @Override
    public Page<UserLoginLog> page(Pageable pageable) {
        return dao.page(pageable);
    }


    @Override
    public Page<UserLoginLog> page(Pageable pageable, Object search) {
        List<Filter> filters = FilterUtils.getFilters(search);
        if (filters != null) {
            pageable.getFilters().addAll(filters);
        }
        return dao.page(pageable);
    }

    @Override
    public List<UserLoginLog> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first, size, filters, orders);
    }

    @Override
    public UserLoginLog save(UsernamePasswordToken token, LoginState loginState) {
        UserLoginLog bean = new UserLoginLog();
        bean.setAccount(token.getUsername());
        bean.setIp(token.getHost());
        //bean.setState(token.getLoginState());
        bean.setClient("web");
        UserBind bind = bindDao.findByName(token.getUsername());
        if (bind != null) {
            bean.setUser(bind.getUser());
        }
        if (loginState == null) {
            loginState = LoginState.success;
        }
        bean.setState(loginState);
        if (loginState == LoginState.fail) {
            bean.setPassword(new String(token.getPassword()));
        } else {
            if (bind != null) {
                UserInfo user = bind.getUser();
                if (user != null) {
                    Integer loginSize = user.getLoginSize();
                    if (loginSize == null) {
                        loginSize = 0;
                    }
                    loginSize++;
                    user.setLoginSize(loginSize);
                }
                Long loginSize = bind.getLoginSize();
                if (loginSize == null) {
                    loginSize = 0L;
                }
                loginSize++;
                bind.setLoginSize(loginSize);
            }

        }
        dao.save(bean);
        return bean;
    }
}