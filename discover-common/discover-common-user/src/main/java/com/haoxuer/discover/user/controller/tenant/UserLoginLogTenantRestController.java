package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.UserLoginLogApi;
import com.haoxuer.discover.user.api.domain.list.UserLoginLogList;
import com.haoxuer.discover.user.api.domain.page.UserLoginLogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserLoginLogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/userloginlog")
@RestController
public class UserLoginLogTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("userloginlog")
    @RequiresUser
    @RequestMapping("create")
    public UserLoginLogResponse create(UserLoginLogDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("userloginlog")
    @RequiresUser
    @RequestMapping("update")
    public UserLoginLogResponse update(UserLoginLogDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("userloginlog")
    @RequiresUser
    @RequestMapping("delete")
    public UserLoginLogResponse delete(UserLoginLogDataRequest request) {
        init(request);
        UserLoginLogResponse result = new UserLoginLogResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("userloginlog")
    @RequiresUser
    @RequestMapping("view")
    public UserLoginLogResponse view(UserLoginLogDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("userloginlog")
    @RequiresUser
    @RequestMapping("list")
    public UserLoginLogList list(UserLoginLogSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("userloginlog")
    @RequiresUser
    @RequestMapping("search")
    public UserLoginLogPage search(UserLoginLogSearchRequest request) {
        init(request);
        return api.search(request);
    }
    @RequiresUser
    @RequestMapping("searchByUser")
    public UserLoginLogPage searchByUser(UserLoginLogSearchRequest request) {
        init(request);
        request.setUserId(request.getCreateUser());
        return api.search(request);
    }
    @Autowired
    private UserLoginLogApi api;

}
