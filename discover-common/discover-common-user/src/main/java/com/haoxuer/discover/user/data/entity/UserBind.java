package com.haoxuer.discover.user.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.user.data.enums.BindType;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import com.nbsaas.codemake.annotation.SearchItem;
import lombok.Data;

import javax.persistence.*;

/**
 * 用户账号集合
 *
 */
@Data
@Entity
@Table(name = "user_identification")
public class UserBind extends AbstractEntity {


  @SearchItem(label = "用户",name = "user",key = "user.id",classType = "Long",operator = "eq")
  @FieldName
  @FieldConvert
  @ManyToOne(fetch = FetchType.LAZY)
  private UserInfo user;

  private BindType bindType;

  @Column(length = 50)
  private String no;

  private Long loginSize;


}
