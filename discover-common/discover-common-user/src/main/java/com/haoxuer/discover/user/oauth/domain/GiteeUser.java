package com.haoxuer.discover.user.oauth.domain;

public class GiteeUser implements OauthResponse {

  private String login;

  private Integer id;

  private String avatar_url;

  private String name;

  private String email;

  private String phone;

  private String address;

  private Integer followers;

  private Integer following;

  private Integer stared;

  private Integer watched;

  @Override
  public String getOpenid() {
    return id + "";
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getAvatar() {
    return avatar_url;
  }

  @Override
  public String type() {
    return "gitee";
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getAvatar_url() {
    return avatar_url;
  }

  public void setAvatar_url(String avatar_url) {
    this.avatar_url = avatar_url;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "GiteeUser{" +
        "login='" + login + '\'' +
        ", id=" + id +
        ", avatar_url='" + avatar_url + '\'' +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        ", phone='" + phone + '\'' +
        ", address='" + address + '\'' +
        '}';
  }

  public Integer getFollowers() {
    return followers;
  }

  public void setFollowers(Integer followers) {
    this.followers = followers;
  }

  public Integer getFollowing() {
    return following;
  }

  public void setFollowing(Integer following) {
    this.following = following;
  }

  public Integer getStared() {
    return stared;
  }

  public void setStared(Integer stared) {
    this.stared = stared;
  }

  public Integer getWatched() {
    return watched;
  }

  public void setWatched(Integer watched) {
    this.watched = watched;
  }
}
