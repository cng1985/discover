package com.haoxuer.discover.user.api.apis;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.api.domain.request.UserFeedBackPostRequest;

public interface UserFeedBackApi {

  ResponseObject post(UserFeedBackPostRequest request);
}
