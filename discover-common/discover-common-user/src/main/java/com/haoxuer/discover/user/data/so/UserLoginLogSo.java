package com.haoxuer.discover.user.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年12月21日12:34:57.
*/
@Data
public class UserLoginLogSo implements Serializable {

    //用户
     @Search(name = "user.id",operator = Filter.Operator.eq)
     private Long user;



    private String sortField;

    private String sortMethod;

}
