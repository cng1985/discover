package com.haoxuer.discover.user.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import java.io.Serializable;

/**
 * Created by imake on 2017年07月20日16:24:20.
 */
public class UserAccountSo implements Serializable {
  
  @Search(name = "user.name", operator = Filter.Operator.like)
  private String name;

  @Search(name = "username", operator = Filter.Operator.like)
  private String userName;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}
