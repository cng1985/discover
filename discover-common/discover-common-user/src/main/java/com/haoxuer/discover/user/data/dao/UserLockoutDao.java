package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.user.data.entity.UserLockout;

/**
 * Created by imake on 2019年03月19日10:24:16.
 */
public interface UserLockoutDao extends BaseDao<UserLockout, Long> {

  UserLockout findById(Long id);

  UserLockout save(UserLockout bean);

  /**
   * 检查用户是否锁定
   *
   * @param user
   * @return
   */
  Boolean check(Long user);


  UserLockout updateByUpdater(Updater<UserLockout> updater);

  UserLockout deleteById(Long id);
}