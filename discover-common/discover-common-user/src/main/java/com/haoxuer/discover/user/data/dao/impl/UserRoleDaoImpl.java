package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.user.data.dao.UserRoleDao;
import com.haoxuer.discover.user.data.entity.UserRole;

/**
 * Created by imake on 2020年07月27日11:36:00.
 */
@Repository

public class UserRoleDaoImpl extends CriteriaDaoImpl<UserRole, Long> implements UserRoleDao {

    @Override
    public UserRole findById(Long id) {
        if (id == null) {
            return null;
        }
        return get(id);
    }

    @Override
    public UserRole save(UserRole bean) {

        getSession().save(bean);


        return bean;
    }

    @Override
    public UserRole deleteById(Long id) {
        UserRole entity = super.get(id);
        if (entity != null) {
            getSession().delete(entity);
        }
        return entity;
    }

    @Override
    public UserRole findByName(String name) {
        return one(Filter.eq("name", name));
    }

    @Override
    protected Class<UserRole> getEntityClass() {
        return UserRole.class;
    }

    @Autowired
    public void setSuperSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}