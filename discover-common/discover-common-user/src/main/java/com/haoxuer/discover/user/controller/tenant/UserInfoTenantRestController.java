package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.UserInfoApi;
import com.haoxuer.discover.user.api.domain.list.MenuList;
import com.haoxuer.discover.user.api.domain.list.UserInfoList;
import com.haoxuer.discover.user.api.domain.page.UserInfoPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserInfoResponse;
import com.haoxuer.discover.user.data.request.ResetPasswordRequest;
import com.haoxuer.discover.user.data.request.UpdatePasswordRequest;
import com.haoxuer.discover.user.shiro.realm.ShiroUser;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import com.haoxuer.discover.user.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/userinfo")
@RestController
public class UserInfoTenantRestController extends BaseTenantRestController {


    @RequiresPermissions("userinfo")
    @RequiresUser
    @RequestMapping("create")
    public UserInfoResponse create(UserInfoDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequiresUser
    @RequestMapping("updateCurrent")
    public UserInfoResponse updateCurrent(UserInfoDataRequest request) {
        init(request);
        ShiroUser user = UserUtil.getCurrentShiroUser();
        if (user != null) {
            request.setId(user.getId());
        }
        return api.update(request);
    }

    @RequiresPermissions("userinfo")
    @RequiresUser
    @RequestMapping("delete")
    public UserInfoResponse delete(UserInfoDataRequest request) {
        init(request);
        UserInfoResponse result = new UserInfoResponse();
        try {
            result = api.delete(request);
        } catch (Exception e) {
            result.setCode(501);
            result.setMsg("删除失败!");
        }
        return result;
    }

    @RequiresUser
    @RequestMapping("current")
    public UserInfoResponse current(UserInfoDataRequest request) {
        init(request);
        ShiroUser user = UserUtil.getCurrentShiroUser();
        if (user != null) {
            request.setId(user.getId());
        }
        return api.view(request);
    }

    @RequiresPermissions("userinfo")
    @RequiresUser
    @RequestMapping("list")
    public UserInfoList list(UserInfoSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequiresPermissions("userinfo")
    @RequiresUser
    @RequestMapping("search")
    public UserInfoPage search(UserInfoSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @RequiresUser
    @RequestMapping("changePassword")
    public UserInfoPage changePassword(UpdatePasswordRequest request) {
        ShiroUser user = UserUtil.getCurrentShiroUser();
        if (user != null) {
            request.setId(user.getId());
        }
        return api.changePassword(request);
    }

    @RequiresUser
    @RequestMapping("restPassword")
    public UserInfoPage restPassword(ResetPasswordRequest request) {
        return api.restPassword(request);
    }



    @RequestMapping("login")
    public UserInfoResponse login(UserInfoDataRequest request) {
        return api.login(request);
    }


    @RequiresUser
    @RequestMapping("menu")
    public MenuList menu() {
        return api.menu();
    }

    @Autowired
    private UserInfoApi api;

}
