package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class UserFeedBackPostRequest extends RequestUserTokenObject {

  private String title;


  private String note;

  /**
   * 反馈人名字
   */
  private String name;

  /**
   * 反馈人QQ
   */
  private String qq;

  /**
   * 反馈人电话
   */
  private String phone;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getQq() {
    return qq;
  }

  public void setQq(String qq) {
    this.qq = qq;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
}
