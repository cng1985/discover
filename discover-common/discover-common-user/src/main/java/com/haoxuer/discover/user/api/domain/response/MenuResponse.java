package com.haoxuer.discover.user.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by imake on 2021年01月11日23:03:48.
 */

@Data
public class MenuResponse extends ResponseObject {

    private Integer id;

    private String path;

    private Integer levelInfo;

    private Integer sortNum;

    private String ids;

    private Integer catalog;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date lastDate;

    private String icon;

    private String name;

    private String permission;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;

    private Long nums;
    private String router;


}