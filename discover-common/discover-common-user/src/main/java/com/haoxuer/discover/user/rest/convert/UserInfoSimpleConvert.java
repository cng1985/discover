package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.simple.UserInfoSimple;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.data.rest.core.Conver;
public class UserInfoSimpleConvert implements Conver<UserInfoSimple, UserInfo> {


    @Override
    public UserInfoSimple conver(UserInfo source) {
        UserInfoSimple result = new UserInfoSimple();

            result.setId(source.getId());
             result.setStoreState(source.getStoreState());
             result.setPhone(source.getPhone());
             result.setCatalog(source.getCatalog());
             result.setName(source.getName());
             result.setAvatar(source.getAvatar());
             result.setState(source.getState());
             result.setLoginSize(source.getLoginSize());
             result.setAddDate(source.getAddDate());

             result.setStateName(source.getState()+"");
             result.setStoreStateName(source.getStoreState()+"");
        return result;
    }
}
