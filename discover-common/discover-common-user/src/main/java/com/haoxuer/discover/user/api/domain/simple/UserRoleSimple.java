package com.haoxuer.discover.user.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.user.data.enums.RoleType;

/**
*
* Created by BigWorld on 2021年05月16日16:09:08.
*/
@Data
public class UserRoleSimple implements Serializable {

    private Long id;

     private String alias;
     private Integer catalog;
     private String catalogName;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String description;
     private RoleType type;

     private String typeName;

}
