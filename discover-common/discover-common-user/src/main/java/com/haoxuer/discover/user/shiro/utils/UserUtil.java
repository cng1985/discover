package com.haoxuer.discover.user.shiro.utils;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.shiro.realm.ShiroUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

public class UserUtil {
  /**
   * 获取当前用户对象shiro
   *
   * @return shirouser
   */
  public static ShiroUser getCurrentShiroUser() {
    ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
    return user;
  }

  /**
   * 获取当前用户session
   *
   * @return session
   */
  public static Session getSession() {
    Session session = SecurityUtils.getSubject().getSession();
    return session;
  }

  /**
   * 获取当前用户httpsession
   *
   * @return httpsession
   */
  public static Session getHttpSession() {
    Session session = SecurityUtils.getSubject().getSession();
    return session;
  }

  /**
   * 获取当前用户对象
   *
   * @return user
   */
  public static UserInfo getCurrentUser() {
    Session session = SecurityUtils.getSubject().getSession();
    if (null != session) {
      return (UserInfo) session.getAttribute("user");
    } else {
      return null;
    }

  }

  public static User getUser() {
    UserInfo user = getCurrentUser();
    if (user == null) {
      return null;
    }
    User result = new User();
    result.setId(user.getId());
    result.setName(user.getName());
    result.setAvatar(user.getAvatar());
    result.setPhone(user.getPhone());
    result.setLoginSize(user.getLoginSize());
    result.setLastDate(user.getLastDate());
    result.setAddDate(user.getAddDate());
    return result;

  }

  public static UserInfo setCurrentUser(UserInfo user) {
    Session session = SecurityUtils.getSubject().getSession();
    if (null != session) {
      session.setAttribute("user", user);
      return (UserInfo) session.getAttribute("user");
    } else {
      return null;
    }
  }

}
