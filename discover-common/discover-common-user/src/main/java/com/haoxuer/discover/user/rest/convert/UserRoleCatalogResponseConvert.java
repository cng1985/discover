package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.response.UserRoleCatalogResponse;
import com.haoxuer.discover.user.data.entity.UserRoleCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class UserRoleCatalogResponseConvert implements Conver<UserRoleCatalogResponse, UserRoleCatalog> {
    @Override
    public UserRoleCatalogResponse conver(UserRoleCatalog source) {
        UserRoleCatalogResponse result = new UserRoleCatalogResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getParent()!=null){
           result.setParent(source.getParent().getId());
        }
         if(source.getParent()!=null){
            result.setParentName(source.getParent().getName());
         }


        return result;
    }
}
