package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.user.data.entity.UserLabel;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.user.data.dao.UserLabelDao;

import java.util.Date;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2017年12月06日15:59:13.
 */
@Repository

public class UserLabelDaoImpl extends CriteriaDaoImpl<UserLabel, Long> implements UserLabelDao {

  @Override
  public UserLabel findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserLabel save(UserLabel bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserLabel deleteById(Long id) {
    UserLabel entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public UserLabel label(String name) {
    Finder finder = Finder.create();
    finder.append("from UserLabel u where u.name =:name");
    finder.setParam("name", name);
    UserLabel result = findOne(finder);
    if (result == null) {
      result = new UserLabel();
      result.setName(name);
      save(result);
    }
    result.setLastDate(new Date());
    return result;
  }

  @Override
  protected Class<UserLabel> getEntityClass() {
    return UserLabel.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}