package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.user.data.entity.UserOauthToken;
import com.haoxuer.discover.user.data.response.UserLoginAuthResponse;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2017年07月21日16:04:29.
 */
public interface UserOauthTokenService {
  
  UserOauthToken findById(Long id);

  UserOauthToken bind(Long id,Long user);


  UserOauthToken save(UserOauthToken bean);
  
  UserOauthToken update(UserOauthToken bean);
  
  UserOauthToken deleteById(Long id);
  
  UserOauthToken[] deleteByIds(Long[] ids);
  
  
  Page<UserOauthToken> page(Pageable pageable);
  
  Page<UserOauthToken> page(Pageable pageable, Object search);
  
  
  List<UserOauthToken> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
  UserOauthToken findByUid(String ticket);
  
  UserOauthToken login(OauthResponse response);

  UserLoginAuthResponse loginAuth(OauthResponse response);


}