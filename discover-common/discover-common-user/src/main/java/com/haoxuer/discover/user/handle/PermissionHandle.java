package com.haoxuer.discover.user.handle;


import com.haoxuer.discover.user.api.domain.request.BasePageRequest;

public interface PermissionHandle {
    void handle(BasePageRequest request);
}
