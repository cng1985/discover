package com.haoxuer.discover.user.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;

/**
*
* Created by BigWorld on 2021年01月11日23:03:48.
*/

@Data
public class MenuSimple implements Serializable {
    private Integer id;
    private String value;
    private String label;
    private String name;

    private String path;

    private String icon;

    private String permission;

    private Integer catalog;

    private Integer sortNum;
    private String ids;

    private Integer menuType;

    private Integer parent;

    private List<MenuSimple> children;

    private String parentPermission;
    private String  router;

}
