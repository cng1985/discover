package com.haoxuer.discover.user.api.domain.page;


import com.haoxuer.discover.user.api.domain.simple.UserInfoSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年04月28日11:36:16.
*/

@Data
public class UserInfoPage  extends ResponsePage<UserInfoSimple> {



}