package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.user.data.enums.SecurityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.user.data.dao.UserSecurityDao;
import com.haoxuer.discover.user.data.entity.UserSecurity;
import com.haoxuer.discover.user.data.service.UserSecurityService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
 * Created by imake on 2018年11月28日09:53:01.
 */


@Scope("prototype")
@Service
@Transactional
public class UserSecurityServiceImpl implements UserSecurityService {

  private UserSecurityDao dao;


  @Override
  @Transactional(readOnly = true)
  public UserSecurity findById(Long id) {
    return dao.findById(id);
  }


  @Override
  @Transactional
  public UserSecurity save(UserSecurity bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public UserSecurity update(UserSecurity bean) {
    Updater<UserSecurity> updater = new Updater<UserSecurity>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public UserSecurity deleteById(Long id) {
    UserSecurity bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public UserSecurity[] deleteByIds(Long[] ids) {
    UserSecurity[] beans = new UserSecurity[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(UserSecurityDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<UserSecurity> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<UserSecurity> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<UserSecurity> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }

  @Override
  public UserSecurity findByUser(Long user, SecurityType type) {
    return dao.findByUser(user, type);
  }
}