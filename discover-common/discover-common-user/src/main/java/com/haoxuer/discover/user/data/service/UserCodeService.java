package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.entity.UserCode;
import java.util.List;

/**
 * Created by imake on 2018年02月23日16:25:55.
 */
public interface UserCodeService {
  
  UserCode findById(Long id);
  
  UserCode save(UserCode bean);
  
  UserCode update(UserCode bean);
  
  UserCode deleteById(Long id);
  
  UserCode[] deleteByIds(Long[] ids);
  
  Page<UserCode> page(Pageable pageable);
  
  Page<UserCode> page(Pageable pageable, Object search);
  
  
  List<UserCode> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}