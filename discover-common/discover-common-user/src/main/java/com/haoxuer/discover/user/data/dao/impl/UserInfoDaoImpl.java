package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.data.dao.*;
import com.haoxuer.discover.user.data.entity.*;
import com.haoxuer.discover.user.data.enums.BindType;
import com.haoxuer.discover.user.data.enums.SecurityType;
import com.haoxuer.discover.user.data.request.ResetPasswordRequest;
import com.haoxuer.discover.user.data.request.UpdatePasswordRequest;
import com.haoxuer.discover.user.data.request.UserRegisterRequest;
import com.haoxuer.discover.user.data.response.UserBasicResponse;
import com.haoxuer.discover.user.rest.conver.UserResponseConver;
import com.haoxuer.discover.user.api.domain.request.UserLoginRequest;
import com.haoxuer.discover.user.api.domain.response.UserResponse;
import com.haoxuer.discover.user.utils.SecurityUtil;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

/**
 * Created by imake on 2017年07月20日16:35:48.
 */
@Repository

public class UserInfoDaoImpl extends CriteriaDaoImpl<UserInfo, Long> implements UserInfoDao {

    @Autowired
    UserRoleDao roleDao;

    @Autowired
    UserBindDao bindDao;

    @Autowired
    UserSecurityDao securityDao;

    @Autowired
    UserAccountDao accountDao;

    @Override
    public UserInfo findByAccount(String account) {
        UserInfo userInfo = null;
        UserBind bind = bindDao.findByName(account);
        if (bind != null) {
            userInfo = bind.getUser();
        }
        if (userInfo == null) {
            UserAccount userAccount = accountDao.findByName(account);
            if (userAccount != null) {
                userInfo = userAccount.getUser();
            }
        }
        return userInfo;
    }

    @Override
    public UserInfo findById(Long id) {
        if (id == null) {
            return null;
        }
        return get(id);
    }

    @Override
    public UserInfo save(UserInfo bean) {

        getSession().save(bean);


        return bean;
    }

    @Override
    public UserInfo deleteById(Long id) {
        UserInfo entity = super.get(id);
        if (entity != null) {
            getSession().delete(entity);
        }
        return entity;
    }

    @Override
    public UserInfo addRole(Long id, Long roleid) {
        UserInfo entity = findById(id);
        if (entity != null) {
            UserRole role = roleDao.findById(roleid);
            if (role != null) {
                entity.getRoles().add(role);
            }
        }
        return entity;
    }

    @Override
    public UserInfo addRoles(Long id, Long... role) {
        UserInfo entity = findById(id);
        if (entity != null) {
            for (Long item : role) {
                UserRole temp = roleDao.findById(item);
                if (role != null) {
                    entity.getRoles().add(temp);
                }
            }
        }
        return entity;
    }

    @Override
    public UserInfo addRole(Long id, String roleName) {
        UserInfo entity = findById(id);
        if (entity != null) {
            UserRole temp = roleDao.findByName(roleName);
            if (temp != null) {
                entity.getRoles().add(temp);
            }
        }
        return entity;
    }

    @Override
    public UserBasicResponse register(UserRegisterRequest bean) {
        UserBasicResponse result = new UserBasicResponse();
        if (StringUtils.isEmpty(bean.getNo())) {
            result.setCode(501);
            result.setMsg("账号不能为空");
            return result;
        }
        if (bean.getBindType() == null) {
            bean.setBindType(BindType.account);
        }
        ResponseObject back = bindDao.checkNo(bean.getNo(), bean.getBindType());
        if (back.getCode() != 0) {
            result.setCode(back.getCode());
            result.setMsg(back.getMsg());
            return result;
        }

        UserInfo user = new UserInfo();
        save(user);
        UserBind bind = new UserBind();
        bind.setBindType(bean.getBindType());
        bind.setNo(bean.getNo());
        bind.setUser(user);
        bindDao.save(bind);

        UserSecurity security = new UserSecurity();
        security.setCheckSize(0);
        security.setSecurityType(SecurityType.account);
        SecurityUtil securityUtil = new SecurityUtil();
        security.setSalt(securityUtil.getSalt());
        security.setPassword(securityUtil.entryptPassword(bean.getPassword()));
        security.setUser(user);
        securityDao.save(security);
        result.setId(user.getId());
        return result;
    }

    @Override
    public ResponseObject resetPassword(ResetPasswordRequest request) {
        ResponseObject result = new ResponseObject();
        UserInfo user = findById(request.getId());
        if (user == null) {
            result.setCode(501);
            result.setMsg("用户信息不存在");
            return result;
        }
        if (request.getSecurityType() == null) {
            request.setSecurityType(SecurityType.account);
        }
        UserSecurity security = securityDao.findByUser(user.getId(), request.getSecurityType());
        if (security == null) {
            security = new UserSecurity();
            security.setUser(user);
            security.setSecurityType(request.getSecurityType());
            security.setCheckSize(0);
            securityDao.save(security);
        }
        SecurityUtil securityUtil = new SecurityUtil();
        security.setSalt(securityUtil.getSalt());
        security.setPassword(securityUtil.entryptPassword(request.getPassword()));
        return result;
    }

    @Override
    public ResponseObject updatePassword(UpdatePasswordRequest request) {
        ResponseObject result = new ResponseObject();
        UserInfo user = findById(request.getId());
        if (user == null) {
            result.setCode(501);
            result.setMsg("用户信息不存在");
            return result;
        }
        if (request.getSecurityType()==null){
            request.setSecurityType(SecurityType.account);
        }
        UserSecurity security = securityDao.findByUser(user.getId(), request.getSecurityType());
        if (security == null) {
            result.setCode(502);
            result.setMsg("安全信息不存在");
            return result;
        }

        SecurityUtil securityUtil = new SecurityUtil(security.getSalt());
        if (!securityUtil.checkPassword(security.getPassword(), request.getOldPassword())) {
            result.setCode(503);
            result.setMsg("老密码不正确");
            return result;
        }
        security.setPassword(securityUtil.entryptPassword(request.getPassword()));
        return result;
    }

    @Override
    public UserResponse login(UserLoginRequest request) {
        UserResponse result = new UserResponse();
        UserBind bind = bindDao.findByUserName(request.getNo());
        if (bind == null) {
            bind = bindDao.findByPhone(request.getNo());
        }
        if (bind == null) {
            bind = bindDao.findByEmail(request.getNo());
        }
        if (bind == null) {
            result.setCode(501);
            result.setMsg("该账号不存在");
            return result;
        }
        if (bind.getUser() == null) {
            result.setCode(502);
            result.setMsg("用户信息不存在");
            return result;
        }
        UserSecurity security = securityDao.findByUser(bind.getUser().getId(), SecurityType.account);
        if (security == null) {
            result.setCode(503);
            result.setMsg("安全信息不存在");
            return result;
        }
        SecurityUtil securityUtil = new SecurityUtil(security.getSalt());
        if (!securityUtil.checkPassword(security.getPassword(), request.getPassword())) {
            result.setCode(504);
            result.setMsg("密码错误");
            return result;
        }
        result = new UserResponseConver().conver(bind.getUser());
        return result;
    }

    @Override
    protected Class<UserInfo> getEntityClass() {
        return UserInfo.class;
    }

    @Autowired
    public void setSuperSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}