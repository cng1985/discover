package com.haoxuer.discover.user.data.so;

import com.haoxuer.discover.data.page.Condition;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import java.io.Serializable;

/**
 * Created by imake on 2017年07月20日16:35:49.
 */
public class UserInfoSo implements Serializable {
  
  @Search(name = "name", operator = Filter.Operator.like)
  private String name;


  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }

}
