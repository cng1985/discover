package com.haoxuer.discover.user.rest.resource;

import com.haoxuer.discover.user.api.apis.UserBindApi;
import com.haoxuer.discover.user.api.domain.list.UserBindList;
import com.haoxuer.discover.user.api.domain.page.UserBindPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserBindResponse;
import com.haoxuer.discover.user.data.dao.UserBindDao;
import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.user.rest.convert.UserBindResponseConvert;
import com.haoxuer.discover.user.rest.convert.UserBindSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class UserBindResource implements UserBindApi {

    @Autowired
    private UserBindDao dataDao;

    @Autowired
    private UserInfoDao userDao;

    @Override
    public UserBindResponse create(UserBindDataRequest request) {
        UserBindResponse result = new UserBindResponse();
        if (StringUtils.isEmpty(request.getNo())) {
            result.setCode(501);
            result.setMsg("账号不能为空");
            return result;
        }
        if (request.getUser() == null) {
            result.setCode(502);
            result.setMsg("用户不能为空");
            return result;
        }
        if (request.getBindType() == null) {
            result.setCode(503);
            result.setMsg("账号类型");
            return result;
        }
        UserBind temp = dataDao.one(Filter.eq("no", request.getNo()),
                Filter.eq("bindType", request.getBindType()));
        if (temp != null) {
            result.setCode(503);
            result.setMsg("该账号已经使用");
            return result;
        }
        UserBind bean = new UserBind();
        handleData(request, bean);
        dataDao.save(bean);
        result = new UserBindResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserBindResponse update(UserBindDataRequest request) {
        UserBindResponse result = new UserBindResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        UserBind bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new UserBindResponseConvert().conver(bean);
        return result;
    }

    private void handleData(UserBindDataRequest request, UserBind bean) {
        BeanDataUtils.copyProperties(request, bean);
        if (request.getUser() != null) {
            bean.setUser(userDao.findById(request.getUser()));
        }

    }

    @Override
    public UserBindResponse delete(UserBindDataRequest request) {
        UserBindResponse result = new UserBindResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public UserBindResponse view(UserBindDataRequest request) {
        UserBindResponse result = new UserBindResponse();
        UserBind bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new UserBindResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserBindList list(UserBindSearchRequest request) {
        UserBindList result = new UserBindList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<UserBind> organizations = dataDao.list(0, request.getSize(), filters, orders);
        UserBindSimpleConvert convert = new UserBindSimpleConvert();
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public UserBindPage search(UserBindSearchRequest request) {
        UserBindPage result = new UserBindPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.desc("" + request.getSortField()));
        } else {
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<UserBind> page = dataDao.page(pageable);
        UserBindSimpleConvert convert = new UserBindSimpleConvert();
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }
}
