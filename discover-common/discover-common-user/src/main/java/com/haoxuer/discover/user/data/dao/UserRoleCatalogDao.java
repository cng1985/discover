package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.UserRoleCatalog;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年08月09日13:36:13.
 */
public interface UserRoleCatalogDao extends BaseDao<UserRoleCatalog, Integer> {
  
  UserRoleCatalog findById(Integer id);
  
  UserRoleCatalog save(UserRoleCatalog bean);
  
  UserRoleCatalog updateByUpdater(Updater<UserRoleCatalog> updater);
  
  UserRoleCatalog deleteById(Integer id);
}