package com.haoxuer.discover.user.api.domain.page;


import com.haoxuer.discover.user.api.domain.simple.UserAccessLogSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年12月04日16:19:12.
*/

@Data
public class UserAccessLogPage  extends ResponsePage<UserAccessLogSimple> {

}