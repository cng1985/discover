package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.user.data.entity.UserRole;

/**
 * Created by imake on 2020年07月27日11:36:00.
 */
public interface UserRoleDao extends BaseDao<UserRole, Long> {

    UserRole findById(Long id);

    UserRole save(UserRole bean);

    UserRole updateByUpdater(Updater<UserRole> updater);

    UserRole deleteById(Long id);

    UserRole findByName(String name);

}