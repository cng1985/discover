package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年04月28日11:36:16.
*/

@Data
public class UserInfoSearchRequest extends BasePageRequest {




    private String sortField;


    private String sortMethod;
}