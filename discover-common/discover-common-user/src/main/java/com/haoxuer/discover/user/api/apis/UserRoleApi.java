package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.UserRoleList;
import com.haoxuer.discover.user.api.domain.page.UserRolePage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserRoleResponse;

public interface UserRoleApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    UserRoleResponse create(UserRoleDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    UserRoleResponse update(UserRoleDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    UserRoleResponse delete(UserRoleDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     UserRoleResponse view(UserRoleDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    UserRoleList list(UserRoleSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    UserRolePage search(UserRoleSearchRequest request);

}