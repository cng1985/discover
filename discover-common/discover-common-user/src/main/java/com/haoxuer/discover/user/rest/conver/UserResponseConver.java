package com.haoxuer.discover.user.rest.conver;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.api.domain.response.UserResponse;

public class UserResponseConver implements Conver<UserResponse, UserInfo> {
  @Override
  public UserResponse conver(UserInfo source) {
    UserResponse result = new UserResponse();
    result.setAvatar(source.getAvatar());
    result.setName(source.getName());
    result.setPhone(source.getPhone());
    return result;
  }
}
