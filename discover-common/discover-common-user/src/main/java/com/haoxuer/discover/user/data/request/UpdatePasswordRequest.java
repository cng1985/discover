package com.haoxuer.discover.user.data.request;

import com.haoxuer.discover.user.data.enums.SecurityType;

public class UpdatePasswordRequest {

  private Long id;

  private SecurityType securityType;

  private String oldPassword;

  private String password;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public SecurityType getSecurityType() {
    return securityType;
  }

  public void setSecurityType(SecurityType securityType) {
    this.securityType = securityType;
  }

  public String getOldPassword() {
    return oldPassword;
  }

  public void setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
