package com.haoxuer.discover.user.api.domain.request;

import lombok.Data;

/**
 * Created by ada on 2017/7/4.
 */

@Data
public class UserChangePasswordRequest extends UserTokenRequest {

    private String oldPassword;

    private String password;

    private Long id;


}
