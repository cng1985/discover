package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.entity.UserOauthConfig;
import java.util.List;

/**
 * Created by imake on 2017年07月20日17:47:47.
 */
public interface UserOauthConfigService {
  
  UserOauthConfig findById(Long id);
  
  UserOauthConfig save(UserOauthConfig bean);
  
  UserOauthConfig update(UserOauthConfig bean);
  
  UserOauthConfig deleteById(Long id);
  
  UserOauthConfig[] deleteByIds(Long[] ids);
  
  
  Page<UserOauthConfig> page(Pageable pageable);
  
  Page<UserOauthConfig> page(Pageable pageable, Object search);
  
  
  List<UserOauthConfig> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}