package com.haoxuer.discover.user.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.user.data.entity.Structure;

/**
* Created by imake on 2021年05月21日22:44:58.
*/
public interface StructureDao extends BaseDao<Structure,Integer>{

	 Structure findById(Integer id);

	 Structure save(Structure bean);

	 Structure updateByUpdater(Updater<Structure> updater);

	 Structure deleteById(Integer id);
}