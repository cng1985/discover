package com.haoxuer.discover.user.data.response;

import com.haoxuer.discover.rest.base.ResponseObject;

public class UserBasicResponse extends ResponseObject {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
