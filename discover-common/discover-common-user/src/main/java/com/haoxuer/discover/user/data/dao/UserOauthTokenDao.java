package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.user.data.entity.UserOauthToken;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;

import java.util.List;

/**
 * Created by imake on 2017年07月21日16:04:29.
 */
public interface UserOauthTokenDao extends BaseDao<UserOauthToken, Long> {

  UserOauthToken findById(Long id);

  /**
   * 根据用户id和第三方登陆类型查找第三方登陆信息
   *
   * @param user 用户id
   * @param type 第三方登陆类型
   * @return
   */
  UserOauthToken findByUser(Long user, String type);

  /**
   * 获取这个人所有的第三方登陆信息
   *
   * @param user
   * @return
   */

  List<UserOauthToken> tokens(Long user);


  /**
   * 根据openid和第三方登陆类型查找第三方登陆信息
   *
   * @param openid openid
   * @param type   第三方登陆类型
   * @return
   */
  UserOauthToken findByOpenId(String openid, String type);

  UserOauthToken save(UserOauthToken bean);

  UserOauthToken updateByUpdater(Updater<UserOauthToken> updater);

  UserOauthToken deleteById(Long id);

  UserOauthToken loginAuth(OauthResponse response);
}