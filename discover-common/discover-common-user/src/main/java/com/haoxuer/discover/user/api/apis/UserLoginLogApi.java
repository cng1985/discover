package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.UserLoginLogList;
import com.haoxuer.discover.user.api.domain.page.UserLoginLogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserLoginLogResponse;

public interface UserLoginLogApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    UserLoginLogResponse create(UserLoginLogDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    UserLoginLogResponse update(UserLoginLogDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    UserLoginLogResponse delete(UserLoginLogDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     UserLoginLogResponse view(UserLoginLogDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    UserLoginLogList list(UserLoginLogSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    UserLoginLogPage search(UserLoginLogSearchRequest request);

}