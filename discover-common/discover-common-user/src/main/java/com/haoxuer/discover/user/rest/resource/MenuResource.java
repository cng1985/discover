package com.haoxuer.discover.user.rest.resource;

import com.haoxuer.discover.user.api.apis.MenuApi;
import com.haoxuer.discover.user.api.domain.list.MenuList;
import com.haoxuer.discover.user.api.domain.page.MenuPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.MenuResponse;
import com.haoxuer.discover.user.data.dao.MenuDao;
import com.haoxuer.discover.user.data.entity.Menu;
import com.haoxuer.discover.user.rest.conver.PageableConverter;
import com.haoxuer.discover.user.rest.convert.MenuBasicSimpleConvert;
import com.haoxuer.discover.user.rest.convert.MenuResponseConvert;
import com.haoxuer.discover.user.rest.convert.MenuSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class MenuResource implements MenuApi {

    @Autowired
    private MenuDao dataDao;


    @Override
    public MenuResponse create(MenuDataRequest request) {
        MenuResponse result = new MenuResponse();

        Menu bean = new Menu();
        handleData(request, bean);
        dataDao.save(bean);
        result = new MenuResponseConvert().apply(bean);
        return result;
    }

    @Override
    public MenuResponse update(MenuDataRequest request) {
        MenuResponse result = new MenuResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Menu bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }

        handleData(request, bean);
        result = new MenuResponseConvert().apply(bean);
        return result;
    }

    private void handleData(MenuDataRequest request, Menu bean) {
        BeanDataUtils.copyProperties(request, bean);
        if (request.getParent() != null) {
            bean.setParent(dataDao.findById(request.getParent()));
        }
    }

    @Override
    public MenuResponse delete(MenuDataRequest request) {
        MenuResponse result = new MenuResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public MenuResponse view(MenuDataRequest request) {
        MenuResponse result = new MenuResponse();
        Menu bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new MenuResponseConvert().apply(bean);
        return result;
    }

    @Override
    public MenuList list(MenuSearchRequest request) {
        MenuList result = new MenuList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<Menu> organizations = dataDao.list(0, request.getSize(), filters, orders);
        MenuSimpleConvert convert = new MenuSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public MenuList menus(MenuSearchRequest request) {
        request.setCatalog(0);
        MenuList result = new MenuList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<Menu> organizations = dataDao.list(0, request.getSize(), filters, orders);
        MenuBasicSimpleConvert convert = new MenuBasicSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public MenuPage search(MenuSearchRequest request) {
        MenuPage result = new MenuPage();
        Pageable pageable = new PageableConverter().convert(request);
        Page<Menu> page = dataDao.page(pageable);
        MenuSimpleConvert convert = new MenuSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }
}
