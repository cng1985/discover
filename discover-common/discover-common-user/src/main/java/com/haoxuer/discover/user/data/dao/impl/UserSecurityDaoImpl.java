package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.enums.SecurityType;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.user.data.dao.UserSecurityDao;
import com.haoxuer.discover.user.data.entity.UserSecurity;

/**
 * Created by imake on 2018年11月28日09:03:55.
 */
@Repository

public class UserSecurityDaoImpl extends CriteriaDaoImpl<UserSecurity, Long> implements UserSecurityDao {

  @Override
  public UserSecurity findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserSecurity save(UserSecurity bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserSecurity deleteById(Long id) {
    UserSecurity entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public UserSecurity findByUser(Long user, SecurityType type) {
    Finder finder = Finder.create();
    finder.append(" from  UserSecurity u where u.user.id=:user ");
    finder.append(" and u.securityType=:securityType");
    finder.setParam("user", user);
    finder.setParam("securityType", type);
    return findOne(finder);
  }

  @Override
  protected Class<UserSecurity> getEntityClass() {
    return UserSecurity.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}