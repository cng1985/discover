package com.haoxuer.discover.user.controller.rest;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.api.apis.UserFeedBackApi;
import com.haoxuer.discover.user.api.domain.request.UserFeedBackPostRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "rest")
public class UserFeedBackRestController {

  @RequestMapping(value = "/feedback/post")
  public ResponseObject post(UserFeedBackPostRequest request) {
    return api.post(request);
  }

  @Autowired
  private UserFeedBackApi api;

}
