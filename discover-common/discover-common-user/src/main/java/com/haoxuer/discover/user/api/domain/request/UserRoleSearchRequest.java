package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月16日16:09:08.
*/

@Data
public class UserRoleSearchRequest extends BasePageRequest {

    //角色名
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;




    private String sortField;


    private String sortMethod;
}