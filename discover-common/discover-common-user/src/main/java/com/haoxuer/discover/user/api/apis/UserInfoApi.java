package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.MenuList;
import com.haoxuer.discover.user.api.domain.list.UserInfoList;
import com.haoxuer.discover.user.api.domain.page.UserInfoPage;
import com.haoxuer.discover.user.api.domain.request.UserInfoDataRequest;
import com.haoxuer.discover.user.api.domain.request.UserInfoSearchRequest;
import com.haoxuer.discover.user.api.domain.response.UserInfoResponse;
import com.haoxuer.discover.user.data.request.ResetPasswordRequest;
import com.haoxuer.discover.user.data.request.UpdatePasswordRequest;

public interface UserInfoApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    UserInfoResponse create(UserInfoDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    UserInfoResponse update(UserInfoDataRequest request);

    /**
     * 删除
     *
     * @param request
     * @return
     */
    UserInfoResponse delete(UserInfoDataRequest request);


    /**
     * 详情
     *
     * @param request
     * @return
     */
    UserInfoResponse view(UserInfoDataRequest request);


    /**
     * 集合功能
     *
     * @param request
     * @return
     */
    UserInfoList list(UserInfoSearchRequest request);


    /**
     * 客户端登录
     *
     * @param request
     * @return
     */
    UserInfoResponse login(UserInfoDataRequest request);


    /**
     * 获取当前用户的菜单
     * @return
     */
    MenuList menu();

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    UserInfoPage search(UserInfoSearchRequest request);


    /**
     * 修改密码
     *
     * @param request
     * @return
     */
    UserInfoPage changePassword(UpdatePasswordRequest request);

    /**
     * 重置密码
     *
     * @param request
     * @return
     */
    UserInfoPage restPassword(ResetPasswordRequest request);

}