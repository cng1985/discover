package com.haoxuer.discover.user.api.domain.page;


import com.haoxuer.discover.user.api.domain.simple.UserLoginLogSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月22日10:49:09.
*/

@Data
public class UserLoginLogPage  extends ResponsePage<UserLoginLogSimple> {

}