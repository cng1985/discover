package com.haoxuer.discover.user.data.so;

import java.io.Serializable;

/**
* Created by imake on 2018年11月28日09:53:01.
*/
public class UserSecuritySo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
