package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.user.data.dao.UserLockoutDao;
import com.haoxuer.discover.user.data.entity.UserLockout;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by imake on 2019年03月19日10:24:16.
 */
@Repository

public class UserLockoutDaoImpl extends CriteriaDaoImpl<UserLockout, Long> implements UserLockoutDao {

  @Override
  public UserLockout findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserLockout save(UserLockout bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public Boolean check(Long user) {
    UserLockout lockout = one(Filter.eq("user.id", user), Filter.ge("endDate", new Date()));
    if (lockout != null) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public UserLockout deleteById(Long id) {
    UserLockout entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<UserLockout> getEntityClass() {
    return UserLockout.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}