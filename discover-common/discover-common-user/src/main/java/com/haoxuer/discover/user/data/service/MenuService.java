package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.entity.Menu;
import org.springframework.cache.annotation.CacheEvict;

import java.util.List;

/**
 * Created by imake on 2017年07月21日14:37:30.
 */
public interface MenuService {
  
  Menu findById(Integer id);

  @CacheEvict(value = "menuCache", allEntries = true)
  Menu save(Menu bean);

  @CacheEvict(value = "menuCache", allEntries = true)
  Menu update(Menu bean);

  @CacheEvict(value = "menuCache", allEntries = true)
  Menu deleteById(Integer id);

  @CacheEvict(value = "menuCache", allEntries = true)
  Menu[] deleteByIds(Integer[] ids);
  
  
  Page<Menu> page(Pageable pageable);
  
  Page<Menu> page(Pageable pageable, Object search);
  
  
  List<Menu> findByTops(Integer pid);
  
  List<Menu> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
  List<Menu> childs(Integer id);
  
  List<Menu> findChildMenu(Integer id);
  
  List<Menu> findChild(Integer id);
}