package com.haoxuer.discover.user.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年05月21日22:44:58.
*/

@Data
public class StructureResponse extends ResponseObject {

    private Integer id;

     private Integer parent;

     private Integer levelInfo;

     private String parentName;

     private Integer sortNum;

     private String ids;

     private Integer lft;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;

     private String name;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private Integer rgt;


}