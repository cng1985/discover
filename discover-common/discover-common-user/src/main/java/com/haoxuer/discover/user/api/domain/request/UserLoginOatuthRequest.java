package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.rest.base.RequestTokenObject;

/**
 * 用户通过第三方登陆对象
 * Created by ada on 2017/6/29.
 */
public class UserLoginOatuthRequest extends RequestTokenObject {
    /**
     * 第三方登陆类型
     */
    String type;

    String code;

    /**
     * 用户策略
     */
    String strategy;
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getCode() {
        return code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }
}
