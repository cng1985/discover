package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.user.data.entity.UserVerification;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.user.data.dao.UserVerificationDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2017年07月20日16:37:49.
 */
@Repository

public class UserVerificationDaoImpl extends CriteriaDaoImpl<UserVerification, Long> implements UserVerificationDao {

  @Override
  public UserVerification findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserVerification save(UserVerification bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserVerification deleteById(Long id) {
    UserVerification entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<UserVerification> getEntityClass() {
    return UserVerification.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public UserVerification findByName(String phone, Integer catalog) {
    Finder finder = Finder.create();
    finder.append("from UserVerification u where u.name =:name ");
    finder.setParam("name", phone);
    finder.append(" and u.catalog =:catalog ");
    finder.setParam("catalog", catalog);
    return findOne(finder);
  }


}