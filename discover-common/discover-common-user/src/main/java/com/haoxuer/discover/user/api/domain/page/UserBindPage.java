package com.haoxuer.discover.user.api.domain.page;


import com.haoxuer.discover.user.api.domain.simple.UserBindSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月23日09:43:09.
*/

@Data
public class UserBindPage  extends ResponsePage<UserBindSimple> {

}