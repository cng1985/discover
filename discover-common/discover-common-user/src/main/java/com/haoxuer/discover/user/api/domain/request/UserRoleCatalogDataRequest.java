package com.haoxuer.discover.user.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月16日16:35:38.
*/

@Data
public class UserRoleCatalogDataRequest extends BaseRequest {

    private Integer id;

     private Integer parent;

     private String code;

     private Integer levelInfo;

     private Integer sortNum;

     private String ids;

     private Integer lft;

     private Date lastDate;

     private String name;

     private Date addDate;

     private Integer rgt;


}