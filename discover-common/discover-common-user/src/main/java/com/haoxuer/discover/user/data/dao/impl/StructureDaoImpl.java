package com.haoxuer.discover.user.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.user.data.dao.StructureDao;
import com.haoxuer.discover.user.data.entity.Structure;
import com.haoxuer.discover.data.core.CatalogDaoImpl;

/**
* Created by imake on 2021年05月21日22:44:58.
*/
@Repository

public class StructureDaoImpl extends CatalogDaoImpl<Structure, Integer> implements StructureDao {

	@Override
	public Structure findById(Integer id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Structure save(Structure bean) {

		add(bean);
		
		
		return bean;
	}

    @Override
	public Structure deleteById(Integer id) {
		Structure entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Structure> getEntityClass() {
		return Structure.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}