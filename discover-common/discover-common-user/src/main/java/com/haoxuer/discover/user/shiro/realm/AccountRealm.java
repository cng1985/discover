package com.haoxuer.discover.user.shiro.realm;

import com.haoxuer.discover.user.data.entity.UserAccount;
import com.haoxuer.discover.user.data.entity.UserLockout;
import com.haoxuer.discover.user.data.enums.AccountType;
import com.haoxuer.discover.user.data.service.UserAccountService;
import com.haoxuer.discover.user.data.service.UserInfoService;
import com.haoxuer.discover.user.data.service.UserLockoutService;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import com.haoxuer.discover.user.utils.Encodes;
import com.haoxuer.discover.user.utils.SecurityUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * 用户登录授权service(shrioRealm)
 *
 * @author ty
 */
@Service
public class AccountRealm extends AuthorizingRealm {

  Logger logger = LoggerFactory.getLogger("ada");
  @Autowired
  private UserAccountService accountService;
  @Autowired
  private UserInfoService userService;

  @Autowired
  private UserLockoutService lockoutService;

  private UserAccount userAccount;

  public void clearAllCache() {
    clearAllCachedAuthenticationInfo();
    clearAllCachedAuthorizationInfo();
  }

  public void clearAllCachedAuthenticationInfo() {
    getAuthenticationCache().clear();
  }

  public void clearAllCachedAuthorizationInfo() {
    getAuthorizationCache().clear();
  }

  @Override
  public void clearCache(PrincipalCollection principals) {
    super.clearCache(principals);
  }

  @Override
  public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
    super.clearCachedAuthenticationInfo(principals);
  }

  @Override
  public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
    super.clearCachedAuthorizationInfo(principals);
  }

  /**
   * 认证回调函数,登录时调用.
   */
  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException{
    UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
    UserAccount account = accountService.findByUserName(token.getUsername(), AccountType.Account);
    if (account == null) {
      account = accountService.findByUserName(token.getUsername(), AccountType.Phone);
    }
    if (account == null) {
      account = accountService.findByUserName(token.getUsername(), AccountType.Email);
    }
    if (account==null){
      throw new UnknownAccountException();
    }
    if (account.getUser()==null){
      throw new UnknownAccountException();
    }
    if (lockoutService.check(account.getUser().getId())){
      throw  new LockedAccountException();
    }

    userAccount = account;
    logger.info("account:" + token.getUsername());
    byte[] salt = Encodes.decodeHex(account.getSalt());
    ShiroUser shiroUser = new ShiroUser(account.getUser().getId(), account.getUsername(), account.getUser().getName());
    // 设置用户session
    UserUtil.setCurrentUser(account.getUser());
    try {
      SimpleAuthenticationInfo aa = new SimpleAuthenticationInfo(shiroUser, account.getPassword(),
          ByteSource.Util.bytes(salt), getName());
      return aa;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
   */
  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    UserAuthorization authorization = new UserAuthorization(userService);
    if (userAccount != null) {
      accountService.updateUserLogin(userAccount);
    }
    return authorization.doGetAuthorizationInfo(principals);
  }

  @Autowired
  CacheManager cacheManager;

  /**
   * 设定Password校验的Hash算法与迭代次数.
   */
  @SuppressWarnings("static-access")
  @PostConstruct
  public void initCredentialsMatcher() {
    RetryLimitHashedCredentialsMatcher matcher = new RetryLimitHashedCredentialsMatcher(cacheManager);
    matcher.setHashAlgorithmName(SecurityUtil.HASH_ALGORITHM);
    matcher.setHashIterations(SecurityUtil.HASH_INTERATIONS);
    setCredentialsMatcher(matcher);
  }

}
