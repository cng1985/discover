package com.haoxuer.discover.user.data.so;

import java.io.Serializable;

/**
* Created by imake on 2019年03月19日10:24:16.
*/
public class UserLockoutSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
