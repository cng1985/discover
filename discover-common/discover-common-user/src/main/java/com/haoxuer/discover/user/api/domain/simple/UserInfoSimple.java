package com.haoxuer.discover.user.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.data.enums.State;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by BigWorld on 2021年04月28日11:36:16.
*/
@Data
public class UserInfoSimple implements Serializable {

    private Long id;

     private StoreState storeState;
     private String phone;
     private Integer catalog;
     private String name;
     private String avatar;
     private State state;
     private Integer loginSize;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private String stateName;
     private String storeStateName;

}
