package com.haoxuer.discover.user.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年12月04日16:19:12.
*/

@Scope("prototype")
@Controller
public class UserAccessLogAction extends BaseAction{


	@RequiresPermissions("useraccesslog")
	@RequestMapping("/admin/useraccesslog/view_list")
	public String list() {
		return getView("useraccesslog/list");
	}

}