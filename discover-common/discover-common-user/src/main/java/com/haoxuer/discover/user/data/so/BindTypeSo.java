package com.haoxuer.discover.user.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;

import java.io.Serializable;

/**
* Created by imake on 2018年10月31日10:31:32.
*/
public class BindTypeSo implements Serializable {

    @Search(name = "user.name", operator = Filter.Operator.like)
    private String name;

    @Search(name = "no", operator = Filter.Operator.like)
    private String no;


    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
