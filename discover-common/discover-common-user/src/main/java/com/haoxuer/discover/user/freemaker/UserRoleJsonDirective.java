package com.haoxuer.discover.user.freemaker;

import com.google.gson.Gson;
import com.haoxuer.discover.common.freemarker.TextDirective;
import com.haoxuer.discover.user.api.apis.UserRoleApi;
import com.haoxuer.discover.user.api.domain.list.UserRoleList;
import com.haoxuer.discover.user.api.domain.request.UserRoleSearchRequest;
import freemarker.core.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component("userRoleJsonDirective")
public class UserRoleJsonDirective extends TextDirective {


    @Autowired
    private UserRoleApi api;


    @Override
    public void handle(Environment env) throws IOException {
        UserRoleSearchRequest request = new UserRoleSearchRequest();
        UserRoleList list = api.list(request);
        if (list.getList()!=null){
            env.getOut().append(new Gson().toJson(list.getList()));
        }else{
            env.getOut().append("[]");
        }
    }
}
