package com.haoxuer.discover.user.controller.rest;

import com.haoxuer.discover.user.api.apis.MenuApi;
import com.haoxuer.discover.user.api.domain.list.MenuList;
import com.haoxuer.discover.user.api.domain.page.MenuPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.MenuResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/menu")
@RestController
public class MenuRestController extends BaseRestController {


    @RequestMapping("create")
    public MenuResponse create(MenuDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public MenuResponse update(MenuDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public MenuResponse delete(MenuDataRequest request) {
        init(request);
        return api.delete(request);
    }

    @RequestMapping("view")
    public MenuResponse view(MenuDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public MenuList list(MenuSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public MenuPage search(MenuSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private MenuApi api;

}
