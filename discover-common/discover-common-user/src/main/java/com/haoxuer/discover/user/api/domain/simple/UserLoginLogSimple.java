package com.haoxuer.discover.user.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.user.data.enums.LoginState;

/**
*
* Created by BigWorld on 2021年05月22日10:49:09.
*/
@Data
public class UserLoginLogSimple implements Serializable {

    private Long id;

     private String note;
     private String password;
     private String ip;
     private String client;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;
     private LoginState state;
     private Long user;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String userName;
     private String account;

     private String stateName;

}
