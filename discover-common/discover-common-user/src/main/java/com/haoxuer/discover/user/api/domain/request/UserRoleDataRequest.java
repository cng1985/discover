package com.haoxuer.discover.user.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.user.data.enums.RoleType;

import java.util.Date;
import java.util.List;

/**
 * Created by imake on 2021年05月16日16:09:08.
 */

@Data
public class UserRoleDataRequest extends BaseRequest {

    private Long id;

    private String alias;

    private Integer catalog;

    private Date lastDate;

    private String name;

    private Date addDate;

    private String description;

    private RoleType type;

    private List<String> authorities;

}