package com.haoxuer.discover.user.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年05月16日16:09:08.
*/

@Scope("prototype")
@Controller
public class UserRoleAction extends BaseAction{


	@RequiresPermissions("userrole")
	@RequestMapping("/admin/userrole/view_list")
	public String list() {
		return getView("userrole/list");
	}

}