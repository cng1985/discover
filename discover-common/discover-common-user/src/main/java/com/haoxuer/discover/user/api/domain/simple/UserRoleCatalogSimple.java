package com.haoxuer.discover.user.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;

/**
*
* Created by BigWorld on 2021年05月16日16:35:38.
*/

@Data
public class UserRoleCatalogSimple implements Serializable {
    private Integer id;
    private String value;
    private String label;
    private List<UserRoleCatalogSimple> children;

     private Integer parent;
     private String code;
     private Integer levelInfo;
     private String parentName;
     private Integer sortNum;
     private String ids;
     private Integer lft;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;
     private String name;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private Integer rgt;

}
