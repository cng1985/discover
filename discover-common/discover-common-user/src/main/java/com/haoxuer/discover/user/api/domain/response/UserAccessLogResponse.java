package com.haoxuer.discover.user.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年12月04日16:19:12.
*/

@Data
public class UserAccessLogResponse extends ResponseObject {

    private Long id;

     private Long consumeTime;

     private Long creator;

     private String ip;

     private String creatorName;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private String url;


}