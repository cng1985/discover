package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class UserBindPhoneRequest extends RequestUserTokenObject {

  private String phone;

  private String code;

  private String type;

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
