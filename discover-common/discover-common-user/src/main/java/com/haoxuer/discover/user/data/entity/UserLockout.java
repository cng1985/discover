package com.haoxuer.discover.user.data.entity;

import com.haoxuer.discover.data.entity.LongEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;


/**
 * 账号锁定
 */
@Entity
@Table(name = "user_lockout")
public class UserLockout extends LongEntity {

  @ManyToOne(fetch = FetchType.LAZY)
  private UserInfo user;

  private Date beginDate;

  private Date endDate;

  /**
   * 锁定原因
   */
  private String note;

  public UserInfo getUser() {
    return user;
  }

  public void setUser(UserInfo user) {
    this.user = user;
  }

  public Date getBeginDate() {
    return beginDate;
  }

  public void setBeginDate(Date beginDate) {
    this.beginDate = beginDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
