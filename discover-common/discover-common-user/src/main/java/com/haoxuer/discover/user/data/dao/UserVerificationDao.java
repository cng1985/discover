package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.UserVerification;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年07月20日16:37:49.
 */
public interface UserVerificationDao extends BaseDao<UserVerification, Long> {
  
  UserVerification findById(Long id);
  
  UserVerification save(UserVerification bean);
  
  UserVerification updateByUpdater(Updater<UserVerification> updater);
  
  UserVerification deleteById(Long id);
  
  UserVerification findByName(String phone, Integer catalog);
}