package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import com.haoxuer.discover.rest.base.RequestUserTokenPageObject;
import lombok.Data;
import lombok.ToString;


@ToString(callSuper = true)
@Data
public class BasePageRequest extends RequestUserTokenPageObject {

    private Long user;
    private Long createUser;
    @Search(name = "creator.id",operator = Filter.Operator.eq)
    private Long creator;

    @Search(name = "creator.structure.id",operator = Filter.Operator.eq)
    private Integer structure;

    @Search(name = "creator.structure.lft",operator = Filter.Operator.ge)
    private Integer lft;

    @Search(name = "creator.structure.rgt",operator = Filter.Operator.le)
    private Integer rgt;
}
