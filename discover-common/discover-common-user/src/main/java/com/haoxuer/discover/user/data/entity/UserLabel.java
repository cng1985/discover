package com.haoxuer.discover.user.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 用户标签
 */
@Entity
@Table(name = "user_label")
public class UserLabel extends AbstractEntity {
  
  @Column(unique = true)
  private String name;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
}
