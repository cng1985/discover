package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.request.ResetPasswordRequest;
import com.haoxuer.discover.user.data.request.UpdatePasswordRequest;
import com.haoxuer.discover.user.data.request.UserRegisterRequest;
import com.haoxuer.discover.user.data.response.UserBasicResponse;

import java.util.Collection;
import java.util.List;

/**
 * 用户服务
 */
public interface UserInfoService {
  
  UserInfo findById(Long id);
  
  UserInfo save(UserInfo bean);
  
  
  UserInfo addLabel(Long user, String... lables);
  
  
  UserInfo update(UserInfo bean);
  
  UserInfo deleteById(Long id);
  
  UserInfo[] deleteByIds(Long[] ids);
  
  
  Page<UserInfo> page(Pageable pageable);
  
  Page<UserInfo> page(Pageable pageable, Object search);
  
  
  List<UserInfo> list(int first, Integer size, List<Filter> filters, List<Order> orders);

  UserInfo findByAccount(String account);

  Collection<? extends String> findAuthorities(Long id);
  
  void updateUserLogin(UserInfo user);
  
  UserInfo addRole(Long id, Long roleid);
  
  /**
   * 给用户添加角色
   *
   * @param id
   * @param roles
   * @return
   */
  UserInfo addRoles(Long id, Long... roles);
  
  /**
   * 移除用户的所有角色
   *
   * @param id
   * @return
   */
  UserInfo clearRoles(Long id);


  /**
   * 用户注册
   * @param bean
   * @return
   */
  UserBasicResponse register(UserRegisterRequest bean);

  /**
   * 重置密码
   * @param request
   * @return
   */
  ResponseObject resetPassword(ResetPasswordRequest request);


  /**
   * 修改密码
   * @param request
   * @return
   */
  ResponseObject updatePassword(UpdatePasswordRequest request);
  
}