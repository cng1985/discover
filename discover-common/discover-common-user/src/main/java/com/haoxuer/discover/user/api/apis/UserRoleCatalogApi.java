package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.UserRoleCatalogList;
import com.haoxuer.discover.user.api.domain.page.UserRoleCatalogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserRoleCatalogResponse;

public interface UserRoleCatalogApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    UserRoleCatalogResponse create(UserRoleCatalogDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    UserRoleCatalogResponse update(UserRoleCatalogDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    UserRoleCatalogResponse delete(UserRoleCatalogDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     UserRoleCatalogResponse view(UserRoleCatalogDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    UserRoleCatalogList list(UserRoleCatalogSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    UserRoleCatalogPage search(UserRoleCatalogSearchRequest request);

}