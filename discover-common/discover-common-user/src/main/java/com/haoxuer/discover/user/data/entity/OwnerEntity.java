package com.haoxuer.discover.user.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import lombok.Data;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class OwnerEntity extends AbstractEntity {

    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;

    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private User updater;

}
