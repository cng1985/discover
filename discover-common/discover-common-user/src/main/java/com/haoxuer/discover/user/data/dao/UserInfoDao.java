package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.request.ResetPasswordRequest;
import com.haoxuer.discover.user.data.request.UpdatePasswordRequest;
import com.haoxuer.discover.user.data.request.UserRegisterRequest;
import com.haoxuer.discover.user.data.response.UserBasicResponse;
import com.haoxuer.discover.user.api.domain.request.UserLoginRequest;
import com.haoxuer.discover.user.api.domain.response.UserResponse;

/**
 * Created by imake on 2017年07月20日16:35:48.
 */
public interface UserInfoDao extends BaseDao<UserInfo, Long> {

  UserInfo findByAccount(String account);


  UserInfo findById(Long id);
  
  UserInfo save(UserInfo bean);
  
  UserInfo updateByUpdater(Updater<UserInfo> updater);
  
  UserInfo deleteById(Long id);
  
  /**
   * 添加角色
   *
   * @param id
   * @param roleid
   * @return
   */
  UserInfo addRole(Long id, Long roleid);
  
  
  /**
   * 添加多个角色
   *
   * @param id
   * @param role
   * @return
   */
  UserInfo addRoles(Long id, Long... role);
  
  
  /**
   * 添加角色
   *
   * @param id
   * @param roleName
   * @return
   */
  UserInfo addRole(Long id, String roleName);


  /**
   * 用户注册
   * @param bean
   * @return
   */
  UserBasicResponse register(UserRegisterRequest bean);

  /**
   * 重置密码
   * @param request
   * @return
   */
  ResponseObject resetPassword(ResetPasswordRequest request);


  /**
   * 修改密码
   * @param request
   * @return
   */
  ResponseObject updatePassword(UpdatePasswordRequest request);

  /**
   * 通过账号密码登陆
   * @param request
   * @return
   */
  UserResponse login(UserLoginRequest request);



}