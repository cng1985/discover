package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.UserBindApi;
import com.haoxuer.discover.user.api.domain.list.UserBindList;
import com.haoxuer.discover.user.api.domain.page.UserBindPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserBindResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/userbind")
@RestController
public class UserBindTenantRestController extends BaseTenantRestController {


    @RequiresUser
    @RequestMapping("create")
    public UserBindResponse create(UserBindDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("userbind")
    @RequiresUser
    @RequestMapping("update")
    public UserBindResponse update(UserBindDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequiresUser
    @RequestMapping("delete")
    public UserBindResponse delete(UserBindDataRequest request) {
        init(request);
        UserBindResponse result = new UserBindResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequiresUser
    @RequestMapping("view")
    public UserBindResponse view(UserBindDataRequest request) {
       init(request);
       return api.view(request);
   }

    @RequiresUser
    @RequestMapping("list")
    public UserBindList list(UserBindSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequiresUser
    @RequestMapping("search")
    public UserBindPage search(UserBindSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private UserBindApi api;

}
