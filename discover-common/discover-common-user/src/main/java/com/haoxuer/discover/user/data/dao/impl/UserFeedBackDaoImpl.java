package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.user.data.dao.UserFeedBackDao;
import com.haoxuer.discover.user.data.entity.UserFeedBack;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2017年11月14日18:38:15.
 */
@Repository

public class UserFeedBackDaoImpl extends CriteriaDaoImpl<UserFeedBack, Long> implements UserFeedBackDao {

  @Override
  public UserFeedBack findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public UserFeedBack save(UserFeedBack bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public UserFeedBack deleteById(Long id) {
    UserFeedBack entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<UserFeedBack> getEntityClass() {
    return UserFeedBack.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}