package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.MenuList;
import com.haoxuer.discover.user.api.domain.page.MenuPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.MenuResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

public interface MenuApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    @CacheEvict(value = "menuCache", allEntries = true)
    MenuResponse create(MenuDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    @CacheEvict(value = "menuCache", allEntries = true)
    MenuResponse update(MenuDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    @CacheEvict(value = "menuCache", allEntries = true)
    MenuResponse delete(MenuDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
    @Cacheable(value = "menuCache", keyGenerator = "sysKeyGenerator")
    MenuResponse view(MenuDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    @Cacheable(value = "menuCache", keyGenerator = "sysKeyGenerator")
    MenuList list(MenuSearchRequest request);

    @Cacheable(value = "menuCache", keyGenerator = "sysKeyGenerator")
    MenuList menus(MenuSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    @Cacheable(value = "menuCache", keyGenerator = "sysKeyGenerator")
    MenuPage search(MenuSearchRequest request);

}