package com.haoxuer.discover.user.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class UserEntity extends AbstractEntity {

    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private UserInfo creator;
}
