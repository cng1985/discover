package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.simple.UserLoginLogSimple;
import com.haoxuer.discover.user.data.entity.UserLoginLog;
import com.haoxuer.discover.data.rest.core.Conver;
public class UserLoginLogSimpleConvert implements Conver<UserLoginLogSimple, UserLoginLog> {


    @Override
    public UserLoginLogSimple conver(UserLoginLog source) {
        UserLoginLogSimple result = new UserLoginLogSimple();

            result.setId(source.getId());
             result.setNote(source.getNote());
             result.setPassword(source.getPassword());
             result.setIp(source.getIp());
             result.setClient(source.getClient());
             result.setLastDate(source.getLastDate());
             result.setState(source.getState());
            if(source.getUser()!=null){
               result.setUser(source.getUser().getId());
            }
             result.setAddDate(source.getAddDate());
             if(source.getUser()!=null){
                result.setUserName(source.getUser().getName());
             }
             result.setAccount(source.getAccount());

             result.setStateName(source.getState()+"");
        return result;
    }
}
