package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.enums.BindType;

import java.util.List;

/**
* Created by imake on 2018年11月28日09:53:00.
*/
public interface UserBindService {

	UserBind findById(Long id);

	UserBind save(UserBind bean);

	UserBind update(UserBind bean);

	UserBind deleteById(Long id);
	
	UserBind[] deleteByIds(Long[] ids);
	
	Page<UserBind> page(Pageable pageable);
	
	Page<UserBind> page(Pageable pageable, Object search);

	UserBind findByType(String no, BindType type);

	UserBind findByName(String name);


	List<UserBind> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}