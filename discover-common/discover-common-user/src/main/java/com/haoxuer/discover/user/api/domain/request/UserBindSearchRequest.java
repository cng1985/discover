package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月23日09:43:09.
*/

@Data
public class UserBindSearchRequest extends BasePageRequest {

    //用户
     @Search(name = "user.id",operator = Filter.Operator.eq)
     private Long user;




    private String sortField;


    private String sortMethod;
}