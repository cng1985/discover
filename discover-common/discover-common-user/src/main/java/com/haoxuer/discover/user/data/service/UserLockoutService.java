package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.user.data.entity.UserLockout;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2019年03月19日10:24:16.
*/
public interface UserLockoutService {

	UserLockout findById(Long id);

	UserLockout save(UserLockout bean);

	UserLockout update(UserLockout bean);

	UserLockout deleteById(Long id);
	
	UserLockout[] deleteByIds(Long[] ids);
	
	Page<UserLockout> page(Pageable pageable);
	
	Page<UserLockout> page(Pageable pageable, Object search);


	List<UserLockout> list(int first, Integer size, List<Filter> filters, List<Order> orders);

	/**
	 * 检查用户是否锁定
	 *
	 * @param user
	 * @return
	 */
	Boolean check(Long user);

}