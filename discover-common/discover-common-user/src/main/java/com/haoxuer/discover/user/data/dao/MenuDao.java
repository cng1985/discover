package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.Menu;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年07月21日14:37:30.
 */
public interface MenuDao extends BaseDao<Menu, Integer> {
  
  Menu findById(Integer id);
  
  Menu save(Menu bean);
  
  Menu updateByUpdater(Updater<Menu> updater);
  
  Menu deleteById(Integer id);
}