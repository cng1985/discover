package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.StructureApi;
import com.haoxuer.discover.user.api.domain.list.StructureList;
import com.haoxuer.discover.user.api.domain.page.StructurePage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.StructureResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/structure")
@RestController
public class StructureTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("structure")
    @RequiresUser
    @RequestMapping("create")
    public StructureResponse create(StructureDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("structure")
    @RequiresUser
    @RequestMapping("update")
    public StructureResponse update(StructureDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("structure")
    @RequiresUser
    @RequestMapping("delete")
    public StructureResponse delete(StructureDataRequest request) {
        init(request);
        StructureResponse result = new StructureResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("structure")
    @RequiresUser
    @RequestMapping("view")
    public StructureResponse view(StructureDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("structure")
    @RequiresUser
    @RequestMapping("list")
    public StructureList list(StructureSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("structure")
    @RequiresUser
    @RequestMapping("search")
    public StructurePage search(StructureSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private StructureApi api;

}
