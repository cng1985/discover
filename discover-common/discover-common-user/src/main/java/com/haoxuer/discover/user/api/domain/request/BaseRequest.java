package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;


public class BaseRequest extends RequestUserTokenObject {

    private Long user;

    private Long createUser;


    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }
}
