package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.user.data.entity.UserSecurity;
import com.haoxuer.discover.user.data.enums.SecurityType;

/**
 * Created by imake on 2018年11月28日09:03:55.
 */
public interface UserSecurityDao extends BaseDao<UserSecurity, Long> {

  UserSecurity findById(Long id);

  UserSecurity save(UserSecurity bean);

  UserSecurity updateByUpdater(Updater<UserSecurity> updater);

  UserSecurity deleteById(Long id);

  /**
   * 查找用户安全信息
   *
   * @param user
   * @param type
   * @return
   */
  UserSecurity findByUser(Long user, SecurityType type);

}