package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.user.data.entity.UserVerification;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2017年07月20日16:37:49.
 */
public interface UserVerificationService {
  
  UserVerification findById(Long id);
  
  UserVerification save(UserVerification bean);
  
  UserVerification update(UserVerification bean);
  
  UserVerification deleteById(Long id);
  
  UserVerification[] deleteByIds(Long[] ids);
  
  
  Page<UserVerification> page(Pageable pageable);
  
  Page<UserVerification> page(Pageable pageable, Object search);
  
  
  List<UserVerification> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}