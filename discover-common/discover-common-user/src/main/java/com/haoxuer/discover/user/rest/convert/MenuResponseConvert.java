package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.data.rest.core.Converter;
import com.haoxuer.discover.user.api.domain.response.MenuResponse;
import com.haoxuer.discover.user.data.entity.Menu;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class MenuResponseConvert implements Converter<MenuResponse, Menu> {
    @Override
    public MenuResponse convert(Menu source) {
        MenuResponse result = new MenuResponse();
        BeanDataUtils.copyProperties(source,result);
        return result;
    }

}
