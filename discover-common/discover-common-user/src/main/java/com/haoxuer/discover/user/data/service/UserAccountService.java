package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.data.enums.AccountType;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.rest.domain.AbstractVo;
import com.haoxuer.discover.user.data.entity.UserAccount;
import com.haoxuer.discover.user.data.vo.UserAccountVo;

import java.util.List;

/**
 * Created by imake on 2017年07月20日16:24:20.
 */
public interface UserAccountService {
  
  UserAccountVo reg(UserAccount bean);
  
  /**
   * 修改账号密码
   *
   * @param user
   * @param accountType
   * @param oldpassword
   * @param password
   * @return
   */
  AbstractVo updatePassword(Long user, AccountType accountType, String oldpassword, String password);
  
  UserAccount findById(Long id);
  
  UserAccount save(UserAccount bean);
  
  UserAccount update(UserAccount bean);
  
  UserAccount deleteById(Long id);
  
  UserAccount[] deleteByIds(Long[] ids);
  
  
  Page<UserAccount> page(Pageable pageable);
  
  Page<UserAccount> page(Pageable pageable, Object search);
  
  
  List<UserAccount> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
  UserAccount findByUserName(String username, AccountType account);
  
  UserAccount updateUserLogin(UserAccount userAccount);
  
  
  ResponseObject restPassword(UserAccount userAccount);
  
}