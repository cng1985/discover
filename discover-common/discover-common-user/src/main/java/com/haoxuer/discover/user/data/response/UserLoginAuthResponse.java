package com.haoxuer.discover.user.data.response;

import com.haoxuer.discover.rest.base.ResponseObject;

public class UserLoginAuthResponse extends ResponseObject {

  private Long id;

  private Long user;

  public Long getUser() {
    return user;
  }

  public void setUser(Long user) {
    this.user = user;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
