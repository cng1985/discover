package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.simple.StructureSimple;
import com.haoxuer.discover.user.data.entity.Structure;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import lombok.Data;


@Data
public class StructureSimpleConvert implements Conver<StructureSimple, Structure> {

    private int fetch;

    @Override
    public StructureSimple conver(Structure source) {
        StructureSimple result = new StructureSimple();

         result.setLabel(source.getName());
         result.setValue(""+source.getId());
         if (source.getChildren()!=null&&source.getChildren().size()>0){
             if (fetch!=0){
                 result.setChildren(ConverResourceUtils.converList(source.getChildren(),this));
             }
         }
            result.setId(source.getId());
            if(source.getParent()!=null){
               result.setParent(source.getParent().getId());
            }
             result.setCode(source.getCode());
             result.setLevelInfo(source.getLevelInfo());
             if(source.getParent()!=null){
                result.setParentName(source.getParent().getName());
             }
             result.setSortNum(source.getSortNum());
             result.setIds(source.getIds());
             result.setLft(source.getLft());
             result.setLastDate(source.getLastDate());
             result.setName(source.getName());
             result.setAddDate(source.getAddDate());
             result.setRgt(source.getRgt());

        return result;
    }
}
