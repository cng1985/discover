package com.haoxuer.discover.user.api.domain.list;


import com.haoxuer.discover.user.api.domain.simple.UserAccessLogSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年12月04日16:19:12.
*/

@Data
public class UserAccessLogList  extends ResponseList<UserAccessLogSimple> {

}