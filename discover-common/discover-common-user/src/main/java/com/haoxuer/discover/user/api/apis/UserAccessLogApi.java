package com.haoxuer.discover.user.api.apis;


import com.haoxuer.discover.user.api.domain.list.UserAccessLogList;
import com.haoxuer.discover.user.api.domain.page.UserAccessLogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserAccessLogResponse;

public interface UserAccessLogApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    UserAccessLogResponse create(UserAccessLogDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    UserAccessLogResponse update(UserAccessLogDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    UserAccessLogResponse delete(UserAccessLogDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     UserAccessLogResponse view(UserAccessLogDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    UserAccessLogList list(UserAccessLogSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    UserAccessLogPage search(UserAccessLogSearchRequest request);

}