package com.haoxuer.discover.user.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.data.enums.State;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by imake on 2021年04月28日11:36:16.
*/

@Data
public class UserInfoResponse extends ResponseObject {

    private Long id;

     private StoreState storeState;

     private String phone;

     private Integer catalog;

     private String name;

     private String avatar;

     private State state;

     private Integer loginSize;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;


     private String stateName;
     private String storeStateName;

     private String note;

     private String sessionId;
}