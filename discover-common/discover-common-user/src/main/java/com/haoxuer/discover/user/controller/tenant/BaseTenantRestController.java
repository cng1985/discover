package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.shiro.utils.UserUtil;

public class BaseTenantRestController {


    protected void init(BaseRequest request) {
        UserInfo user = UserUtil.getCurrentUser();
        if (user != null) {
            request.setCreateUser(user.getId());
        }
    }

    protected void init(BasePageRequest request) {
        UserInfo user = UserUtil.getCurrentUser();
        if (user != null) {
            request.setCreateUser(user.getId());
        }
    }

}
