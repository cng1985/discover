package com.haoxuer.discover.user.rest.resource;

import com.haoxuer.discover.user.api.apis.UserRoleCatalogApi;
import com.haoxuer.discover.user.api.domain.list.UserRoleCatalogList;
import com.haoxuer.discover.user.api.domain.page.UserRoleCatalogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserRoleCatalogResponse;
import com.haoxuer.discover.user.data.dao.UserRoleCatalogDao;
import com.haoxuer.discover.user.data.entity.UserRoleCatalog;
import com.haoxuer.discover.user.rest.convert.UserRoleCatalogResponseConvert;
import com.haoxuer.discover.user.rest.convert.UserRoleCatalogSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.UserRoleCatalogDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class UserRoleCatalogResource implements UserRoleCatalogApi {

    @Autowired
    private UserRoleCatalogDao dataDao;

    @Autowired
    private UserRoleCatalogDao parentDao;

    @Override
    public UserRoleCatalogResponse create(UserRoleCatalogDataRequest request) {
        UserRoleCatalogResponse result = new UserRoleCatalogResponse();

        UserRoleCatalog bean = new UserRoleCatalog();
        handleData(request, bean);
        dataDao.save(bean);
        result = new UserRoleCatalogResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserRoleCatalogResponse update(UserRoleCatalogDataRequest request) {
        UserRoleCatalogResponse result = new UserRoleCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        UserRoleCatalog bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new UserRoleCatalogResponseConvert().conver(bean);
        return result;
    }

    private void handleData(UserRoleCatalogDataRequest request, UserRoleCatalog bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getParent()!=null){
               bean.setParent(parentDao.findById(request.getParent()));
            }

    }

    @Override
    public UserRoleCatalogResponse delete(UserRoleCatalogDataRequest request) {
        UserRoleCatalogResponse result = new UserRoleCatalogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public UserRoleCatalogResponse view(UserRoleCatalogDataRequest request) {
        UserRoleCatalogResponse result=new UserRoleCatalogResponse();
        UserRoleCatalog bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new UserRoleCatalogResponseConvert().conver(bean);
        return result;
    }
    @Override
    public UserRoleCatalogList list(UserRoleCatalogSearchRequest request) {
        UserRoleCatalogList result = new UserRoleCatalogList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<UserRoleCatalog> organizations = dataDao.list(0, request.getSize(), filters, orders);
        UserRoleCatalogSimpleConvert convert=new UserRoleCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public UserRoleCatalogPage search(UserRoleCatalogSearchRequest request) {
        UserRoleCatalogPage result=new UserRoleCatalogPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<UserRoleCatalog> page=dataDao.page(pageable);
        UserRoleCatalogSimpleConvert convert=new UserRoleCatalogSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
