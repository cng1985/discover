package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.MenuApi;
import com.haoxuer.discover.user.api.domain.list.MenuList;
import com.haoxuer.discover.user.api.domain.page.MenuPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.MenuResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;

@RequestMapping("/tenantRest/menu")
@RestController
public class MenuTenantRestController extends BaseTenantRestController {


    @RequiresUser
    @RequestMapping("create")
    public MenuResponse create(MenuDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequiresUser
    @RequestMapping("update")
    public MenuResponse update(MenuDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequiresUser
    @RequestMapping("delete")
    public MenuResponse delete(MenuDataRequest request) {
        init(request);
        return api.delete(request);
    }

    @RequiresUser
    @RequestMapping("view")
    public MenuResponse view(MenuDataRequest request) {
       init(request);
       return api.view(request);
   }

    @RequiresUser
    @RequestMapping("list")
    public MenuList list(MenuSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequiresUser
    @RequestMapping("search")
    public MenuPage search(MenuSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private MenuApi api;

}
