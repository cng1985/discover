package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.response.UserInfoResponse;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class UserInfoResponseConvert implements Conver<UserInfoResponse, UserInfo> {
    @Override
    public UserInfoResponse conver(UserInfo source) {
        UserInfoResponse result = new UserInfoResponse();
        BeanDataUtils.copyProperties(source,result);


         result.setStateName(source.getState()+"");
         result.setStoreStateName(source.getStoreState()+"");

        return result;
    }
}
