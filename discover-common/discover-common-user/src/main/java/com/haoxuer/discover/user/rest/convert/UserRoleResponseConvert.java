package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.response.UserRoleResponse;
import com.haoxuer.discover.user.data.entity.UserRole;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class UserRoleResponseConvert implements Conver<UserRoleResponse, UserRole> {
    @Override
    public UserRoleResponse conver(UserRole source) {
        UserRoleResponse result = new UserRoleResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getCatalog()!=null){
           result.setCatalog(source.getCatalog().getId());
        }
         if(source.getCatalog()!=null){
            result.setCatalogName(source.getCatalog().getName());
         }

         result.setTypeName(source.getType()+"");

        return result;
    }
}
