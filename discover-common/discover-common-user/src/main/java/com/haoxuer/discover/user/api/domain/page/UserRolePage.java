package com.haoxuer.discover.user.api.domain.page;


import com.haoxuer.discover.user.api.domain.simple.UserRoleSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月16日16:09:08.
*/

@Data
public class UserRolePage  extends ResponsePage<UserRoleSimple> {

}