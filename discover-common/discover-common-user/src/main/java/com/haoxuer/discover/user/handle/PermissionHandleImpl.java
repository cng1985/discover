package com.haoxuer.discover.user.handle;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.enums.DataScope;
import com.haoxuer.discover.user.data.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PermissionHandleImpl implements PermissionHandle {


    @Autowired
    private UserInfoService userService;

    @Override
    public void handle(BasePageRequest request) {
        UserInfo employee = userService.findById(request.getCreateUser());
        if (employee != null) {
            if (employee.getDataScope() == null) {
                employee.setDataScope(DataScope.my);
            }
            if (employee.getDataScope() == DataScope.my) {
                request.setCreator(employee.getId());
            } else if (employee.getDataScope() == DataScope.myDepartment) {
                if (employee.getStructure() != null) {
                    request.setStructure(employee.getStructure().getId());
                }
            } else if (employee.getDataScope() == DataScope.myAllDepartment) {
                if (employee.getStructure() != null) {
                    request.setLft(employee.getStructure().getLft());
                    request.setRgt(employee.getStructure().getRgt());
                }
            } else if (employee.getDataScope() == DataScope.all) {
            } else {
            }
        }
    }
}
