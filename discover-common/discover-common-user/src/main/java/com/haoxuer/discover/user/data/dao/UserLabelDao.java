package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.UserLabel;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年12月06日15:59:13.
 */
public interface UserLabelDao extends BaseDao<UserLabel, Long> {
  
  UserLabel findById(Long id);
  
  UserLabel save(UserLabel bean);
  
  UserLabel updateByUpdater(Updater<UserLabel> updater);
  
  UserLabel deleteById(Long id);
  
  
  UserLabel label(String name);
  
}