package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import com.haoxuer.discover.user.data.enums.LoginState;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月22日10:49:09.
*/

@Data
public class UserLoginLogSearchRequest extends BasePageRequest {

    //用户
     @Search(name = "user.id",operator = Filter.Operator.eq)
     private Long userId;

    //用户姓名
     @Search(name = "user.name",operator = Filter.Operator.like)
     private String userName;


    @Search(name = "state",operator = Filter.Operator.eq)
    private LoginState state;

    @Search(name = "storeState",operator = Filter.Operator.eq)
    private StoreState storeState=StoreState.normal;

    private String sortField;


    private String sortMethod;
}