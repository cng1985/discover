package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import com.haoxuer.discover.user.data.dao.UserLabelDao;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.entity.UserRole;
import com.haoxuer.discover.user.data.request.ResetPasswordRequest;
import com.haoxuer.discover.user.data.request.UpdatePasswordRequest;
import com.haoxuer.discover.user.data.request.UserRegisterRequest;
import com.haoxuer.discover.user.data.response.UserBasicResponse;
import com.haoxuer.discover.user.data.service.UserInfoService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2017年07月20日16:35:49.
 */
@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {

  private UserInfoDao dao;

  private UserLabelDao labelDao;


  @Override
  @Transactional(readOnly = true)
  public UserInfo findById(Long id) {
    return dao.findById(id);
  }


  @Override
  @Transactional
  public UserInfo save(UserInfo bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  public UserInfo addLabel(Long user, String... lables) {
    UserInfo result = dao.findById(user);
    for (String lable : lables) {
      if (lable != null) {
        result.getLabels().add(labelDao.label(lable.trim()));
      }
    }
    return result;
  }

  @Override
  @Transactional
  public UserInfo update(UserInfo bean) {
    Updater<UserInfo> updater = new Updater<UserInfo>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public UserInfo deleteById(Long id) {
    UserInfo bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public UserInfo[] deleteByIds(Long[] ids) {
    UserInfo[] beans = new UserInfo[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(UserInfoDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<UserInfo> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<UserInfo> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<UserInfo> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }

  @Override
  public UserInfo findByAccount(String account) {
    return dao.findByAccount(account);
  }

  @Override
  public Collection<? extends String> findAuthorities(Long id) {
    List<String> authorities = new ArrayList<String>();
    UserInfo admin = dao.findById(id);
    if (admin != null) {
      for (UserRole role : admin.getRoles()) {
        authorities.addAll(role.getAuthorities());
      }
    }
    return authorities;
  }

  @Override
  public void updateUserLogin(UserInfo user) {
    user = dao.findById(user.getId());
    user.setLastDate(new Date());
    Integer times = user.getLoginSize();
    if (times == null) {
      times = 0;
    }
    times++;
    user.setLoginSize(times);
  }

  @Override
  public UserInfo addRole(Long id, Long roleid) {
    UserInfo entity = dao.findById(id);
    if (entity != null) {
      UserRole bean = new UserRole();
      bean.setId(roleid);
      entity.getRoles().add(bean);
    }

    return entity;
  }

  @Override
  public UserInfo addRoles(Long id, Long... roles) {
    UserInfo entity = dao.findById(id);
    if (entity != null) {
      if (roles == null) {
        return entity;
      }
      entity.getRoles().clear();
      for (Long role : roles) {
        UserRole bean = new UserRole();
        bean.setId(role);
        entity.getRoles().add(bean);
      }
    }

    return entity;
  }

  @Override
  public UserInfo clearRoles(Long id) {
    UserInfo entity = dao.findById(id);
    if (entity != null) {
      entity.getRoles().clear();
    }
    return entity;
  }

  @Override
  public UserBasicResponse register(UserRegisterRequest bean) {
    return dao.register(bean);
  }

  @Override
  public ResponseObject resetPassword(ResetPasswordRequest request) {
    return dao.resetPassword(request);
  }

  @Override
  public ResponseObject updatePassword(UpdatePasswordRequest request) {
    return dao.updatePassword(request);
  }
}