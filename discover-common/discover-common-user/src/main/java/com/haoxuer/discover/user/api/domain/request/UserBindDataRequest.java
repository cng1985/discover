package com.haoxuer.discover.user.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.user.data.enums.BindType;
import java.util.Date;

/**
*
* Created by imake on 2021年05月23日09:43:09.
*/

@Data
public class UserBindDataRequest extends BaseRequest {

    private Long id;

     private String no;

     private Date lastDate;

     private BindType bindType;

     private Long user;

     private Long loginSize;

     private Date addDate;


}