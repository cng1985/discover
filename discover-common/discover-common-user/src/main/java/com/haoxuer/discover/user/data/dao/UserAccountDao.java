package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.UserAccount;
import com.haoxuer.discover.user.data.vo.UserAccountVo;
import com.haoxuer.discover.user.data.enums.AccountType;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年07月20日16:24:20.
 */
public interface UserAccountDao extends BaseDao<UserAccount, Long> {
  
  UserAccount findById(Long id);
  
  /**
   * 根据用户名查找账号.
   *
   * @param username
   * @return
   */
  UserAccount findByUserName(String username);

  UserAccount findByName(String name);

  /**
   * 根据手机号查找账号.
   *
   * @param phone
   * @return
   */
  UserAccount findByPhone(String phone);
  
  /**
   * 根据电子邮箱查找账号.
   *
   * @param email
   * @return
   */
  UserAccount findByEmail(String email);
  
  
  UserAccount save(UserAccount bean);
  
  UserAccount updateByUpdater(Updater<UserAccount> updater);
  
  UserAccount deleteById(Long id);
  
  UserAccountVo reg(UserAccount bean);
  
  
  UserAccountVo binding(String phone, Long user, AccountType type, String password);
  
  Long checkUserName(String phone, AccountType type);
}