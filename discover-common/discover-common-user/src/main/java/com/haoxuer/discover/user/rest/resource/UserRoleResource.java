package com.haoxuer.discover.user.rest.resource;

import com.haoxuer.discover.user.api.apis.UserRoleApi;
import com.haoxuer.discover.user.api.domain.list.UserRoleList;
import com.haoxuer.discover.user.api.domain.page.UserRolePage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserRoleResponse;
import com.haoxuer.discover.user.data.dao.UserRoleDao;
import com.haoxuer.discover.user.data.entity.UserRole;
import com.haoxuer.discover.user.data.enums.RoleType;
import com.haoxuer.discover.user.rest.convert.UserRoleResponseConvert;
import com.haoxuer.discover.user.rest.convert.UserRoleSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.UserRoleCatalogDao;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class UserRoleResource implements UserRoleApi {

    @Autowired
    private UserRoleDao dataDao;

    @Autowired
    private UserRoleCatalogDao catalogDao;

    @Override
    public UserRoleResponse create(UserRoleDataRequest request) {
        UserRoleResponse result = new UserRoleResponse();

        UserRole bean = new UserRole();
        handleData(request, bean);
        dataDao.save(bean);
        result = new UserRoleResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserRoleResponse update(UserRoleDataRequest request) {
        UserRoleResponse result = new UserRoleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        UserRole bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new UserRoleResponseConvert().conver(bean);
        return result;
    }

    private void handleData(UserRoleDataRequest request, UserRole bean) {
        BeanDataUtils.copyProperties(request, bean);
        if (request.getCatalog() != null) {
            bean.setCatalog(catalogDao.findById(request.getCatalog()));
        }
        if (request.getAuthorities() != null) {
            bean.setAuthorities(request.getAuthorities());
        }
    }

    @Override
    public UserRoleResponse delete(UserRoleDataRequest request) {
        UserRoleResponse result = new UserRoleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        UserRole role = dataDao.findById(request.getId());
        if (role == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        if (role.getType() == null) {
            role.setType(RoleType.CUSTOM);
        }
        if (role.getType() == RoleType.SYSTEM) {
            result.setCode(503);
            result.setMsg("系统角色不能删除");
            return result;
        }
        dataDao.delete(role);
        return result;
    }

    @Override
    public UserRoleResponse view(UserRoleDataRequest request) {
        UserRoleResponse result = new UserRoleResponse();
        UserRole bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new UserRoleResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserRoleList list(UserRoleSearchRequest request) {
        UserRoleList result = new UserRoleList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<UserRole> organizations = dataDao.list(0, request.getSize(), filters, orders);
        UserRoleSimpleConvert convert = new UserRoleSimpleConvert();
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public UserRolePage search(UserRoleSearchRequest request) {
        UserRolePage result = new UserRolePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.desc("" + request.getSortField()));
        } else {
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<UserRole> page = dataDao.page(pageable);
        UserRoleSimpleConvert convert = new UserRoleSimpleConvert();
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }
}
