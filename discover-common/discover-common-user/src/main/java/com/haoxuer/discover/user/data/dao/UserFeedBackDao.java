package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.UserFeedBack;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年11月14日18:38:15.
 */
public interface UserFeedBackDao extends BaseDao<UserFeedBack, Long> {
  
  UserFeedBack findById(Long id);
  
  UserFeedBack save(UserFeedBack bean);
  
  UserFeedBack updateByUpdater(Updater<UserFeedBack> updater);
  
  UserFeedBack deleteById(Long id);
}