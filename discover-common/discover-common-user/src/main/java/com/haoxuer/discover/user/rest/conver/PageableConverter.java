package com.haoxuer.discover.user.rest.conver;

import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.rest.core.Converter;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.rest.base.RequestUserTokenPageObject;

public class PageableConverter implements Converter<Pageable, RequestUserTokenPageObject> {
    @Override
    public Pageable convert(RequestUserTokenPageObject source) {
        Pageable result = new Pageable();
        result.setPageNo(source.getNo());
        result.setPageSize(source.getSize());
        result.getFilters().addAll(FilterUtils.getFilters(source));

        if ("asc".equals(source.getSortMethod())){
            result.getOrders().add(Order.asc(""+source.getSortField()));
        }
        else if ("desc".equals(source.getSortMethod())){
            result.getOrders().add(Order.desc(""+source.getSortField()));
        }else{
            result.getOrders().add(Order.desc("id"));
        }

        return result;
    }
}
