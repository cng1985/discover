package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.simple.MenuSimple;
import com.haoxuer.discover.user.data.entity.Menu;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import lombok.Data;


@Data
public class MenuSimpleConvert implements Conver<MenuSimple, Menu> {

    private int fetch;

    @Override
    public MenuSimple conver(Menu source) {
        MenuSimple result = new MenuSimple();

        result.setId(source.getId());
        result.setLabel(source.getName());
        result.setValue("" + source.getId());
        result.setName(source.getName());
        result.setIcon(source.getIcon());
        result.setPath(source.getPath());
        result.setPermission(source.getPermission());
        result.setCatalog(source.getCatalog());
        result.setIds(source.getIds());
        result.setSortNum(source.getSortNum());
        result.setMenuType(source.getMenuType());
        result.setParent(source.getParentId());
        result.setRouter(source.getRouter());
        if (source.getParent()!=null){
            result.setParentPermission(source.getParent().getPermission());
        }
        if (fetch != 0 && source.getChildrens() != null && source.getChildrens().size() > 0) {
            result.setChildren(ConverResourceUtils.converList(source.getChildrens(), this));
        }
        return result;
    }
}
