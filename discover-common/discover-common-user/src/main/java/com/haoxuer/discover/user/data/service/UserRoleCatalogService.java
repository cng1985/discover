package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.entity.UserRoleCatalog;
import java.util.List;

/**
 * Created by imake on 2017年08月09日13:36:13.
 */
public interface UserRoleCatalogService {
  
  UserRoleCatalog findById(Integer id);
  
  UserRoleCatalog save(UserRoleCatalog bean);
  
  UserRoleCatalog update(UserRoleCatalog bean);
  
  UserRoleCatalog deleteById(Integer id);
  
  UserRoleCatalog[] deleteByIds(Integer[] ids);
  
  Page<UserRoleCatalog> page(Pageable pageable);
  
  Page<UserRoleCatalog> page(Pageable pageable, Object search);
  
  List<UserRoleCatalog> findByTops(Integer pid);
  
  List<UserRoleCatalog> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}