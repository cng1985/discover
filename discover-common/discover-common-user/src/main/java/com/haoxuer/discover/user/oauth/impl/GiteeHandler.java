package com.haoxuer.discover.user.oauth.impl;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.haoxuer.discover.user.oauth.api.OauthHandler;
import com.haoxuer.discover.user.oauth.domain.GiteeUser;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.discover.user.oauth.domain.TokenResponse;
import com.haoxuer.utils.http.Connection;
import com.haoxuer.utils.http.HttpConnection;
import java.io.IOException;

public class GiteeHandler implements OauthHandler {

  private String client_id;

  private String client_secret;

  @Override
  public void setKey(String key) {
    this.client_id = key;
  }

  @Override
  public void setSecret(String secret) {
    this.client_secret = secret;
  }

  @Override
  public OauthResponse login(String access_token, String openid) {
    Connection con = HttpConnection.connect("http://gitee.com/api/v5/user");
    con.data("access_token", access_token);
    String body = "";
    try {
      body = con.execute().body();
    } catch (IOException e) {
      e.printStackTrace();
    }
    Gson gson = new Gson();
    GiteeUser user = gson.fromJson(body, GiteeUser.class);
    return user;
  }

  @Override
  public TokenResponse getToken(String code) {
    TokenResponse response = new TokenResponse();
    Connection con = HttpConnection.connect("https://gitee.com/oauth/token").method(Connection.Method.POST);
    con.data("client_id", client_id);
    con.data("client_secret", client_secret);
    con.data("grant_type", "authorization_code");
    // con.data("redirect_uri", "http://www.yichisancun.com/");
    con.data("code", code);
    try {
      String body = con.execute().body();
      JsonParser parser = new JsonParser();
      JsonElement element = parser.parse(body);
      response.setAccessToken(ElementUtils.getString(element, "access_token"));
      response.setRefreshToken(ElementUtils.getString(element, "refresh_token"));
      response.setTokenType(ElementUtils.getString(element, "token_type"));
      response.setExpiresIn(ElementUtils.getInt(element, "expires_in"));
      response.setScope(ElementUtils.getString(element, "scope"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return response;
  }
}
