package com.haoxuer.discover.user.rest.resource;

import com.haoxuer.discover.user.api.apis.StructureApi;
import com.haoxuer.discover.user.api.domain.list.StructureList;
import com.haoxuer.discover.user.api.domain.page.StructurePage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.StructureResponse;
import com.haoxuer.discover.user.data.dao.StructureDao;
import com.haoxuer.discover.user.data.entity.Structure;
import com.haoxuer.discover.user.rest.conver.PageableConverter;
import com.haoxuer.discover.user.rest.convert.StructureResponseConvert;
import com.haoxuer.discover.user.rest.convert.StructureSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.StructureDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class StructureResource implements StructureApi {

    @Autowired
    private StructureDao dataDao;

    @Autowired
    private StructureDao parentDao;

    @Override
    public StructureResponse create(StructureDataRequest request) {
        StructureResponse result = new StructureResponse();

        Structure bean = new Structure();
        handleData(request, bean);
        dataDao.save(bean);
        result = new StructureResponseConvert().conver(bean);
        return result;
    }

    @Override
    public StructureResponse update(StructureDataRequest request) {
        StructureResponse result = new StructureResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Structure bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new StructureResponseConvert().conver(bean);
        return result;
    }

    private void handleData(StructureDataRequest request, Structure bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getParent()!=null){
               bean.setParent(parentDao.findById(request.getParent()));
            }

    }

    @Override
    public StructureResponse delete(StructureDataRequest request) {
        StructureResponse result = new StructureResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public StructureResponse view(StructureDataRequest request) {
        StructureResponse result=new StructureResponse();
        Structure bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new StructureResponseConvert().conver(bean);
        return result;
    }
    @Override
    public StructureList list(StructureSearchRequest request) {
        StructureList result = new StructureList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<Structure> organizations = dataDao.list(0, request.getSize(), filters, orders);
        StructureSimpleConvert convert=new StructureSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public StructurePage search(StructureSearchRequest request) {
        StructurePage result=new StructurePage();
        Pageable pageable = new PageableConverter().apply(request);

        Page<Structure> page=dataDao.page(pageable);
        StructureSimpleConvert convert=new StructureSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
