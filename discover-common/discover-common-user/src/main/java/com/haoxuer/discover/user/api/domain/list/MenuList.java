package com.haoxuer.discover.user.api.domain.list;


import com.haoxuer.discover.user.api.domain.simple.MenuSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年01月11日23:03:48.
*/

@Data
public class MenuList  extends ResponseList<MenuSimple> {

}