package com.haoxuer.discover.user.data.entity;


import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.user.data.enums.SecurityType;

import javax.persistence.*;

/**
 * 用户密码集合
 */
@Entity
@Table(name = "user_security")
public class UserSecurity extends AbstractEntity {

  @ManyToOne(fetch = FetchType.LAZY)
  private UserInfo user;

  /**
   * 校验次数
   */
  private Integer checkSize;

  /**
   * 密码
   */
  @Column(length = 50)
  private String password;

  /**
   * 盐
   */
  @Column(length = 50)
  private String salt;

  private SecurityType securityType;

  public UserInfo getUser() {
    return user;
  }

  public void setUser(UserInfo user) {
    this.user = user;
  }

  public Integer getCheckSize() {
    return checkSize;
  }

  public void setCheckSize(Integer checkSize) {
    this.checkSize = checkSize;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getSalt() {
    return salt;
  }

  public void setSalt(String salt) {
    this.salt = salt;
  }

  public SecurityType getSecurityType() {
    return securityType;
  }

  public void setSecurityType(SecurityType securityType) {
    this.securityType = securityType;
  }
}
