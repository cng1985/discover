package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.response.UserBindResponse;
import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class UserBindResponseConvert implements Conver<UserBindResponse, UserBind> {
    @Override
    public UserBindResponse conver(UserBind source) {
        UserBindResponse result = new UserBindResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getUser()!=null){
           result.setUser(source.getUser().getId());
        }
         if(source.getUser()!=null){
            result.setUserName(source.getUser().getName());
         }

         result.setBindTypeName(source.getBindType()+"");

        return result;
    }
}
