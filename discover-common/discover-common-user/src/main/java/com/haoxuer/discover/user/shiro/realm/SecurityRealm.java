package com.haoxuer.discover.user.shiro.realm;

import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.entity.UserSecurity;
import com.haoxuer.discover.user.data.enums.BindType;
import com.haoxuer.discover.user.data.enums.SecurityType;
import com.haoxuer.discover.user.data.service.UserBindService;
import com.haoxuer.discover.user.data.service.UserInfoService;
import com.haoxuer.discover.user.data.service.UserLockoutService;
import com.haoxuer.discover.user.data.service.UserSecurityService;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import com.haoxuer.discover.user.utils.Encodes;
import com.haoxuer.discover.user.utils.SecurityUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class SecurityRealm extends AuthorizingRealm {
  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    UserAuthorization authorization = new UserAuthorization(userService);
    return authorization.doGetAuthorizationInfo(principals);
  }

  public void clearAllCache() {
    clearAllCachedAuthenticationInfo();
    clearAllCachedAuthorizationInfo();
  }

  public void clearAllCachedAuthenticationInfo() {
    getAuthenticationCache().clear();
  }

  public void clearAllCachedAuthorizationInfo() {
    getAuthorizationCache().clear();
  }

  @Override
  public void clearCache(PrincipalCollection principals) {
    super.clearCache(principals);
  }

  @Override
  public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
    super.clearCachedAuthenticationInfo(principals);
  }

  @Override
  public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
    super.clearCachedAuthorizationInfo(principals);
  }

  @Autowired
  private UserBindService bindService;

  @Autowired
  private UserSecurityService securityService;

  @Autowired
  private UserInfoService userService;


  @Autowired
  private UserLockoutService lockoutService;

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
    UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
    UserBind userBind = bindService.findByType(token.getUsername(), BindType.account);
    if (userBind == null) {
      userBind = bindService.findByType(token.getUsername(), BindType.phone);
    }
    if (userBind == null) {
      userBind = bindService.findByType(token.getUsername(), BindType.email);
    }
    if (userBind == null) {
      userBind = bindService.findByType(token.getUsername(), BindType.other);
    }
    if (userBind==null){
      throw new UnknownAccountException();
    }
    if (userBind.getUser()==null){
      throw new UnknownAccountException();
    }
    if (lockoutService.check(userBind.getUser().getId())){
      throw  new LockedAccountException();
    }
    UserSecurity security = securityService.findByUser(userBind.getUser().getId(), SecurityType.account);
    if (security != null) {
      byte[] salt = Encodes.decodeHex(security.getSalt());
      UserInfo user=userService.findById(userBind.getUser().getId());
      ShiroUser shiroUser = new ShiroUser(user.getId(), userBind.getNo(), user.getName());
      // 设置用户session
      UserUtil.setCurrentUser(user);
      try {
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(shiroUser, security.getPassword(),
            ByteSource.Util.bytes(salt), getName());
        return authenticationInfo;
      } catch (Exception e) {
        return null;
      }
    } else {
      return null;
    }
  }


  /**
   * 设定Password校验的Hash算法与迭代次数.
   */
  @PostConstruct
  public void initCredentialsMatcher() {
    HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(SecurityUtil.HASH_ALGORITHM);
    matcher.setHashIterations(SecurityUtil.HASH_INTERATIONS);
    setCredentialsMatcher(matcher);
  }

}
