package com.haoxuer.discover.user.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.user.data.enums.BindType;

/**
*
* Created by imake on 2021年05月23日09:43:09.
*/

@Data
public class UserBindResponse extends ResponseObject {

    private Long id;

     private String no;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;

     private BindType bindType;

     private Long user;

     private Long loginSize;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private String userName;


     private String bindTypeName;
}