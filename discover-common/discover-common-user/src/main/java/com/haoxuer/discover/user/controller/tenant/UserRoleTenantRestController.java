package com.haoxuer.discover.user.controller.tenant;

import com.haoxuer.discover.user.api.apis.UserRoleApi;
import com.haoxuer.discover.user.api.domain.list.UserRoleList;
import com.haoxuer.discover.user.api.domain.page.UserRolePage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserRoleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/userrole")
@RestController
public class UserRoleTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("userrole")
    @RequiresUser
    @RequestMapping("create")
    public UserRoleResponse create(UserRoleDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("userrole")
    @RequiresUser
    @RequestMapping("update")
    public UserRoleResponse update(UserRoleDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequiresPermissions("userrole")
    @RequiresUser
    @RequestMapping("updateJson")
    public UserRoleResponse updateJson(@RequestBody UserRoleDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("userrole")
    @RequiresUser
    @RequestMapping("delete")
    public UserRoleResponse delete(UserRoleDataRequest request) {
        init(request);
        UserRoleResponse result = new UserRoleResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("userrole")
    @RequiresUser
    @RequestMapping("view")
    public UserRoleResponse view(UserRoleDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("userrole")
    @RequiresUser
    @RequestMapping("list")
    public UserRoleList list(UserRoleSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("userrole")
    @RequiresUser
    @RequestMapping("search")
    public UserRolePage search(UserRoleSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private UserRoleApi api;

}
