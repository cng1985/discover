package com.haoxuer.discover.user.rest.resource;

import com.haoxuer.discover.user.api.apis.UserLoginLogApi;
import com.haoxuer.discover.user.api.domain.list.UserLoginLogList;
import com.haoxuer.discover.user.api.domain.page.UserLoginLogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserLoginLogResponse;
import com.haoxuer.discover.user.data.dao.UserLoginLogDao;
import com.haoxuer.discover.user.data.entity.UserLoginLog;
import com.haoxuer.discover.user.rest.convert.UserLoginLogResponseConvert;
import com.haoxuer.discover.user.rest.convert.UserLoginLogSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class UserLoginLogResource implements UserLoginLogApi {

    @Autowired
    private UserLoginLogDao dataDao;

    @Autowired
    private UserInfoDao userDao;

    @Override
    public UserLoginLogResponse create(UserLoginLogDataRequest request) {
        UserLoginLogResponse result = new UserLoginLogResponse();

        UserLoginLog bean = new UserLoginLog();
        handleData(request, bean);
        dataDao.save(bean);
        result = new UserLoginLogResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserLoginLogResponse update(UserLoginLogDataRequest request) {
        UserLoginLogResponse result = new UserLoginLogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        UserLoginLog bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new UserLoginLogResponseConvert().conver(bean);
        return result;
    }

    private void handleData(UserLoginLogDataRequest request, UserLoginLog bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getUser()!=null){
               bean.setUser(userDao.findById(request.getUser()));
            }

    }

    @Override
    public UserLoginLogResponse delete(UserLoginLogDataRequest request) {
        UserLoginLogResponse result = new UserLoginLogResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public UserLoginLogResponse view(UserLoginLogDataRequest request) {
        UserLoginLogResponse result=new UserLoginLogResponse();
        UserLoginLog bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new UserLoginLogResponseConvert().conver(bean);
        return result;
    }
    @Override
    public UserLoginLogList list(UserLoginLogSearchRequest request) {
        UserLoginLogList result = new UserLoginLogList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<UserLoginLog> organizations = dataDao.list(0, request.getSize(), filters, orders);
        UserLoginLogSimpleConvert convert=new UserLoginLogSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public UserLoginLogPage search(UserLoginLogSearchRequest request) {
        UserLoginLogPage result=new UserLoginLogPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<UserLoginLog> page=dataDao.page(pageable);
        UserLoginLogSimpleConvert convert=new UserLoginLogSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
