package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.simple.UserBindSimple;
import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.data.rest.core.Conver;
public class UserBindSimpleConvert implements Conver<UserBindSimple, UserBind> {


    @Override
    public UserBindSimple conver(UserBind source) {
        UserBindSimple result = new UserBindSimple();

            result.setId(source.getId());
             result.setNo(source.getNo());
             result.setLastDate(source.getLastDate());
             result.setBindType(source.getBindType());
            if(source.getUser()!=null){
               result.setUser(source.getUser().getId());
            }
             result.setLoginSize(source.getLoginSize());
             result.setAddDate(source.getAddDate());
             if(source.getUser()!=null){
                result.setUserName(source.getUser().getName());
             }

             result.setBindTypeName(source.getBindType()+"");
        return result;
    }
}
