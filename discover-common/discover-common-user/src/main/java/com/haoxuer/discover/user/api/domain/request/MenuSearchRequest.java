package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import lombok.ToString;

/**
*
* Created by imake on 2021年01月11日23:03:48.
*/

@ToString(callSuper = true)
@Data
public class MenuSearchRequest extends BasePageRequest {



    private int fetch;

    @Search(name = "levelInfo",operator = Filter.Operator.eq)
    private Integer level;

    @Search(name = "parent.id",operator = Filter.Operator.eq)
    private Integer parent;

    @Search(name = "catalog",operator = Filter.Operator.eq)
    private Integer catalog;

    @Search(name = "menuType",operator = Filter.Operator.eq)
    private Integer menuType;

    private String sortField;


    private String sortMethod;
}