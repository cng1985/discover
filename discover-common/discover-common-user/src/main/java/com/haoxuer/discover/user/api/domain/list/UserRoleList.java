package com.haoxuer.discover.user.api.domain.list;


import com.haoxuer.discover.user.api.domain.simple.UserRoleSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年05月16日16:09:08.
*/

@Data
public class UserRoleList  extends ResponseList<UserRoleSimple> {

}