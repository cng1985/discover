package com.haoxuer.discover.user.rest.resource;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.api.apis.UserFeedBackApi;
import com.haoxuer.discover.user.api.domain.request.UserFeedBackPostRequest;
import com.haoxuer.discover.user.data.dao.UserFeedBackDao;
import com.haoxuer.discover.user.data.entity.UserFeedBack;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Scope("prototype")
@Transactional
@Component
public class UserFeedBackResource implements UserFeedBackApi {

  @Autowired
  private UserFeedBackDao feedBackDao;

  @Autowired
  private UserTokenService tokenService;

  @Override
  public ResponseObject post(UserFeedBackPostRequest request) {
    ResponseObject result = new ResponseObject();
    if (StringUtils.isEmpty(request.getNote())) {
      result.setCode(505);
      result.setMsg("内容不能为空");
      return result;
    }
    UserFeedBack bean = new UserFeedBack();
    bean.setTitle(request.getTitle());
    bean.setNote(request.getNote());
    bean.setName(request.getName());
    bean.setQq(request.getQq());
    bean.setPhone(request.getPhone());
    if (StringUtils.hasText(request.getUserToken())) {
      Long uid = tokenService.user(request.getUserToken());
      bean.setUser(UserInfo.fromId(uid));
    }

    feedBackDao.save(bean);
    return result;
  }
}
