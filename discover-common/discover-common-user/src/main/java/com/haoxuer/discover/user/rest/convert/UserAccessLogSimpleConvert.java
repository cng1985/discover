package com.haoxuer.discover.user.rest.convert;

import com.haoxuer.discover.user.api.domain.simple.UserAccessLogSimple;
import com.haoxuer.discover.user.data.entity.UserAccessLog;
import com.haoxuer.discover.data.rest.core.Conver;
public class UserAccessLogSimpleConvert implements Conver<UserAccessLogSimple, UserAccessLog> {


    @Override
    public UserAccessLogSimple conver(UserAccessLog source) {
        UserAccessLogSimple result = new UserAccessLogSimple();

            result.setId(source.getId());
             result.setConsumeTime(source.getConsumeTime());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setIp(source.getIp());
             if(source.getCreator()!=null){
                result.setCreatorName(source.getCreator().getName());
             }
             result.setAddDate(source.getAddDate());
             result.setUrl(source.getUrl());

        return result;
    }
}
