package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.user.data.dao.UserLockoutDao;
import com.haoxuer.discover.user.data.entity.UserLockout;
import com.haoxuer.discover.user.data.service.UserLockoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by imake on 2019年03月19日10:24:16.
 */


@Scope("prototype")
@Service
@Transactional
public class UserLockoutServiceImpl implements UserLockoutService {

  private UserLockoutDao dao;


  @Override
  @Transactional(readOnly = true)
  public UserLockout findById(Long id) {
    return dao.findById(id);
  }


  @Override
  @Transactional
  public UserLockout save(UserLockout bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public UserLockout update(UserLockout bean) {
    Updater<UserLockout> updater = new Updater<UserLockout>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public UserLockout deleteById(Long id) {
    UserLockout bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public UserLockout[] deleteByIds(Long[] ids) {
    UserLockout[] beans = new UserLockout[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(UserLockoutDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<UserLockout> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<UserLockout> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<UserLockout> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }

  @Override
  public Boolean check(Long user) {
    return dao.check(user);
  }
}