package com.haoxuer.discover.user.controller.rest;

import com.haoxuer.discover.user.api.apis.UserAccessLogApi;
import com.haoxuer.discover.user.api.domain.list.UserAccessLogList;
import com.haoxuer.discover.user.api.domain.page.UserAccessLogPage;
import com.haoxuer.discover.user.api.domain.request.*;
import com.haoxuer.discover.user.api.domain.response.UserAccessLogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/useraccesslog")
@RestController
public class UserAccessLogRestController extends BaseRestController {


    @RequestMapping("create")
    public UserAccessLogResponse create(UserAccessLogDataRequest request) {
        init(request);
        request.setCreator(request.getUser());
        return api.create(request);
    }

    @RequestMapping("update")
    public UserAccessLogResponse update(UserAccessLogDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public UserAccessLogResponse delete(UserAccessLogDataRequest request) {
        init(request);
        UserAccessLogResponse result = new UserAccessLogResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public UserAccessLogResponse view(UserAccessLogDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public UserAccessLogList list(UserAccessLogSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public UserAccessLogPage search(UserAccessLogSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private UserAccessLogApi api;

}
