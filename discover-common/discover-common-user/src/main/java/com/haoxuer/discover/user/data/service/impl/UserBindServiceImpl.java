package com.haoxuer.discover.user.data.service.impl;

import com.haoxuer.discover.user.data.enums.BindType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.user.data.dao.UserBindDao;
import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.user.data.service.UserBindService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年11月28日09:53:01.
*/


@Scope("prototype")
@Service
@Transactional
public class UserBindServiceImpl implements UserBindService {

	private UserBindDao dao;


	@Override
	@Transactional(readOnly = true)
	public UserBind findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public UserBind save(UserBind bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public UserBind update(UserBind bean) {
		Updater<UserBind> updater = new Updater<UserBind>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public UserBind deleteById(Long id) {
		UserBind bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public UserBind[] deleteByIds(Long[] ids) {
		UserBind[] beans = new UserBind[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(UserBindDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<UserBind> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<UserBind> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

	@Override
	public UserBind findByType(String no, BindType type) {
		return dao.findByType(no,type);
	}

	@Override
	public UserBind findByName(String name) {
		return dao.findByName(name);
	}

	@Override
    public List<UserBind> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}