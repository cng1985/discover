package com.haoxuer.discover.user.api.domain.page;


import com.haoxuer.discover.user.api.domain.simple.StructureSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月21日22:44:58.
*/

@Data
public class StructurePage  extends ResponsePage<StructureSimple> {

}