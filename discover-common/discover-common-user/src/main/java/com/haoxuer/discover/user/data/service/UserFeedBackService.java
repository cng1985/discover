package com.haoxuer.discover.user.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.entity.UserFeedBack;
import java.util.List;

/**
 * Created by imake on 2017年11月14日18:38:15.
 */
public interface UserFeedBackService {
  
  UserFeedBack findById(Long id);
  
  UserFeedBack save(UserFeedBack bean);
  
  UserFeedBack update(UserFeedBack bean);
  
  UserFeedBack deleteById(Long id);
  
  UserFeedBack[] deleteByIds(Long[] ids);
  
  Page<UserFeedBack> page(Pageable pageable);
  
  Page<UserFeedBack> page(Pageable pageable, Object search);
  
  
  List<UserFeedBack> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}