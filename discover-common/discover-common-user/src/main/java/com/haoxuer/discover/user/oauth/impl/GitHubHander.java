package com.haoxuer.discover.user.oauth.impl;

import com.google.gson.Gson;
import com.haoxuer.discover.user.oauth.api.OauthHandler;
import com.haoxuer.discover.user.oauth.domain.GitHubUser;
import com.haoxuer.discover.user.oauth.domain.TokenResponse;
import com.haoxuer.discover.user.oauth.domain.OauthResponse;
import com.haoxuer.utils.http.Connection;
import com.haoxuer.utils.http.HttpConnection;

public class GitHubHander implements OauthHandler {
  @Override
  public void setKey(String key) {

  }

  @Override
  public void setSecret(String secret) {

  }

  @Override
  public OauthResponse login(String access_token, String openid) {
    GitHubUser result = null;

    Gson gson = new Gson();

    Connection info = HttpConnection.connect("https://api.github.com/user");

    info.data("access_token", access_token);
    info.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
    String infojson;
    try {
      infojson = info.execute().body();
      result = gson.fromJson(infojson, GitHubUser.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  @Override
  public TokenResponse getToken(String code) {
    return null;
  }
}
