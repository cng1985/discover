package com.haoxuer.discover.user.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

/**
 * Created by ada on 2017/6/29.
 */
public class UserLoginCodeRequest extends RequestUserTokenObject {

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 验证码
     */
    private String code;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
