package com.haoxuer.discover.user.data.dao.impl;

import com.haoxuer.discover.data.enums.StoreState;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.user.data.dao.UserLoginLogDao;
import com.haoxuer.discover.user.data.entity.UserLoginLog;

/**
 * Created by imake on 2021年05月22日10:49:09.
 */
@Repository

public class UserLoginLogDaoImpl extends CriteriaDaoImpl<UserLoginLog, Long> implements UserLoginLogDao {

    @Override
    public UserLoginLog findById(Long id) {
        if (id == null) {
            return null;
        }
        return get(id);
    }

    @Override
    public UserLoginLog save(UserLoginLog bean) {
        bean.setStoreState(StoreState.normal);
        getSession().save(bean);
        return bean;
    }

    @Override
    public UserLoginLog deleteById(Long id) {
        UserLoginLog entity = super.get(id);
        if (entity != null) {
            entity.setStoreState(StoreState.recycle);
        }
        return entity;
    }

    @Override
    protected Class<UserLoginLog> getEntityClass() {
        return UserLoginLog.class;
    }

    @Autowired
    public void setSuperSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}