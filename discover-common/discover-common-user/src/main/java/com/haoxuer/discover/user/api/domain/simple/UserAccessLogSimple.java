package com.haoxuer.discover.user.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年12月04日16:19:12.
*/
@Data
public class UserAccessLogSimple implements Serializable {

    private Long id;

     private Long consumeTime;
     private Long creator;
     private String ip;
     private String creatorName;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String url;


}
