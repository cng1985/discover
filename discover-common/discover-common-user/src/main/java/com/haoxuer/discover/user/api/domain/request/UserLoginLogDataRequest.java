package com.haoxuer.discover.user.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.user.data.enums.LoginState;
import java.util.Date;

/**
*
* Created by imake on 2021年05月22日10:49:09.
*/

@Data
public class UserLoginLogDataRequest extends BaseRequest {

    private Long id;

     private String note;

     private String password;

     private String ip;

     private String client;

     private Date lastDate;

     private LoginState state;

     private Long user;

     private Date addDate;

     private String account;


}