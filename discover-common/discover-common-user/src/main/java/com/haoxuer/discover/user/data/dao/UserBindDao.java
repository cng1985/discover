package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.data.entity.UserBind;
import com.haoxuer.discover.user.data.enums.BindType;

/**
 * Created by imake on 2018年10月31日10:33:01.
 */
public interface UserBindDao extends BaseDao<UserBind, Long> {

  UserBind findById(Long id);

  UserBind save(UserBind bean);

  UserBind updateByUpdater(Updater<UserBind> updater);

  UserBind deleteById(Long id);


  UserBind findByType(String no, BindType type);

  UserBind findByUser(Long user, BindType type);

  /**
   * 根据用户名查找账号.
   *
   * @param username
   * @return
   */
  UserBind findByUserName(String username);

  /**
   * 根据账号查询绑定信息
   * @param name
   * @return
   */
  UserBind findByName(String name);

  /**
   * 根据手机号查找账号.
   *
   * @param phone
   * @return
   */
  UserBind findByPhone(String phone);

  /**
   * 根据电子邮箱查找账号.
   *
   * @param email
   * @return
   */
  UserBind findByEmail(String email);

  /**
   * 检查某个账号是否注册过
   * @param no
   * @param type
   * @return
   */
  ResponseObject checkNo(String no, BindType type);

}