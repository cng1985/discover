package com.haoxuer.discover.user.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;

import java.io.Serializable;

/**
 * Created by imake on 2017年07月21日16:04:29.
 */
public class UserOauthTokenSo implements Serializable {

  @Search(name = "user.name", operator = Filter.Operator.like)
  private String name;


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
