package com.haoxuer.discover.user.data.request;

import com.haoxuer.discover.user.data.enums.BindType;

import java.io.Serializable;

public class UserRegisterRequest implements Serializable {

  private String no;

  private String password;

  private BindType bindType;

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public BindType getBindType() {
    return bindType;
  }

  public void setBindType(BindType bindType) {
    this.bindType = bindType;
  }
}
