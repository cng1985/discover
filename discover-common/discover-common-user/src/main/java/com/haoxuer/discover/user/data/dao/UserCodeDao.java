package com.haoxuer.discover.user.data.dao;


import com.haoxuer.discover.user.data.entity.UserCode;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2018年02月23日16:25:54.
 */
public interface UserCodeDao extends BaseDao<UserCode, Long> {
  
  UserCode findById(Long id);
  
  UserCode save(UserCode bean);
  
  UserCode updateByUpdater(Updater<UserCode> updater);
  
  UserCode deleteById(Long id);
  
  UserCode findByCode(String code);
  
}