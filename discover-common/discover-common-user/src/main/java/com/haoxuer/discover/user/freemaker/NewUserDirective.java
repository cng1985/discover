package com.haoxuer.discover.user.freemaker;

import com.haoxuer.discover.user.utils.ListUtils;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.data.service.UserInfoService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by cng19 on 2017/6/26.
 */
public class NewUserDirective extends ListDirective<UserInfo> {
  
  
  @Override
  public List<UserInfo> list() {
    
    List<UserInfo> list = userInfoService.list(0, getInt("size"), null, ListUtils.list(Order.desc("id")));
    return list;
  }
  
  @Autowired
  UserInfoService userInfoService;
}
