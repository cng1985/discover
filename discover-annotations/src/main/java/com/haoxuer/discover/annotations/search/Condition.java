package com.haoxuer.discover.annotations.search;

/**条件.
 * @author 陈联高
 * @version 1.01 2017年02月114日
 */
public enum Condition {
  AND, OR
}
