package com.haoxuer.discover.plug.getui;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.LinkTemplate;
import com.haoxuer.discover.plug.api.PushPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.vo.PushBack;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component("geTuiPlug")
public class GeTuiPlug extends PushPlugin {

  private static String url = "http://sdk.open.api.igexin.com/apiex.htm";


  @Override
  public PushBack pushAll(String msg, Map<String, String> keys) {
    PushBack result = new PushBack();
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String appId = pluginConfig.getAttribute("appId");
      String appKey = pluginConfig.getAttribute("appKey");
      String masterSecret = pluginConfig.getAttribute("masterSecret");
      IGtPush push = new IGtPush(url, appKey, masterSecret);

      // 定义"点击链接打开通知模板"，并设置标题、内容、链接
      LinkTemplate template = new LinkTemplate();
      template.setAppId(appId);
      template.setAppkey(appKey);
      template.setTitle(keys.get("title"));
      template.setText(msg);
      template.setUrl(keys.get("url"));

      List<String> appIds = new ArrayList<String>();
      appIds.add(appId);

      // 定义"AppMessage"类型消息对象，设置消息内容模板、发送的目标App列表、是否支持离线发送、以及离线消息有效期(单位毫秒)
      AppMessage message = new AppMessage();
      message.setData(template);
      message.setAppIdList(appIds);
      message.setOffline(true);
      message.setOfflineExpireTime(1000 * 600);

      IPushResult ret = push.pushMessageToApp(message);
    }
    return result;
  }

  @Override
  public PushBack pushToSingleDevice(String chanel, String msg, Map<String, String> keys) {
    PushBack result = new PushBack();
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String appId = pluginConfig.getAttribute("appId");
      String appKey = pluginConfig.getAttribute("appKey");
      String masterSecret = pluginConfig.getAttribute("masterSecret");

      IGtPush push = new IGtPush(url, appKey, masterSecret);
      LinkTemplate template = new LinkTemplate();
      template.setAppId(appId);
      template.setAppkey(appKey);
      template.setTitle(keys.get("title"));
      template.setText(msg);
      template.setUrl(keys.get("url"));
      SingleMessage message = new SingleMessage();
      message.setOffline(true);
      // 离线有效时间，单位为毫秒，可选
      message.setOfflineExpireTime(24 * 3600 * 1000);
      message.setData(template);
      // 可选，1为wifi，0为不限制网络环境。根据手机处于的网络情况，决定是否下发
      message.setPushNetWorkType(0);
      Target target = new Target();
      target.setAppId(appId);
      target.setClientId(chanel);
      //target.setAlias(Alias);
      IPushResult ret = null;
      try {
        ret = push.pushMessageToSingle(message, target);
      } catch (RequestException e) {
        e.printStackTrace();
        ret = push.pushMessageToSingle(message, target, e.getRequestId());
      }
      if (ret != null) {
        System.out.println(ret.getResponse().toString());
      } else {
        System.out.println("服务器响应异常");
      }
    }
    return result;
  }

  @Override
  public PushBack pushToTag(String tag, String msg, Map<String, String> keys) {
    return null;
  }

  @Override
  public PushBack pushToChannels(String[] chanels, String msg, Map<String, String> keys) {
    return null;
  }

  @Override
  public String getName() {
    return "个推推送插件";
  }

  @Override
  public String getVersion() {
    return "1.01";
  }


  @Override
  public String viewName() {
    return "getui";
  }
}
