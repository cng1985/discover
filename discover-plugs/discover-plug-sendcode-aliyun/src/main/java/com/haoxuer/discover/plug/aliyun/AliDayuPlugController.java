/*
 * 
 * 
 * 
 */
package com.haoxuer.discover.plug.aliyun;

import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Controller - 支付宝(双接口)
 */
@Controller("aliDayuPlugController")
@RequestMapping("/admin/plugin_sendcode/aliyun")
public class AliDayuPlugController extends PlugTemplateController {

    @Resource(name = "aliSmsPlugin")
    private AliSmsPlugin dayuPlug;

    @Resource(name = "pluginConfigServiceImpl")
    private PluginConfigService pluginConfigService;
    
    
    @Override
    public PluginConfig getPluginConfig() {
        return dayuPlug.getPluginConfig();
    }
    
    @Override
    public IPlugin getPlug() {
        return dayuPlug;
    }
    
    @Override
    public PluginConfigService getPluginConfigService() {
        return pluginConfigService;
    }
    
    @Override
    public String getView() {
        return "/admin/plugin_sendcode";
    }
    
    @Override
    public String getSettingView() {
        return "aliyun";
    }
}