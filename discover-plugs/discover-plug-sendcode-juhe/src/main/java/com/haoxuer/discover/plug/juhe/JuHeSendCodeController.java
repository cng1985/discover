package com.haoxuer.discover.plug.juhe;


import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/admin/plugin_sendcode/juhesendcode")
public class JuHeSendCodeController extends PlugTemplateController {
  
  @Resource(name = "juHeSendCodePlug")
  private JuHeSendCodePlug juHeSendCodePlug;
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  @Override
  public PluginConfig getPluginConfig() {
    return juHeSendCodePlug.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return juHeSendCodePlug;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_sendcode";
  }
  
  @Override
  public String getSettingView() {
    return "juhe";
  }
}
