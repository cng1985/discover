package com.haoxuer.discover.plug.juhe;

import com.haoxuer.discover.plug.api.SendCodePlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.rest.base.ResponseObject;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.net.URLDecoder;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Map;


@Component("juHeSendCodePlug")
public class JuHeSendCodePlug extends SendCodePlugin {
  
  public String encode(String value){
    String result = new String();
    try {
      result=URLEncoder.encode(value, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return result;
  }
  @Override
  public ResponseObject sendCode(Map<String, String> params) {
    ResponseObject result = new ResponseObject();
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String tpl_value = params.get("tpl_value");
      String template = params.get("template");
      String code = params.get("code");
      String phone = params.get("phone");

      if (template.equals("failcheck")){
        String tpl = "#user#=" + phone+"&" + "#note#=" + params.get("note");
        tpl_value=encode(tpl);
      }
      else if (template.equals("sendpassword")){
        String tpl = "#user#=" + phone+"&" + "#password#=" + "123456";
        tpl_value=encode(tpl);
      }else{
        tpl_value="#code#=" + code;
      }
      
      String appkey = pluginConfig.getAttribute("appkey");
      String temp = pluginConfig.getAttribute(template);
      
      HttpResponse response = HttpRequest
          .get("http://v.juhe.cn/sms/send")
          .query("mobile", phone)
          .query("tpl_id", temp)
          .query("tpl_value", tpl_value)
          .query("key", appkey)
          .query("dtype", "json")
          .send();
      String back = response.body();
      JSONObject object = new JSONObject(back);
      if (object.getInt("error_code") == 0) {
        return result;
      }else {
        result.setCode(301);
        result.setMsg("发送短信失败");
        return result;
      }
    } else {
      result.setCode(-1);
      result.setMsg("配置异常");
    }
    
    return result;
  }
  
  @Override
  public String getName() {
    return "聚合发送短信接口";
  }
  
  @Override
  public String getVersion() {
    return "1.01";
  }
  
  @Override
  public String getAuthor() {
    return "ada";
  }
  

  @Override
  public String viewName() {
    return "juhesendcode";
  }

  public static void main(String[] args) {
    String keyWord = URLDecoder.decode("%23code%23%3D1234%26%23company%23%3D%E8%81%9A%E5%90%88%E6%95%B0%E6%8D%AE", Charset.defaultCharset());
    try {
      System.out.println(keyWord);
      System.out.println(URLEncoder.encode("#code#=1234&#company#=聚合数据", "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }
}
