package com.haoxuer.discover.plug.weiapp;

import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;


@Controller("weiAppPushController")
@RequestMapping("/admin/plugin_proxy/weiapp")
public class WeiAppPushController  extends PlugTemplateController {

  @Resource(name = "weiAppPushPlug")
  private WeiAppPushPlug weiAppPushPlug;

  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;


  @Override
  public PluginConfig getPluginConfig() {
    return weiAppPushPlug.getPluginConfig();
  }

  @Override
  public IPlugin getPlug() {
    return weiAppPushPlug;
  }

  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }

  @Override
  public String getView() {
    return "/admin/plugin_proxy";
  }

  @Override
  public String getSettingView() {
    return "weiapp";
  }
}
