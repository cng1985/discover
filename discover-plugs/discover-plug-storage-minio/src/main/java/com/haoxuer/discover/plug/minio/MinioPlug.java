package com.haoxuer.discover.plug.minio;


import com.haoxuer.discover.plug.api.StoragePlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.file.FilePlugin;
import com.haoxuer.discover.plug.data.vo.FileInfo;
import com.haoxuer.discover.plug.utils.OS;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component("minioPlug")
public class MinioPlug extends StoragePlugin {
    @Override
    public String getName() {
        return "minio";
    }

    @Override
    public String getVersion() {
        return "1.0.1";
    }

    @Override
    public String viewName() {
        return "minio";
    }

    @Override
    public void upload(String path, File file, String contentType) {
        PluginConfig pluginConfig = getPluginConfig();
        if (pluginConfig != null) {
            String disk = pluginConfig.getAttribute("disk");
            String server = pluginConfig.getAttribute("server");
            String accessKey = pluginConfig.getAttribute("accessKey");
            String secretKey = pluginConfig.getAttribute("secretKey");
            String bucket = pluginConfig.getAttribute("bucket");
            if (bucket==null){
                bucket="pic";
            }

            File destFile = new File(disk + path);
            try {
                FileUtils.moveFile(file, destFile);
                if (!OS.isWindows()) {
                    Runtime.getRuntime().exec("chmod 444 " + destFile.getPath());
                    Runtime.getRuntime().exec("chmod 777 " + destFile.getParentFile().getPath());
                }

                MinioClient minioClient =
                        MinioClient.builder()
                                .endpoint(server)
                                .credentials(accessKey, secretKey)
                                .build();

                boolean found =
                        minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucket).build());
                if (!found) {
                    minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
                }
                minioClient.uploadObject(
                        UploadObjectArgs.builder()
                                .bucket(bucket)
                                .object(path)
                                .filename(destFile.getAbsolutePath())
                                .build());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getUrl(String path) {
        PluginConfig pluginConfig = getPluginConfig();
        if (pluginConfig != null) {
            String url = pluginConfig.getAttribute("url");
            return url + path;
        }
        return path;
    }

    @Override
    public List<FileInfo> browser(String path) {
        return null;
    }
}
