package com.haoxuer.discover.plug.minio;


import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/admin/plugin_storage/minio")
public class MinioController extends PlugTemplateController {
  
  @Resource(name = "minioPlug")
  private MinioPlug minioPlug;
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  @Override
  public PluginConfig getPluginConfig() {
    return minioPlug.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return minioPlug;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_storage";
  }
  
  @Override
  public String getSettingView() {
    return "minio";
  }
}
