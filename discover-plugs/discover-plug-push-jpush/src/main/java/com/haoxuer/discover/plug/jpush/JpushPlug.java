package com.haoxuer.discover.plug.jpush;


import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import com.haoxuer.discover.plug.api.PushPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.vo.PushBack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

import static cn.jpush.api.push.model.notification.PlatformNotification.ALERT;


@Component("jpushPlug")
public class JpushPlug extends PushPlugin {

    private Logger logger = LoggerFactory.getLogger("plug_push");

    public static PushPayload buildPushObject_all_all_alert() {
        return PushPayload.alertAll(ALERT);
    }

    @Override
    public PushBack pushAll(String msg, Map<String, String> map) {
        PushBack result = new PushBack();
        PluginConfig pluginConfig = getPluginConfig();
        if (pluginConfig != null) {
            JPushClient jpushClient = getClient(pluginConfig);
            PushPayload payload = PushPayload.newBuilder()
                .setPlatform(Platform.all())
                .setAudience(Audience.all())
                .setMessage(Message.content(msg))
                .build();
            try {
                PushResult back = jpushClient.sendPush(payload);
                logger.info("Got result - " + result);

            } catch (APIConnectionException e) {
                // Connection error, should retry later
                logger.error("Connection error, should retry later", e);

            } catch (APIRequestException e) {
                // Should review the error, and fix the request
                logger.error("Should review the error, and fix the request", e);
                logger.info("HTTP Status: " + e.getStatus());
                logger.info("Error Code: " + e.getErrorCode());
                logger.info("Error Message: " + e.getErrorMessage());
            }
        }
        return result;
    }

    private JPushClient getClient(PluginConfig pluginConfig) {
        String appKey = pluginConfig.getAttribute("appKey");
        String masterSecret = pluginConfig.getAttribute("masterSecret");
        return new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
    }

    @Override
    public PushBack pushToSingleDevice(String chanel, String msg, Map<String, String> map) {
        PushBack result = new PushBack();
        PluginConfig pluginConfig = getPluginConfig();
        if (pluginConfig != null) {
            JPushClient jpushClient = getClient(pluginConfig);
            PushPayload payload = buildPushObject_all_all_alert();

            PushPayload.newBuilder()
                .setPlatform(Platform.all())
                .setAudience(Audience.alias(chanel))
                .setNotification(Notification.newBuilder()
                    .addPlatformNotification(IosNotification.newBuilder()
                        .setAlert(ALERT)
                        .setBadge(5)
                        .setSound("happy")
                        .addExtra("from", "JPush")
                        .build())
                    .build())
                .setMessage(Message.content(msg))
                .setOptions(Options.newBuilder()
                    .setApnsProduction(true)
                    .build())
                .build();
            try {
                PushResult back = jpushClient.sendPush(payload);
                logger.info("Got result - " + result);

            } catch (APIConnectionException e) {
                // Connection error, should retry later
                logger.error("Connection error, should retry later", e);

            } catch (APIRequestException e) {
                // Should review the error, and fix the request
                logger.error("Should review the error, and fix the request", e);
                logger.info("HTTP Status: " + e.getStatus());
                logger.info("Error Code: " + e.getErrorCode());
                logger.info("Error Message: " + e.getErrorMessage());
            }
        }
        return result;
    }

    @Override
    public PushBack pushToTag(String s, String s1, Map<String, String> map) {
        return null;
    }

    @Override
    public PushBack pushToChannels(String[] strings, String s, Map<String, String> map) {
        return null;
    }

    @Override
    public String getName() {
        return "极光推送";
    }

    @Override
    public String getVersion() {
        return "1.0.1";
    }

    @Override
    public String viewName() {
        return "jpush";
    }
}
