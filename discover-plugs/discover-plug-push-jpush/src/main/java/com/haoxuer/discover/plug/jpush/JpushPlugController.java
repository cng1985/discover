/*
 * 
 * 
 * 
 */
package com.haoxuer.discover.plug.jpush;

import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Controller - 支付宝(双接口)
 */
@Controller("geTuiPlugController")
@RequestMapping("/admin/plugin_push/jpush")
public class JpushPlugController extends PlugTemplateController {

    @Resource(name = "jpushPlug")
    private JpushPlug geTuiPlug;

    @Resource(name = "pluginConfigServiceImpl")
    private PluginConfigService pluginConfigService;
    
    
    @Override
    public PluginConfig getPluginConfig() {
        return geTuiPlug.getPluginConfig();
    }
    
    @Override
    public IPlugin getPlug() {
        return geTuiPlug;
    }
    
    @Override
    public PluginConfigService getPluginConfigService() {
        return pluginConfigService;
    }
    
    @Override
    public String getView() {
        return "/admin/plugin_push";
    }
    
    @Override
    public String getSettingView() {
        return "jpush";
    }
}