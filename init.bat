gpg --keyserver hkp://pgp.mit.edu:11371 --send-keys   CD4E8687445B382B428D67392D5A009342AD31BB
gpg --keyserver hkp://keyserver.ubuntu.com:11371 --send-keys   CD4E8687445B382B428D67392D5A009342AD31BB
gpg --keyserver hkp://pool.sks-keyservers.net:11371 --send-keys   CD4E8687445B382B428D67392D5A009342AD31BB

gpg --keyserver hkp://pgp.mit.edu:11371 --recv-keys  2D5A009342AD31BB
gpg --keyserver hkp://keyserver.ubuntu.com:11371 --recv-keys  2D5A009342AD31BB
gpg --keyserver hkp://pool.sks-keyservers.net:11371 --recv-keys  2D5A009342AD31BB

gpg --keyserver hkp://pgp.mit.edu --recv-keys  f9b83708e4ae5ea3
gpg --keyserver hkp://keyserver.ubuntu.com --recv-keys  F9B83708E4AE5EA3
gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys  f9b83708e4ae5ea3
gpg --keyserver http://pool.sks-keyservers.net:11371 --recv-keys  f9b83708e4ae5ea3
gpg --keyserver hkp://pool.sks-keyservers.net:11371 --search-key b11edae6d2d564d4
gpg --list-keys
gpg --delete-secret-keys 1E2A512328B064898DCBE951F9B83708E4AE5EA3
gpg --delete-key  1E2A512328B064898DCBE951F9B83708E4AE5EA3
gpg –-gen-key
gpg --armor --output public-key.txt --export 9D91FFA409D071B25A0509E2B11EDAE6D2D564D4

mvn dependency:tree