package com.haoxuer.discover.views.rss;


import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Item;
import org.springframework.web.servlet.view.feed.AbstractRssFeedView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class RssViewer extends AbstractRssFeedView {

  @Override
  protected void buildFeedMetadata(Map<String, Object> model, Channel feed,
                                   HttpServletRequest request) {

    feed.setTitle(model.get("title") + "");
    feed.setDescription(model.get("description") + "");
    feed.setLink(model.get("link") + "");
    super.buildFeedMetadata(model, feed, request);
  }


  @Override
  protected List<Item> buildFeedItems(Map<String, Object> model,
                                      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    List<Item> items = (List<Item>) model.get("items");
    return items;
  }

}