package com.haoxuer.discover.workflow.data.dao.impl;

import com.haoxuer.discover.workflow.data.dao.FlowDefineDao;
import com.haoxuer.discover.workflow.data.entity.FlowDefine;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年01月08日14:52:00.
 */
@Repository

public class FlowDefineDaoImpl extends CriteriaDaoImpl<FlowDefine, Long> implements FlowDefineDao {

  @Override
  public FlowDefine findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public FlowDefine save(FlowDefine bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public FlowDefine deleteById(Long id) {
    FlowDefine entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<FlowDefine> getEntityClass() {
    return FlowDefine.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}