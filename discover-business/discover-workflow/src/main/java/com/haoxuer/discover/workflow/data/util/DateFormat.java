package com.haoxuer.discover.workflow.data.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ada on 2016/11/4.
 */
public class DateFormat {
  private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
  
  public static String format(Date date) {
    return simpleDateFormat.format(date);
  }
  
  public static String formatTime(Date date) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return df.format(date);
  }
}
