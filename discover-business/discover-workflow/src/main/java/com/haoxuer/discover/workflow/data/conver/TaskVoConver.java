package com.haoxuer.discover.workflow.data.conver;

import com.haoxuer.discover.workflow.data.vo.TaskVo;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.workflow.data.util.DateFormat;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;

/**
 * @author 陈联高
 * @version 1.01 2017年03月108日
 */
public class TaskVoConver implements Conver<TaskVo, Task> {
  protected TaskService taskService;
  
  public TaskVoConver(TaskService taskService) {
    this.taskService = taskService;
  }
  
  @Override
  public TaskVo conver(Task task) {
    TaskVo result = new TaskVo();
    result.setCatalog(task.getCategory());
    result.setId(task.getId());
    result.setProcessDefinitionId(task.getProcessDefinitionId());
    result.setName(task.getName());
    result.setFlowName("" + taskService.getVariable(task.getId(), "name"));
    result.setUpdateUrl("" + taskService.getVariable(task.getId(), "updateurl"));
    result.setOid("" + taskService.getVariable(task.getId(), "oid"));
    result.setAddDate(DateFormat.format(task.getCreateTime()));
    return result;
  }
}
