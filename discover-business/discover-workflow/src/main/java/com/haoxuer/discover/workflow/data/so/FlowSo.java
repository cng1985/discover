package com.haoxuer.discover.workflow.data.so;

import java.io.Serializable;

/**
 * Created by ada on 2016/12/9.
 */
public class FlowSo implements Serializable {
  
  private String name;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
}
