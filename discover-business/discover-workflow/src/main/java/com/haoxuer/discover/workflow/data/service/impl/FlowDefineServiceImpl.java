package com.haoxuer.discover.workflow.data.service.impl;

import com.haoxuer.discover.workflow.data.service.FlowDefineService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.workflow.data.dao.FlowDefineDao;
import com.haoxuer.discover.workflow.data.entity.FlowDefine;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年01月08日14:52:01.
 */


@Scope("prototype")
@Service
@Transactional
public class FlowDefineServiceImpl implements FlowDefineService {
  
  private FlowDefineDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public FlowDefine findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public FlowDefine save(FlowDefine bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public FlowDefine update(FlowDefine bean) {
    Updater<FlowDefine> updater = new Updater<FlowDefine>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public FlowDefine deleteById(Long id) {
    FlowDefine bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public FlowDefine[] deleteByIds(Long[] ids) {
    FlowDefine[] beans = new FlowDefine[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(FlowDefineDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<FlowDefine> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<FlowDefine> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<FlowDefine> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}