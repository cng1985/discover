package com.haoxuer.discover.workflow.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.workflow.data.entity.FlowDefine;
import java.util.List;

/**
 * Created by imake on 2018年01月08日14:52:00.
 */
public interface FlowDefineService {
  
  FlowDefine findById(Long id);
  
  FlowDefine save(FlowDefine bean);
  
  FlowDefine update(FlowDefine bean);
  
  FlowDefine deleteById(Long id);
  
  FlowDefine[] deleteByIds(Long[] ids);
  
  Page<FlowDefine> page(Pageable pageable);
  
  Page<FlowDefine> page(Pageable pageable, Object search);
  
  
  List<FlowDefine> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}