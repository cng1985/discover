package com.haoxuer.discover.workflow.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.workflow.data.entity.FlowDefine;

/**
 * Created by imake on 2018年01月08日14:52:00.
 */
public interface FlowDefineDao extends BaseDao<FlowDefine, Long> {
  
  FlowDefine findById(Long id);
  
  FlowDefine save(FlowDefine bean);
  
  FlowDefine updateByUpdater(Updater<FlowDefine> updater);
  
  FlowDefine deleteById(Long id);
}