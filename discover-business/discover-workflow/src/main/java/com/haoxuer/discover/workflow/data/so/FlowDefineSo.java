package com.haoxuer.discover.workflow.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import java.io.Serializable;

/**
 * Created by imake on 2018年01月08日14:52:01.
 */
public class FlowDefineSo implements Serializable {

  @Search(name = "name", operator = Filter.Operator.like)
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
