package com.haoxuer.discover.workflow.data.vo;

import java.io.Serializable;
import java.util.Date;

public class TaskHistoryVo implements Serializable {
  
  private String user;
  
  private String note;
  
  private String state;
  
  private Date addDate;
  
  
  public Date getAddDate() {
    return addDate;
  }
  
  public void setAddDate(Date addDate) {
    this.addDate = addDate;
  }
  
  public String getUser() {
    return user;
  }
  
  public void setUser(String user) {
    this.user = user;
  }
  
  public String getNote() {
    return note;
  }
  
  public void setNote(String note) {
    this.note = note;
  }
  
  public String getState() {
    return state;
  }
  
  public void setState(String state) {
    this.state = state;
  }
  
  @Override
  public String toString() {
    return "TaskHistoryVo [user=" + user + ", note=" + note + ", state=" + state + "]";
  }
  
  
}
