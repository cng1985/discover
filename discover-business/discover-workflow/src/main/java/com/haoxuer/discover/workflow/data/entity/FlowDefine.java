package com.haoxuer.discover.workflow.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "flow_define")
public class FlowDefine extends AbstractEntity {
  
  
  /**
   * 流程名称
   */
  private String name;
  
  
  /**
   * 流程定义xml
   */
  private String content;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getContent() {
    return content;
  }
  
  public void setContent(String content) {
    this.content = content;
  }
}
