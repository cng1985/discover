package com.haoxuer.discover.workflow.data.service;


import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.workflow.data.so.FlowSo;
import com.haoxuer.discover.workflow.data.vo.MyTaskHistoryVo;
import com.haoxuer.discover.workflow.data.vo.TaskHistoryVo;
import com.haoxuer.discover.workflow.data.vo.TaskVo;
import java.util.List;

public interface FlowService {
  
  /**
   * 查新某个人的任务
   *
   * @param username
   * @param page
   * @param so
   * @return
   */
  Page<TaskVo> page(String username, Pageable page, FlowSo so);
  
  /**
   * 查新历史记录
   *
   * @param busykey
   * @return
   */
  List<TaskHistoryVo> records(String busykey);
  
  /**
   * 查询系统中的任务
   *
   * @param page
   * @param so
   * @return
   */
  Page<TaskVo> page(Pageable page, FlowSo so);
  
  /**
   * 查询某个人以往的审批清单
   *
   * @param page
   * @param so
   * @return
   */
  Page<MyTaskHistoryVo> pageHis(Pageable page, FlowSo so);
  
}
