package com.haoxuer.discover.workflow.data.vo;

import java.io.Serializable;

/**
 * $msg$.
 *
 * @author caojingyao
 * @version 1.01 2017年05月17日
 */
public class MyTaskHistoryVo implements Serializable {
  private String name;
  
  private String processDefinitionId;
  
  private String oid;
  
  private String id;
  
  private String startTime;
  
  private String endTime;
  
  private String processInstanceId;
  
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getProcessDefinitionId() {
    return processDefinitionId;
  }
  
  public void setProcessDefinitionId(String processDefinitionId) {
    this.processDefinitionId = processDefinitionId;
  }
  
  public String getOid() {
    return oid;
  }
  
  public void setOid(String oid) {
    this.oid = oid;
  }
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getStartTime() {
    return startTime;
  }
  
  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }
  
  public String getEndTime() {
    return endTime;
  }
  
  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }
  
  public String getProcessInstanceId() {
    return processInstanceId;
  }
  
  public void setProcessInstanceId(String processInstanceId) {
    this.processInstanceId = processInstanceId;
  }
}
