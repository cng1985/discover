package com.haoxuer.discover.team.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.team.data.entity.TeamMember;

/**
* Created by imake on 2018年10月13日14:56:45.
*/
public interface TeamMemberDao extends BaseDao<TeamMember,Long>{

	 TeamMember findById(Long id);

	 TeamMember save(TeamMember bean);

	 TeamMember updateByUpdater(Updater<TeamMember> updater);

	 TeamMember deleteById(Long id);
}