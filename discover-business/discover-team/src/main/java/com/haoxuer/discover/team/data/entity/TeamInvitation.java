package com.haoxuer.discover.team.data.entity;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "bs_team_invitation")
public class TeamInvitation extends TeamEntity {

  private String name;

  private String email;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
