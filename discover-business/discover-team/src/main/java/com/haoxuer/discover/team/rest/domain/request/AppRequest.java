package com.haoxuer.discover.team.rest.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class AppRequest extends RequestUserTokenObject {

  private String formId;

  public String getFormId() {
    return formId;
  }

  public void setFormId(String formId) {
    this.formId = formId;
  }
}
