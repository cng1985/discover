package com.haoxuer.discover.team.rest.api;


import com.haoxuer.discover.team.rest.domain.list.TeamList;
import com.haoxuer.discover.team.rest.domain.request.PageRequest;
import com.haoxuer.discover.team.rest.domain.request.TeamCreateRequest;
import com.haoxuer.discover.team.rest.domain.response.TeamResponse;

/**
 * 团队接口
 */
public interface TeamApi {

  TeamResponse create(TeamCreateRequest request);


  TeamList my(PageRequest request);
}
