package com.haoxuer.discover.team.rest.resources;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.team.data.dao.TeamDao;
import com.haoxuer.discover.team.data.dao.TeamMemberDao;
import com.haoxuer.discover.team.data.entity.Team;
import com.haoxuer.discover.team.data.entity.TeamMember;
import com.haoxuer.discover.team.data.enums.TeamMemberType;
import com.haoxuer.discover.team.rest.api.TeamApi;
import com.haoxuer.discover.team.rest.conver.PageableConver;
import com.haoxuer.discover.team.rest.conver.TeamMemberSimpleConver;
import com.haoxuer.discover.team.rest.conver.TeamResponseConver;
import com.haoxuer.discover.team.rest.domain.list.TeamList;
import com.haoxuer.discover.team.rest.domain.request.PageRequest;
import com.haoxuer.discover.team.rest.domain.request.TeamCreateRequest;
import com.haoxuer.discover.team.rest.domain.response.TeamResponse;
import com.haoxuer.discover.team.rest.domain.simple.TeamSimple;
import com.haoxuer.discover.team.rest.listeners.OnTeamCreate;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.haoxuer.discover.user.utils.ConverResourceUtils;
import com.haoxuer.discover.user.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@Transactional
@Component
public class TeamResource implements TeamApi {

  @Autowired
  private TeamDao teamDao;

  @Autowired
  private TeamMemberDao teamMemberDao;

  @Autowired
  private UserInfoDao memberDao;


  @Resource
  private List<OnTeamCreate> creates = new ArrayList<>();

  @Override
  public TeamResponse create(TeamCreateRequest request) {
    TeamResponse result = new TeamResponse();

    Long id = UserUtils.getMember(request.getUserToken());
    User member = User.fromId(id);
    if (member == null) {
      result.setMsg("该用户不存在!");
      result.setCode(-101);
      return result;
    }
    if (StringUtils.isEmpty(request.getName())) {
      result.setMsg("团队名称不能为空!");
      result.setCode(-102);
      return result;
    }
    Team team = new Team();
    team.setName(request.getName());
    teamDao.save(team);

    TeamMember teamMember = new TeamMember();
    teamMember.setTeam(team);
    teamMember.setMember(member);
    teamMember.setMemberType(TeamMemberType.creater);
    teamMemberDao.save(teamMember);
    if (creates != null) {
      for (OnTeamCreate create : creates) {
        create.create(team);
      }
    }

    result = new TeamResponseConver().conver(team);
    return result;
  }

  @Override
  public TeamList my(PageRequest request) {
    TeamList result = new TeamList();
    Long mid = UserUtils.getMember(request.getUserToken());
    Pageable pageable = new PageableConver().conver(request);
    pageable.getFilters().add(Filter.eq("member.id", mid));
    pageable.getOrders().add(Order.desc("id"));

    List<TeamMember> members = teamMemberDao.list(pageable);
    List<TeamSimple> simples = ConverResourceUtils.coverList(members, new TeamMemberSimpleConver());
    result.setList(simples);
    return result;
  }

}
