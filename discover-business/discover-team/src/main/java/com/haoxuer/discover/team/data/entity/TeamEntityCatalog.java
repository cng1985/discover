package com.haoxuer.discover.team.data.entity;

import com.haoxuer.discover.data.entity.CatalogEntity;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class TeamEntityCatalog extends CatalogEntity {

  @ManyToOne(fetch = FetchType.LAZY)
  private Team team;

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }
}
