package com.haoxuer.discover.team.controller.rest;

import com.haoxuer.discover.team.rest.api.TeamApi;
import com.haoxuer.discover.team.rest.domain.list.TeamList;
import com.haoxuer.discover.team.rest.domain.request.PageRequest;
import com.haoxuer.discover.team.rest.domain.request.TeamCreateRequest;
import com.haoxuer.discover.team.rest.domain.response.TeamResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest")
@RestController
public class TeamRestController {

  @RequestMapping("/team/create")
  public TeamResponse create(TeamCreateRequest request) {
    return api.create(request);
  }

  @RequestMapping("/team/my")
  public TeamList my(PageRequest request) {
    return api.my(request);
  }

  @Autowired
  private TeamApi api;
}
