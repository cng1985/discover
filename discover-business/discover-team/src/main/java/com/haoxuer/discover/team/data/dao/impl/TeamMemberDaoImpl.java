package com.haoxuer.discover.team.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.team.data.dao.TeamMemberDao;
import com.haoxuer.discover.team.data.entity.TeamMember;

/**
* Created by imake on 2018年10月13日14:56:45.
*/
@Repository

public class TeamMemberDaoImpl extends CriteriaDaoImpl<TeamMember, Long> implements TeamMemberDao {

	@Override
	public TeamMember findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public TeamMember save(TeamMember bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public TeamMember deleteById(Long id) {
		TeamMember entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<TeamMember> getEntityClass() {
		return TeamMember.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}