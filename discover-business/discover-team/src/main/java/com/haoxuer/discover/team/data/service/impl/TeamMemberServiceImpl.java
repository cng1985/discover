package com.haoxuer.discover.team.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.team.data.dao.TeamMemberDao;
import com.haoxuer.discover.team.data.entity.TeamMember;
import com.haoxuer.discover.team.data.service.TeamMemberService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年10月13日14:56:45.
*/


@Scope("prototype")
@Service
@Transactional
public class TeamMemberServiceImpl implements TeamMemberService {

	private TeamMemberDao dao;


	@Override
	@Transactional(readOnly = true)
	public TeamMember findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public TeamMember save(TeamMember bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public TeamMember update(TeamMember bean) {
		Updater<TeamMember> updater = new Updater<TeamMember>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public TeamMember deleteById(Long id) {
		TeamMember bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public TeamMember[] deleteByIds(Long[] ids) {
		TeamMember[] beans = new TeamMember[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TeamMemberDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<TeamMember> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<TeamMember> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<TeamMember> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}