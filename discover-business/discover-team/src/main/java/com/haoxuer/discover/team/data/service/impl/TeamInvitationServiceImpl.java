package com.haoxuer.discover.team.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.team.data.dao.TeamInvitationDao;
import com.haoxuer.discover.team.data.entity.TeamInvitation;
import com.haoxuer.discover.team.data.service.TeamInvitationService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年10月13日14:56:45.
*/


@Scope("prototype")
@Service
@Transactional
public class TeamInvitationServiceImpl implements TeamInvitationService {

	private TeamInvitationDao dao;


	@Override
	@Transactional(readOnly = true)
	public TeamInvitation findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public TeamInvitation save(TeamInvitation bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public TeamInvitation update(TeamInvitation bean) {
		Updater<TeamInvitation> updater = new Updater<TeamInvitation>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public TeamInvitation deleteById(Long id) {
		TeamInvitation bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public TeamInvitation[] deleteByIds(Long[] ids) {
		TeamInvitation[] beans = new TeamInvitation[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TeamInvitationDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<TeamInvitation> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<TeamInvitation> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<TeamInvitation> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}