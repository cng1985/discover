package com.haoxuer.discover.team.data.app;

import com.haoxuer.discover.team.controller.admin.Controllers;
import com.haoxuer.discover.team.data.entity.Team;
import com.haoxuer.discover.team.data.entity.TeamInvitation;
import com.haoxuer.discover.team.data.entity.TeamMember;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println(Controllers.class.getResource("/").getPath());
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);
        make.setAction(Controllers.class.getPackage().getName());
        File view = new File("D:\\mvnspace\\adminstore\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(Team.class);
        cs.add(TeamMember.class);
        cs.add(TeamInvitation.class);



        make.setDao(true);
        make.setService(true);
        make.setView(false);
        make.setAction(false);
        make.makes(cs);


    }
}
