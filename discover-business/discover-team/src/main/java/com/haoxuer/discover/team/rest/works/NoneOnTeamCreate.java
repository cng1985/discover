package com.haoxuer.discover.team.rest.works;

import com.haoxuer.discover.team.data.entity.Team;
import com.haoxuer.discover.team.rest.listeners.OnTeamCreate;
import org.springframework.stereotype.Component;

@Component
public class NoneOnTeamCreate implements OnTeamCreate {
  @Override
  public void create(Team team) {

  }
}
