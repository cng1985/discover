package com.haoxuer.discover.team.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.team.data.enums.TeamMemberType;
import com.nbsaas.codemake.annotation.FormAnnotation;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@FormAnnotation(title = "团队")
@Entity
@Table(name = "bs_team_member")
public class TeamMember extends TeamEntity {

  @ManyToOne(fetch = FetchType.LAZY)
  private User member;


  public User getMember() {
    return member;
  }

  public void setMember(User member) {
    this.member = member;
  }

  private TeamMemberType memberType;


  public TeamMemberType getMemberType() {
    return memberType;
  }

  public void setMemberType(TeamMemberType memberType) {
    this.memberType = memberType;
  }
}
