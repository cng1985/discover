package com.haoxuer.discover.team.data.service;

import com.haoxuer.discover.team.data.entity.Team;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年10月13日14:56:44.
*/
public interface TeamService {

	Team findById(Long id);

	Team save(Team bean);

	Team update(Team bean);

	Team deleteById(Long id);
	
	Team[] deleteByIds(Long[] ids);
	
	Page<Team> page(Pageable pageable);
	
	Page<Team> page(Pageable pageable, Object search);


	List<Team> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}