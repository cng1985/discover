package com.haoxuer.discover.team.data.so;

import java.io.Serializable;

/**
* Created by imake on 2018年10月13日14:56:45.
*/
public class TeamMemberSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
