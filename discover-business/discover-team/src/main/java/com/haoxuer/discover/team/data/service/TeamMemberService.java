package com.haoxuer.discover.team.data.service;

import com.haoxuer.discover.team.data.entity.TeamMember;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年10月13日14:56:45.
*/
public interface TeamMemberService {

	TeamMember findById(Long id);

	TeamMember save(TeamMember bean);

	TeamMember update(TeamMember bean);

	TeamMember deleteById(Long id);
	
	TeamMember[] deleteByIds(Long[] ids);
	
	Page<TeamMember> page(Pageable pageable);
	
	Page<TeamMember> page(Pageable pageable, Object search);


	List<TeamMember> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}