package com.haoxuer.discover.team.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.team.data.dao.TeamDao;
import com.haoxuer.discover.team.data.entity.Team;
import com.haoxuer.discover.team.data.service.TeamService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年10月13日14:56:45.
*/


@Scope("prototype")
@Service
@Transactional
public class TeamServiceImpl implements TeamService {

	private TeamDao dao;


	@Override
	@Transactional(readOnly = true)
	public Team findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public Team save(Team bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public Team update(Team bean) {
		Updater<Team> updater = new Updater<Team>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public Team deleteById(Long id) {
		Team bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public Team[] deleteByIds(Long[] ids) {
		Team[] beans = new Team[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TeamDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<Team> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<Team> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<Team> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}