package com.haoxuer.discover.team.rest.domain.request;

public class LongRequest extends AppRequest {
  
  private Long id;
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
}
