package com.haoxuer.discover.team.rest.conver;

import com.haoxuer.discover.team.data.entity.Team;
import com.haoxuer.discover.team.rest.domain.response.TeamResponse;
import com.haoxuer.discover.data.rest.core.Conver;

public class TeamResponseConver implements Conver<TeamResponse, Team> {
  @Override
  public TeamResponse conver(Team source) {
    TeamResponse result=new TeamResponse();
    result.setId(source.getId());
    result.setName(source.getName());
    return result;
  }
}
