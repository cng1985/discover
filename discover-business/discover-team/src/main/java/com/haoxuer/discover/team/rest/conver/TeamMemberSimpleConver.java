package com.haoxuer.discover.team.rest.conver;

import com.haoxuer.discover.team.data.entity.TeamMember;
import com.haoxuer.discover.team.rest.domain.simple.TeamSimple;
import com.haoxuer.discover.data.rest.core.Conver;

public class TeamMemberSimpleConver implements Conver<TeamSimple, TeamMember> {
  @Override
  public TeamSimple conver(TeamMember source) {
    TeamSimple result=new TeamSimple();
    if (source.getTeam()!=null){
      result.setId(source.getTeam().getId());
      result.setName(source.getTeam().getName());
    }
    return result;
  }
}
