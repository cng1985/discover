package com.haoxuer.discover.team.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.team.data.entity.Team;

/**
* Created by imake on 2018年10月13日14:56:44.
*/
public interface TeamDao extends BaseDao<Team,Long>{

	 Team findById(Long id);

	 Team save(Team bean);

	 Team updateByUpdater(Updater<Team> updater);

	 Team deleteById(Long id);
}