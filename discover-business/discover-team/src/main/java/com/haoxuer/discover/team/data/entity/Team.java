package com.haoxuer.discover.team.data.entity;

import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


@Data
@FormAnnotation(title = "团队")
@Entity
@Table(name = "bs_team")
public class Team extends AbstractEntity {

  /**
   * 团队介绍
   */
  private String name;

  /**
   * 团队logo
   */
  private String icon;


  /**
   * 团队描述
   */
  private String note;

  @OneToMany(fetch = FetchType.LAZY,mappedBy = "team")
  private List<TeamMember> members;

}
