package com.haoxuer.discover.team.data.service;

import com.haoxuer.discover.team.data.entity.TeamInvitation;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年10月13日14:56:45.
*/
public interface TeamInvitationService {

	TeamInvitation findById(Long id);

	TeamInvitation save(TeamInvitation bean);

	TeamInvitation update(TeamInvitation bean);

	TeamInvitation deleteById(Long id);
	
	TeamInvitation[] deleteByIds(Long[] ids);
	
	Page<TeamInvitation> page(Pageable pageable);
	
	Page<TeamInvitation> page(Pageable pageable, Object search);


	List<TeamInvitation> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}