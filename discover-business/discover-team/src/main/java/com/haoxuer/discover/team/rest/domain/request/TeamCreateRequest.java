package com.haoxuer.discover.team.rest.domain.request;

public class TeamCreateRequest extends AppRequest {

  private String name;

  private String icon;

  private String note;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
