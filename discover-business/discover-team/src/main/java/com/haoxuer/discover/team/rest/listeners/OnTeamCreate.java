package com.haoxuer.discover.team.rest.listeners;

import com.haoxuer.discover.team.data.entity.Team;

public interface OnTeamCreate {

  void create(Team team);
}
