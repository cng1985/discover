package com.haoxuer.discover.team.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.team.data.dao.TeamDao;
import com.haoxuer.discover.team.data.entity.Team;

/**
* Created by imake on 2018年10月13日14:56:44.
*/
@Repository

public class TeamDaoImpl extends CriteriaDaoImpl<Team, Long> implements TeamDao {

	@Override
	public Team findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Team save(Team bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Team deleteById(Long id) {
		Team entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Team> getEntityClass() {
		return Team.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}