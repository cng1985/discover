package com.haoxuer.discover.team.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.team.data.dao.TeamInvitationDao;
import com.haoxuer.discover.team.data.entity.TeamInvitation;

/**
* Created by imake on 2018年10月13日14:56:45.
*/
@Repository

public class TeamInvitationDaoImpl extends CriteriaDaoImpl<TeamInvitation, Long> implements TeamInvitationDao {

	@Override
	public TeamInvitation findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public TeamInvitation save(TeamInvitation bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public TeamInvitation deleteById(Long id) {
		TeamInvitation entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<TeamInvitation> getEntityClass() {
		return TeamInvitation.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}