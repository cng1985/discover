package com.haoxuer.discover.team.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.team.data.entity.TeamInvitation;

/**
* Created by imake on 2018年10月13日14:56:45.
*/
public interface TeamInvitationDao extends BaseDao<TeamInvitation,Long>{

	 TeamInvitation findById(Long id);

	 TeamInvitation save(TeamInvitation bean);

	 TeamInvitation updateByUpdater(Updater<TeamInvitation> updater);

	 TeamInvitation deleteById(Long id);
}