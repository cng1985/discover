package com.haoxuer.discover.team.rest.domain.response;

import com.haoxuer.discover.rest.base.ResponseObject;

public class TeamResponse extends ResponseObject {

  private Long id;

  private String name;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
