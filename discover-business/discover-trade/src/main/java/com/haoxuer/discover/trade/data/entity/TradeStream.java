package com.haoxuer.discover.trade.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.SearchItem;

import javax.persistence.*;
import java.math.BigDecimal;


@FormAnnotation(title = "资金流水", menu = "更新资金流水")
@Entity
@Table(name = "sys_trade_stream")
public class TradeStream extends AbstractEntity {

  @SearchItem(label = "资金账号",name = "account",key = "account.id")
  @FieldConvert
  @ManyToOne(fetch = FetchType.LAZY)
  TradeAccount account;

  @ManyToOne(fetch = FetchType.LAZY)
  TradeInfo info;

  private Integer changeType;

  /**
   * 变动前金额
   */
  @FormField(title = "变动前金额", sortNum = "2", grid = true, col = 12)
  private BigDecimal preAmount;

  /**
   * 变动后金额
   */
  @FormField(title = "变动后金额", sortNum = "2", grid = true, col = 12)
  private BigDecimal afterAmount;

  /**
   * 金额
   */
  @FormField(title = "金额", sortNum = "2", grid = true, col = 12)
  private BigDecimal amount;

  @Column(length = 100)
  @FormField(title = "备注", sortNum = "2", grid = true, col = 12)
  private String note;

  public TradeAccount getAccount() {
    return account;
  }

  public void setAccount(TradeAccount account) {
    this.account = account;
  }

  public TradeInfo getInfo() {
    return info;
  }

  public void setInfo(TradeInfo info) {
    this.info = info;
  }

  public BigDecimal getPreAmount() {
    return preAmount;
  }

  public void setPreAmount(BigDecimal preAmount) {
    this.preAmount = preAmount;
  }

  public BigDecimal getAfterAmount() {
    return afterAmount;
  }

  public void setAfterAmount(BigDecimal afterAmount) {
    this.afterAmount = afterAmount;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Integer getChangeType() {
    return changeType;
  }

  public void setChangeType(Integer changeType) {
    this.changeType = changeType;
  }
}
