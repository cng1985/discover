package com.haoxuer.discover.trade.data.request;

import com.haoxuer.discover.trade.data.others.ChangeType;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeRequest implements Serializable {

  private Long from;

  private Long to;

  private ChangeType changeType;

  private BigDecimal amount;

  private String note;

  public Long getFrom() {
    return from;
  }

  public void setFrom(Long from) {
    this.from = from;
  }

  public Long getTo() {
    return to;
  }

  public void setTo(Long to) {
    this.to = to;
  }

  public ChangeType getChangeType() {
    return changeType;
  }

  public void setChangeType(ChangeType changeType) {
    this.changeType = changeType;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
