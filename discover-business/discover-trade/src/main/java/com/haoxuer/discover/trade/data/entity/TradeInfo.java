package com.haoxuer.discover.trade.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.trade.data.enums.TradeState;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;


@Data
@FormAnnotation(title = "交易单")
@Entity
@Table(name = "sys_trade_info")
public class TradeInfo extends AbstractEntity {

  /**
   * 转出账号
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private TradeAccount from;


  /**
   * 转入账号
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private TradeAccount to;

  /**
   * 交易金额
   */
  private BigDecimal amount;


  /**
   * 交易状态
   */
  private TradeState tradeState;


}
