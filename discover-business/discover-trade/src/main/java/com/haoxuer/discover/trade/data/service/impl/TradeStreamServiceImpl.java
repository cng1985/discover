package com.haoxuer.discover.trade.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.trade.data.dao.TradeStreamDao;
import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.haoxuer.discover.trade.data.service.TradeStreamService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月10日10:02:02.
*/


@Scope("prototype")
@Service
@Transactional
public class TradeStreamServiceImpl implements TradeStreamService {

	private TradeStreamDao dao;


	@Override
	@Transactional(readOnly = true)
	public TradeStream findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public TradeStream save(TradeStream bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public TradeStream update(TradeStream bean) {
		Updater<TradeStream> updater = new Updater<TradeStream>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public TradeStream deleteById(Long id) {
		TradeStream bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public TradeStream[] deleteByIds(Long[] ids) {
		TradeStream[] beans = new TradeStream[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TradeStreamDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<TradeStream> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<TradeStream> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<TradeStream> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}