package com.haoxuer.discover.trade.api.domain.simple;


import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by BigWorld on 2020年09月21日22:28:46.
 */

@Data
public class TradeStreamSimple implements Serializable {

    private Long id;

    private BigDecimal preAmount;
    private String note;
    private Integer changeType;
    private BigDecimal afterAmount;
    private BigDecimal amount;
    private Long account;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;
}