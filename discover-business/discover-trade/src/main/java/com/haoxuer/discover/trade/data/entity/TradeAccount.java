package com.haoxuer.discover.trade.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.trade.data.enums.AccountType;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;


@FormAnnotation(title = "资金账户")
@Entity
@Table(name = "sys_trade_account")
public class TradeAccount extends AbstractEntity {
  
  
  /**
   * 总金额
   */
  @FormField(title = "总金额", sortNum = "2", grid = true, col = 12)
  private BigDecimal amount;


  /**
   * 账号类型
   */
  private AccountType accountType;
  
  /**
   * 盐
   */
  private String salt;
  
  
  /**
   * 校验值
   */
  private String checkValue;
  
  public BigDecimal getAmount() {
    return amount;
  }
  
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
  
  public String getSalt() {
    return salt;
  }
  
  public void setSalt(String salt) {
    this.salt = salt;
  }
  
  public String getCheckValue() {
    return checkValue;
  }
  
  public void setCheckValue(String checkValue) {
    this.checkValue = checkValue;
  }

  public AccountType getAccountType() {
    return accountType;
  }

  public void setAccountType(AccountType accountType) {
    this.accountType = accountType;
  }
}
