package com.haoxuer.discover.trade.data.entity;


import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;


@FormAnnotation(title = "资金账户")
@Entity
@Table(name = "sys_trade_account_exception")
public class TradeAccountException extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private TradeAccount account;

    /**
     * 盐
     */

    private String checkValue;

    private String oldSalt;

    private String oldCheckValue;

    private BigDecimal oldAmount;

    private BigDecimal amount;



    public String getCheckValue() {
        return checkValue;
    }

    public void setCheckValue(String checkValue) {
        this.checkValue = checkValue;
    }


    public String getOldSalt() {
        return oldSalt;
    }

    public void setOldSalt(String oldSalt) {
        this.oldSalt = oldSalt;
    }

    public String getOldCheckValue() {
        return oldCheckValue;
    }

    public void setOldCheckValue(String oldCheckValue) {
        this.oldCheckValue = oldCheckValue;
    }

    public BigDecimal getOldAmount() {
        return oldAmount;
    }

    public void setOldAmount(BigDecimal oldAmount) {
        this.oldAmount = oldAmount;
    }

    public TradeAccount getAccount() {
        return account;
    }

    public void setAccount(TradeAccount account) {
        this.account = account;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
