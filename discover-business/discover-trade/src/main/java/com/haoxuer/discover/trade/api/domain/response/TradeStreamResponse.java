package com.haoxuer.discover.trade.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.math.BigDecimal;

/**
*
* Created by imake on 2020年09月21日22:28:46.
*/

@Data
public class TradeStreamResponse extends ResponseObject {

    private Long id;

     private BigDecimal preAmount;

     private String note;

     private Integer changeType;

     private BigDecimal afterAmount;

     private BigDecimal amount;

     private Long account;


}