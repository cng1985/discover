package com.haoxuer.discover.trade.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.trade.data.entity.TradeInfo;

/**
* Created by imake on 2018年12月10日10:13:54.
*/
public interface TradeInfoDao extends BaseDao<TradeInfo,Long>{

	 TradeInfo findById(Long id);

	 TradeInfo save(TradeInfo bean);

	 TradeInfo updateByUpdater(Updater<TradeInfo> updater);

	 TradeInfo deleteById(Long id);
}