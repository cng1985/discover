package com.haoxuer.discover;

import com.haoxuer.discover.trade.controller.Controllers;
import com.haoxuer.discover.trade.data.entity.*;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println(Controllers.class.getResource("/").getPath());
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);
        make.setAction(Controllers.class.getPackage().getName());
        File view = new File("E:\\worktongna\\hormone\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(TradeStream.class);

        make.setDao(false);
        make.setService(false);
        make.setAction(false);
        make.setView(false);
        make.setRest(true);
        make.setApi(true);
        make.makes(cs);
    }
}
