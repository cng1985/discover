package com.haoxuer.discover.trade.rest.conver;

import com.haoxuer.discover.trade.api.domain.simple.TradeStreamSimple;
import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.haoxuer.discover.data.rest.core.Conver;


public class TradeStreamSimpleConver implements Conver<TradeStreamSimple, TradeStream> {
    @Override
    public TradeStreamSimple conver(TradeStream source) {
        TradeStreamSimple result = new TradeStreamSimple();
        result.setId(source.getId());
        result.setPreAmount(source.getPreAmount());
        result.setNote(source.getNote());
        result.setChangeType(source.getChangeType());
        result.setAfterAmount(source.getAfterAmount());
        result.setAmount(source.getAmount());
        result.setAddDate(source.getAddDate());
        if (source.getAccount() != null) {
            result.setAccount(source.getAccount().getId());
        }


        return result;
    }
}
