package com.haoxuer.discover.trade.rest.conver;

import com.haoxuer.discover.trade.api.domain.response.TradeStreamResponse;
import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class TradeStreamResponseConver implements Conver<TradeStreamResponse, TradeStream> {
    @Override
    public TradeStreamResponse conver(TradeStream source) {
        TradeStreamResponse result = new TradeStreamResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getAccount()!=null){
           result.setAccount(source.getAccount().getId());
        }


        return result;
    }
}
