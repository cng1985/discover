package com.haoxuer.discover.trade.controller.rest;

import com.haoxuer.discover.trade.api.apis.TradeStreamApi;
import com.haoxuer.discover.trade.api.domain.list.TradeStreamList;
import com.haoxuer.discover.trade.api.domain.page.TradeStreamPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeStreamResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/tradestream")
@RestController
public class TradeStreamRestController extends BaseRestController {


    @RequestMapping("create")
    public TradeStreamResponse create(TradeStreamDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public TradeStreamResponse update(TradeStreamDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public TradeStreamResponse delete(TradeStreamDataRequest request) {
        init(request);
        return api.delete(request);
    }

    @RequestMapping("view")
    public TradeStreamResponse view(TradeStreamDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public TradeStreamList list(TradeStreamSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public TradeStreamPage search(TradeStreamSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private TradeStreamApi api;

}
