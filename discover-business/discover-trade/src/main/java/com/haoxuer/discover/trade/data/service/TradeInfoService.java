package com.haoxuer.discover.trade.data.service;

import com.haoxuer.discover.trade.data.entity.TradeInfo;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月10日10:13:54.
*/
public interface TradeInfoService {

	TradeInfo findById(Long id);

	TradeInfo save(TradeInfo bean);

	TradeInfo update(TradeInfo bean);

	TradeInfo deleteById(Long id);
	
	TradeInfo[] deleteByIds(Long[] ids);
	
	Page<TradeInfo> page(Pageable pageable);
	
	Page<TradeInfo> page(Pageable pageable, Object search);


	List<TradeInfo> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}