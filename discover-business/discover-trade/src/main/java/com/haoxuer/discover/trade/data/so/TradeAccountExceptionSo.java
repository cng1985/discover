package com.haoxuer.discover.trade.data.so;

import java.io.Serializable;

/**
* Created by imake on 2020年02月18日12:45:29.
*/
public class TradeAccountExceptionSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
