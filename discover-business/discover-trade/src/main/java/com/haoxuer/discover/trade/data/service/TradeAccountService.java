package com.haoxuer.discover.trade.data.service;

import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月10日09:54:27.
*/
public interface TradeAccountService {

	TradeAccount findById(Long id);

	TradeAccount save(TradeAccount bean);

	TradeAccount update(TradeAccount bean);

	TradeAccount deleteById(Long id);
	
	TradeAccount[] deleteByIds(Long[] ids);
	
	Page<TradeAccount> page(Pageable pageable);
	
	Page<TradeAccount> page(Pageable pageable, Object search);


	List<TradeAccount> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}