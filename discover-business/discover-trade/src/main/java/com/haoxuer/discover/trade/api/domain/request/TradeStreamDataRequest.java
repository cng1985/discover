package com.haoxuer.discover.trade.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;

import java.math.BigDecimal;

/**
*
* Created by imake on 2020年09月21日22:28:46.
*/

@Data
public class TradeStreamDataRequest extends BaseRequest {

    private Long id;

     private BigDecimal preAmount;

     private String note;

     private Integer changeType;

     private BigDecimal afterAmount;

     private BigDecimal amount;

     private Long account;


}