package com.haoxuer.discover.trade.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;

/**
* Created by imake on 2020年09月21日22:28:46.
*/
@Data
public class TradeStreamSo implements Serializable {

    //资金账号
     @Search(name = "account.id",operator = Filter.Operator.like)
     private String account;



    private String sortField;

    private String sortMethod;

}
