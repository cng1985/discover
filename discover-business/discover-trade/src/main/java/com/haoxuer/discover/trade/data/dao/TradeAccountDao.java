package com.haoxuer.discover.trade.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.rest.base.ResponseObject;
import  com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.others.ChangeType;
import com.haoxuer.discover.trade.data.request.TradeRequest;

import java.math.BigDecimal;

/**
* Created by imake on 2018年12月10日09:54:27.
*/
public interface TradeAccountDao extends BaseDao<TradeAccount,Long>{

	 TradeAccount findById(Long id);

	 TradeAccount save(TradeAccount bean);

	 TradeAccount updateByUpdater(Updater<TradeAccount> updater);

	 TradeAccount deleteById(Long id);

	/**
	 * 正常账户，余额不能为负数
	 * @return
	 */
	TradeAccount initNormal();

	/**
	 * 特殊账户,余额可以为负数
	 * @return
	 */
	TradeAccount initSpecial();


	ResponseObject trade(TradeRequest request);


}