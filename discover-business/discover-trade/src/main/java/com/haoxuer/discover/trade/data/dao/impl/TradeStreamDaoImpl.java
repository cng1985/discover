package com.haoxuer.discover.trade.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.trade.data.dao.TradeStreamDao;
import com.haoxuer.discover.trade.data.entity.TradeStream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by imake on 2018年12月10日10:02:02.
 */
@Repository

public class TradeStreamDaoImpl extends CriteriaDaoImpl<TradeStream, Long> implements TradeStreamDao {

    @Override
    public TradeStream findById(Long id) {
        if (id == null) {
            return null;
        }
        return get(id);
    }

    @Override
    public BigDecimal lastMoney(Long account) {
        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("account.id", account));
        List<Order> orders = new ArrayList<>();
        orders.add(Order.desc("id"));
        List<TradeStream> streams = list(0, 2, filters, orders);
        if (streams != null && streams.size() > 0) {
            return streams.get(0).getAfterAmount();
        }
        return null;
    }

    @Override
    public TradeStream save(TradeStream bean) {

        getSession().save(bean);


        return bean;
    }

    @Override
    public TradeStream deleteById(Long id) {
        TradeStream entity = super.get(id);
        if (entity != null) {
            getSession().delete(entity);
        }
        return entity;
    }

    @Override
    protected Class<TradeStream> getEntityClass() {
        return TradeStream.class;
    }

    @Autowired
    public void setSuperSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}