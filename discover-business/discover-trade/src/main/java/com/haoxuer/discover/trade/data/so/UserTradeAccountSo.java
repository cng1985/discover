package com.haoxuer.discover.trade.data.so;

import java.io.Serializable;

/**
* Created by imake on 2018年12月21日11:06:32.
*/
public class UserTradeAccountSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
