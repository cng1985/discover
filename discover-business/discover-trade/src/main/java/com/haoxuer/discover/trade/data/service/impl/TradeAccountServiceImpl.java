package com.haoxuer.discover.trade.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.service.TradeAccountService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月10日09:54:28.
*/


@Scope("prototype")
@Service
@Transactional
public class TradeAccountServiceImpl implements TradeAccountService {

	private TradeAccountDao dao;


	@Override
	@Transactional(readOnly = true)
	public TradeAccount findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public TradeAccount save(TradeAccount bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public TradeAccount update(TradeAccount bean) {
		Updater<TradeAccount> updater = new Updater<TradeAccount>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public TradeAccount deleteById(Long id) {
		TradeAccount bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public TradeAccount[] deleteByIds(Long[] ids) {
		TradeAccount[] beans = new TradeAccount[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TradeAccountDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<TradeAccount> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<TradeAccount> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<TradeAccount> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}