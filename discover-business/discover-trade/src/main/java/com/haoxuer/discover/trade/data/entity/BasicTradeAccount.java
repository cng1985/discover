package com.haoxuer.discover.trade.data.entity;


import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.*;

@FormAnnotation(title = "资金账户")
@Entity
@Table(name = "sys_trade_basic_account")
public class BasicTradeAccount extends AbstractEntity {

  @Column(name = "trade_key", unique = true, length = 50)
  private String key;

  @ManyToOne(fetch = FetchType.LAZY)
  private TradeAccount account;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public TradeAccount getAccount() {
    return account;
  }

  public void setAccount(TradeAccount account) {
    this.account = account;
  }
}
