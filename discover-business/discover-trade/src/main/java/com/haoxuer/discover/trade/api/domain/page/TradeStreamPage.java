package com.haoxuer.discover.trade.api.domain.page;


import com.haoxuer.discover.trade.api.domain.simple.TradeStreamSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2020年09月21日22:28:46.
*/

@Data
public class TradeStreamPage  extends ResponsePage<TradeStreamSimple> {

}