package com.haoxuer.discover.trade.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.trade.data.entity.TradeAccountException;

/**
* Created by imake on 2020年02月18日12:45:29.
*/
public interface TradeAccountExceptionDao extends BaseDao<TradeAccountException,Long>{

	 TradeAccountException findById(Long id);

	 TradeAccountException save(TradeAccountException bean);

	 TradeAccountException updateByUpdater(Updater<TradeAccountException> updater);

	 TradeAccountException deleteById(Long id);
}