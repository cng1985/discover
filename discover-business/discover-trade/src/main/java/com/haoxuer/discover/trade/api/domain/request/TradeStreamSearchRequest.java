package com.haoxuer.discover.trade.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;

/**
*
* Created by imake on 2020年09月21日22:28:46.
*/

@Data
public class TradeStreamSearchRequest extends BasePageRequest {

    //资金账号
     @Search(name = "account.id",operator = Filter.Operator.eq)
     private Long account;


    private String sortField;


    private String sortMethod;
}