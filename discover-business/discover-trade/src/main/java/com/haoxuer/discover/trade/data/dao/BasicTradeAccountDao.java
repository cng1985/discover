package com.haoxuer.discover.trade.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.trade.data.entity.BasicTradeAccount;
import com.haoxuer.discover.trade.data.entity.TradeAccount;

/**
 * Created by imake on 2018年12月21日11:06:32.
 */
public interface BasicTradeAccountDao extends BaseDao<BasicTradeAccount, Long> {

  BasicTradeAccount findById(Long id);


  /**
   * 正常账户，余额不能为负数
   * @return
   */
  TradeAccount normal(String key);

  /**
   * 特殊账户,余额可以为负数
   * @return
   */
  TradeAccount special(String key);

  BasicTradeAccount save(BasicTradeAccount bean);

  BasicTradeAccount updateByUpdater(Updater<BasicTradeAccount> updater);

  BasicTradeAccount deleteById(Long id);
}