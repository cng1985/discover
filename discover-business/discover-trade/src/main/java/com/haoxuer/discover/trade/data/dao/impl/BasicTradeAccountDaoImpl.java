package com.haoxuer.discover.trade.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.trade.data.dao.BasicTradeAccountDao;
import com.haoxuer.discover.trade.data.entity.BasicTradeAccount;

/**
 * Created by imake on 2018年12月21日11:06:32.
 */
@Repository
public class BasicTradeAccountDaoImpl extends CriteriaDaoImpl<BasicTradeAccount, Long> implements BasicTradeAccountDao {

  @Autowired
  TradeAccountDao accountDao;

  @Override
  public BasicTradeAccount findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public TradeAccount normal(String key) {
    BasicTradeAccount account = one(Filter.eq("key", key));
    if (account == null) {
      account = new BasicTradeAccount();
      account.setAccount(accountDao.initNormal());
      account.setKey(key);
      save(account);
    }
    return account.getAccount();
  }

  @Override
  public TradeAccount special(String key) {
    BasicTradeAccount account = one(Filter.eq("key", key));
    if (account == null) {
      account = new BasicTradeAccount();
      account.setAccount(accountDao.initSpecial());
      account.setKey(key);
      save(account);
    }
    return account.getAccount();
  }

  @Override
  public BasicTradeAccount save(BasicTradeAccount bean) {
    getSession().save(bean);
    return bean;
  }

  @Override
  public BasicTradeAccount deleteById(Long id) {
    BasicTradeAccount entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<BasicTradeAccount> getEntityClass() {
    return BasicTradeAccount.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}