package com.haoxuer.discover.trade.data.service;

import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月10日10:02:02.
*/
public interface TradeStreamService {

	TradeStream findById(Long id);

	TradeStream save(TradeStream bean);

	TradeStream update(TradeStream bean);

	TradeStream deleteById(Long id);
	
	TradeStream[] deleteByIds(Long[] ids);
	
	Page<TradeStream> page(Pageable pageable);
	
	Page<TradeStream> page(Pageable pageable, Object search);


	List<TradeStream> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}