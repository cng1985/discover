package com.haoxuer.discover.trade.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.trade.data.dao.TradeInfoDao;
import com.haoxuer.discover.trade.data.entity.TradeInfo;
import com.haoxuer.discover.trade.data.service.TradeInfoService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月10日10:13:54.
*/


@Scope("prototype")
@Service
@Transactional
public class TradeInfoServiceImpl implements TradeInfoService {

	private TradeInfoDao dao;


	@Override
	@Transactional(readOnly = true)
	public TradeInfo findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public TradeInfo save(TradeInfo bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public TradeInfo update(TradeInfo bean) {
		Updater<TradeInfo> updater = new Updater<TradeInfo>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public TradeInfo deleteById(Long id) {
		TradeInfo bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public TradeInfo[] deleteByIds(Long[] ids) {
		TradeInfo[] beans = new TradeInfo[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TradeInfoDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<TradeInfo> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<TradeInfo> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<TradeInfo> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}