package com.haoxuer.discover.trade.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.trade.data.entity.TradeStream;

import java.math.BigDecimal;

/**
 * Created by imake on 2018年12月10日10:02:02.
 */
public interface TradeStreamDao extends BaseDao<TradeStream, Long> {

    TradeStream findById(Long id);

	/**
	 * 最后一笔钱的金额
	 * @param account
	 * @return
	 */
	BigDecimal lastMoney(Long account);


    TradeStream save(TradeStream bean);

    TradeStream updateByUpdater(Updater<TradeStream> updater);

    TradeStream deleteById(Long id);
}