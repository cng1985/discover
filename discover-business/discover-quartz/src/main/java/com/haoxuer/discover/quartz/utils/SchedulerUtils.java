package com.haoxuer.discover.quartz.utils;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class SchedulerUtils {
  
  public static Scheduler getScheduler() {
    Scheduler scheduler = null;
    try {
      scheduler = StdSchedulerFactory.getDefaultScheduler();
    } catch (SchedulerException e) {
      e.printStackTrace();
    }
    return scheduler;
  }
}
