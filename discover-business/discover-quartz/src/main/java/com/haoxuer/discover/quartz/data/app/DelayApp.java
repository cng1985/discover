package com.haoxuer.discover.quartz.data.app;

import com.haoxuer.discover.quartz.task.HttpJob;
import com.haoxuer.discover.quartz.utils.SchedulerUtils;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;

import java.util.Calendar;
import java.util.Timer;

public class DelayApp {

    public static void main(String[] args) throws SchedulerException {

        JobDetail detail = JobBuilder.newJob(HelloJob.class).withIdentity("ada").build();

        Scheduler scheduler = SchedulerUtils.getScheduler();
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.SECOND,5);
        Trigger trigger = TriggerBuilder.newTrigger()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule())
                .startAt(calendar.getTime())
                .build();
        scheduler.start();
        scheduler.scheduleJob(detail,trigger);
        calendar=Calendar.getInstance();
        Trigger trigger1 = TriggerBuilder.newTrigger()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule())
                .startAt(calendar.getTime())
                .forJob(detail)
                .build();
        scheduler.scheduleJob(trigger1);

    }
}
