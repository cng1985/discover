package com.haoxuer.discover.quartz.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.enums.State;
import com.haoxuer.discover.quartz.data.enums.TaskState;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

@Entity
@Table(name = "task")
public class CronTask extends AbstractEntity {
  
  /**
   * 任务名称
   */
  private String name;
  
  
  /**
   * 表达式
   */
  private String cron;
  
  /**
   * url
   */
  private String url;
  
  /**
   * 执行数量
   */
  private String note;
  
  /**
   * 执行数量
   */
  private Long nums;
  
  /**
   * 是否开启日志功能 on是off不是
   */
  private State recordState;
  /**
   * 添加用户
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;
  
  /**
   * 0停止 1运行
   */
  private TaskState state;
  
  /**
   * 下一次触发时间
   */
  private Date nextDate;
  
  /**
   * 属性
   */
  @ElementCollection(fetch = FetchType.LAZY)
  @CollectionTable(name = "task_attribute", joinColumns = {@JoinColumn(name = "task_id")})
  @MapKeyColumn(name = "name", length = 36)
  @Column(name = "attr", length = 100)
  private Map<String, String> attributes = new HashMap<String, String>();
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  
  public Long getNums() {
    return nums;
  }
  
  public void setNums(Long nums) {
    this.nums = nums;
  }
  
  
  public String getNote() {
    return note;
  }
  
  public void setNote(String note) {
    this.note = note;
  }
  
  
  public String getCron() {
    return cron;
  }
  
  public void setCron(String cron) {
    this.cron = cron;
  }
  
  public String getUrl() {
    return url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public Map<String, String> getAttributes() {
    return attributes;
  }
  
  public void setAttributes(Map<String, String> attributes) {
    this.attributes = attributes;
  }
  
  public State getRecordState() {
    return recordState;
  }
  
  public void setRecordState(State recordState) {
    this.recordState = recordState;
  }
  
  public User getUser() {
    return user;
  }
  
  public void setUser(User user) {
    this.user = user;
  }
  
  public Date getNextDate() {
    return nextDate;
  }
  
  public void setNextDate(Date nextDate) {
    this.nextDate = nextDate;
  }
  
  public TaskState getState() {
    return state;
  }
  
  public void setState(TaskState state) {
    this.state = state;
  }
}
