package com.haoxuer.discover.quartz.data.enums;

public enum TaskState {
  NONE,
  NORMAL,
  PAUSED,
  COMPLETE,
  ERROR,
  BLOCKED;
  
  
  @Override
  public String toString() {
    if (name().equals("NONE")) {
      return "不存在";
    } else if (name().equals("NORMAL")) {
      return "正常";
    } else if (name().equals("PAUSED")) {
      return "暂停";
    } else if (name().equals("COMPLETE")) {
      return "完成 ";
    } else if (name().equals("ERROR")) {
      return "错误 ";
    } else if (name().equals("BLOCKED")) {
      return "阻塞";
    }
    return super.toString();
  }
}
