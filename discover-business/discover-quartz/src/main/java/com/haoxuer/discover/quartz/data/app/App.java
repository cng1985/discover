package com.haoxuer.discover.quartz.data.app;

import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;
import com.haoxuer.discover.quartz.data.entity.CronTaskRecord;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);
    make.setAction("com.ada.quartz.action");
    
    File view = new File("E:\\mvnspace\\usite\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
    make.setView(view);
    
    List<Class<?>> cs = new ArrayList<Class<?>>();
    cs.add(CronTaskRecord.class);
    
    
    make.setDao(true);
    make.setService(true);
    make.setView(false);
    make.setAction(true);
    make.makes(cs);
    System.out.println("ok");
  }
}
