package com.haoxuer.discover.web.handle;

import com.haoxuer.discover.controller.ConfigHandle;
import com.haoxuer.discover.web.data.common.InitConfig;
import org.springframework.stereotype.Component;

@Component
public class SiteConfigHandle implements ConfigHandle {
    @Override
    public String theme() {
        return InitConfig.getWebConfig().getTheme();
    }

    @Override
    public String admin() {
        return InitConfig.getWebConfig().getAdminTheme();
    }
}
