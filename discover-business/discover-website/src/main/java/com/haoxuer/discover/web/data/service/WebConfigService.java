package com.haoxuer.discover.web.data.service;

import com.haoxuer.discover.web.data.entity.WebConfig;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2017年08月30日09:46:28.
 */
public interface WebConfigService {

  WebConfig findById(Long id);
  
  WebConfig config();
  
  
  WebConfig save(WebConfig bean);

  WebConfig update(WebConfig bean);

  WebConfig deleteById(Long id);

  WebConfig[] deleteByIds(Long[] ids);

  Page<WebConfig> page(Pageable pageable);

  Page<WebConfig> page(Pageable pageable, Object search);


  List<WebConfig> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}