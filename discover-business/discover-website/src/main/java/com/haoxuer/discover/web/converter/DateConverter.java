package com.haoxuer.discover.web.converter;

import com.haoxuer.discover.web.conver.DateConver;
import java.util.Date;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

public class DateConverter implements Converter<String, Date> {
  @Nullable
  @Override
  public Date convert(String text) {
    if (StringUtils.hasText(text)) {
      return DateConver.getDefault().conver(text);
    } else {
      return null;
    }
  }
}