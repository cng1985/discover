package com.haoxuer.discover.web.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.web.data.entity.Theme;

/**
* Created by imake on 2022年02月28日22:22:14.
*/
public interface ThemeDao extends BaseDao<Theme,Long>{

	 Theme findById(Long id);

	 Theme save(Theme bean);

	 Theme updateByUpdater(Updater<Theme> updater);

	 Theme deleteById(Long id);
}