package com.haoxuer.discover.web.data.common;

import com.haoxuer.discover.web.data.entity.WebConfig;
import com.haoxuer.discover.web.data.service.WebConfigService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 网站配置初始化.
 */
public class InitConfig {
  
  @Autowired
  private WebConfigService webConfigMapper;
  
  private static WebConfig webConfig;
  
  
  public void initWebConfig() {
    webConfig = webConfigMapper.config();
  }
  
  public static WebConfig getWebConfig() {
    if (webConfig == null) {
      webConfig = new WebConfig();
      webConfig.setTheme("default");
      webConfig.setAdminTheme("default");
      webConfig.setMobile(false);
    }
    if (webConfig.getMobile()==null){
      webConfig.setMobile(false);
    }
    return webConfig;
  }
  
  public static void setWebConfig(WebConfig webConfig) {
    InitConfig.webConfig = webConfig;
  }
}
