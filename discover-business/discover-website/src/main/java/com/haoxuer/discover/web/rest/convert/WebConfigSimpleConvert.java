package com.haoxuer.discover.web.rest.convert;

import com.haoxuer.discover.web.api.domain.simple.WebConfigSimple;
import com.haoxuer.discover.web.data.entity.WebConfig;
import com.haoxuer.discover.data.rest.core.Conver;
public class WebConfigSimpleConvert implements Conver<WebConfigSimple, WebConfig> {


    @Override
    public WebConfigSimple conver(WebConfig source) {
        WebConfigSimple result = new WebConfigSimple();

            result.setId(source.getId());
             result.setFavicon(source.getFavicon());
             result.setNote(source.getNote());
             result.setTitle(source.getTitle());
             result.setAddress(source.getAddress());
             result.setLng(source.getLng());
             result.setKeywords(source.getKeywords());
             result.setAddDate(source.getAddDate());
             result.setDescription(source.getDescription());
             result.setIcp(source.getIcp());
             result.setDomainName(source.getDomainName());
             result.setPageSize(source.getPageSize());
             result.setLogo(source.getLogo());
             result.setLastDate(source.getLastDate());
             result.setGa(source.getGa());
             result.setAdminTheme(source.getAdminTheme());
             result.setTheme(source.getTheme());
             result.setShortName(source.getShortName());
             result.setSlogan(source.getSlogan());
             result.setLat(source.getLat());

        return result;
    }
}
