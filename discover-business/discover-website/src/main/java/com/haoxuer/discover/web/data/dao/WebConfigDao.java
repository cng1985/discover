package com.haoxuer.discover.web.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.web.data.entity.WebConfig;

/**
 * Created by imake on 2017年08月30日09:46:28.
 */
public interface WebConfigDao extends BaseDao<WebConfig, Long> {

  WebConfig findById(Long id);

  WebConfig save(WebConfig bean);

  WebConfig updateByUpdater(Updater<WebConfig> updater);

  WebConfig deleteById(Long id);
}