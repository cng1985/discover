package com.haoxuer.discover.web.rest.convert;

import com.haoxuer.discover.web.api.domain.response.WebConfigResponse;
import com.haoxuer.discover.web.data.entity.WebConfig;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class WebConfigResponseConvert implements Conver<WebConfigResponse, WebConfig> {
    @Override
    public WebConfigResponse conver(WebConfig source) {
        WebConfigResponse result = new WebConfigResponse();
        BeanDataUtils.copyProperties(source,result);



        return result;
    }
}
