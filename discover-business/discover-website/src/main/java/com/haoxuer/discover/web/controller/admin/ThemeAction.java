package com.haoxuer.discover.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2022年02月28日22:22:14.
*/

@Scope("prototype")
@Controller
public class ThemeAction extends BaseAction{


	@RequiresPermissions("theme")
	@RequestMapping("/admin/theme/view_list")
	public String list() {
		return getView("theme/list");
	}

}