package com.haoxuer.discover.web.data.dao;


import com.haoxuer.discover.web.data.entity.WebTheme;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年08月30日10:16:29.
 */
public interface WebThemeDao extends BaseDao<WebTheme, String> {

  WebTheme findById(String id);

  WebTheme save(WebTheme bean);

  WebTheme updateByUpdater(Updater<WebTheme> updater);

  WebTheme deleteById(String id);
}