package com.haoxuer.discover.web.api.domain.page;


import com.haoxuer.discover.web.api.domain.simple.ThemeSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2022年02月28日22:22:14.
*/

@Data
public class ThemePage  extends ResponsePage<ThemeSimple> {

}