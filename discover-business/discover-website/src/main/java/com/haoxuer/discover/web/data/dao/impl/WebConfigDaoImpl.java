package com.haoxuer.discover.web.data.dao.impl;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.web.data.dao.WebConfigDao;
import com.haoxuer.discover.web.data.entity.WebConfig;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2017年08月30日09:46:28.
 */
@Repository

public class WebConfigDaoImpl extends CriteriaDaoImpl<WebConfig, Long> implements WebConfigDao {

  @Override
  public WebConfig findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public WebConfig save(WebConfig bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public WebConfig deleteById(Long id) {
    WebConfig entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<WebConfig> getEntityClass() {
    return WebConfig.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}