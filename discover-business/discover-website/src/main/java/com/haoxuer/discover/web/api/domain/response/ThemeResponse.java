package com.haoxuer.discover.web.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2022年02月28日22:22:14.
*/

@Data
public class ThemeResponse extends ResponseObject {

    private Long id;

     private String path;

     private String note;

     private Long creator;

     private String name;

     private String creatorName;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private String screen;


}