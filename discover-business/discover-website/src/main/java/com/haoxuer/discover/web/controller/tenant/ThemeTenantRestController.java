package com.haoxuer.discover.web.controller.tenant;

import com.haoxuer.discover.web.api.apis.ThemeApi;
import com.haoxuer.discover.web.api.domain.list.ThemeList;
import com.haoxuer.discover.web.api.domain.page.ThemePage;
import com.haoxuer.discover.web.api.domain.request.*;
import com.haoxuer.discover.web.api.domain.response.ThemeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/theme")
@RestController
public class ThemeTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("theme")
    @RequiresUser
    @RequestMapping("create")
    public ThemeResponse create(ThemeDataRequest request) {
        init(request);
        request.setCreator(request.getCreateUser());
        return api.create(request);
    }

	@RequiresPermissions("theme")
    @RequiresUser
    @RequestMapping("update")
    public ThemeResponse update(ThemeDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("theme")
    @RequiresUser
    @RequestMapping("delete")
    public ThemeResponse delete(ThemeDataRequest request) {
        init(request);
        ThemeResponse result = new ThemeResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("theme")
    @RequiresUser
    @RequestMapping("view")
    public ThemeResponse view(ThemeDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("theme")
    @RequiresUser
    @RequestMapping("list")
    public ThemeList list(ThemeSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("theme")
    @RequiresUser
    @RequestMapping("search")
    public ThemePage search(ThemeSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private ThemeApi api;

}
