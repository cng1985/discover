package com.haoxuer.discover.web.api.domain.page;


import com.haoxuer.discover.web.api.domain.simple.WebConfigSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月16日18:44:21.
*/

@Data
public class WebConfigPage  extends ResponsePage<WebConfigSimple> {

}