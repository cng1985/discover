package com.haoxuer.discover.web.data.entity;

import com.haoxuer.discover.data.entity.UUIDEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "site_theme_config")
public class WebTheme extends UUIDEntity {

  @Column(length = 20)
  private String name;

  @Column(length = 20)
  private String path;

  /**
   * 图片路径
   */
  private String screenShot;

}
