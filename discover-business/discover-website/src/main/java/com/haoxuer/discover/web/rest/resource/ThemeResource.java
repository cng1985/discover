package com.haoxuer.discover.web.rest.resource;

import com.haoxuer.discover.web.api.apis.ThemeApi;
import com.haoxuer.discover.web.api.domain.list.ThemeList;
import com.haoxuer.discover.web.api.domain.page.ThemePage;
import com.haoxuer.discover.web.api.domain.request.*;
import com.haoxuer.discover.web.api.domain.response.ThemeResponse;
import com.haoxuer.discover.web.data.dao.ThemeDao;
import com.haoxuer.discover.web.data.entity.Theme;
import com.haoxuer.discover.web.rest.convert.ThemeResponseConvert;
import com.haoxuer.discover.web.rest.convert.ThemeSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.user.data.dao.UserInfoDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ThemeResource implements ThemeApi {

    @Autowired
    private ThemeDao dataDao;

    @Autowired
    private UserInfoDao creatorDao;

    @Override
    public ThemeResponse create(ThemeDataRequest request) {
        ThemeResponse result = new ThemeResponse();

        Theme bean = new Theme();
        handleData(request, bean);
        dataDao.save(bean);
        result = new ThemeResponseConvert().conver(bean);
        return result;
    }

    @Override
    public ThemeResponse update(ThemeDataRequest request) {
        ThemeResponse result = new ThemeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Theme bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ThemeResponseConvert().conver(bean);
        return result;
    }

    private void handleData(ThemeDataRequest request, Theme bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getCreator()!=null){
               bean.setCreator(creatorDao.findById(request.getCreator()));
            }

    }

    @Override
    public ThemeResponse delete(ThemeDataRequest request) {
        ThemeResponse result = new ThemeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public ThemeResponse view(ThemeDataRequest request) {
        ThemeResponse result=new ThemeResponse();
        Theme bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ThemeResponseConvert().conver(bean);
        return result;
    }
    @Override
    public ThemeList list(ThemeSearchRequest request) {
        ThemeList result = new ThemeList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<Theme> organizations = dataDao.list(0, request.getSize(), filters, orders);
        ThemeSimpleConvert convert=new ThemeSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public ThemePage search(ThemeSearchRequest request) {
        ThemePage result=new ThemePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<Theme> page=dataDao.page(pageable);
        ThemeSimpleConvert convert=new ThemeSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
