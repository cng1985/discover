package com.haoxuer.discover.web.binding;

import com.haoxuer.discover.web.conver.CustomDateEditor;
import java.util.Date;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;

public class BindingInitializer implements WebBindingInitializer {
  @Override
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(String.class, new StringTrimmerEditor(false));
    binder.registerCustomEditor(Date.class, new CustomDateEditor());
  }
}
