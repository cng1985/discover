package com.haoxuer.discover.web.api.apis;


import com.haoxuer.discover.web.api.domain.list.WebConfigList;
import com.haoxuer.discover.web.api.domain.page.WebConfigPage;
import com.haoxuer.discover.web.api.domain.request.*;
import com.haoxuer.discover.web.api.domain.response.WebConfigResponse;

public interface WebConfigApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    WebConfigResponse create(WebConfigDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    WebConfigResponse update(WebConfigDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    WebConfigResponse delete(WebConfigDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     WebConfigResponse view(WebConfigDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    WebConfigList list(WebConfigSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    WebConfigPage search(WebConfigSearchRequest request);

    WebConfigResponse config();
}