package com.haoxuer.discover.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年05月16日18:30:01.
*/

@Scope("prototype")
@Controller
public class WebConfigAction extends BaseAction{


	@RequiresPermissions("webconfig")
	@RequestMapping("/admin/webconfig/view_list")
	public String list() {
		return getView("webconfig/list");
	}

}