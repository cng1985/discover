package com.haoxuer.discover.web.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2022年02月28日22:22:14.
*/

@Data
public class ThemeSearchRequest extends BasePageRequest {

    //主题名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;




    private String sortField;


    private String sortMethod;
}