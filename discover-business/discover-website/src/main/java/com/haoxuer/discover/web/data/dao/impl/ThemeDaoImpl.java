package com.haoxuer.discover.web.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.web.data.dao.ThemeDao;
import com.haoxuer.discover.web.data.entity.Theme;

/**
* Created by imake on 2022年02月28日22:22:14.
*/
@Repository

public class ThemeDaoImpl extends CriteriaDaoImpl<Theme, Long> implements ThemeDao {

	@Override
	public Theme findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Theme save(Theme bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Theme deleteById(Long id) {
		Theme entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Theme> getEntityClass() {
		return Theme.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}