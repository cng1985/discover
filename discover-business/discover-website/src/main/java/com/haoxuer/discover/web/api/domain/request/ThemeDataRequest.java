package com.haoxuer.discover.web.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2022年02月28日22:22:14.
*/

@Data
public class ThemeDataRequest extends BaseRequest {

    private Long id;

     private String path;

     private String note;

     private Long creator;

     private String name;

     private String screen;


}