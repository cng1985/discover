package com.haoxuer.discover.web.controller.rest;

import com.haoxuer.discover.web.api.apis.WebConfigApi;
import com.haoxuer.discover.web.api.domain.list.WebConfigList;
import com.haoxuer.discover.web.api.domain.page.WebConfigPage;
import com.haoxuer.discover.web.api.domain.request.*;
import com.haoxuer.discover.web.api.domain.response.WebConfigResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/webconfig")
@RestController
public class WebConfigRestController extends BaseRestController {


    @RequestMapping("create")
    public WebConfigResponse create(WebConfigDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public WebConfigResponse update(WebConfigDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public WebConfigResponse delete(WebConfigDataRequest request) {
        init(request);
        WebConfigResponse result = new WebConfigResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public WebConfigResponse view(WebConfigDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public WebConfigList list(WebConfigSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public WebConfigPage search(WebConfigSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private WebConfigApi api;

}
