package com.haoxuer.discover.web.api.domain.list;


import com.haoxuer.discover.web.api.domain.simple.WebConfigSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年05月16日18:44:21.
*/

@Data
public class WebConfigList  extends ResponseList<WebConfigSimple> {

}