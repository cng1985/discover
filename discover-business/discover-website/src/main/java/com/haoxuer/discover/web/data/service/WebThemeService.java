package com.haoxuer.discover.web.data.service;

import com.haoxuer.discover.web.data.entity.WebTheme;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2017年08月30日10:16:29.
 */
public interface WebThemeService {

  WebTheme findById(String id);

  WebTheme key(String name);


  WebTheme save(WebTheme bean);

  WebTheme update(WebTheme bean);

  WebTheme deleteById(String id);

  WebTheme[] deleteByIds(String[] ids);

  Page<WebTheme> page(Pageable pageable);

  Page<WebTheme> page(Pageable pageable, Object search);


  List<WebTheme> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}