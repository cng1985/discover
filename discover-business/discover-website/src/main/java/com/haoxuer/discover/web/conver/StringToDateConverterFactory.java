package com.haoxuer.discover.web.conver;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

public class StringToDateConverterFactory implements Converter<String, Date> {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Override
  public Date convert(String source) {
    if (source == null || "".equals(source)) {
      return null;
    }
    source = source.trim();
    if (source.equals("")) {
      return null;
    }
    return DateConver.getDefault().conver(source);

  }

}
