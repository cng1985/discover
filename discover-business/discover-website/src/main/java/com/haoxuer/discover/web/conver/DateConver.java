package com.haoxuer.discover.web.conver;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateConver {

  private Logger logger = LoggerFactory.getLogger(DateConver.class);

  private List<DateFormatCommand> commands = new ArrayList<>();

  public void addCommand(DateFormatCommand command) {
    commands.add(command);
  }

  public Date conver(String timestr) {
    Date result = null;
    for (DateFormatCommand command : commands) {
      result = command.conver(timestr);
      if (result != null) {
        logger.info("格式:" + command.toString());
        return result;
      }
    }
    return result;
  }

  public static DateConver getDefault() {
    DateConver conver = new DateConver();
    conver.addCommand(new DateFormatCommand("yyyy年MM月dd日 HH点mm分ss秒"));
    conver.addCommand(new DateFormatCommand("yyyy-MM-dd HH:mm:ss"));
    conver.addCommand(new DateFormatCommand("yyyy年MM月dd日 HH点mm分"));
    conver.addCommand(new DateFormatCommand("yyyy-MM-dd HH:mm"));
    conver.addCommand(new DateFormatCommand("yyyy-MM-dd"));
    conver.addCommand(new DateFormatCommand("yyyy/MM/dd"));
    conver.addCommand(new DateFormatCommand("yyyy年MM月dd日"));
    conver.addCommand(new DateFormatCommand("yyyy年MM月"));
    conver.addCommand(new DateFormatCommand("yyyy-MM"));
    conver.addCommand(new DateFormatCommand("yyyy年"));
    conver.addCommand(new DateFormatCommand("yyyy"));
    return conver;
  }

  public static void main(String[] args) {
    DateConver conver = getDefault();
    System.out.println(conver.conver("2018/11").toLocaleString());
  }
}
