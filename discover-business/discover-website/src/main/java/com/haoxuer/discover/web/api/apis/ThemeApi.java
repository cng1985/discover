package com.haoxuer.discover.web.api.apis;


import com.haoxuer.discover.web.api.domain.list.ThemeList;
import com.haoxuer.discover.web.api.domain.page.ThemePage;
import com.haoxuer.discover.web.api.domain.request.*;
import com.haoxuer.discover.web.api.domain.response.ThemeResponse;

public interface ThemeApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ThemeResponse create(ThemeDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ThemeResponse update(ThemeDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ThemeResponse delete(ThemeDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ThemeResponse view(ThemeDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    ThemeList list(ThemeSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ThemePage search(ThemeSearchRequest request);

}