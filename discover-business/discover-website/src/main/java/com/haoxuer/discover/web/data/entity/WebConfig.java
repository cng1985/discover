package com.haoxuer.discover.web.data.entity;


import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "site_config")
public class WebConfig extends AbstractEntity {

    /**
     * 网站标题
     */
    private String title;


    /**
     * 关键词
     */
    private String keywords;


    /**
     * 网站描述
     */
    private String description;


    /**
     * 网站logo
     */
    private String favicon;


    /**
     * 网站logo
     */
    private String logo;


    /**
     * 主题模块
     */
    private String theme;

    /**
     * 后台主题
     */
    private String adminTheme;


    /**
     * 域名
     */
    private String domainName;

    private String shortName;

    /**
     * 是否支持移动版本
     */
    private Boolean mobile=false;


    /**
     * 备案号
     */
    private String icp;

    /**
     * 分页大小
     */
    private Integer pageSize;

    private Double lat;
    private Double lng;
    private String address;
    private String ga;

    private String slogan;
    private String note;

}
