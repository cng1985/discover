package com.haoxuer.discover.web.rest.convert;

import com.haoxuer.discover.web.api.domain.response.ThemeResponse;
import com.haoxuer.discover.web.data.entity.Theme;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class ThemeResponseConvert implements Conver<ThemeResponse, Theme> {
    @Override
    public ThemeResponse conver(Theme source) {
        ThemeResponse result = new ThemeResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getCreator()!=null){
           result.setCreator(source.getCreator().getId());
        }
         if(source.getCreator()!=null){
            result.setCreatorName(source.getCreator().getName());
         }


        return result;
    }
}
