/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/

package com.haoxuer.discover.web.controller.front;

import com.google.gson.Gson;
import com.haoxuer.discover.web.data.common.InitConfig;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Controller 基类
 *
 * @author langhsu
 */
public class BaseController {
  
  private static List<String> agents = new ArrayList<String>();
  static {
    agents.add("Android");
    agents.add("iPhone");
    agents.add("SymbianOS");
    agents.add("Phone");
    agents.add("iPod");
  }
  protected String getSuffix(String name) {
    int pos = name.lastIndexOf(".");
    return name.substring(pos);
  }

  public String toJson(Object obj) {
    return new Gson().toJson(obj);
  }

  protected String getView(String view) {
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    String agent = request.getHeader("User-Agent");
    if (agent != null&&InitConfig.getWebConfig().getMobile()) {
      for (String item : agents) {
        if (agent.indexOf(item) > 0) {
          return "/theme/" + InitConfig.getWebConfig().getTheme() + "mobile/" + view;
        }
      }
    }
    return "/theme/" + InitConfig.getWebConfig().getTheme() + "/" + view;
  }

  protected String redirect(String view) {
    return "redirect:" + view;
  }


  protected String routeView(String route, String group) {
    String format = "/default" + route;
    return String.format(format, group);
  }

  public static String getIpAddr(HttpServletRequest request) throws Exception {
    String ip = request.getHeader("X-Real-IP");
    if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
      return ip;
    }
    ip = request.getHeader("X-Forwarded-For");
    if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
      // 多次反向代理后会有多个IP值，第一个为真实IP。
      int index = ip.indexOf(',');
      if (index != -1) {
        return ip.substring(0, index);
      } else {
        return ip;
      }
    } else {
      return request.getRemoteAddr();
    }
  }


}
