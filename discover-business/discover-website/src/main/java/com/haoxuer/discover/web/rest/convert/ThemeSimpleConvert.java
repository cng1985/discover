package com.haoxuer.discover.web.rest.convert;

import com.haoxuer.discover.web.api.domain.simple.ThemeSimple;
import com.haoxuer.discover.web.data.entity.Theme;
import com.haoxuer.discover.data.rest.core.Conver;
public class ThemeSimpleConvert implements Conver<ThemeSimple, Theme> {


    @Override
    public ThemeSimple conver(Theme source) {
        ThemeSimple result = new ThemeSimple();

            result.setId(source.getId());
             result.setPath(source.getPath());
             result.setNote(source.getNote());
            if(source.getCreator()!=null){
               result.setCreator(source.getCreator().getId());
            }
             result.setName(source.getName());
             if(source.getCreator()!=null){
                result.setCreatorName(source.getCreator().getName());
             }
             result.setAddDate(source.getAddDate());
             result.setScreen(source.getScreen());

        return result;
    }
}
