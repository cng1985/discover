package com.haoxuer.discover.web.controller.admin;

import com.haoxuer.discover.controller.BaseAction;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.web.data.common.InitConfig;
import com.haoxuer.discover.web.data.entity.WebConfig;
import com.haoxuer.discover.web.data.entity.WebTheme;
import com.haoxuer.discover.web.data.service.WebConfigService;
import com.haoxuer.discover.web.data.service.WebThemeService;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/21
 * Time: 22:53
 * Describe:
 */
@Controller
@RequestMapping(value = "/admin")
public class SystemAction extends BaseAction {

  @Autowired
  private WebConfigService webconfigDao;

  @Autowired
  WebThemeService themeService;

  @RequiresPermissions("admin_system_index")
  @RequestMapping("/system/index")
  public String index(ModelMap model) {
    return getView("system/index");
  }

  @RequiresPermissions("admin_system_profile")
  @RequestMapping("/system/profile")
  public String add(ModelMap model) {
    return getView("system/profile");
  }
  /**
   * 主题视图
   *
   * @return
   */
  @RequestMapping(value = "/theme")
  public String themes(HttpServletRequest request, ModelMap modelMap) {

    ArrayList<WebTheme> themes = new ArrayList<WebTheme>();
    ;


    //扫描文件夹 Start
    String basePath = request.getSession().getServletContext().getRealPath("/");
    basePath = basePath + "\\WEB-INF\\ftl\\theme";

    File[] listFiles = new File(basePath).listFiles();

    if (listFiles != null) {
      for (int i = 0; i < listFiles.length; i++) {
        if (listFiles[i].isDirectory()) {
          String name=listFiles[i].getName();
          if (name!=null&&name.endsWith("mobile")){
            continue;
          }
          WebTheme tempThemes = themeService.key(name);
          themes.add(tempThemes);
        }
      }
    }
    /**
     * 如果本地文件夹读取失败，启用数据库配置的主题
     */
    if (themes.size() == 0) {
      List<WebTheme> dbthemes = themeService.list(0, 0, null, null);
      if (dbthemes != null) {
        themes.addAll(dbthemes);
      }
    }


    modelMap.put("webConfig", InitConfig.getWebConfig());
    modelMap.put("themes", themes);
    return "admin/system/theme";
  }

  /**
   * 更新主题
   *
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/updatetheme")
  public ResponseObject updateTheme(String theme) {
    ResponseObject result = new ResponseObject();

    WebConfig webConfig = InitConfig.getWebConfig();
    webConfig.setTheme(theme);
    webconfigDao.update(webConfig);
    InitConfig.setWebConfig(webconfigDao.findById(1L));

    return result;
  }


  /**
   * 数据监控 视图
   *
   * @return
   */
  @RequestMapping(value = "/druid")
  public String druid() {

    return "admin/system/druid";
  }


  /**
   * 性能监控 视图
   *
   * @return
   */
  @RequestMapping(value = "/monitoring")
  public String monitoring() {

    return "admin/system/monitoring";
  }

  /**
   * 多说 视图
   *
   * @return
   */
  @RequestMapping(value = "/duoshuo")
  public String duoshuo() {

    return "admin/system/duoshuo";
  }

  /**
   * 系统设置 视图
   *
   * @return
   */
  @RequestMapping(value = "/config")
  public String config(ModelMap modelMap) {

    WebConfig webConfig = webconfigDao.findById(1l);
    modelMap.put("webConfig", webConfig);
    return "admin/system/config";
  }

  /**
   * 更新网站配置
   *
   * @param webConfig
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/updatewebconfig")
  public Integer updateWebConfig(WebConfig webConfig) {
    webconfigDao.update(webConfig);
    return 0;
  }

}
