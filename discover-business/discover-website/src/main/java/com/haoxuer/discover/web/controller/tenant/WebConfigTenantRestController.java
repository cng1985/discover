package com.haoxuer.discover.web.controller.tenant;

import com.haoxuer.discover.web.api.apis.WebConfigApi;
import com.haoxuer.discover.web.api.domain.list.WebConfigList;
import com.haoxuer.discover.web.api.domain.page.WebConfigPage;
import com.haoxuer.discover.web.api.domain.request.*;
import com.haoxuer.discover.web.api.domain.response.WebConfigResponse;
import com.haoxuer.discover.web.data.common.InitConfig;
import com.haoxuer.discover.web.data.entity.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/webconfig")
@RestController
public class WebConfigTenantRestController extends BaseTenantRestController {



    @RequiresUser
    @RequestMapping("update")
    public WebConfigResponse update(WebConfigDataRequest request) {
        init(request);
        WebConfig config = InitConfig.getWebConfig();
        if (config.getId() != null) {
            request.setId(config.getId());
        }
        return api.update(request);
    }


    @RequiresUser
    @RequestMapping("view")
    public WebConfigResponse view(WebConfigDataRequest request) {
       init(request);
        WebConfig config = InitConfig.getWebConfig();
        if (config.getId() != null) {
            request.setId(config.getId());
        }
       return api.view(request);
   }


    @Autowired
    private WebConfigApi api;

}
