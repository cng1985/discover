package com.haoxuer.discover.web.data.entity;


import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.user.data.entity.UserInfo;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;


@CreateByUser
@ComposeView
@FormAnnotation(title = "主题管理", model = "主题", menu = "1,110,111")
@Data
@Entity
@Table(name = "site_theme")
public class Theme extends AbstractEntity {


    @SearchItem(label = "主题名称", name = "name")
    @FormField(title = "主题名称", sortNum = "2", grid = true, col = 22, width = "160")
    @Column(length = 20)
    private String name;

    /**
     * 样式图
     */
    @FormField(title = "样式图", sortNum = "2", grid = true, col = 22, width = "160", type = InputType.image)
    private String screen;

    @FormField(title = "主题路径", sortNum = "2", grid = true, col = 22, width = "160")
    @Column(length = 20)
    private String path;


    @FormField(title = "主题介绍", sortNum = "2", col = 22, width = "160", type = InputType.textarea)
    private String note;

    @FieldName
    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private UserInfo creator;
}
