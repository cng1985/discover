package com.haoxuer.discover.web.rest.resource;

import com.haoxuer.discover.web.api.apis.WebConfigApi;
import com.haoxuer.discover.web.api.domain.list.WebConfigList;
import com.haoxuer.discover.web.api.domain.page.WebConfigPage;
import com.haoxuer.discover.web.api.domain.request.*;
import com.haoxuer.discover.web.api.domain.response.WebConfigResponse;
import com.haoxuer.discover.web.data.dao.WebConfigDao;
import com.haoxuer.discover.web.data.entity.WebConfig;
import com.haoxuer.discover.web.rest.convert.WebConfigResponseConvert;
import com.haoxuer.discover.web.rest.convert.WebConfigSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class WebConfigResource implements WebConfigApi {

    @Autowired
    private WebConfigDao dataDao;


    @Override
    public WebConfigResponse create(WebConfigDataRequest request) {
        WebConfigResponse result = new WebConfigResponse();

        WebConfig bean = new WebConfig();
        handleData(request, bean);
        dataDao.save(bean);
        result = new WebConfigResponseConvert().conver(bean);
        return result;
    }

    @Override
    public WebConfigResponse update(WebConfigDataRequest request) {
        WebConfigResponse result = new WebConfigResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        WebConfig bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new WebConfigResponseConvert().conver(bean);
        return result;
    }

    private void handleData(WebConfigDataRequest request, WebConfig bean) {
        BeanDataUtils.copyProperties(request,bean);

    }

    @Override
    public WebConfigResponse delete(WebConfigDataRequest request) {
        WebConfigResponse result = new WebConfigResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public WebConfigResponse view(WebConfigDataRequest request) {
        WebConfigResponse result=new WebConfigResponse();
        WebConfig bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new WebConfigResponseConvert().conver(bean);
        return result;
    }
    @Override
    public WebConfigList list(WebConfigSearchRequest request) {
        WebConfigList result = new WebConfigList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<WebConfig> organizations = dataDao.list(0, request.getSize(), filters, orders);
        WebConfigSimpleConvert convert=new WebConfigSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public WebConfigPage search(WebConfigSearchRequest request) {
        WebConfigPage result=new WebConfigPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<WebConfig> page=dataDao.page(pageable);
        WebConfigSimpleConvert convert=new WebConfigSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }

    @Override
    public WebConfigResponse config() {
        WebConfigResponse result=new WebConfigResponse();
        WebConfig config=null;
        List<WebConfig> configs = dataDao.list(0, 10, null, null);
        if (configs!=null&&configs.size()>0){
            config=configs.get(0);
        }
        if (config==null){
            config=new WebConfig();
            dataDao.save(config);
        }
        result=new WebConfigResponseConvert().conver(config);
        return result;
    }
}
