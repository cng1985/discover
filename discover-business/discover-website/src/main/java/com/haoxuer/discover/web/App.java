package com.haoxuer.discover.web;

import com.haoxuer.discover.web.data.entity.Theme;
import com.haoxuer.discover.web.data.entity.WebConfig;
import com.haoxuer.discover.web.data.entity.WebTheme;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernateSimple.TemplateHibernateSimpleDir;
import com.nbsaas.codemake.templates.elementuiForm.ElementUIFormDir;

import java.io.File;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    File file = new File("E:\\mvnspace\\xjob\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
    CodeMake make = new CodeMake(ElementUIFormDir.class, TemplateHibernateSimpleDir.class);
    make.setView(file);
    make.setDao(true);
    make.setService(false);
    make.setAction(true);
    make.setView(false);
    make.setRest(true);
    make.setApi(true);

    // UserOauthToken.
    make.makes(Theme.class);
  }
}
