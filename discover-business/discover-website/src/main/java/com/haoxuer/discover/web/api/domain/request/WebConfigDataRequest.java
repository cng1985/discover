package com.haoxuer.discover.web.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月16日18:44:21.
*/

@Data
public class WebConfigDataRequest extends BaseRequest {

    private Long id;

     private String favicon;

     private String note;

     private String title;

     private String address;

     private Double lng;

     private String keywords;

     private Date addDate;

     private String description;

     private String icp;

     private String domainName;

     private Integer pageSize;

     private String logo;

     private Date lastDate;

     private String ga;

     private String adminTheme;

     private String theme;

     private String shortName;

     private String slogan;

     private Double lat;


}