package com.haoxuer.discover.web.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年05月16日18:44:20.
*/
@Data
public class WebConfigSimple implements Serializable {

    private Long id;

     private String favicon;
     private String note;
     private String title;
     private String address;
     private Double lng;
     private String keywords;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String description;
     private String icp;
     private String domainName;
     private Integer pageSize;
     private String logo;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;
     private String ga;
     private String adminTheme;
     private String theme;
     private String shortName;
     private String slogan;
     private Double lat;


}
