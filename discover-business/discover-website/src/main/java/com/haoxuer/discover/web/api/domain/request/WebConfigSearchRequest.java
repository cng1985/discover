package com.haoxuer.discover.web.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月16日18:44:21.
*/

@Data
public class WebConfigSearchRequest extends BasePageRequest {




    private String sortField;


    private String sortMethod;
}