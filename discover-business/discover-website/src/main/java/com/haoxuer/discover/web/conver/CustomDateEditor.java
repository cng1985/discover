package com.haoxuer.discover.web.conver;

import java.beans.PropertyEditorSupport;
import org.springframework.util.StringUtils;

public class CustomDateEditor extends PropertyEditorSupport {
  
  @Override
  public void setAsText(String text) {
    if (StringUtils.hasText(text)) {
      this.setValue(DateConver.getDefault().conver(text));
    } else {
      this.setValue((Object) null);
    }
  }
}
