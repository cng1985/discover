package com.haoxuer.discover.web.freemaker;


import com.haoxuer.discover.common.freemarker.ObjectDirectiveModel;
import com.haoxuer.discover.web.api.apis.WebConfigApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("webConfigDirectiveModel")
public class WebConfigDirectiveModel extends ObjectDirectiveModel {

    @Autowired
    private WebConfigApi api;

    @Override
    public Object data() {
        return api.config();
    }
}
