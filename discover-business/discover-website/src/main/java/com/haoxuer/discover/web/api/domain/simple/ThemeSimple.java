package com.haoxuer.discover.web.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2022年02月28日22:22:14.
*/
@Data
public class ThemeSimple implements Serializable {

    private Long id;

     private String path;
     private String note;
     private Long creator;
     private String name;
     private String creatorName;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String screen;


}
