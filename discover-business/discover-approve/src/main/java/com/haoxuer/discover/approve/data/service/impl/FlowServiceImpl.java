package com.haoxuer.discover.approve.data.service.impl;

import com.haoxuer.discover.approve.data.dao.*;
import com.haoxuer.discover.approve.data.entity.Flow;
import com.haoxuer.discover.approve.data.service.FlowService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.rest.domain.AbstractVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FlowServiceImpl implements FlowService {

  private FlowDao dao;


  public FlowServiceImpl() {
  }

  @Transactional
  @Override
  public AbstractVo approve(Long taskid, Integer state, String note, Long user) {
   return dao.approve(taskid, state, note, user);
  }

  @Transactional(readOnly = true)
  public long count(Filter... filters) {

    return dao.count(filters);

  }

  @Transactional
  public Flow deleteById(Long id) {
    Flow bean = dao.deleteById(id);
    return bean;
  }

  @Transactional
  public Flow[] deleteByIds(Long[] ids) {
    Flow[] beans = new Flow[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  @Transactional(readOnly = true)
  public Flow findById(Long id) {
    Flow entity = dao.findById(id);
    return entity;
  }

  @Transactional(readOnly = true)
  public List<Flow> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders) {

    return dao.findList(first, count, filters, orders);

  }

  @Override
  public Page<Flow> page(Pageable pageable) {
    return dao.page(pageable);
  }

  @Transactional(readOnly = true)
  public Page<Flow> findPage(Pageable pageable) {
    return dao.findPage(pageable);
  }


  @Transactional
  public Flow save(Flow bean) {
    dao.save(bean);
    return bean;
  }

  @Autowired
  public void setDao(FlowDao dao) {
    this.dao = dao;
  }

  @Override
  public Flow start(Flow flow) {
    return dao.start(flow);
  }

  @Transactional
  public Flow update(Flow bean) {
    Updater<Flow> updater = new Updater<Flow>(bean);
    bean = dao.updateByUpdater(updater);
    return bean;
  }

  @Override
  public AbstractVo restart(Long taskid) {
    return dao.restart(taskid);
  }
}