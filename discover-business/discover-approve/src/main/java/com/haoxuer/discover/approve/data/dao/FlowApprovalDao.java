package com.haoxuer.discover.approve.data.dao;


import com.haoxuer.discover.approve.data.entity.FlowApproval;
import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

import java.util.List;

public interface FlowApprovalDao extends CriteriaDao<FlowApproval, Long> {
	Pagination getPage(int pageNo, int pageSize);

	FlowApproval findById(Long id);

	FlowApproval save(FlowApproval bean);

	FlowApproval updateByUpdater(Updater<FlowApproval> updater);

	FlowApproval deleteById(Long id);
	
	Integer deleteByFlow(Long id);


	FlowApproval findNext(Long id, Integer hierarchy);
	
	FlowApproval findCur(Long id, Integer hierarchy);

	FlowApproval findPre(Long id, Integer hierarchy);

	List<FlowApproval> findByFlow(Long id);
}