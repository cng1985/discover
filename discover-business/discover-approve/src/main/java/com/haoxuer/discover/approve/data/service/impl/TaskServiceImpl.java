package com.haoxuer.discover.approve.data.service.impl;

import com.haoxuer.discover.approve.data.entity.Task;
import com.haoxuer.discover.approve.data.dao.TaskDao;
import com.haoxuer.discover.approve.data.service.TaskService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class TaskServiceImpl implements TaskService {
	

	@Transactional(readOnly = true)
	public Task findById(Long id) {
		Task entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public Task save(Task bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public Task update(Task bean) {
		Updater<Task> updater = new Updater<Task>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public Task deleteById(Long id) {
		Task bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public Task[] deleteByIds(Long[] ids) {
		Task[] beans = new Task[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private TaskDao dao;

	@Autowired
	public void setDao(TaskDao dao) {
		this.dao = dao;
	}
	

	@Transactional(readOnly = true)
	public Page<Task> findPage(Pageable pageable){
	     return dao.page(pageable);
	}

	@Transactional(readOnly = true)
	public long count(Filter... filters){
	     
	     return dao.count(filters);
	     
	}

	@Transactional(readOnly = true)
	public List<Task> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders){
	
		     return dao.list(first,count,filters,orders);
	
	}
}