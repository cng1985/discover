package com.haoxuer.discover.approve.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;

import java.io.Serializable;

/**
* Created by imake on 2019年03月02日10:09:06.
*/
public class FlowSo implements Serializable {

    @Search(name = "user.name",operator = Filter.Operator.like)
    private String name;

    @Search(name = "title",operator = Filter.Operator.like)
    private String title;

    @Search(name = "flow.id")
    private Long flow;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getFlow() {
        return flow;
    }

    public void setFlow(Long flow) {
        this.flow = flow;
    }
}
