package com.haoxuer.discover.approve.data.dao;


import com.haoxuer.discover.approve.data.entity.Flow;
import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.rest.domain.AbstractVo;

public interface FlowDao extends CriteriaDao<Flow, Long> {
  Pagination getPage(int pageNo, int pageSize);

  Flow findById(Long id);

  Flow findByOId(Integer catalog, Long id);

  int deleteForCatalog(Long oid, Integer catalog);

  Flow save(Flow bean);

  Flow updateByUpdater(Updater<Flow> updater);

  Flow deleteById(Long id);


  Flow stop(Long id,String note);


  Flow start(Flow bean);

  /**
   * 审批
   *
   * @param taskid
   *            任务id
   * @param state
   *            审批状态
   * @param note
   *            审批意见
   * @param user
   *            用户id
   *
   * @return 审批状态
   */
  AbstractVo approve(Long taskid, Integer state, String note, Long user);

  AbstractVo restart(Long taskid);

}