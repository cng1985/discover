package com.haoxuer.discover.approve.data.dao;


import com.haoxuer.discover.approve.data.entity.FlowDefinition;
import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FlowDefinitionDao extends CriteriaDao<FlowDefinition,Long> {
	Pagination getPage(int pageNo, int pageSize);

	FlowDefinition findById(Long id);

	FlowDefinition save(FlowDefinition bean);

	FlowDefinition updateByUpdater(Updater<FlowDefinition> updater);

	FlowDefinition deleteById(Long id);
}