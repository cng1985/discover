package com.haoxuer.discover.approve.data.dao;


import com.haoxuer.discover.approve.data.entity.FlowRecord;
import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

import java.util.List;

public interface FlowRecordDao extends CriteriaDao<FlowRecord, Long> {
	Pagination getPage(int pageNo, int pageSize);

	FlowRecord findById(Long id);

	FlowRecord save(FlowRecord bean);

	FlowRecord updateByUpdater(Updater<FlowRecord> updater);

	FlowRecord deleteById(Long id);

	List<FlowRecord> findByFlow(Long id);
}