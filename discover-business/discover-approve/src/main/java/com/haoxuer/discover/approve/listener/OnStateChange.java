package com.haoxuer.discover.approve.listener;

import com.haoxuer.discover.approve.data.entity.Flow;

public interface OnStateChange {
	
	void change(Flow flow, String state);

}
