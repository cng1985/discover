package com.haoxuer.discover.approve.data.service;

import com.haoxuer.discover.approve.data.entity.Flow;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.rest.domain.AbstractVo;

import java.util.List;

public interface FlowService {

	Flow findById(Long id);

	Flow save(Flow bean);

	/**
	 * 启动一个流程
	 * 
	 * @param bean
	 * @return
	 */
	Flow start(Flow bean);

	/**
	 * 审批
	 * 
	 * @param taskid
	 *            任务id
	 * @param state
	 *            审批状态
	 * @param note
	 *            审批意见
	 * @param user
	 *            用户id
	 * 
	 * @return 审批状态
	 */
	AbstractVo approve(Long taskid, Integer state, String note, Long user);
	
	AbstractVo restart(Long taskid);


	Flow update(Flow bean);

	Flow deleteById(Long id);

	Flow[] deleteByIds(Long[] ids);


	Page<Flow> findPage(Pageable pageable);

	long count(Filter... filters);

	List<Flow> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders);

  Page<Flow> page(Pageable pageable);
}