package com.haoxuer.discover.approve.data.dao;


import com.haoxuer.discover.approve.data.entity.Task;
import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

import java.util.Date;
import java.util.List;

public interface TaskDao extends CriteriaDao<Task, Long> {
	Pagination getPage(int pageNo, int pageSize);

	Task findById(Long id);

	Task findByFlow(Long id);


	Task save(Task bean);
	
	int deleteForCatalog(Long oid, Integer catalog);


	Task updateByUpdater(Updater<Task> updater);

	Task deleteById(Long id);
	
	List<Date> list(long uid, int year, int month);
	
	
}