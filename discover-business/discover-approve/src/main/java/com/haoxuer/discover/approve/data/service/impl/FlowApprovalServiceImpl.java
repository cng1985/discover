package com.haoxuer.discover.approve.data.service.impl;

import com.haoxuer.discover.approve.data.dao.FlowApprovalDao;
import com.haoxuer.discover.approve.data.entity.FlowApproval;
import com.haoxuer.discover.approve.data.service.FlowApprovalService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class FlowApprovalServiceImpl implements FlowApprovalService {
	

	@Transactional(readOnly = true)
	public FlowApproval findById(Long id) {
		FlowApproval entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public FlowApproval save(FlowApproval bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public FlowApproval update(FlowApproval bean) {
		Updater<FlowApproval> updater = new Updater<FlowApproval>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public FlowApproval deleteById(Long id) {
		FlowApproval bean = dao.deleteById(id);
		return bean;
	}

    @Transactional	
	public FlowApproval[] deleteByIds(Long[] ids) {
		FlowApproval[] beans = new FlowApproval[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private FlowApprovalDao dao;

	@Autowired
	public void setDao(FlowApprovalDao dao) {
		this.dao = dao;
	}
	

	
	@Transactional(readOnly = true)
	public Page<FlowApproval> findPage(Pageable pageable){
	     return dao.findPage(pageable);
	}

	@Transactional(readOnly = true)
	public long count(Filter... filters){
	     
	     return dao.count(filters);
	     
	}

	@Transactional(readOnly = true)
	public List<FlowApproval> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders){
	
		     return dao.findList(first,count,filters,orders);
	
	}
}