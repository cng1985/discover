package com.haoxuer.discover.approve.data.service.impl;

import com.haoxuer.discover.approve.data.entity.FlowDefinitionItem;
import com.haoxuer.discover.approve.data.service.FlowDefinitionItemService;
import com.haoxuer.discover.approve.data.dao.FlowDefinitionItemDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FlowDefinitionItemServiceImpl implements FlowDefinitionItemService {
	
	
    
    
	@Transactional(readOnly = true)
	public FlowDefinitionItem findById(Long id) {
		FlowDefinitionItem entity = dao.findById(id);
		return entity;
	}

    @Transactional
	public FlowDefinitionItem save(FlowDefinitionItem bean) {
		dao.save(bean);
		return bean;
	}

    @Transactional
	public FlowDefinitionItem update(FlowDefinitionItem bean) {
		Updater<FlowDefinitionItem> updater = new Updater<FlowDefinitionItem>(bean);
		bean = dao.updateByUpdater(updater);
		return bean;
	}

    @Transactional
	public FlowDefinitionItem deleteById(Long id) {
		FlowDefinitionItem bean = dao.findById(id);
		return bean;
	}

    @Transactional	
	public FlowDefinitionItem[] deleteByIds(Long[] ids) {
		FlowDefinitionItem[] beans = new FlowDefinitionItem[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	private FlowDefinitionItemDao dao;

	@Autowired
	public void setDao(FlowDefinitionItemDao dao) {
		this.dao = dao;
	}
	
	
	@Transactional(readOnly = true)
	public Page<FlowDefinitionItem> findPage(Pageable pageable){
	     return dao.findPage(pageable);
	}

	@Transactional(readOnly = true)
	public long count(Filter... filters){
	     
	     return dao.count(filters);
	     
	}

	@Transactional(readOnly = true)
	public List<FlowDefinitionItem> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders){
	
		     return dao.findList(first,count,filters,orders);
	
	}
}