package com.haoxuer.discover.approve.data.service;

import com.haoxuer.discover.approve.data.entity.FlowApproval;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;


public interface FlowApprovalService {

	FlowApproval findById(Long id);

	FlowApproval save(FlowApproval bean);

	FlowApproval update(FlowApproval bean);

	FlowApproval deleteById(Long id);
	
	FlowApproval[] deleteByIds(Long[] ids);
	

	Page<FlowApproval> findPage(Pageable pageable);

	long count(Filter... filters);

	List<FlowApproval> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders);
	
}