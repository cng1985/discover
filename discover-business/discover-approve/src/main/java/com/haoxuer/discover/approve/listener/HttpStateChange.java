package com.haoxuer.discover.approve.listener;

import com.haoxuer.discover.approve.data.entity.Flow;
import com.haoxuer.utils.http.Connection;
import com.haoxuer.utils.http.HttpConnection;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component()
public class HttpStateChange implements OnStateChange {

	@Override
	public void change(Flow flow, String state) {

		for (int i = 0; i < 3; i++) {
			try {
				if (flow.getFlow().getStateurl() == null) {
					return;
				}
				HttpConnection.connect(flow.getFlow().getStateurl()).data("state", state).data("id", "" + flow.getOid())
						.method(Connection.Method.POST).execute().body();
				break;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
