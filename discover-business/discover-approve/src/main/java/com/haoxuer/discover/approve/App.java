package com.haoxuer.discover.approve;

import com.haoxuer.discover.approve.data.entity.Flow;
import com.haoxuer.discover.approve.data.entity.FlowDefinition;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.simple.TemplateSimpleDir;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class App {

  public static void main(String[] args) {

    CodeMake make=	new CodeMake(TemplateSimpleDir.class, TemplateHibernateDir.class);
    make.setAction("com.haoxuer.discover.approve.controller.admin");
    //E:\workspace\icrm\icrm_web\src\main\webapp\WEB-INF\ftl\admin
    File view=new File("E:\\workspace\\icrm\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
    make.setView(view);

    List<Class<?>> cs=new ArrayList<Class<?>>();
    cs.add(FlowDefinition.class);
    cs.add(Flow.class);

    make.setDao(false);
    make.setService(false);
    make.setView(false);
    make.setAction(true);
    make.makes(cs);
    System.out.println("ok");

  }
}
