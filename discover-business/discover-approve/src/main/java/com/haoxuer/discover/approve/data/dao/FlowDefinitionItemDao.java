package com.haoxuer.discover.approve.data.dao;


import com.haoxuer.discover.approve.data.entity.FlowDefinitionItem;
import com.haoxuer.discover.data.core.CriteriaDao;
import com.haoxuer.discover.data.core.Pagination;
import com.haoxuer.discover.data.core.Updater;

public interface FlowDefinitionItemDao extends CriteriaDao<FlowDefinitionItem,Long> {
	Pagination getPage(int pageNo, int pageSize);

	FlowDefinitionItem findById(Long id);


	FlowDefinitionItem findByDefintion(Long flow, Integer hierarchy);

	FlowDefinitionItem save(FlowDefinitionItem bean);

	FlowDefinitionItem updateByUpdater(Updater<FlowDefinitionItem> updater);

	FlowDefinitionItem deleteById(Long id);
}