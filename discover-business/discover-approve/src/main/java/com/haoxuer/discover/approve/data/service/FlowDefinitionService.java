package com.haoxuer.discover.approve.data.service;

import com.haoxuer.discover.approve.data.entity.FlowDefinition;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;


public interface FlowDefinitionService {

	FlowDefinition findById(Long id);

	FlowDefinition save(FlowDefinition bean);

	FlowDefinition update(FlowDefinition bean);

	FlowDefinition deleteById(Long id);
	
	FlowDefinition[] deleteByIds(Long[] ids);
	
	Page<FlowDefinition> findPage(Pageable pageable);

	long count(Filter... filters);

	List<FlowDefinition> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders);

  Page<FlowDefinition> page(Pageable pageable);
}