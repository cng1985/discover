package com.haoxuer.discover.approve.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 审批流程
 * 
 * @author ada
 *
 */

@FormAnnotation(title = "流程实例")
@Entity
@Table(name = "approve_flow")
public class Flow extends AbstractEntity {

	/**
	 * 流程分类
	 */

	@FormField(title = "流程分类", sortNum = "5", grid = true, col = 12)
	private Integer catalog;

	/**
	 * 当前审批层级
	 */
	@FormField(title = "审批层级", sortNum = "3", grid = true, col = 12)
	private Integer hierarchy;

	/**
	 * 审核状态 0为未审核1为审核中2审核完成 -1审批失败
	 */
	private Integer state;

	/**
	 * 流程描述
	 */
	@FormField(title = "流程介绍", sortNum = "4", grid = true, col = 12)
	private String note;

	/**
	 * 数据id
	 */
	private Long oid;

	/**
	 * 流程名称
	 */
	@FormField(title = "流程名称", sortNum = "1", grid = true, col = 12)
	private String title;

	/**
	 * 流程开启用户
	 */
	@FormField(title = "流程发起人", sortNum = "2", grid = true, col = 12)
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	@OneToMany(fetch = FetchType.LAZY,mappedBy = "flow")
	private List<FlowRecord> records=new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY,mappedBy = "flow")
	private List<FlowApproval> approvals=new ArrayList<>();

	/**
	 * 启动哪一个流程
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	private FlowDefinition flow;

	public FlowDefinition getFlow() {
		return flow;
	}

	public void setFlow(FlowDefinition flow) {
		this.flow = flow;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getCatalog() {
		return catalog;
	}

	public Integer getHierarchy() {
		return hierarchy;
	}

	public String getNote() {
		return note;
	}

	public Long getOid() {
		return oid;
	}

	public String getTitle() {
		return title;
	}

	public User getUser() {
		return user;
	}

	public void setCatalog(Integer catalog) {
		this.catalog = catalog;
	}

	public void setHierarchy(Integer hierarchy) {
		this.hierarchy = hierarchy;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setOid(Long oid) {
		this.oid = oid;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<FlowRecord> getRecords() {
		return records;
	}

	public void setRecords(List<FlowRecord> records) {
		this.records = records;
	}

	public List<FlowApproval> getApprovals() {
		return approvals;
	}

	public void setApprovals(List<FlowApproval> approvals) {
		this.approvals = approvals;
	}
}
