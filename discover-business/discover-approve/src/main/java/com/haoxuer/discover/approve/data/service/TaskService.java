package com.haoxuer.discover.approve.data.service;

import com.haoxuer.discover.approve.data.entity.Task;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;


public interface TaskService {

	Task findById(Long id);

	Task save(Task bean);

	Task update(Task bean);

	Task deleteById(Long id);
	
	Task[] deleteByIds(Long[] ids);
	

	
	Page<Task> findPage(Pageable pageable);

	long count(Filter... filters);

	List<Task> findList(Integer first, Integer count, List<Filter> filters, List<Order> orders);
	
}