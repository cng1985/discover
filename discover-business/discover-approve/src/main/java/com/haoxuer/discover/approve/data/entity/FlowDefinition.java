package com.haoxuer.discover.approve.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程定义
 * 
 * @author ada
 *
 */

@FormAnnotation(title = "流程定义")
@Entity
@Table(name = "approve_flow_definition")
public class FlowDefinition extends AbstractEntity {

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "flow")
	private List<FlowDefinitionItem> items;

	@FormField(title = "流程名称", sortNum = "1", grid = true, col = 12)
	private String name;

	@FormField(title = "流程介绍", sortNum = "1", grid = true, col = 12)
	private String note;

	/**
	 * 内容查看url
	 */
	@FormField(title = "查看网址", sortNum = "1", grid = false, col = 12)
	private String viewurl;

	/**
	 * 状态改变通知url
	 */
	@FormField(title = "状态网址", sortNum = "1", grid = false, col = 12)
	private String stateurl;

	@FormField(title = "修改网址", sortNum = "1", grid = false, col = 12)
	private String updateurl;


	public String getUpdateurl() {
		return updateurl;
	}

	public void setUpdateurl(String updateurl) {
		this.updateurl = updateurl;
	}

	public String getStateurl() {
		return stateurl;
	}

	public void setStateurl(String stateurl) {
		this.stateurl = stateurl;
	}

	public List<FlowDefinitionItem> getItems() {
		return items;
	}

	public String getName() {
		return name;
	}

	public String getViewurl() {
		return viewurl;
	}

	public void setItems(List<FlowDefinitionItem> items) {
		this.items = items;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setViewurl(String viewurl) {
		this.viewurl = viewurl;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
