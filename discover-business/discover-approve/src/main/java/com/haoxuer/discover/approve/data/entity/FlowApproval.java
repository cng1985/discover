package com.haoxuer.discover.approve.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 流程审批节点
 * 
 * @author ada
 *
 */
@Entity
@Table(name = "approve_flow_approval")
public class FlowApproval extends AbstractEntity {

	/**
	 * 审批人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	/**
	 * 流程
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	private Flow flow;

	/**
	 * 层级
	 */
	private Integer hierarchy;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Flow getFlow() {
		return flow;
	}

	public void setFlow(Flow flow) {
		this.flow = flow;
	}

	public Integer getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(Integer hierarchy) {
		this.hierarchy = hierarchy;
	}
	
	

}
