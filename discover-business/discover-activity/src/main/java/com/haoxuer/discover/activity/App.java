package com.haoxuer.discover.activity;

import com.haoxuer.discover.activity.data.entity.Activity;
import com.haoxuer.discover.activity.data.entity.ActivityCatalog;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);
        make.setAction("com.ada.activity.controller.admin");

        File view = new File("E:\\mvnspace\\usite\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(Activity.class);
        cs.add(ActivityCatalog.class);


        make.setDao(true);
        make.setService(true);
        make.setView(false);
        make.setAction(true);
        make.makes(cs);
        System.out.println("ok");
    }
}
