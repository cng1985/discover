package com.haoxuer.discover.activity.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.activity.data.entity.Activity;

/**
* Created by imake on 2017年08月15日09:57:55.
*/
public interface ActivityDao extends BaseDao<Activity,Long>{

	 Activity findById(Long id);

	 Activity save(Activity bean);

	 Activity updateByUpdater(Updater<Activity> updater);

	 Activity deleteById(Long id);
}