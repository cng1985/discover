package com.haoxuer.discover.activity.data.dao;


import com.haoxuer.discover.activity.data.entity.ActivityCatalog;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日09:57:56.
*/
public interface ActivityCatalogDao extends BaseDao<ActivityCatalog,Integer>{

	 ActivityCatalog findById(Integer id);

	 ActivityCatalog save(ActivityCatalog bean);

	 ActivityCatalog updateByUpdater(Updater<ActivityCatalog> updater);

	 ActivityCatalog deleteById(Integer id);
}