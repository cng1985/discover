package com.haoxuer.discover.album.data.dao;


import com.haoxuer.discover.album.data.entity.PhotoFeed;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日10:04:55.
*/
public interface PhotoFeedDao extends BaseDao<PhotoFeed,Long>{

	 PhotoFeed findById(Long id);

	 PhotoFeed save(PhotoFeed bean);

	 PhotoFeed updateByUpdater(Updater<PhotoFeed> updater);

	 PhotoFeed deleteById(Long id);
}