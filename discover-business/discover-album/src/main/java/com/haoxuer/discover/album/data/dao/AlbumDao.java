package com.haoxuer.discover.album.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.album.data.entity.Album;

/**
* Created by imake on 2017年08月15日10:04:54.
*/
public interface AlbumDao extends BaseDao<Album,Long>{

	 Album findById(Long id);

	 Album save(Album bean);

	 Album updateByUpdater(Updater<Album> updater);

	 Album deleteById(Long id);
}