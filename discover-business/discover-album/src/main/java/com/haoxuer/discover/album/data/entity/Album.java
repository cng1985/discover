package com.haoxuer.discover.album.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "album_album")
public class Album extends AbstractEntity {


	private String name;

	@ManyToOne()
	private User user;



	public String getName() {
		return name;
	}

	public User getUser() {
		return user;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
