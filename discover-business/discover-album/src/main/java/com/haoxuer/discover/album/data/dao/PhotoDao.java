package com.haoxuer.discover.album.data.dao;


import com.haoxuer.discover.album.data.entity.Photo;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日10:04:55.
*/
public interface PhotoDao extends BaseDao<Photo,String>{

	 Photo findById(String id);

	 Photo save(Photo bean);

	 Photo updateByUpdater(Updater<Photo> updater);

	 Photo deleteById(String id);
}