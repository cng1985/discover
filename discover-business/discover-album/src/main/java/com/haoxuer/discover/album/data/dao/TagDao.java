package com.haoxuer.discover.album.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.album.data.entity.Tag;

/**
* Created by imake on 2017年08月15日10:04:55.
*/
public interface TagDao extends BaseDao<Tag,String>{

	 Tag findById(String id);

	 Tag save(Tag bean);

	 Tag updateByUpdater(Updater<Tag> updater);

	 Tag deleteById(String id);
}