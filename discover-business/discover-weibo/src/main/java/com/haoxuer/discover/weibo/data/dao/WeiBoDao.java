package com.haoxuer.discover.weibo.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.entity.WeiBo;

/**
 * Created by imake on 2019年01月02日09:59:44.
 */
public interface WeiBoDao extends BaseDao<WeiBo, Long> {

  WeiBo findById(Long id);

  WeiBo countLike(Long id);

  WeiBo countComment(Long id);


  WeiBo save(WeiBo bean);

  WeiBo updateByUpdater(Updater<WeiBo> updater);

  WeiBo deleteById(Long id);
}