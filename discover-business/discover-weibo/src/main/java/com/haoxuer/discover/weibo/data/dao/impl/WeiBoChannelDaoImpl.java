package com.haoxuer.discover.weibo.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.weibo.data.dao.WeiBoChannelDao;
import com.haoxuer.discover.weibo.data.entity.WeiBoChannel;

/**
* Created by imake on 2019年02月28日21:28:30.
*/
@Repository

public class WeiBoChannelDaoImpl extends CriteriaDaoImpl<WeiBoChannel, Long> implements WeiBoChannelDao {

	@Override
	public WeiBoChannel findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public WeiBoChannel save(WeiBoChannel bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public WeiBoChannel deleteById(Long id) {
		WeiBoChannel entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<WeiBoChannel> getEntityClass() {
		return WeiBoChannel.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}