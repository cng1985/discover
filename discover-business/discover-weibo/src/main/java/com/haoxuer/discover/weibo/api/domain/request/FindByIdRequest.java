package com.haoxuer.discover.weibo.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class FindByIdRequest extends RequestUserTokenObject {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
