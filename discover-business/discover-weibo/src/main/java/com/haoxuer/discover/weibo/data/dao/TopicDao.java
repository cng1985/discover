package com.haoxuer.discover.weibo.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.weibo.data.entity.Topic;

/**
* Created by imake on 2018年12月19日11:32:12.
*/
public interface TopicDao extends BaseDao<Topic,Long>{



	Topic findById(Long id);

	 Topic save(Topic bean);

	 Topic updateByUpdater(Updater<Topic> updater);

	 Topic deleteById(Long id);
}