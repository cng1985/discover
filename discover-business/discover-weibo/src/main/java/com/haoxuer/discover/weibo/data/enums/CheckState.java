package com.haoxuer.discover.weibo.data.enums;

public enum CheckState {

  init, success, fail;

  @Override
  public String toString() {
    if (name().equals("init")){
      return "未审核";
    }
    else if (name().equals("success")){
      return "审核成功";
    }
    else if (name().equals("fail")){
      return "审核失败";
    }
    return super.toString();
  }
}
