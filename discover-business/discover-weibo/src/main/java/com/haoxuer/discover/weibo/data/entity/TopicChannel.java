package com.haoxuer.discover.weibo.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.entity.LongEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@FormAnnotation(title = "主题的频道")
@Entity
@Table(name = "weibo_topic_channel")
public class TopicChannel extends LongEntity {

  /**
   * 主题
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Topic topic;

  /**
   * 频道
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Channel channel;

  public Topic getTopic() {
    return topic;
  }

  public void setTopic(Topic topic) {
    this.topic = topic;
  }

  public Channel getChannel() {
    return channel;
  }

  public void setChannel(Channel channel) {
    this.channel = channel;
  }
}
