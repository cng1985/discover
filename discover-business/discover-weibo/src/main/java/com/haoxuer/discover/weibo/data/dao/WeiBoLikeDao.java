package com.haoxuer.discover.weibo.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.entity.WeiBoLike;

/**
 * Created by imake on 2019年02月28日21:43:51.
 */
public interface WeiBoLikeDao extends BaseDao<WeiBoLike, Long> {

  WeiBoLike findById(Long id);

  WeiBoLike findByUser(Long user, Long id);


  WeiBoLike save(WeiBoLike bean);

  WeiBoLike updateByUpdater(Updater<WeiBoLike> updater);

  WeiBoLike deleteById(Long id);

  Boolean liked(Long id, Long user);
}