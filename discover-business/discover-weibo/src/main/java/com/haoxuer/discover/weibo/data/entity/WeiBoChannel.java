package com.haoxuer.discover.weibo.data.entity;

import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.LongEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@FormAnnotation(title = "主题的频道")
@Entity
@Table(name = "weibo_content_channel")
public class WeiBoChannel extends LongEntity {

  /**
   * 主题
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private WeiBo weiBo;

  /**
   * 频道
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Channel channel;

  public WeiBo getWeiBo() {
    return weiBo;
  }

  public void setWeiBo(WeiBo weiBo) {
    this.weiBo = weiBo;
  }

  public Channel getChannel() {
    return channel;
  }

  public void setChannel(Channel channel) {
    this.channel = channel;
  }
}
