package com.haoxuer.discover.weibo.rest.filter;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.filter.common.FilterChain;
import com.haoxuer.discover.filter.common.HandlerRequest;
import com.haoxuer.discover.filter.common.HandlerResponse;
import com.haoxuer.discover.user.service.UserTokenService;
import com.haoxuer.discover.weibo.api.domain.request.FindByIdRequest;
import com.haoxuer.discover.weibo.api.domain.request.WeiboCommentRequest;
import com.haoxuer.discover.weibo.api.domain.request.WeiboPostRequest;
import com.haoxuer.discover.weibo.data.dao.WeiBoDao;
import com.haoxuer.discover.weibo.data.entity.WeiBo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("prototype")
@Component
public class CommentCheckFilter implements FilterProxy {


  @Autowired
  UserTokenService tokenService;

  @Autowired
  WeiBoDao weiBoDao;

  @Override
  public void doFilter(HandlerRequest handlerRequest, HandlerResponse response, FilterChain filterChain) {
    WeiboCommentRequest request = (WeiboCommentRequest) handlerRequest.getAttribute("request");
    WeiBo root = weiBoDao.findById(request.getId());
    if (root == null) {
      response.setCode(503);
      response.setMsg("无效id");
      return;
    }
    handlerRequest.setAttribute("root", root);
    filterChain.doFilter(handlerRequest, response);
  }
}
