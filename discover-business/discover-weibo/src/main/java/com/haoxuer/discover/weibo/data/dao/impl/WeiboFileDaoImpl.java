package com.haoxuer.discover.weibo.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.weibo.data.dao.WeiboFileDao;
import com.haoxuer.discover.weibo.data.entity.WeiboFile;

/**
* Created by imake on 2019年02月28日21:31:43.
*/
@Repository

public class WeiboFileDaoImpl extends CriteriaDaoImpl<WeiboFile, Long> implements WeiboFileDao {

	@Override
	public WeiboFile findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public WeiboFile save(WeiboFile bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public WeiboFile deleteById(Long id) {
		WeiboFile entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<WeiboFile> getEntityClass() {
		return WeiboFile.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}