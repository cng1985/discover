package com.haoxuer.discover.weibo.rest.conver;

import com.haoxuer.discover.config.api.domain.simple.UserSimple;
import com.haoxuer.discover.config.rest.conver.UserSimpleConver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.weibo.api.domain.simple.WeiBoCommentSimple;
import com.haoxuer.discover.weibo.data.entity.WeiBo;
import com.vdurmont.emoji.EmojiParser;

public class WeiBoCommentSimpleConver implements Conver<WeiBoCommentSimple, WeiBo> {
  @Override
  public WeiBoCommentSimple conver(WeiBo source) {
    WeiBoCommentSimple result=new WeiBoCommentSimple();
    if (source.getAddDate() != null) {
      result.setAddDate(source.getAddDate().getTime());
    }
    result.setId(source.getId());
    String note = EmojiParser.parseToUnicode(source.getNote());
    result.setNote(note);
    if (source.getUser() != null) {
      result.setUser(new UserSimpleConver().conver(source.getUser()));
    } else {
      result.setUser(new UserSimple());
    }
    result.setComments(source.getReplys());
    result.setLikes(source.getLikes());
    if (result.getComments() == null) {
      result.setComments(0L);
    }
    if (result.getLikes() == null) {
      result.setLikes(0L);
    }
    return result;
  }
}
