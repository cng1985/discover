package com.haoxuer.discover.weibo.data.entity;

import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.weibo.data.enums.FileType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@FormAnnotation(title = "微博点赞")
@Entity
@Table(name = "weibo_content_file")
public class WeiboFile extends AbstractEntity {

  private FileType fileType;

  @ManyToOne(fetch = FetchType.LAZY)
  private WeiBo weiBo;

  private String url;

  public FileType getFileType() {
    return fileType;
  }

  public void setFileType(FileType fileType) {
    this.fileType = fileType;
  }

  public WeiBo getWeiBo() {
    return weiBo;
  }

  public void setWeiBo(WeiBo weiBo) {
    this.weiBo = weiBo;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
