package com.haoxuer.discover.weibo.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.dao.TopicChannelDao;
import com.haoxuer.discover.weibo.data.entity.TopicChannel;
import com.haoxuer.discover.weibo.data.service.TopicChannelService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月19日11:43:37.
*/


@Scope("prototype")
@Service
@Transactional
public class TopicChannelServiceImpl implements TopicChannelService {

	private TopicChannelDao dao;


	@Override
	@Transactional(readOnly = true)
	public TopicChannel findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public TopicChannel save(TopicChannel bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public TopicChannel update(TopicChannel bean) {
		Updater<TopicChannel> updater = new Updater<TopicChannel>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public TopicChannel deleteById(Long id) {
		TopicChannel bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public TopicChannel[] deleteByIds(Long[] ids) {
		TopicChannel[] beans = new TopicChannel[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TopicChannelDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<TopicChannel> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<TopicChannel> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<TopicChannel> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}