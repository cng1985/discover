package com.haoxuer.discover.weibo.data.service;

import com.haoxuer.discover.weibo.data.entity.WeiBoLike;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2019年02月28日21:43:51.
*/
public interface WeiBoLikeService {

	WeiBoLike findById(Long id);

	WeiBoLike save(WeiBoLike bean);

	WeiBoLike update(WeiBoLike bean);

	WeiBoLike deleteById(Long id);
	
	WeiBoLike[] deleteByIds(Long[] ids);
	
	Page<WeiBoLike> page(Pageable pageable);
	
	Page<WeiBoLike> page(Pageable pageable, Object search);


	List<WeiBoLike> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}