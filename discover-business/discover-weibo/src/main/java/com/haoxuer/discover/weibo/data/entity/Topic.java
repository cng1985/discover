package com.haoxuer.discover.weibo.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.weibo.data.enums.CheckState;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@FormAnnotation(title = "主题")
@Entity
@Table(name = "weibo_topic")
public class Topic extends AbstractEntity {

  /**
   * 用户
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  /**
   * 讨论的内容
   */
  private String note;

  private CheckState checkState;


  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public CheckState getCheckState() {
    return checkState;
  }

  public void setCheckState(CheckState checkState) {
    this.checkState = checkState;
  }
}
