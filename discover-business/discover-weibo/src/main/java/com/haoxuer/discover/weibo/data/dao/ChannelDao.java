package com.haoxuer.discover.weibo.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.entity.Channel;

/**
 * Created by imake on 2018年12月19日11:32:12.
 */
public interface ChannelDao extends BaseDao<Channel, Long> {

  Channel findById(Long id);

  Channel channel(String name);

  Channel save(Channel bean);

  Channel updateByUpdater(Updater<Channel> updater);

  Channel deleteById(Long id);
}