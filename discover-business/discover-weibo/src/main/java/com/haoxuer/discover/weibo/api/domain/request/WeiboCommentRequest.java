package com.haoxuer.discover.weibo.api.domain.request;

public class WeiboCommentRequest extends WeiboPostRequest {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
