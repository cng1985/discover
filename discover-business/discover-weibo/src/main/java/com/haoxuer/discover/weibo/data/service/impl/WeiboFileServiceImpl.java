package com.haoxuer.discover.weibo.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.dao.WeiboFileDao;
import com.haoxuer.discover.weibo.data.entity.WeiboFile;
import com.haoxuer.discover.weibo.data.service.WeiboFileService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2019年02月28日21:31:43.
*/


@Scope("prototype")
@Service
@Transactional
public class WeiboFileServiceImpl implements WeiboFileService {

	private WeiboFileDao dao;


	@Override
	@Transactional(readOnly = true)
	public WeiboFile findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public WeiboFile save(WeiboFile bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public WeiboFile update(WeiboFile bean) {
		Updater<WeiboFile> updater = new Updater<WeiboFile>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public WeiboFile deleteById(Long id) {
		WeiboFile bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public WeiboFile[] deleteByIds(Long[] ids) {
		WeiboFile[] beans = new WeiboFile[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(WeiboFileDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<WeiboFile> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<WeiboFile> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<WeiboFile> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}