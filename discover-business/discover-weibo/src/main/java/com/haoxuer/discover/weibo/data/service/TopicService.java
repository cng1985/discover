package com.haoxuer.discover.weibo.data.service;

import com.haoxuer.discover.weibo.data.entity.Topic;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月19日11:32:12.
*/
public interface TopicService {

	Topic findById(Long id);

	Topic save(Topic bean);

	Topic update(Topic bean);

	Topic deleteById(Long id);
	
	Topic[] deleteByIds(Long[] ids);
	
	Page<Topic> page(Pageable pageable);
	
	Page<Topic> page(Pageable pageable, Object search);


	List<Topic> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}