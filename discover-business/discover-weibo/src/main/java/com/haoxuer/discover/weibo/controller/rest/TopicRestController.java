package com.haoxuer.discover.weibo.controller.rest;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.weibo.api.domain.page.TopicNewsPage;
import com.haoxuer.discover.weibo.api.domain.page.TopicPage;
import com.haoxuer.discover.weibo.api.domain.request.TopicChannelPageRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicNewsRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicPageRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicPostRequest;
import com.haoxuer.discover.weibo.api.apis.TopicApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/rest/topic")
public class TopicRestController {

  @RequestMapping(value = "/post")
  public ResponseObject post(TopicPostRequest request) {
    return api.post(request);
  }

  @RequestMapping(value = "/page")
  public TopicPage page(TopicPageRequest request) {
    return api.page(request);
  }

  @RequestMapping(value = "/channel")
  public TopicPage channel(TopicChannelPageRequest request) {
    return api.channel(request);
  }

  @RequestMapping(value = "/news")
  public TopicNewsPage news(TopicNewsRequest request) {
    return api.news(request);
  }

  @RequestMapping(value = "/mypub")
  public TopicPage myPub(TopicPageRequest request) {
    return api.myPub(request);
  }

  @Autowired
  private TopicApi api;
}
