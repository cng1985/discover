package com.haoxuer.discover.weibo.data.so;

import java.io.Serializable;

/**
* Created by imake on 2019年02月28日21:28:30.
*/
public class WeiBoChannelSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
