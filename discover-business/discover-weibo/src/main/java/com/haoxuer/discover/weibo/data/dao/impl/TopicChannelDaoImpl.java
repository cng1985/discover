package com.haoxuer.discover.weibo.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.weibo.data.dao.TopicChannelDao;
import com.haoxuer.discover.weibo.data.entity.TopicChannel;

/**
* Created by imake on 2018年12月19日11:43:37.
*/
@Repository

public class TopicChannelDaoImpl extends CriteriaDaoImpl<TopicChannel, Long> implements TopicChannelDao {

	@Override
	public TopicChannel findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public TopicChannel save(TopicChannel bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public TopicChannel deleteById(Long id) {
		TopicChannel entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<TopicChannel> getEntityClass() {
		return TopicChannel.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}