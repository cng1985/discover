package com.haoxuer.discover.weibo.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenPageObject;

public class PageByIdRequest extends RequestUserTokenPageObject {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
