package com.haoxuer.discover.weibo.data.entity;

import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@FormAnnotation(title = "频道")
@Entity
@Table(name = "weibo_channel")
public class Channel extends AbstractEntity {

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
