package com.haoxuer.discover.weibo.rest.conver;

import com.haoxuer.discover.config.rest.conver.UserSimpleConver;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.weibo.api.domain.simple.WeiBoLikeSimple;
import com.haoxuer.discover.weibo.data.entity.WeiBoLike;

public class WeiBoLikeSimpleConver implements Conver<WeiBoLikeSimple, WeiBoLike> {
  @Override
  public WeiBoLikeSimple conver(WeiBoLike source) {
    WeiBoLikeSimple result=new WeiBoLikeSimple();
    result.setId(source.getId());
    if (source.getAddDate()!=null){
      result.setAddDate(source.getAddDate().getTime());
    }
    if (source.getUser()!=null){
      result.setUser(new UserSimpleConver().conver(source.getUser()));
    }
    return result;
  }
}
