package com.haoxuer.discover.weibo.data.service;

import com.haoxuer.discover.weibo.data.entity.WeiBo;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2019年01月02日09:59:44.
*/
public interface WeiBoService {

	WeiBo findById(Long id);

	WeiBo save(WeiBo bean);

	WeiBo update(WeiBo bean);

	WeiBo deleteById(Long id);
	
	WeiBo[] deleteByIds(Long[] ids);
	
	Page<WeiBo> page(Pageable pageable);
	
	Page<WeiBo> page(Pageable pageable, Object search);


	List<WeiBo> list(int first, Integer size, List<Filter> filters, List<Order> orders);

  void confirm(Long id);
}