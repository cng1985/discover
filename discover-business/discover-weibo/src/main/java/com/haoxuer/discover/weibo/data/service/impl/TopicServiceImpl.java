package com.haoxuer.discover.weibo.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.dao.TopicDao;
import com.haoxuer.discover.weibo.data.entity.Topic;
import com.haoxuer.discover.weibo.data.service.TopicService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月19日11:32:12.
*/


@Scope("prototype")
@Service
@Transactional
public class TopicServiceImpl implements TopicService {

	private TopicDao dao;


	@Override
	@Transactional(readOnly = true)
	public Topic findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public Topic save(Topic bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public Topic update(Topic bean) {
		Updater<Topic> updater = new Updater<Topic>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public Topic deleteById(Long id) {
		Topic bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public Topic[] deleteByIds(Long[] ids) {
		Topic[] beans = new Topic[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(TopicDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<Topic> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<Topic> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<Topic> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}