package com.haoxuer.discover.weibo.controller.rest;


import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.weibo.api.apis.WeiBoApi;
import com.haoxuer.discover.weibo.api.domain.page.WeiBoCommentPage;
import com.haoxuer.discover.weibo.api.domain.page.WeiBoLikePage;
import com.haoxuer.discover.weibo.api.domain.page.WeiboPage;
import com.haoxuer.discover.weibo.api.domain.request.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/json/weibo")
public class WeiBoJsonController {

  @RequestMapping(value = "/findbyid")
  public ResponseObject findById(@RequestBody FindByIdRequest request) {
    return api.findById(request);
  }

  @RequestMapping(value = "/post")
  public ResponseObject post(@RequestBody WeiboPostRequest request) {
    return api.post(request);
  }

  @RequestMapping(value = "/page")
  public WeiboPage page(@RequestBody WeiboPageRequest request) {
    return api.page(request);
  }

  @RequestMapping(value = "/mypub")
  public WeiboPage myPub(@RequestBody WeiboPageRequest request) {
    return api.myPub(request);
  }

  @RequestMapping(value = "/like")
  public ResponseObject like(@RequestBody FindByIdRequest request) {
    return api.like(request);
  }

  @RequestMapping(value = "/unlike")
  public ResponseObject unLike(@RequestBody FindByIdRequest request) {
    return api.unLike(request);
  }

  @RequestMapping(value = "/comment")
  public ResponseObject comment(@RequestBody WeiboCommentRequest request) {
    return api.comment(request);
  }

  @RequestMapping(value = "/pageforcomment")
  public WeiBoCommentPage pageForComment(@RequestBody PageByIdRequest request) {
    return api.pageForComment(request);
  }

  @RequestMapping(value = "/pageforlike")
  public WeiBoLikePage pageForLike(@RequestBody PageByIdRequest request) {
    return api.pageForLike(request);
  }

  @Autowired
  WeiBoApi api;
}
