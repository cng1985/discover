package com.haoxuer.discover.weibo.data.so;

import java.io.Serializable;

/**
* Created by imake on 2018年12月19日11:43:37.
*/
public class TopicChannelSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
