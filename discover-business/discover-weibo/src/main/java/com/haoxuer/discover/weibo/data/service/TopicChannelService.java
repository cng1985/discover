package com.haoxuer.discover.weibo.data.service;

import com.haoxuer.discover.weibo.data.entity.TopicChannel;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月19日11:43:37.
*/
public interface TopicChannelService {

	TopicChannel findById(Long id);

	TopicChannel save(TopicChannel bean);

	TopicChannel update(TopicChannel bean);

	TopicChannel deleteById(Long id);
	
	TopicChannel[] deleteByIds(Long[] ids);
	
	Page<TopicChannel> page(Pageable pageable);
	
	Page<TopicChannel> page(Pageable pageable, Object search);


	List<TopicChannel> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}