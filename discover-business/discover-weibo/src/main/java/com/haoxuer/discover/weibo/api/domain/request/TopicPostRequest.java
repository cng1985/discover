package com.haoxuer.discover.weibo.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

import java.util.List;

public class TopicPostRequest extends RequestUserTokenObject {

  private String note;

  private List<String> channels;

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public List<String> getChannels() {
    return channels;
  }

  public void setChannels(List<String> channels) {
    this.channels = channels;
  }
}
