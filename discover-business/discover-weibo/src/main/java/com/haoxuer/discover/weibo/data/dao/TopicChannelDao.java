package com.haoxuer.discover.weibo.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.weibo.data.entity.TopicChannel;

/**
* Created by imake on 2018年12月19日11:43:37.
*/
public interface TopicChannelDao extends BaseDao<TopicChannel,Long>{

	 TopicChannel findById(Long id);

	 TopicChannel save(TopicChannel bean);

	 TopicChannel updateByUpdater(Updater<TopicChannel> updater);

	 TopicChannel deleteById(Long id);
}