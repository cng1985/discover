package com.haoxuer.discover.weibo.rest.conver;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.weibo.data.entity.WeiBoChannel;

public class ChannelStringConver implements Conver<String, WeiBoChannel> {
  @Override
  public String conver(WeiBoChannel weiBoChannel) {
    if (weiBoChannel.getChannel() != null) {
      return weiBoChannel.getChannel().getName();
    } else {
      return "";
    }
  }
}
