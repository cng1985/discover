package com.haoxuer.discover.weibo.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.weibo.data.entity.WeiboFile;

/**
* Created by imake on 2019年02月28日21:31:43.
*/
public interface WeiboFileDao extends BaseDao<WeiboFile,Long>{

	 WeiboFile findById(Long id);

	 WeiboFile save(WeiboFile bean);

	 WeiboFile updateByUpdater(Updater<WeiboFile> updater);

	 WeiboFile deleteById(Long id);
}