package com.haoxuer.discover.weibo.api.domain.request;

public class TopicNewsRequest extends TopicPageRequest {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
