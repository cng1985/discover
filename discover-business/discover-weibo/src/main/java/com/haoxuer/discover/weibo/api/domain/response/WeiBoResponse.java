package com.haoxuer.discover.weibo.api.domain.response;

import com.haoxuer.discover.config.api.domain.simple.UserSimple;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.weibo.api.domain.simple.WeiBoLikeSimple;
import com.haoxuer.discover.weibo.api.domain.simple.WeiBoCommentSimple;

import java.util.ArrayList;
import java.util.List;

public class WeiBoResponse extends ResponseObject {

  private Long id;

  private Long addDate;

  private String note;

  private UserSimple user;

  private List<String> channels=new ArrayList<>();

  private List<WeiBoCommentSimple> commentList=new ArrayList<>();

  private List<WeiBoLikeSimple> likeList=new ArrayList<>();

  private List<String> images=new ArrayList<>();


  private Long likes;

  private Long comments;

  /**
   * 是否点赞
   */
  private Boolean liked;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAddDate() {
    return addDate;
  }

  public void setAddDate(Long addDate) {
    this.addDate = addDate;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public UserSimple getUser() {
    return user;
  }

  public void setUser(UserSimple user) {
    this.user = user;
  }

  public List<String> getChannels() {
    return channels;
  }

  public void setChannels(List<String> channels) {
    this.channels = channels;
  }

  public List<WeiBoCommentSimple> getCommentList() {
    return commentList;
  }

  public void setCommentList(List<WeiBoCommentSimple> commentList) {
    this.commentList = commentList;
  }

  public List<WeiBoLikeSimple> getLikeList() {
    return likeList;
  }

  public void setLikeList(List<WeiBoLikeSimple> likeList) {
    this.likeList = likeList;
  }

  public Long getLikes() {
    return likes;
  }

  public void setLikes(Long likes) {
    this.likes = likes;
  }

  public Long getComments() {
    return comments;
  }

  public void setComments(Long comments) {
    this.comments = comments;
  }

  public List<String> getImages() {
    return images;
  }

  public void setImages(List<String> images) {
    this.images = images;
  }

  public Boolean getLiked() {
    return liked;
  }

  public void setLiked(Boolean liked) {
    this.liked = liked;
  }
}
