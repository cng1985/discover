package com.haoxuer.discover.weibo.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.dao.WeiBoChannelDao;
import com.haoxuer.discover.weibo.data.entity.WeiBoChannel;
import com.haoxuer.discover.weibo.data.service.WeiBoChannelService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2019年02月28日21:28:30.
*/


@Scope("prototype")
@Service
@Transactional
public class WeiBoChannelServiceImpl implements WeiBoChannelService {

	private WeiBoChannelDao dao;


	@Override
	@Transactional(readOnly = true)
	public WeiBoChannel findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public WeiBoChannel save(WeiBoChannel bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public WeiBoChannel update(WeiBoChannel bean) {
		Updater<WeiBoChannel> updater = new Updater<WeiBoChannel>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public WeiBoChannel deleteById(Long id) {
		WeiBoChannel bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public WeiBoChannel[] deleteByIds(Long[] ids) {
		WeiBoChannel[] beans = new WeiBoChannel[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(WeiBoChannelDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<WeiBoChannel> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<WeiBoChannel> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<WeiBoChannel> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}