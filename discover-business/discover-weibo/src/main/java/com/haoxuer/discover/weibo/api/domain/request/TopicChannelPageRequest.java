package com.haoxuer.discover.weibo.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenPageObject;

public class TopicChannelPageRequest extends RequestUserTokenPageObject {

  private String channel;

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }
}
