package com.haoxuer.discover.weibo.data.service;

import com.haoxuer.discover.weibo.data.entity.WeiBoChannel;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2019年02月28日21:28:30.
*/
public interface WeiBoChannelService {

	WeiBoChannel findById(Long id);

	WeiBoChannel save(WeiBoChannel bean);

	WeiBoChannel update(WeiBoChannel bean);

	WeiBoChannel deleteById(Long id);
	
	WeiBoChannel[] deleteByIds(Long[] ids);
	
	Page<WeiBoChannel> page(Pageable pageable);
	
	Page<WeiBoChannel> page(Pageable pageable, Object search);


	List<WeiBoChannel> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}