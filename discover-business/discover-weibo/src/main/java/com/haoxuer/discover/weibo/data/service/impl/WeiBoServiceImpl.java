package com.haoxuer.discover.weibo.data.service.impl;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.weibo.data.dao.TopicDao;
import com.haoxuer.discover.weibo.data.entity.Topic;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.dao.WeiBoDao;
import com.haoxuer.discover.weibo.data.entity.WeiBo;
import com.haoxuer.discover.weibo.data.service.WeiBoService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
 * Created by imake on 2019年01月02日09:59:44.
 */


@Scope("prototype")
@Service
@Transactional
public class WeiBoServiceImpl implements WeiBoService {

  private WeiBoDao dao;

  @Autowired
  private TopicDao topicDao;


  @Override
  @Transactional(readOnly = true)
  public WeiBo findById(Long id) {
    return dao.findById(id);
  }


  @Override
  @Transactional
  public WeiBo save(WeiBo bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public WeiBo update(WeiBo bean) {
    Updater<WeiBo> updater = new Updater<WeiBo>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public WeiBo deleteById(Long id) {
    WeiBo bean = dao.findById(id);
    if (bean != null) {
      bean.setStoreState(StoreState.draft);
    }
    return bean;
  }

  @Override
  @Transactional
  public WeiBo[] deleteByIds(Long[] ids) {
    WeiBo[] beans = new WeiBo[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(WeiBoDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<WeiBo> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<WeiBo> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<WeiBo> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }

  @Override
  public void confirm(Long id) {
    WeiBo bean = dao.findById(id);
    if (bean != null) {
      bean.setStoreState(StoreState.archive);
      Topic topic = new Topic();
      topic.setNote(bean.getNote());
      topic.setUser(bean.getUser());
      topicDao.save(topic);
    }
  }
}