package com.haoxuer.discover.weibo.data.dao.impl;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.weibo.data.dao.WeiBoLikeDao;
import com.haoxuer.discover.weibo.data.entity.WeiBoLike;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imake on 2019年02月28日21:43:51.
 */
@Repository

public class WeiBoLikeDaoImpl extends CriteriaDaoImpl<WeiBoLike, Long> implements WeiBoLikeDao {

  @Override
  public WeiBoLike findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public WeiBoLike findByUser(Long user, Long id) {
    return one(Filter.eq("user.id", user), Filter.eq("weiBo.id", id));
  }

  @Override
  public WeiBoLike save(WeiBoLike bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public WeiBoLike deleteById(Long id) {
    WeiBoLike entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public Boolean liked(Long id, Long user) {
    List<Filter> filters = new ArrayList<>();
    filters.add(Filter.eq("weiBo.id", id));
    filters.add(Filter.eq("user.id", user));
    List<WeiBoLike> likes = list(0, 10, filters, null);
    if (likes != null && likes.size() > 0) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  protected Class<WeiBoLike> getEntityClass() {
    return WeiBoLike.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}