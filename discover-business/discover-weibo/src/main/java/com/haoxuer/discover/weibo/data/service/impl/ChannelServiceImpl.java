package com.haoxuer.discover.weibo.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.weibo.data.dao.ChannelDao;
import com.haoxuer.discover.weibo.data.entity.Channel;
import com.haoxuer.discover.weibo.data.service.ChannelService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月19日11:32:12.
*/


@Scope("prototype")
@Service
@Transactional
public class ChannelServiceImpl implements ChannelService {

	private ChannelDao dao;


	@Override
	@Transactional(readOnly = true)
	public Channel findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public Channel save(Channel bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public Channel update(Channel bean) {
		Updater<Channel> updater = new Updater<Channel>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public Channel deleteById(Long id) {
		Channel bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public Channel[] deleteByIds(Long[] ids) {
		Channel[] beans = new Channel[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ChannelDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<Channel> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<Channel> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<Channel> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}