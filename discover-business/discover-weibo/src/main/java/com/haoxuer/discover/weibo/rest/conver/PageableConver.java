package com.haoxuer.discover.weibo.rest.conver;

import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.rest.base.RequestUserTokenPageObject;

public class PageableConver implements Conver<Pageable, RequestUserTokenPageObject> {
  @Override
  public Pageable conver(RequestUserTokenPageObject source) {
    Pageable result=new Pageable();
    result.setPageNo(source.getNo());
    result.setPageSize(source.getSize());
    return result;
  }
}
