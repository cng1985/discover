package com.haoxuer.discover.weibo.api.domain.page;

import com.haoxuer.discover.rest.base.ResponsePage;
import com.haoxuer.discover.weibo.api.domain.simple.TopicSimple;

public class TopicNewsPage extends ResponsePage<TopicSimple> {

  /**
   * 最新数据的id
   */
  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


}
