package com.haoxuer.discover.weibo.rest.conver;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.weibo.api.domain.simple.TopicSimple;
import com.haoxuer.discover.weibo.data.entity.TopicChannel;

public class TopicSimple2Conver implements Conver<TopicSimple, TopicChannel> {
  @Override
  public TopicSimple conver(TopicChannel source) {

    TopicSimple result = new TopicSimple();
    if (source.getChannel() != null) {
      result = new TopicSimpleConver().conver(source.getTopic());
    }
    return result;
  }
}
