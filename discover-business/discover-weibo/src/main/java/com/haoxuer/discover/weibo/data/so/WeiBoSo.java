package com.haoxuer.discover.weibo.data.so;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;

import java.io.Serializable;

/**
* Created by imake on 2019年01月02日09:59:44.
*/
public class WeiBoSo implements Serializable {

    @Search(name = "note",operator = Filter.Operator.like)
    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
