package com.haoxuer.discover.weibo.controller.admin;

import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.weibo.data.so.WeiBoSo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import  com.haoxuer.discover.weibo.data.entity.WeiBo;
import com.haoxuer.discover.weibo.data.service.WeiBoService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.context.annotation.Scope;

/**
*
* Created by imake on 2019年01月02日09:59:44.
*/


@Scope("prototype")
@Controller
public class WeiBoAction {

	public static final String MODEL = "model";

	public static final String REDIRECT_LIST_HTML = "redirect:/admin/weibo/view_list.htm";

	private static final Logger log = LoggerFactory.getLogger(WeiBoAction.class);

	@Autowired
	private WeiBoService manager;

	@RequiresPermissions("weibo")
	@RequestMapping("/admin/weibo/view_list")
	public String list(Pageable pageable,WeiBoSo so,ModelMap model) {

		if (pageable==null){
			pageable=new Pageable();
		}
		if (pageable.getOrders().isEmpty()) {
			pageable.getOrders().add(Order.desc("id"));
		}
		pageable.getFilters().addAll(FilterUtils.getFilters(so));
		pageable.getFilters().add(Filter.eq("storeState", StoreState.normal));
		Page<WeiBo> pagination = manager.page(pageable);
		model.addAttribute("list", pagination.getContent());
		model.addAttribute("page", pagination);
		model.addAttribute("so", so);
		return "/admin/weibo/list";
	}

	@RequiresPermissions("weibo")
	@RequestMapping("/admin/weibo/confirm")
	public String confirm(Pageable pageable, long id, RedirectAttributes redirectAttributes, ModelMap model) {

		String view = REDIRECT_LIST_HTML;
		manager.confirm(id);
		return REDIRECT_LIST_HTML;
	}


	@RequiresPermissions("weibo")
	@RequestMapping("/admin/weibo/model_delete")
	public String delete(Pageable pageable, Long id, RedirectAttributes redirectAttributes) {

		String view=REDIRECT_LIST_HTML;
		try {
			initRedirectData(pageable, redirectAttributes);
			manager.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			log.error("删除失败",e);
			redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
		}

		return view;
	}

	@RequiresPermissions("weibo")
	@RequestMapping("/admin/weibo/model_deletes")
	public String deletes(Pageable pageable, Long[] ids,RedirectAttributes redirectAttributes) {

		String view=REDIRECT_LIST_HTML;

		try{
				initRedirectData(pageable, redirectAttributes);
				manager.deleteByIds(ids);
			} catch (DataIntegrityViolationException e) {
				log.error("批量删除失败",e);
				redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
			}
		return view;
	}
	private void initRedirectData(Pageable pageable, RedirectAttributes redirectAttributes) {
		redirectAttributes.addAttribute("pageNumber",pageable.getPageNumber());
	}
}