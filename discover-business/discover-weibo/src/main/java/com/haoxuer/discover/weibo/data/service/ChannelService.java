package com.haoxuer.discover.weibo.data.service;

import com.haoxuer.discover.weibo.data.entity.Channel;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月19日11:32:12.
*/
public interface ChannelService {

	Channel findById(Long id);

	Channel save(Channel bean);

	Channel update(Channel bean);

	Channel deleteById(Long id);
	
	Channel[] deleteByIds(Long[] ids);
	
	Page<Channel> page(Pageable pageable);
	
	Page<Channel> page(Pageable pageable, Object search);


	List<Channel> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}