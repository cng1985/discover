package com.haoxuer.discover.weibo.api.domain.simple;

import com.haoxuer.discover.config.api.domain.simple.UserSimple;

import java.io.Serializable;

public class WeiBoCommentSimple implements Serializable {

  private Long id;

  private Long addDate;

  private String note;

  private UserSimple user;


  private Long likes;

  private Long comments;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAddDate() {
    return addDate;
  }

  public void setAddDate(Long addDate) {
    this.addDate = addDate;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public UserSimple getUser() {
    return user;
  }

  public void setUser(UserSimple user) {
    this.user = user;
  }

  public Long getLikes() {
    return likes;
  }

  public void setLikes(Long likes) {
    this.likes = likes;
  }

  public Long getComments() {
    return comments;
  }

  public void setComments(Long comments) {
    this.comments = comments;
  }
}
