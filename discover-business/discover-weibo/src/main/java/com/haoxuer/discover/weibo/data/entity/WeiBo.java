package com.haoxuer.discover.weibo.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.weibo.data.enums.WeiBoType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@FormAnnotation(title = "微博")
@Entity
@Table(name = "weibo_content")
public class WeiBo extends AbstractEntity {

  public static WeiBo fromId(Long id) {
    WeiBo result = new WeiBo();
    result.setId(id);
    return result;
  }

  @FormField(title = "嘉宾", sortNum = "1", grid = true, col = 12)
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  @FormField(title = "内容", sortNum = "1", grid = true, col = 12)
  private String note;

  private StoreState storeState;

  private WeiBoType weiBoType;

  /**
   * 根话题
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private WeiBo root;

  /**
   * 回复量
   */
  private Long replys;

  /**
   * 转发量
   */
  private Long forwards;


  /**
   * 喜欢量
   */
  private Long likes;

  @OrderBy(" id desc ")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "weiBo")
  private List<WeiboFile> files = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "weiBo")
  private List<WeiBoChannel> channels=new ArrayList<>();


  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public StoreState getStoreState() {
    return storeState;
  }

  public void setStoreState(StoreState storeState) {
    this.storeState = storeState;
  }

  public WeiBoType getWeiBoType() {
    return weiBoType;
  }

  public void setWeiBoType(WeiBoType weiBoType) {
    this.weiBoType = weiBoType;
  }


  public WeiBo getRoot() {
    return root;
  }

  public void setRoot(WeiBo root) {
    this.root = root;
  }

  public Long getReplys() {
    return replys;
  }

  public void setReplys(Long replys) {
    this.replys = replys;
  }

  public Long getForwards() {
    return forwards;
  }

  public void setForwards(Long forwards) {
    this.forwards = forwards;
  }

  public Long getLikes() {
    return likes;
  }

  public void setLikes(Long likes) {
    this.likes = likes;
  }

  public List<WeiboFile> getFiles() {
    return files;
  }

  public void setFiles(List<WeiboFile> files) {
    this.files = files;
  }

  public List<WeiBoChannel> getChannels() {
    return channels;
  }

  public void setChannels(List<WeiBoChannel> channels) {
    this.channels = channels;
  }
}
