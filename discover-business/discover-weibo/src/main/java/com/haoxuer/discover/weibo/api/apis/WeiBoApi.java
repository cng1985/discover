package com.haoxuer.discover.weibo.api.apis;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.weibo.api.domain.page.WeiBoCommentPage;
import com.haoxuer.discover.weibo.api.domain.page.WeiBoLikePage;
import com.haoxuer.discover.weibo.api.domain.page.WeiboPage;
import com.haoxuer.discover.weibo.api.domain.request.*;
import com.haoxuer.discover.weibo.api.domain.response.WeiBoResponse;

public interface WeiBoApi {

  /**
   * 发微博
   *
   * @param request
   * @return
   */
  ResponseObject post(WeiboPostRequest request);

  /**
   * 某条微博详情
   *
   * @param request
   * @return
   */
  WeiBoResponse findById(FindByIdRequest request);


  /**
   * 某条微博的评论
   *
   * @return
   */
  WeiBoCommentPage pageForComment(PageByIdRequest request);


  /**
   * 某条微博的点赞人
   * @param request
   * @return
   */
  WeiBoLikePage pageForLike(PageByIdRequest request);


  /**
   * 微博列表
   *
   * @param request
   * @return
   */
  WeiboPage page(WeiboPageRequest request);


  /**
   * 我发布的微博
   *
   * @param request
   * @return
   */
  WeiboPage myPub(WeiboPageRequest request);


  /**
   * 点赞
   *
   * @param request
   * @return
   */
  ResponseObject like(FindByIdRequest request);

  /**
   * 取消点赞
   *
   * @param request
   * @return
   */
  ResponseObject unLike(FindByIdRequest request);


  /**
   * 回复某条微博
   *
   * @param request
   * @return
   */
  ResponseObject comment(WeiboCommentRequest request);


}
