package com.haoxuer.discover.weibo.rest.resource;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.user.service.UserTokenService;
import com.haoxuer.discover.weibo.api.apis.TopicApi;
import com.haoxuer.discover.weibo.api.domain.page.TopicNewsPage;
import com.haoxuer.discover.weibo.api.domain.page.TopicPage;
import com.haoxuer.discover.weibo.api.domain.request.TopicChannelPageRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicNewsRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicPageRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicPostRequest;
import com.haoxuer.discover.weibo.api.domain.simple.TopicSimple;
import com.haoxuer.discover.weibo.data.dao.ChannelDao;
import com.haoxuer.discover.weibo.data.dao.TopicChannelDao;
import com.haoxuer.discover.weibo.data.dao.TopicDao;
import com.haoxuer.discover.weibo.data.dao.WeiBoDao;
import com.haoxuer.discover.weibo.data.entity.Channel;
import com.haoxuer.discover.weibo.data.entity.Topic;
import com.haoxuer.discover.weibo.data.entity.TopicChannel;
import com.haoxuer.discover.weibo.rest.conver.PageableConver;
import com.haoxuer.discover.weibo.rest.conver.TopicSimple2Conver;
import com.haoxuer.discover.weibo.rest.conver.TopicSimpleConver;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;


@Scope("prototype")
@Component
@Transactional
public class TopicResource implements TopicApi {

  @Autowired
  TopicDao topicDao;

  @Autowired
  ChannelDao channelDao;

  @Autowired
  TopicChannelDao topicChannelDao;

  @Autowired
  UserTokenService tokenService;

  @Autowired
  WeiBoDao weiBoDao;


  @Override
  public ResponseObject post(TopicPostRequest request) {
    return handle(request);
  }

  private ResponseObject handle(TopicPostRequest request) {
    ResponseObject result = new ResponseObject();
    if (StringUtils.isEmpty(request.getNote())) {
      result.setCode(501);
      result.setMsg("内容不能为空");
      return result;
    }
    Long id = tokenService.user(request.getUserToken());
    if (id == null) {
      result.setCode(502);
      result.setMsg("用户不存在");
      return result;
    }
    Topic bean = new Topic();
    String note = EmojiParser.parseToUnicode(request.getNote());
    bean.setNote(note);
    bean.setUser(User.fromId(id));
    topicDao.save(bean);
    if (request.getChannels() != null) {
      for (String channel : request.getChannels()) {
        if (StringUtils.isEmpty(channel)) {
          continue;
        }
        Channel item = channelDao.channel(channel);
        TopicChannel topicChannel = new TopicChannel();
        topicChannel.setChannel(item);
        topicChannel.setTopic(bean);
        topicChannelDao.save(topicChannel);
      }
    }

    return result;
  }

  @Override
  public TopicPage page(TopicPageRequest request) {
    TopicPage result = new TopicPage();
    Pageable pageable = new PageableConver().conver(request);
    pageable.getOrders().add(Order.desc("id"));
    Page<Topic> page = topicDao.page(pageable);
    ConverResourceUtils.converPage(result, page, new TopicSimpleConver());
    return result;
  }

  @Override
  public TopicPage myPub(TopicPageRequest request) {
    Long uid = tokenService.user(request.getUserToken());

    TopicPage result = new TopicPage();
    Pageable pageable = new PageableConver().conver(request);
    pageable.getOrders().add(Order.desc("id"));
    pageable.getFilters().add(Filter.eq("user.id", uid));
    Page<Topic> page = topicDao.page(pageable);
    ConverResourceUtils.converPage(result, page, new TopicSimpleConver());
    return result;
  }

  @Override
  public TopicNewsPage news(TopicNewsRequest request) {
    TopicNewsPage result = new TopicNewsPage();
    Pageable pageable = new PageableConver().conver(request);
    if (request.getId() != null) {
      pageable.getFilters().add(Filter.gt("id", request.getId()));
      pageable.getOrders().add(Order.asc("id"));
    } else {
      pageable.getOrders().add(Order.desc("id"));
    }
    Page<Topic> page = topicDao.page(pageable);
    ConverResourceUtils.converPage(result, page, new TopicSimpleConver());
    List<TopicSimple> simples = result.getList();
    if (simples != null && simples.size() > 0) {
      Long id = simples.stream().map(item -> item.getId()).max((a, b) -> a.compareTo(b)).get();
      result.setId(id);
    }
    if (result.getId() == null) {
      result.setId(request.getId());
    }
    return result;
  }

  @Override
  public TopicPage channel(TopicChannelPageRequest request) {
    TopicPage result = new TopicPage();
    if (StringUtils.isEmpty(request.getChannel())) {
      result.setCode(501);
      result.setMsg("无效频道");
      return result;
    }
    Channel channel = channelDao.channel(request.getChannel());
    Pageable pageable = new PageableConver().conver(request);
    pageable.getOrders().add(Order.desc("id"));
    pageable.getFilters().add(Filter.ge("channel.id", channel.getId()));
    Page<TopicChannel> page = topicChannelDao.page(pageable);
    ConverResourceUtils.converPage(result, page, new TopicSimple2Conver());
    return result;
  }
}
