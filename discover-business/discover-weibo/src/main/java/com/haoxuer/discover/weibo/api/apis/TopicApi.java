package com.haoxuer.discover.weibo.api.apis;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.weibo.api.domain.page.TopicNewsPage;
import com.haoxuer.discover.weibo.api.domain.page.TopicPage;
import com.haoxuer.discover.weibo.api.domain.request.TopicChannelPageRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicNewsRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicPageRequest;
import com.haoxuer.discover.weibo.api.domain.request.TopicPostRequest;

public interface TopicApi {

  /**
   * 发布一个微博
   *
   * @param request
   * @return
   */
  ResponseObject post(TopicPostRequest request);

  /**
   * 微博列表
   *
   * @param request
   * @return
   */
  TopicPage page(TopicPageRequest request);


  /**
   * 我发布的微博
   * @param request
   * @return
   */
  TopicPage myPub(TopicPageRequest request);



  /**
   * 根据最新的id获取最新的信息
   * @param request
   * @return
   */
  TopicNewsPage news(TopicNewsRequest request);


  /**
   * 某个频道的微博
   * @param request
   * @return
   */
  TopicPage channel(TopicChannelPageRequest request);

}
