package com.haoxuer.discover.weibo.rest.conver;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.weibo.data.entity.WeiboFile;

public class ImageStringConver implements Conver<String, WeiboFile> {
  @Override
  public String conver(WeiboFile weiboFile) {
    return weiboFile.getUrl();
  }
}
