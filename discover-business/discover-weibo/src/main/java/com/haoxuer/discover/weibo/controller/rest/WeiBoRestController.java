package com.haoxuer.discover.weibo.controller.rest;


import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.weibo.api.apis.WeiBoApi;
import com.haoxuer.discover.weibo.api.domain.page.WeiBoCommentPage;
import com.haoxuer.discover.weibo.api.domain.page.WeiBoLikePage;
import com.haoxuer.discover.weibo.api.domain.page.WeiboPage;
import com.haoxuer.discover.weibo.api.domain.request.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/rest/weibo")
public class WeiBoRestController {

  @RequestMapping(value = "/findbyid")
  public ResponseObject findById(FindByIdRequest request) {
    return api.findById(request);
  }

  @RequestMapping(value = "/post")
  public ResponseObject post(WeiboPostRequest request) {
    return api.post(request);
  }

  @RequestMapping(value = "/page")
  public WeiboPage page(WeiboPageRequest request) {
    return api.page(request);
  }

  @RequestMapping(value = "/mypub")
  public WeiboPage myPub(WeiboPageRequest request) {
    return api.myPub(request);
  }

  @RequestMapping(value = "/like")
  public ResponseObject like(FindByIdRequest request) {
    return api.like(request);
  }

  @RequestMapping(value = "/unlike")
  public ResponseObject unLike(FindByIdRequest request) {
    return api.unLike(request);
  }

  @RequestMapping(value = "/comment")
  public ResponseObject comment(WeiboCommentRequest request) {
    return api.comment(request);
  }

  @RequestMapping(value = "/pageforcomment")
  public WeiBoCommentPage pageForComment(PageByIdRequest request) {
    return api.pageForComment(request);
  }

  @RequestMapping(value = "/pageforlike")
  public WeiBoLikePage pageForLike(PageByIdRequest request) {
    return api.pageForLike(request);
  }

  @Autowired
  WeiBoApi api;
}
