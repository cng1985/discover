package com.haoxuer.discover.weibo.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.weibo.data.dao.TopicDao;
import com.haoxuer.discover.weibo.data.entity.Topic;

/**
* Created by imake on 2018年12月19日11:32:12.
*/
@Repository

public class TopicDaoImpl extends CriteriaDaoImpl<Topic, Long> implements TopicDao {


	@Override
	public Topic findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Topic save(Topic bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Topic deleteById(Long id) {
		Topic entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Topic> getEntityClass() {
		return Topic.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}