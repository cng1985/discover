package com.haoxuer.discover.weibo.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.weibo.data.enums.LikeType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@FormAnnotation(title = "微博点赞")
@Entity
@Table(name = "weibo_content_like")
public class WeiBoLike extends AbstractEntity {

  @ManyToOne(fetch = FetchType.LAZY)
  private WeiBo weiBo;

  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  private LikeType likeType;

  public WeiBo getWeiBo() {
    return weiBo;
  }

  public void setWeiBo(WeiBo weiBo) {
    this.weiBo = weiBo;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public LikeType getLikeType() {
    return likeType;
  }

  public void setLikeType(LikeType likeType) {
    this.likeType = likeType;
  }
}
