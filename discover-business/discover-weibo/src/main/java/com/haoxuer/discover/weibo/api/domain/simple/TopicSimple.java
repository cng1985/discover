package com.haoxuer.discover.weibo.api.domain.simple;

import com.haoxuer.discover.config.api.domain.simple.UserSimple;
import com.haoxuer.discover.config.data.entity.User;

import java.io.Serializable;

public class TopicSimple implements Serializable {

  private Long id;

  private Long addDate;

  private String note;

  private UserSimple user;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAddDate() {
    return addDate;
  }

  public void setAddDate(Long addDate) {
    this.addDate = addDate;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public UserSimple getUser() {
    return user;
  }

  public void setUser(UserSimple user) {
    this.user = user;
  }
}
