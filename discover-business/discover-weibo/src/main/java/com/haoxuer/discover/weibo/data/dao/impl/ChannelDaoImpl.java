package com.haoxuer.discover.weibo.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.weibo.data.dao.ChannelDao;
import com.haoxuer.discover.weibo.data.entity.Channel;

/**
 * Created by imake on 2018年12月19日11:32:12.
 */
@Repository

public class ChannelDaoImpl extends CriteriaDaoImpl<Channel, Long> implements ChannelDao {

  @Override
  public Channel findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public Channel channel(String name) {
    Channel channel = one(Filter.eq("name", name));
    if (channel == null) {
      channel = new Channel();
      channel.setName(name);
      save(channel);
    }
    return channel;
  }

  @Override
  public Channel save(Channel bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public Channel deleteById(Long id) {
    Channel entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<Channel> getEntityClass() {
    return Channel.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}