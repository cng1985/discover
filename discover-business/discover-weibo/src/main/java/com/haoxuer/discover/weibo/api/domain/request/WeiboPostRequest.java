package com.haoxuer.discover.weibo.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

import java.util.List;

public class WeiboPostRequest extends RequestUserTokenObject {

  private String note;

  private List<String> channels;

  private List<String> images;

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public List<String> getChannels() {
    return channels;
  }

  public void setChannels(List<String> channels) {
    this.channels = channels;
  }

  public List<String> getImages() {
    return images;
  }

  public void setImages(List<String> images) {
    this.images = images;
  }
}
