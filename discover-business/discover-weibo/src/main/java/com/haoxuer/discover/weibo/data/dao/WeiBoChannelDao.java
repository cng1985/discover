package com.haoxuer.discover.weibo.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.weibo.data.entity.WeiBoChannel;

/**
* Created by imake on 2019年02月28日21:28:30.
*/
public interface WeiBoChannelDao extends BaseDao<WeiBoChannel,Long>{

	 WeiBoChannel findById(Long id);

	 WeiBoChannel save(WeiBoChannel bean);

	 WeiBoChannel updateByUpdater(Updater<WeiBoChannel> updater);

	 WeiBoChannel deleteById(Long id);
}