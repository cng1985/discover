package com.haoxuer.discover.weibo.rest.conver;

import com.haoxuer.discover.config.api.domain.simple.UserSimple;
import com.haoxuer.discover.config.rest.conver.UserSimpleConver;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.weibo.api.domain.simple.TopicSimple;
import com.haoxuer.discover.weibo.data.entity.Topic;
import com.vdurmont.emoji.EmojiParser;

public class TopicSimpleConver implements Conver<TopicSimple, Topic> {
  @Override
  public TopicSimple conver(Topic source) {
    TopicSimple result = new TopicSimple();
    if (source.getAddDate() != null) {
      result.setAddDate(source.getAddDate().getTime());
    }
    result.setId(source.getId());
    String note = EmojiParser.parseToUnicode(source.getNote());
    result.setNote(note);
    if (source.getUser() != null) {
      result.setUser(new UserSimpleConver().conver(source.getUser()));
    } else {
      result.setUser(new UserSimple());
    }
    return result;
  }
}
