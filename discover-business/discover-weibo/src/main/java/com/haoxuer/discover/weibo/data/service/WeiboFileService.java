package com.haoxuer.discover.weibo.data.service;

import com.haoxuer.discover.weibo.data.entity.WeiboFile;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2019年02月28日21:31:43.
*/
public interface WeiboFileService {

	WeiboFile findById(Long id);

	WeiboFile save(WeiboFile bean);

	WeiboFile update(WeiboFile bean);

	WeiboFile deleteById(Long id);
	
	WeiboFile[] deleteByIds(Long[] ids);
	
	Page<WeiboFile> page(Pageable pageable);
	
	Page<WeiboFile> page(Pageable pageable, Object search);


	List<WeiboFile> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}