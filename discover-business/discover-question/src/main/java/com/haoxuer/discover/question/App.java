package com.haoxuer.discover.question;

import com.haoxuer.discover.question.data.entity.*;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);

        File view = new File("E:\\mvnspace\\usite\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(Question.class);
        cs.add(QuestionAnswer.class);
        cs.add(QuestionAnswerVote.class);
        cs.add(QuestionCatalog.class);
        cs.add(QuestionFavorite.class);
        cs.add(QuestionPoints.class);
        cs.add(QuestionTag.class);
        cs.add(QuestionVote.class);
        cs.add(QuestionWord.class);


        make.setDao(true);
        make.setService(true);
        make.setView(false);
        make.setAction(true);
        make.makes(cs);
        System.out.println("ok");
    }
}
