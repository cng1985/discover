package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionCatalog;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionCatalogDao extends BaseDao<QuestionCatalog,Integer>{

	 QuestionCatalog findById(Integer id);

	 QuestionCatalog save(QuestionCatalog bean);

	 QuestionCatalog updateByUpdater(Updater<QuestionCatalog> updater);

	 QuestionCatalog deleteById(Integer id);
}