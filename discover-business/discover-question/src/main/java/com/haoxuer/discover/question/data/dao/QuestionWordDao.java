package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionWord;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionWordDao extends BaseDao<QuestionWord,Long>{

	 QuestionWord findById(Long id);

	 QuestionWord save(QuestionWord bean);

	 QuestionWord updateByUpdater(Updater<QuestionWord> updater);

	 QuestionWord deleteById(Long id);
}