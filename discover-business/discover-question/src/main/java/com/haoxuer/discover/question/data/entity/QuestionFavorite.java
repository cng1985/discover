package com.haoxuer.discover.question.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "question_favorite")
public class QuestionFavorite extends AbstractEntity {


	
	/**
	 * 用户
	 */
	@JoinColumn(name = "userid")
	@ManyToOne()
	private User user;
	
	@JoinColumn
	@ManyToOne()
	private Question question;
	
	



	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Question getQuestion() {
		return question;
	}


	public void setQuestion(Question question) {
		this.question = question;
	}



	
}
