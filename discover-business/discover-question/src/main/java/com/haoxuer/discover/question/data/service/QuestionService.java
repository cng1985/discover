package com.haoxuer.discover.question.data.service;

import com.haoxuer.discover.question.data.entity.Question;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年08月15日11:06:23.
*/
public interface QuestionService {

	Question findById(Long id);

	Question save(Question bean);

	Question update(Question bean);

	Question deleteById(Long id);
	
	Question[] deleteByIds(Long[] ids);
	
	Page<Question> page(Pageable pageable);
	
	Page<Question> page(Pageable pageable, Object search);


	List<Question> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}