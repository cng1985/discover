package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionAnswerVote;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:23.
*/
public interface QuestionAnswerVoteDao extends BaseDao<QuestionAnswerVote,Long>{

	 QuestionAnswerVote findById(Long id);

	 QuestionAnswerVote save(QuestionAnswerVote bean);

	 QuestionAnswerVote updateByUpdater(Updater<QuestionAnswerVote> updater);

	 QuestionAnswerVote deleteById(Long id);
}