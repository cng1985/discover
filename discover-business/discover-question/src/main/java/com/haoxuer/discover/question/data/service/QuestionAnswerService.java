package com.haoxuer.discover.question.data.service;

import com.haoxuer.discover.question.data.entity.QuestionAnswer;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年08月15日11:06:23.
*/
public interface QuestionAnswerService {

	QuestionAnswer findById(Long id);

	QuestionAnswer save(QuestionAnswer bean);

	QuestionAnswer update(QuestionAnswer bean);

	QuestionAnswer deleteById(Long id);
	
	QuestionAnswer[] deleteByIds(Long[] ids);
	
	Page<QuestionAnswer> page(Pageable pageable);
	
	Page<QuestionAnswer> page(Pageable pageable, Object search);


	List<QuestionAnswer> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}