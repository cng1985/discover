package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionAnswer;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:23.
*/
public interface QuestionAnswerDao extends BaseDao<QuestionAnswer,Long>{

	 QuestionAnswer findById(Long id);

	 QuestionAnswer save(QuestionAnswer bean);

	 QuestionAnswer updateByUpdater(Updater<QuestionAnswer> updater);

	 QuestionAnswer deleteById(Long id);
}