package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionFavorite;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionFavoriteDao extends BaseDao<QuestionFavorite,Long>{

	 QuestionFavorite findById(Long id);

	 QuestionFavorite save(QuestionFavorite bean);

	 QuestionFavorite updateByUpdater(Updater<QuestionFavorite> updater);

	 QuestionFavorite deleteById(Long id);
}