package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionTag;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionTagDao extends BaseDao<QuestionTag,Long>{

	 QuestionTag findById(Long id);

	 QuestionTag save(QuestionTag bean);

	 QuestionTag updateByUpdater(Updater<QuestionTag> updater);

	 QuestionTag deleteById(Long id);
}