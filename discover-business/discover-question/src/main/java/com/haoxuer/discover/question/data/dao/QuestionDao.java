package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.Question;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:23.
*/
public interface QuestionDao extends BaseDao<Question,Long>{

	 Question findById(Long id);

	 Question save(Question bean);

	 Question updateByUpdater(Updater<Question> updater);

	 Question deleteById(Long id);
}