package com.haoxuer.discover.question.data.dao.impl;

import com.haoxuer.discover.question.data.entity.QuestionAnswerVote;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.question.data.dao.QuestionAnswerVoteDao;

/**
* Created by imake on 2017年08月15日11:06:23.
*/
@Repository

public class QuestionAnswerVoteDaoImpl extends CriteriaDaoImpl<QuestionAnswerVote, Long> implements QuestionAnswerVoteDao {

	@Override
	public QuestionAnswerVote findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public QuestionAnswerVote save(QuestionAnswerVote bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public QuestionAnswerVote deleteById(Long id) {
		QuestionAnswerVote entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<QuestionAnswerVote> getEntityClass() {
		return QuestionAnswerVote.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}