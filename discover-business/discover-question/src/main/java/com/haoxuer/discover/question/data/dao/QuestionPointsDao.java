package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionPoints;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionPointsDao extends BaseDao<QuestionPoints,Long>{

	 QuestionPoints findById(Long id);

	 QuestionPoints save(QuestionPoints bean);

	 QuestionPoints updateByUpdater(Updater<QuestionPoints> updater);

	 QuestionPoints deleteById(Long id);
}