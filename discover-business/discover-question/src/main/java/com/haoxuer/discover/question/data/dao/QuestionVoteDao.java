package com.haoxuer.discover.question.data.dao;


import com.haoxuer.discover.question.data.entity.QuestionVote;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionVoteDao extends BaseDao<QuestionVote,Long>{

	 QuestionVote findById(Long id);

	 QuestionVote save(QuestionVote bean);

	 QuestionVote updateByUpdater(Updater<QuestionVote> updater);

	 QuestionVote deleteById(Long id);
}