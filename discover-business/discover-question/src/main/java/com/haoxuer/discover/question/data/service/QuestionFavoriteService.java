package com.haoxuer.discover.question.data.service;

import com.haoxuer.discover.question.data.entity.QuestionFavorite;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionFavoriteService {

	QuestionFavorite findById(Long id);

	QuestionFavorite save(QuestionFavorite bean);

	QuestionFavorite update(QuestionFavorite bean);

	QuestionFavorite deleteById(Long id);
	
	QuestionFavorite[] deleteByIds(Long[] ids);
	
	Page<QuestionFavorite> page(Pageable pageable);
	
	Page<QuestionFavorite> page(Pageable pageable, Object search);


	List<QuestionFavorite> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}