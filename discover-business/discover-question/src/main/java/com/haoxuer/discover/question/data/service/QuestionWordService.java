package com.haoxuer.discover.question.data.service;

import com.haoxuer.discover.question.data.entity.QuestionWord;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionWordService {

	QuestionWord findById(Long id);

	QuestionWord save(QuestionWord bean);

	QuestionWord update(QuestionWord bean);

	QuestionWord deleteById(Long id);
	
	QuestionWord[] deleteByIds(Long[] ids);
	
	Page<QuestionWord> page(Pageable pageable);
	
	Page<QuestionWord> page(Pageable pageable, Object search);


	List<QuestionWord> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}