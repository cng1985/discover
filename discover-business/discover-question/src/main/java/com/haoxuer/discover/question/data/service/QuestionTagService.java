package com.haoxuer.discover.question.data.service;

import com.haoxuer.discover.question.data.entity.QuestionTag;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年08月15日11:06:24.
*/
public interface QuestionTagService {

	QuestionTag findById(Long id);

	QuestionTag save(QuestionTag bean);

	QuestionTag update(QuestionTag bean);

	QuestionTag deleteById(Long id);
	
	QuestionTag[] deleteByIds(Long[] ids);
	
	Page<QuestionTag> page(Pageable pageable);
	
	Page<QuestionTag> page(Pageable pageable, Object search);


	List<QuestionTag> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}