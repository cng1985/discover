package com.haoxuer.discover.site.domain.response;

import com.haoxuer.discover.rest.base.ResponseObject;

public class AppVersionResponse extends ResponseObject {

  private Long id;

  /**
   * 版本数字版本
   */
  private Integer versionCode;

  /**
   * 版本名称
   */
  private String versionName;

  /**
   * 程序描述
   */
  private String note;

  /**
   * 该程序下载地址
   */
  private String downUrl;

  public Integer getVersionCode() {
    return versionCode;
  }

  public void setVersionCode(Integer versionCode) {
    this.versionCode = versionCode;
  }

  public String getVersionName() {
    return versionName;
  }

  public void setVersionName(String versionName) {
    this.versionName = versionName;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getDownUrl() {
    return downUrl;
  }

  public void setDownUrl(String downUrl) {
    this.downUrl = downUrl;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
