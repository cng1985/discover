package com.haoxuer.discover.site.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenObject;

public class VersionRequest extends RequestUserTokenObject {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
