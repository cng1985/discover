package com.haoxuer.discover.site.api;

import com.haoxuer.discover.rest.base.RequestTokenObject;
import com.haoxuer.discover.rest.base.ResponseTokenObject;
import com.haoxuer.discover.site.domain.request.AppRequest;
import com.haoxuer.discover.site.domain.request.VersionRequest;
import com.haoxuer.discover.site.domain.response.AppVersionResponse;

/**
 * Created by ada on 2017/5/16.
 */
public interface AppHandler {

  /**
   * 获取令牌
   *
   * @param request
   * @return
   */
  ResponseTokenObject findToken(AppRequest request);


  /**
   * 刷新令牌
   *
   * @param request
   * @return
   */
  ResponseTokenObject refreshToken(RequestTokenObject request);


  /**
   * 查询某个程序的最新版本信息
   * @param request
   * @return
   */
  AppVersionResponse version(VersionRequest request);

}
