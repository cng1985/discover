package com.haoxuer.discover.storage.controller.tenant;

import com.haoxuer.discover.storage.api.apis.FileEntityApi;
import com.haoxuer.discover.storage.api.domain.list.FileEntityList;
import com.haoxuer.discover.storage.api.domain.page.FileEntityPage;
import com.haoxuer.discover.storage.api.domain.request.*;
import com.haoxuer.discover.storage.api.domain.response.FileEntityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;

@RequestMapping("/tenantRest/fileentity")
@RestController
public class FileEntityTenantRestController extends BaseTenantRestController {


    @RequiresUser
    @RequestMapping("create")
    public FileEntityResponse create(FileEntityDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequiresUser
    @RequestMapping("update")
    public FileEntityResponse update(FileEntityDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequiresUser
    @RequestMapping("delete")
    public FileEntityResponse delete(FileEntityDataRequest request) {
        init(request);
        return api.delete(request);
    }

    @RequiresUser
    @RequestMapping("view")
    public FileEntityResponse view(FileEntityDataRequest request) {
       init(request);
       return api.view(request);
   }

    @RequiresUser
    @RequestMapping("list")
    public FileEntityList list(FileEntitySearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequiresUser
    @RequestMapping("search")
    public FileEntityPage search(FileEntitySearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private FileEntityApi api;

}
