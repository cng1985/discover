package com.haoxuer.discover.storage.data.dao.impl;

import com.haoxuer.discover.storage.data.dao.BucketEntityDao;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.storage.data.entity.BucketEntity;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年07月09日09:34:53.
 */
@Repository

public class BucketEntityDaoImpl extends CriteriaDaoImpl<BucketEntity, Long> implements BucketEntityDao {
  
  @Override
  public BucketEntity findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }
  
  @Override
  public BucketEntity findByName(String name) {
    Finder finder=Finder.create();
    finder.append("from BucketEntity b where b.name=:name");
    finder.setParam("name",name);
    return findOne(finder);
  }
  
  @Override
  public BucketEntity save(BucketEntity bean) {
    
    getSession().save(bean);
    
    
    return bean;
  }
  
  @Override
  public BucketEntity deleteById(Long id) {
    BucketEntity entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }
  
  @Override
  protected Class<BucketEntity> getEntityClass() {
    return BucketEntity.class;
  }
  
  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}