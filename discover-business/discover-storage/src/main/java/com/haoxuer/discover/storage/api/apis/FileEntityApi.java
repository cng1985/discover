package com.haoxuer.discover.storage.api.apis;


import com.haoxuer.discover.storage.api.domain.list.FileEntityList;
import com.haoxuer.discover.storage.api.domain.page.FileEntityPage;
import com.haoxuer.discover.storage.api.domain.request.*;
import com.haoxuer.discover.storage.api.domain.response.FileEntityResponse;

public interface FileEntityApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    FileEntityResponse create(FileEntityDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    FileEntityResponse update(FileEntityDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    FileEntityResponse delete(FileEntityDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     FileEntityResponse view(FileEntityDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    FileEntityList list(FileEntitySearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    FileEntityPage search(FileEntitySearchRequest request);

}