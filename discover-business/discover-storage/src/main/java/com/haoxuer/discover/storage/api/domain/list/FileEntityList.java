package com.haoxuer.discover.storage.api.domain.list;


import com.haoxuer.discover.storage.api.domain.simple.FileEntitySimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2020年12月08日20:02:52.
*/

@Data
public class FileEntityList  extends ResponseList<FileEntitySimple> {

}