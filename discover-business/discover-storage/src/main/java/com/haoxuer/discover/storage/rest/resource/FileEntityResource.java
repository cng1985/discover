package com.haoxuer.discover.storage.rest.resource;

import com.haoxuer.discover.storage.api.apis.FileEntityApi;
import com.haoxuer.discover.storage.api.domain.list.FileEntityList;
import com.haoxuer.discover.storage.api.domain.page.FileEntityPage;
import com.haoxuer.discover.storage.api.domain.request.*;
import com.haoxuer.discover.storage.api.domain.response.FileEntityResponse;
import com.haoxuer.discover.storage.data.dao.FileEntityDao;
import com.haoxuer.discover.storage.data.entity.FileEntity;
import com.haoxuer.discover.storage.rest.convert.FileEntityResponseConvert;
import com.haoxuer.discover.storage.rest.convert.FileEntitySimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class FileEntityResource implements FileEntityApi {

    @Autowired
    private FileEntityDao dataDao;


    @Override
    public FileEntityResponse create(FileEntityDataRequest request) {
        FileEntityResponse result = new FileEntityResponse();

        FileEntity bean = new FileEntity();
        handleData(request, bean);
        dataDao.save(bean);
        result = new FileEntityResponseConvert().conver(bean);
        return result;
    }

    @Override
    public FileEntityResponse update(FileEntityDataRequest request) {
        FileEntityResponse result = new FileEntityResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        FileEntity bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new FileEntityResponseConvert().conver(bean);
        return result;
    }

    private void handleData(FileEntityDataRequest request, FileEntity bean) {
        BeanDataUtils.copyProperties(request,bean);

    }

    @Override
    public FileEntityResponse delete(FileEntityDataRequest request) {
        FileEntityResponse result = new FileEntityResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public FileEntityResponse view(FileEntityDataRequest request) {
        FileEntityResponse result=new FileEntityResponse();
        FileEntity bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new FileEntityResponseConvert().conver(bean);
        return result;
    }
    @Override
    public FileEntityList list(FileEntitySearchRequest request) {
        FileEntityList result = new FileEntityList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<FileEntity> organizations = dataDao.list(0, request.getSize(), filters, orders);
        FileEntitySimpleConvert convert=new FileEntitySimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public FileEntityPage search(FileEntitySearchRequest request) {
        FileEntityPage result=new FileEntityPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<FileEntity> page=dataDao.page(pageable);
        FileEntitySimpleConvert convert=new FileEntitySimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
