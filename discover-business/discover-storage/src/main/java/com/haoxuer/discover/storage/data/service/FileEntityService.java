package com.haoxuer.discover.storage.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.storage.data.entity.FileEntity;
import java.util.List;

/**
 * Created by imake on 2018年07月09日09:23:15.
 */
public interface FileEntityService {
  
  FileEntity findById(Long id);
  
  FileEntity save(FileEntity bean);
  
  FileEntity update(FileEntity bean);
  
  FileEntity deleteById(Long id);
  
  FileEntity[] deleteByIds(Long[] ids);
  
  Page<FileEntity> page(Pageable pageable);
  
  Page<FileEntity> page(Pageable pageable, Object search);
  
  
  List<FileEntity> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}