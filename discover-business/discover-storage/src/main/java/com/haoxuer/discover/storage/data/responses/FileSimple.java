package com.haoxuer.discover.storage.data.responses;

import java.io.Serializable;

public class FileSimple implements Serializable {

  private String url;

  private Integer num;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getNum() {
    return num;
  }

  public void setNum(Integer num) {
    this.num = num;
  }
}
