package com.haoxuer.discover.storage.data.service.impl;

import com.haoxuer.discover.storage.data.service.FileEntityService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.storage.data.dao.FileEntityDao;
import com.haoxuer.discover.storage.data.entity.FileEntity;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年07月09日09:23:15.
 */


@Scope("prototype")
@Service
@Transactional
public class FileEntityServiceImpl implements FileEntityService {
  
  private FileEntityDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public FileEntity findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public FileEntity save(FileEntity bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public FileEntity update(FileEntity bean) {
    Updater<FileEntity> updater = new Updater<FileEntity>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public FileEntity deleteById(Long id) {
    FileEntity bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public FileEntity[] deleteByIds(Long[] ids) {
    FileEntity[] beans = new FileEntity[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(FileEntityDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<FileEntity> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<FileEntity> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<FileEntity> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}