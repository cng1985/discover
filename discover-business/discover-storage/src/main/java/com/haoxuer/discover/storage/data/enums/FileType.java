package com.haoxuer.discover.storage.data.enums;

public enum FileType {
  file, directory;
  
  @Override
  public String toString() {
    if (name().equals("file")) {
      return "文件";
    } else if (name().equals("directory")) {
      return "文件夹";
    } else {
      return super.toString();
    }
  }
}
