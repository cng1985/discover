package com.haoxuer.discover.storage.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.storage.data.entity.BucketEntity;

/**
 * Created by imake on 2018年07月09日09:34:53.
 */
public interface BucketEntityDao extends BaseDao<BucketEntity, Long> {
  
  BucketEntity findById(Long id);
  
  BucketEntity findByName(String name);
  
  
  BucketEntity save(BucketEntity bean);
  
  BucketEntity updateByUpdater(Updater<BucketEntity> updater);
  
  BucketEntity deleteById(Long id);
}