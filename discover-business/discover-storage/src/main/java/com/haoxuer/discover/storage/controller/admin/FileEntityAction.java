package com.haoxuer.discover.storage.controller.admin;

import com.haoxuer.discover.storage.data.service.FileEntityService;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.storage.data.entity.FileEntity;
import com.haoxuer.discover.storage.data.so.FileEntitySo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by imake on 2018年07月09日09:23:15.
 */


@Scope("prototype")
@Controller
public class FileEntityAction {


    @RequiresPermissions("fileentity")
    @RequestMapping("/admin/fileentity/view_list")
    public String list(ModelMap model) {
        return "/admin/fileentity/list";
    }
}