package com.haoxuer.discover.storage.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.storage.data.enums.FileType;
import com.nbsaas.codemake.annotation.SearchItem;
import lombok.Data;

import javax.persistence.*;


@Data
@FormAnnotation(title = "互助基金",menu = "1,2,117")
@Entity
@Table(name = "storage_file")
public class FileEntity extends AbstractEntity {
  
  /**
   * 文件名称
   */
  @SearchItem(label = "文件名称",name = "name")
  @FormField(title = "文件名称", sortNum = "10", grid = true,required = true)
  private String name;
  
  /**
   * 文件路径
   */
  private String path;
  
  /**
   * 文件磁盘路径
   */
  private String diskPath;
  
  private Long volume;

  @ManyToOne(fetch = FetchType.LAZY)
  private User user;
  
  @ManyToOne(fetch = FetchType.LAZY)
  private FileEntity parent;
  
  @ManyToOne(fetch = FetchType.LAZY)
  private BucketEntity bucket;
  
  private FileType fileType;
  
}
