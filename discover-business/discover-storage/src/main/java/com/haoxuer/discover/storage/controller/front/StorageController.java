/*
 *
 *
 *
 */

package com.haoxuer.discover.storage.controller.front;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.plug.data.service.StorageService;
import com.haoxuer.discover.plug.data.vo.FileInfo;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.storage.data.entity.FileEntity;
import com.haoxuer.discover.storage.data.enums.FileType;
import com.haoxuer.discover.storage.data.responses.FileSimple;
import com.haoxuer.discover.storage.data.responses.FilesResponse;
import com.haoxuer.discover.storage.data.service.BucketEntityService;
import com.haoxuer.discover.storage.data.service.FileEntityService;
import com.haoxuer.discover.user.service.UserTokenService;
import com.haoxuer.discover.user.shiro.utils.UserUtil;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller - 文件处理
 */
@Controller
@RequestMapping("/storage")
public class StorageController {

  @Autowired
  StorageService storageService;

  @Autowired
  FileEntityService fileEntityService;

  @Autowired
  BucketEntityService bucketEntityService;

  @Autowired
  UserTokenService tokenService;

  public String uploadLocal(FileInfo.FileType fileType, MultipartFile multipartFile) {
    return storageService.uploadLocal(fileType, multipartFile);
  }

  /**
   * 上传
   */
  @ResponseBody
  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public FileResponse upload(FileInfo.FileType fileType, MultipartFile file, HttpServletRequest request) {
    return local(fileType, file, request);
  }

  @ResponseBody
  @RequestMapping(value = "/uploads", method = RequestMethod.POST)
  public FilesResponse uploads(FileInfo.FileType fileType, MultipartFile[] file, HttpServletRequest request) {
    FilesResponse result = new FilesResponse();
    if (file != null) {
      int i = 1;
      for (MultipartFile item : file) {
        FileResponse temp = local(fileType, item, request);
        if (temp != null) {
          FileSimple simple = new FileSimple();
          simple.setUrl(temp.getUrl());
          simple.setNum(i);
          result.getList().add(simple);
          i++;
        }
      }
    }
    return result;
  }

  @RequiresUser
  @ResponseBody
  @RequestMapping(value = "/user_upload", method = RequestMethod.POST)
  public FileResponse userUpload(FileInfo.FileType fileType, MultipartFile file, HttpServletRequest request) {
    return local(fileType, file, request);
  }

  private FileResponse local(FileInfo.FileType fileType, MultipartFile file, HttpServletRequest request) {
    FileResponse result = new FileResponse();
    Map<String, Object> data = new HashMap<String, Object>();
    String url = storageService.upload(fileType, file);
    result.setUrl(url);
    String num = request.getParameter("num");
    if (StringUtils.hasLength(num)) {
      result.setNum(Integer.valueOf(num));
    }

    FileEntity bean = new FileEntity();
    bean.setBucket(bucketEntityService.findByName("website"));
    bean.setName(file.getOriginalFilename());
    bean.setPath(url);
    bean.setVolume(file.getSize());
    bean.setFileType(FileType.file);
    String userToken = request.getParameter("userToken");
    if (userToken != null) {
      bean.setUser(User.fromId(tokenService.user(userToken)));
    } else {
      bean.setUser(UserUtil.getUser());
    }
    fileEntityService.save(bean);
    bucketEntityService.count("website");
    return result;
  }

  @RequiresUser
  @ResponseBody
  @RequestMapping(value = "/ueupload")
  public ResponseEntity<Map<String, Object>> ueUpload(HttpServletRequest request, HttpServletResponse response) {
    return ueworks(request);
  }

  private ResponseEntity<Map<String, Object>> ueworks(HttpServletRequest request) {
    JSONObject result = new JSONObject();
    String action = request.getParameter("action");
    if ("config".equals(action)) {
      result.put("imageActionName", "uploadimage");
      result.put("imageFieldName", "upfile");
      result.put("imageMaxSize", 2048000);
      result.put("imageCompressEnable", true);
      result.put("imageCompressBorder", 1600);
      result.put("imageInsertAlign", "none");
      result.put("imageUrlPrefix", "");
      JSONArray array = new JSONArray();
      array.put(".png");
      array.put(".jpg");
      array.put(".jpeg");
      array.put(".gif");
      array.put(".bmp");
      result.put("imageAllowFiles", array);
    } else if ("uploadimage".equals(action)) {
      boolean isAjaxUpload = request.getHeader("X_Requested_With") != null;

      if (!ServletFileUpload.isMultipartContent(request)) {
        // return new BaseState(false, AppInfo.NOT_MULTIPART_CONTENT);
      }
      FileItemStream fileStream = null;

      ServletFileUpload upload = new ServletFileUpload(
          new DiskFileItemFactory());

      if (isAjaxUpload) {
        upload.setHeaderEncoding("UTF-8");
      }
      if (request instanceof DefaultMultipartHttpServletRequest) {
        DefaultMultipartHttpServletRequest request1 = (DefaultMultipartHttpServletRequest) request;
        MultipartFile file = request1.getFile("upfile");

        String url = storageService.upload(FileInfo.FileType.image, file);
        result.put("url", url);
        result.put("size", "" + file.getSize());
        result.put("original", "" + file.getOriginalFilename());
        result.put("title", "" + file.getOriginalFilename());


        FileEntity bean = new FileEntity();
        bean.setBucket(bucketEntityService.findByName("website"));
        bean.setName(file.getName());
        bean.setPath(url);
        bean.setVolume(file.getSize());
        bean.setFileType(FileType.file);
        bean.setUser(UserUtil.getUser());
        fileEntityService.save(bean);
        bucketEntityService.count("website");
      }
      result.put("state", "SUCCESS");

      result.put("type", ".jpg");
    } else {

    }


    return new ResponseEntity<Map<String, Object>>(result.toMap(), HttpStatus.OK);
  }

  private static File getTmpFile() {
    File tmpDir = FileUtils.getTempDirectory();
    String tmpFileName = (Math.random() * 10000 + "").replace(".", "");
    return new File(tmpDir, tmpFileName);
  }

  public static class FileResponse extends ResponseObject {


    private String url;

    private Integer num;


    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public Integer getNum() {
      return num;
    }

    public void setNum(Integer num) {
      this.num = num;
    }
  }

}