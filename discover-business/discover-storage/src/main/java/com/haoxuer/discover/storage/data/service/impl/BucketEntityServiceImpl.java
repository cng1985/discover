package com.haoxuer.discover.storage.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.storage.data.dao.BucketEntityDao;
import com.haoxuer.discover.storage.data.dao.FileEntityDao;
import com.haoxuer.discover.storage.data.entity.BucketEntity;
import com.haoxuer.discover.storage.data.enums.FileType;
import com.haoxuer.discover.storage.data.service.BucketEntityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


/**
 * Created by imake on 2018年07月09日09:34:53.
 */


@Scope("prototype")
@Service
@Transactional
public class BucketEntityServiceImpl implements BucketEntityService {
  
  private BucketEntityDao dao;
  
  @Autowired
  private FileEntityDao fileEntityDao;
  
  
  @Override
  @Transactional(readOnly = true)
  public BucketEntity findById(Long id) {
    return dao.findById(id);
  }
  
  @Override
  public BucketEntity findByName(String name) {
    Assert.hasText(name, "不能为空");
    BucketEntity result = dao.findByName(name);
    if (result == null) {
      result = new BucketEntity();
      result.setName(name);
      dao.save(result);
    }
    return result;
  }
  
  @Override
  public BucketEntity count(String name) {
    Assert.hasText(name, "不能为空");
    BucketEntity result = dao.findByName(name);
    if (result != null) {
      Long volume= fileEntityDao.sum( result.getId());
      result.setVolume(volume);
  
      Long nums= fileEntityDao.countQuery(Filter.eq("bucket.id", result.getId()
      ),Filter.eq("fileType", FileType.file));
      result.setNums(nums);
    }
    return result;
  }
  
  
  @Override
  @Transactional
  public BucketEntity save(BucketEntity bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public BucketEntity update(BucketEntity bean) {
    Updater<BucketEntity> updater = new Updater<BucketEntity>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public BucketEntity deleteById(Long id) {
    BucketEntity bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public BucketEntity[] deleteByIds(Long[] ids) {
    BucketEntity[] beans = new BucketEntity[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(BucketEntityDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<BucketEntity> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<BucketEntity> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<BucketEntity> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}