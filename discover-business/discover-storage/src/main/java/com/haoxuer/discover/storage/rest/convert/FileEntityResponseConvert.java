package com.haoxuer.discover.storage.rest.convert;

import com.haoxuer.discover.storage.api.domain.response.FileEntityResponse;
import com.haoxuer.discover.storage.data.entity.FileEntity;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class FileEntityResponseConvert implements Conver<FileEntityResponse, FileEntity> {
    @Override
    public FileEntityResponse conver(FileEntity source) {
        FileEntityResponse result = new FileEntityResponse();
        BeanDataUtils.copyProperties(source,result);


         result.setFileTypeName(source.getFileType()+"");

        return result;
    }
}
