package com.haoxuer.discover.storage.rest.convert;

import com.haoxuer.discover.storage.api.domain.simple.FileEntitySimple;
import com.haoxuer.discover.storage.data.entity.FileEntity;
import com.haoxuer.discover.data.rest.core.Conver;
public class FileEntitySimpleConvert implements Conver<FileEntitySimple, FileEntity> {


    @Override
    public FileEntitySimple conver(FileEntity source) {
        FileEntitySimple result = new FileEntitySimple();

            result.setId(source.getId());
             result.setPath(source.getPath());
             result.setVolume(source.getVolume());
             result.setName(source.getName());
             result.setDiskPath(source.getDiskPath());
             result.setAddDate(source.getAddDate());
             result.setFileType(source.getFileType());

             result.setFileTypeName(source.getFileType()+"");
        return result;
    }
}
