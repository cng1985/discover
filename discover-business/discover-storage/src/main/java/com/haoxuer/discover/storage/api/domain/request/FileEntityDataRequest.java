package com.haoxuer.discover.storage.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.storage.data.enums.FileType;
import java.util.Date;

/**
*
* Created by imake on 2020年12月08日20:02:52.
*/

@Data
public class FileEntityDataRequest extends BaseRequest {

    private Long id;

     private String path;

     private Long volume;

     private String name;

     private String diskPath;

     private FileType fileType;


}