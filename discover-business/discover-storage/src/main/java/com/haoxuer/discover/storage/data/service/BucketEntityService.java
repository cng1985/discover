package com.haoxuer.discover.storage.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.storage.data.entity.BucketEntity;
import java.util.List;

/**
 * Created by imake on 2018年07月09日09:34:53.
 */
public interface BucketEntityService {
  
  BucketEntity findById(Long id);
  
  BucketEntity findByName(String name);
  
  BucketEntity count(String name);
  
  BucketEntity save(BucketEntity bean);
  
  BucketEntity update(BucketEntity bean);
  
  BucketEntity deleteById(Long id);
  
  BucketEntity[] deleteByIds(Long[] ids);
  
  Page<BucketEntity> page(Pageable pageable);
  
  Page<BucketEntity> page(Pageable pageable, Object search);
  
  
  List<BucketEntity> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}