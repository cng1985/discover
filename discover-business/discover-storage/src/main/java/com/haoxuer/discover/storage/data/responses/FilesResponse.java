package com.haoxuer.discover.storage.data.responses;

import com.haoxuer.discover.rest.base.ResponseObject;

import java.util.ArrayList;
import java.util.List;

public class FilesResponse extends ResponseObject {

  private List<FileSimple> list=new ArrayList<>();

  public List<FileSimple> getList() {
    return list;
  }

  public void setList(List<FileSimple> list) {
    this.list = list;
  }
}
