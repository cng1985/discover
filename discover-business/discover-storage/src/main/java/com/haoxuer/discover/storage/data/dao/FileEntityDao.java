package com.haoxuer.discover.storage.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.storage.data.entity.FileEntity;

/**
 * Created by imake on 2018年07月09日09:23:15.
 */
public interface FileEntityDao extends BaseDao<FileEntity, Long> {
  
  FileEntity findById(Long id);
  
  FileEntity findByName(String name);
  
  
  FileEntity save(FileEntity bean);
  
  FileEntity updateByUpdater(Updater<FileEntity> updater);
  
  FileEntity deleteById(Long id);
  
  Long sum(Long id);
}