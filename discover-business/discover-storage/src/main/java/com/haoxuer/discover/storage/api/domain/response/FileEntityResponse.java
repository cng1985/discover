package com.haoxuer.discover.storage.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.storage.data.enums.FileType;

/**
*
* Created by imake on 2020年12月08日20:02:52.
*/

@Data
public class FileEntityResponse extends ResponseObject {

    private Long id;

     private String path;

     private Long volume;

     private String name;

     private String diskPath;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private FileType fileType;


     private String fileTypeName;
}