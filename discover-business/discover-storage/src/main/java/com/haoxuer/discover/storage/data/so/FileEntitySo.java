package com.haoxuer.discover.storage.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年12月08日20:02:52.
*/
@Data
public class FileEntitySo implements Serializable {

    //文件名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;



    private String sortField;

    private String sortMethod;

}
