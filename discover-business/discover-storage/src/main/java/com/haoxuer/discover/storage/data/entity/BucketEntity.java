package com.haoxuer.discover.storage.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "storage_bucket")
public class BucketEntity extends AbstractEntity {
  
  @Column(unique = true)
  private String name;
  
  private Long nums;
  
  private Long volume;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public Long getNums() {
    return nums;
  }
  
  public void setNums(Long nums) {
    this.nums = nums;
  }
  
  public Long getVolume() {
    return volume;
  }
  
  public void setVolume(Long volume) {
    this.volume = volume;
  }
}
