package com.haoxuer.discover.area.api.domain.list;


import com.haoxuer.discover.area.api.domain.simple.AreaSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年03月28日12:28:38.
*/

@Data
public class AreaList  extends ResponseList<AreaSimple> {

}