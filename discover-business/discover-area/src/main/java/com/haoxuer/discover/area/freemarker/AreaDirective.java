package com.haoxuer.discover.area.freemarker;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.area.data.service.AreaService;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by cng19 on 2017/9/3.
 */
public class AreaDirective extends ListDirective<Area> {
  
  @Override
  public List<Area> list() {
    
    Integer size = getInt("size");
    Integer id = getInt("id");
    Pageable pagex = new Pageable();
    pagex.setPageSize(size);
    pagex.setPageNumber(1);
    pagex.getFilters().add(Filter.eq("parent.id", id));
    pagex.getOrders().add(Order.asc("code"));
    return areaService.list(0, size, pagex.getFilters(), pagex.getOrders());
  }
  
  @Autowired
  AreaService areaService;
}