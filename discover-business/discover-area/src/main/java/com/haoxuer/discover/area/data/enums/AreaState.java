package com.haoxuer.discover.area.data.enums;

public enum AreaState {
  ok, closed;
  
  @Override
  public String toString() {
    if (name().equals("ok")) {
      return "已开通";
    } else if (name().equals("closed")) {
      return "关闭";
    }
    return super.toString();
  }
}
