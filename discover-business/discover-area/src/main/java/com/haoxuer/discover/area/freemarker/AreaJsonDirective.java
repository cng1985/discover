package com.haoxuer.discover.area.freemarker;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.area.data.service.AreaService;
import com.haoxuer.discover.area.rest.conver.TreeSimpleConver;
import com.haoxuer.discover.config.api.domain.simple.TreeSimple;
import com.haoxuer.discover.config.freemarker.CatalogJsonDirective;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cng19 on 2017/9/3.
 */
@Component("areaJsonDirective")
public class AreaJsonDirective extends CatalogJsonDirective {


    @Autowired
    AreaService areaService;

    @Override
    public List<TreeSimple> getList() {

        Integer size = getInt("size", 200);
        Integer level = getInt("level", 2);
        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.eq("levelInfo", level));

        List<Area> areas = areaService.list(0, size, filters, orders());
        if (areas != null && areas.size() > 0) {
            return ConverResourceUtils.converList(areas, new TreeSimpleConver());
        }
        return null;
    }
}