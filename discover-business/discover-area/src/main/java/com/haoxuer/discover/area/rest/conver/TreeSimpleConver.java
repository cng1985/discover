package com.haoxuer.discover.area.rest.conver;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.config.api.domain.simple.TreeSimple;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.rest.core.Conver;

public class TreeSimpleConver implements Conver<TreeSimple, Area> {
    @Override
    public TreeSimple conver(Area source) {
        TreeSimple result = new TreeSimple();
        result.setId(source.getId());
        result.setLabel(source.getName());
        if (source.getChildren() != null&&source.getChildren().size()>0) {
            result.setChildren(ConverResourceUtils.converList(source.getChildren(), new TreeSimpleConver()));
        }
        return result;
    }
}
