package com.haoxuer.discover.area.data.service;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
 * Created by imake on 2017年08月04日10:01:33.
 */
public interface AreaService {
  
  Area findById(Integer id);
  
  Area save(Area bean);
  
  Area update(Area bean);
  
  Area deleteById(Integer id);
  
  Area[] deleteByIds(Integer[] ids);
  
  Area findByName(String name);
  
  
  Area findByCode(String code);
  
  
  Page<Area> page(Pageable pageable);
  
  Page<Area> page(Pageable pageable, Object search);
  
  List<Area> child(Integer id);
  
  List<Area> findByTops(Integer pid);
  
  List<Area> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
  List<Area> findByLevel(Integer id);
  
  
}