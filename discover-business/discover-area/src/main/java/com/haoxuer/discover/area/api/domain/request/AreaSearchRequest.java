package com.haoxuer.discover.area.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;


/**
*
* Created by imake on 2021年03月28日12:28:38.
*/

@Data
public class AreaSearchRequest extends BasePageRequest {



    private int fetch;

    @Search(name = "levelInfo",operator = Filter.Operator.eq)
    private Integer level;

    @Search(name = "parent.id",operator = Filter.Operator.eq)
    private Integer parent;

    private String sortField;


    private String sortMethod;
}