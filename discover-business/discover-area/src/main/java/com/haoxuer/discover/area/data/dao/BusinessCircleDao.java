package com.haoxuer.discover.area.data.dao;


import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2018年09月12日14:42:15.
 */
public interface BusinessCircleDao extends BaseDao<BusinessCircle, Long> {
  
  BusinessCircle findById(Long id);
  
  BusinessCircle save(BusinessCircle bean);
  
  BusinessCircle updateByUpdater(Updater<BusinessCircle> updater);
  
  BusinessCircle deleteById(Long id);
}