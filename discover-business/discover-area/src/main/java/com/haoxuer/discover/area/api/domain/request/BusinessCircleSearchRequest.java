package com.haoxuer.discover.area.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月14日17:32:03.
*/

@Data
public class BusinessCircleSearchRequest extends BasePageRequest {

    //省份
     @Search(name = "province.id",operator = Filter.Operator.eq)
     private Integer province;

    //城市
     @Search(name = "city.id",operator = Filter.Operator.eq)
     private Integer city;

    //区县
     @Search(name = "area.id",operator = Filter.Operator.eq)
     private Integer area;




    private String sortField;


    private String sortMethod;
}