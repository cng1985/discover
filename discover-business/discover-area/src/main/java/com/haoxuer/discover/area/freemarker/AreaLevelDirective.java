package com.haoxuer.discover.area.freemarker;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.area.data.service.AreaService;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class AreaLevelDirective extends ListDirective<Area> {
  
  @Override
  public List<Area> list() {
    
    Integer size = getInt("size", 20);
    Integer level = getInt("level", 2);
    Pageable pagex = new Pageable();
    pagex.getFilters().add(Filter.eq("levelInfo", level));
    pagex.getOrders().add(Order.asc("code"));
    return areaService.list(0, size, pagex.getFilters(), pagex.getOrders());
  }
  
  @Autowired
  AreaService areaService;
}