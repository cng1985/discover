package com.haoxuer.discover.area.api.domain.response;

import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;

@Data
public class PoiResponse extends ResponseObject {
  
  private String address;
  
  private String city;
  
  private String province;
  
  private String county;
  
  private Integer cityId;
  
  private Integer provinceId;
  
  private Integer countyId;
  
  private Double lng;
  private Double lat;
}
