package com.haoxuer.discover.area.api.apis;


import com.haoxuer.discover.area.api.domain.list.AreaList;
import com.haoxuer.discover.area.api.domain.page.AreaPage;
import com.haoxuer.discover.area.api.domain.request.*;
import com.haoxuer.discover.area.api.domain.response.AreaResponse;
import com.haoxuer.discover.area.api.domain.response.PoiResponse;

public interface AreaApi {


    PoiResponse search(PoiSearchRequest request);

    /**
     * 创建
     *
     * @param request
     * @return
     */
    AreaResponse create(AreaDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    AreaResponse update(AreaDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    AreaResponse delete(AreaDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     AreaResponse view(AreaDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    AreaList list(AreaSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    AreaPage search(AreaSearchRequest request);

}