package com.haoxuer.discover.area.data.service;

import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
 * Created by imake on 2018年09月12日14:42:16.
 */
public interface BusinessCircleService {
  
  BusinessCircle findById(Long id);
  
  BusinessCircle save(BusinessCircle bean);
  
  BusinessCircle update(BusinessCircle bean);
  
  BusinessCircle deleteById(Long id);
  
  BusinessCircle[] deleteByIds(Long[] ids);
  
  Page<BusinessCircle> page(Pageable pageable);
  
  Page<BusinessCircle> page(Pageable pageable, Object search);
  
  
  List<BusinessCircle> list(int first, Integer size, List<Filter> filters, List<Order> orders);
  
}