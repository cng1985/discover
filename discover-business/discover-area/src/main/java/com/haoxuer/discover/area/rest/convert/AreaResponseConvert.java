package com.haoxuer.discover.area.rest.convert;

import com.haoxuer.discover.area.api.domain.response.AreaResponse;
import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class AreaResponseConvert implements Conver<AreaResponse, Area> {
    @Override
    public AreaResponse conver(Area source) {
        AreaResponse result = new AreaResponse();
        BeanDataUtils.copyProperties(source,result);


         result.setAreaTypeName(source.getAreaType()+"");
         result.setStateName(source.getState()+"");

        return result;
    }
}
