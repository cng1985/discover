package com.haoxuer.discover.area.rest.convert;

import com.haoxuer.discover.area.api.domain.simple.AreaSimple;
import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import lombok.Data;


@Data
public class AreaSimpleConvert implements Conver<AreaSimple, Area> {

    private int fetch;

    @Override
    public AreaSimple conver(Area source) {
        AreaSimple result = new AreaSimple();

         result.setId(source.getId());
         result.setLabel(source.getName());
         result.setValue(""+source.getId());
         result.setName(source.getName());
        result.setCode(source.getCode());
        result.setSortNum(source.getSortNum());
        result.setAddDate(source.getAddDate());
        result.setGovCode(source.getGovCode());
         if (source.getChildren()!=null&&source.getChildren().size()>0){
             result.setHasChildren(true);
             if (fetch!=0){
                 result.setChildren(ConverResourceUtils.converList(source.getChildren(),this));
             }
         }
        return result;
    }
}
