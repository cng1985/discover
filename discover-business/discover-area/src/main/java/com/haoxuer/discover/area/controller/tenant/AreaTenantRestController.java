package com.haoxuer.discover.area.controller.tenant;

import com.haoxuer.discover.area.api.apis.AreaApi;
import com.haoxuer.discover.area.api.domain.list.AreaList;
import com.haoxuer.discover.area.api.domain.page.AreaPage;
import com.haoxuer.discover.area.api.domain.request.*;
import com.haoxuer.discover.area.api.domain.response.AreaResponse;
import com.haoxuer.discover.area.api.domain.response.PoiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/area")
@RestController
public class AreaTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("area")
    @RequiresUser
    @RequestMapping("create")
    public AreaResponse create(AreaDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("area")
    @RequiresUser
    @RequestMapping("update")
    public AreaResponse update(AreaDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("area")
    @RequiresUser
    @RequestMapping("delete")
    public AreaResponse delete(AreaDataRequest request) {
        init(request);
        AreaResponse result = new AreaResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("area")
    @RequiresUser
    @RequestMapping("view")
    public AreaResponse view(AreaDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("area")
    @RequiresUser
    @RequestMapping("list")
    public AreaList list(AreaSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("area")
    @RequiresUser
    @RequestMapping("search")
    public AreaPage search(AreaSearchRequest request) {
        init(request);
        return api.search(request);
    }
    @RequiresUser
    @RequestMapping("lbs")
    public PoiResponse lbs(PoiSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private AreaApi api;

}
