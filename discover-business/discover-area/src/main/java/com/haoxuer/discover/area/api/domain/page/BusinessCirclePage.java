package com.haoxuer.discover.area.api.domain.page;


import com.haoxuer.discover.area.api.domain.simple.BusinessCircleSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月14日17:32:03.
*/

@Data
public class BusinessCirclePage  extends ResponsePage<BusinessCircleSimple> {

}