package com.haoxuer.discover.area.api.domain.simple;


import java.io.Serializable;

import lombok.Data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.area.data.enums.AreaType;
import com.haoxuer.discover.area.data.enums.AreaState;

import javax.persistence.Column;
import java.util.List;

/**
 * Created by BigWorld on 2021年03月28日12:28:38.
 */

@Data
public class AreaSimple implements Serializable {
    private Integer id;
    private String value;
    private String label;
    private String name;

    private Integer sortNum;

    /**
     * 地理经度。
     */
    private Float lng;


    /**
     * 地理纬度。
     */
    private Float lat;

    private AreaType areaType;

    /**
     * 全程
     */
    private String fullName;

    /**
     * 政府编码
     */
    private String govCode;

    private String code;


    private List<AreaSimple> children;

    private boolean hasChildren;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;
}
