package com.haoxuer.discover.area.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;


@Data
public class PoiSearchRequest extends BaseRequest {
  
  
  private String address;
  
}
