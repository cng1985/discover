package com.haoxuer.discover.area.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月14日17:32:03.
*/

@Data
public class BusinessCircleDataRequest extends BaseRequest {

    private Long id;

     private Integer area;

     private Integer city;

     private String address;

     private Float lng;

     private Date lastDate;

     private String name;

     private Integer province;

     private Date addDate;

     private Float lat;


}