package com.haoxuer.discover.area.api.apis;


import com.haoxuer.discover.area.api.domain.list.BusinessCircleList;
import com.haoxuer.discover.area.api.domain.page.BusinessCirclePage;
import com.haoxuer.discover.area.api.domain.request.*;
import com.haoxuer.discover.area.api.domain.response.BusinessCircleResponse;

public interface BusinessCircleApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    BusinessCircleResponse create(BusinessCircleDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    BusinessCircleResponse update(BusinessCircleDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    BusinessCircleResponse delete(BusinessCircleDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     BusinessCircleResponse view(BusinessCircleDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    BusinessCircleList list(BusinessCircleSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    BusinessCirclePage search(BusinessCircleSearchRequest request);

}