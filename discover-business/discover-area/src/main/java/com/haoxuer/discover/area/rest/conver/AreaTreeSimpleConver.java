package com.haoxuer.discover.area.rest.conver;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.rest.core.ConverUtils;
import com.haoxuer.discover.rest.simple.TreeValue;

public class AreaTreeSimpleConver implements Conver<TreeValue, Area> {
    @Override
    public TreeValue conver(Area source) {

        TreeValue result = new TreeValue();
        result.setLabel(source.getName());
        result.setValue("" + source.getId());
        if (source.getChildren() != null && source.getChildren().size() > 0) {
            result.setChildren(ConverUtils.coverCollect(source.getChildren(), this));
        }
        return result;
    }
}
