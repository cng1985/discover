package com.haoxuer.discover.area.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.controller.BaseAction;
/**
*
* Created by imake on 2021年05月14日17:32:03.
*/

@Scope("prototype")
@Controller
public class BusinessCircleAction extends BaseAction{


	@RequiresPermissions("businesscircle")
	@RequestMapping("/admin/businesscircle/view_list")
	public String list() {
		return getView("businesscircle/list");
	}

}