package com.haoxuer.discover.area.data.so;

import java.io.Serializable;

/**
 * Created by imake on 2018年09月12日14:42:16.
 */
public class BusinessCircleSo implements Serializable {
  
  private String name;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
}
