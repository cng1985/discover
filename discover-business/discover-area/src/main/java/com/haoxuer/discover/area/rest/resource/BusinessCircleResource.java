package com.haoxuer.discover.area.rest.resource;

import com.haoxuer.discover.area.api.apis.BusinessCircleApi;
import com.haoxuer.discover.area.api.domain.list.BusinessCircleList;
import com.haoxuer.discover.area.api.domain.page.BusinessCirclePage;
import com.haoxuer.discover.area.api.domain.request.*;
import com.haoxuer.discover.area.api.domain.response.BusinessCircleResponse;
import com.haoxuer.discover.area.data.dao.BusinessCircleDao;
import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.haoxuer.discover.area.rest.convert.BusinessCircleResponseConvert;
import com.haoxuer.discover.area.rest.convert.BusinessCircleSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.area.data.dao.AreaDao;
import com.haoxuer.discover.area.data.dao.AreaDao;
import com.haoxuer.discover.area.data.dao.AreaDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class BusinessCircleResource implements BusinessCircleApi {

    @Autowired
    private BusinessCircleDao dataDao;

    @Autowired
    private AreaDao provinceDao;
    @Autowired
    private AreaDao areaDao;
    @Autowired
    private AreaDao cityDao;

    @Override
    public BusinessCircleResponse create(BusinessCircleDataRequest request) {
        BusinessCircleResponse result = new BusinessCircleResponse();

        BusinessCircle bean = new BusinessCircle();
        handleData(request, bean);
        dataDao.save(bean);
        result = new BusinessCircleResponseConvert().conver(bean);
        return result;
    }

    @Override
    public BusinessCircleResponse update(BusinessCircleDataRequest request) {
        BusinessCircleResponse result = new BusinessCircleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        BusinessCircle bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new BusinessCircleResponseConvert().conver(bean);
        return result;
    }

    private void handleData(BusinessCircleDataRequest request, BusinessCircle bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getArea()!=null){
               bean.setArea(areaDao.findById(request.getArea()));
            }
            if(request.getCity()!=null){
               bean.setCity(cityDao.findById(request.getCity()));
            }
            if(request.getProvince()!=null){
               bean.setProvince(provinceDao.findById(request.getProvince()));
            }

    }

    @Override
    public BusinessCircleResponse delete(BusinessCircleDataRequest request) {
        BusinessCircleResponse result = new BusinessCircleResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public BusinessCircleResponse view(BusinessCircleDataRequest request) {
        BusinessCircleResponse result=new BusinessCircleResponse();
        BusinessCircle bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new BusinessCircleResponseConvert().conver(bean);
        return result;
    }
    @Override
    public BusinessCircleList list(BusinessCircleSearchRequest request) {
        BusinessCircleList result = new BusinessCircleList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<BusinessCircle> organizations = dataDao.list(0, request.getSize(), filters, orders);
        BusinessCircleSimpleConvert convert=new BusinessCircleSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public BusinessCirclePage search(BusinessCircleSearchRequest request) {
        BusinessCirclePage result=new BusinessCirclePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<BusinessCircle> page=dataDao.page(pageable);
        BusinessCircleSimpleConvert convert=new BusinessCircleSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
