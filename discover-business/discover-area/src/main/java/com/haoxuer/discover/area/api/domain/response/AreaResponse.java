package com.haoxuer.discover.area.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.area.data.enums.AreaType;
import com.haoxuer.discover.area.data.enums.AreaState;

/**
*
* Created by imake on 2021年03月28日12:28:38.
*/

@Data
public class AreaResponse extends ResponseObject {

    private Integer id;

     private String govCode;

     private Integer levelInfo;

     private Float lng;

     private Integer sortNum;

     private AreaType areaType;

     private String ids;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;

     private String name;

     private AreaState state;

     private String fullName;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private Float lat;


     private String areaTypeName;
     private String stateName;
}