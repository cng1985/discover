package com.haoxuer.discover.area.data.service.impl;

import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.area.data.service.AreaService;
import com.haoxuer.discover.area.data.dao.AreaDao;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2017年08月04日10:01:33.
 */
@Service
@Transactional
public class AreaServiceImpl implements AreaService {

  private AreaDao dao;


  @Override
  @Transactional(readOnly = true)
  public Area findById(Integer id) {
    return dao.findById(id);
  }

  @Override
  public List<Area> findByTops(Integer pid) {
    LinkedList<Area> result = new LinkedList<Area>();
    Area catalog = dao.findById(pid);
    if (catalog != null) {
      while (catalog != null && catalog.getParent() != null) {
        result.addFirst(catalog);
        catalog = dao.findById(catalog.getParentId());
      }
      result.addFirst(catalog);
    }
    return result;
  }

  @Override
  @Transactional
  public Area save(Area bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public Area update(Area bean) {
    Updater<Area> updater = new Updater<Area>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public Area deleteById(Integer id) {
    Area bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public Area[] deleteByIds(Integer[] ids) {
    Area[] beans = new Area[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  @Override
  public Area findByName(String name) {
    return dao.findByName(name);
  }

  @Override
  public Area findByCode(String code) {
    return dao.findByCode(code);
  }


  @Autowired
  public void setDao(AreaDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<Area> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<Area> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<Area> child(Integer id) {
    List ms = null;
    Area menu = dao.findById(id);
    if (menu != null) {
      Finder finder = Finder.create("from Area t where t.lft >:lft and t.rgt<:rgt ");
      finder.append(" order by t.lft asc");
      finder.setParam("lft", menu.getLft());
      finder.setParam("rgt", menu.getRgt());
      finder.setCacheable(false);
      ms = dao.find(finder);
    }
    return ms;
  }

  @Override
  public List<Area> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }

  @Override
  public List<Area> findByLevel(Integer id) {
    return dao.filters(Filter.eq("levelInfo", id));
  }
}