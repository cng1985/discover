package com.haoxuer.discover.area.controller.rest;

import com.haoxuer.discover.area.api.apis.BusinessCircleApi;
import com.haoxuer.discover.area.api.domain.list.BusinessCircleList;
import com.haoxuer.discover.area.api.domain.page.BusinessCirclePage;
import com.haoxuer.discover.area.api.domain.request.*;
import com.haoxuer.discover.area.api.domain.response.BusinessCircleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/businesscircle")
@RestController
public class BusinessCircleRestController extends BaseRestController {


    @RequestMapping("create")
    public BusinessCircleResponse create(BusinessCircleDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public BusinessCircleResponse update(BusinessCircleDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public BusinessCircleResponse delete(BusinessCircleDataRequest request) {
        init(request);
        BusinessCircleResponse result = new BusinessCircleResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public BusinessCircleResponse view(BusinessCircleDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public BusinessCircleList list(BusinessCircleSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public BusinessCirclePage search(BusinessCircleSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private BusinessCircleApi api;

}
