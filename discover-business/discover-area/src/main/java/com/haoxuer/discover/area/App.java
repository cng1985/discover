package com.haoxuer.discover.area;

import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.template.hibernateSimple.TemplateHibernateSimpleDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateSimpleDir.class);
    File view = new File("E:\\workspace\\iowl\\iowl_web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
    make.setView(view);
    
    List<Class<?>> cs = new ArrayList<Class<?>>();
    cs.add(BusinessCircle.class);
    
    make.setDao(false);
    make.setService(false);
    make.setView(false);
    make.setAction(true);
    make.setApi(true);
    make.setRest(true);
    make.put("adminAction",true);
    make.makes(cs);
    System.out.println("ok");
  }
}
