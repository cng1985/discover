package com.haoxuer.discover.area.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年05月14日17:32:03.
*/

@Data
public class BusinessCircleResponse extends ResponseObject {

    private Long id;

     private Integer area;

     private Integer city;

     private String address;

     private Float lng;

     private String cityName;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;

     private String name;

     private String provinceName;

     private String areaName;

     private Integer province;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private Float lat;


}