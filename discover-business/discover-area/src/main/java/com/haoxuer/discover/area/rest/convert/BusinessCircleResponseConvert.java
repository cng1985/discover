package com.haoxuer.discover.area.rest.convert;

import com.haoxuer.discover.area.api.domain.response.BusinessCircleResponse;
import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class BusinessCircleResponseConvert implements Conver<BusinessCircleResponse, BusinessCircle> {
    @Override
    public BusinessCircleResponse conver(BusinessCircle source) {
        BusinessCircleResponse result = new BusinessCircleResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getArea()!=null){
           result.setArea(source.getArea().getId());
        }
        if(source.getCity()!=null){
           result.setCity(source.getCity().getId());
        }
         if(source.getCity()!=null){
            result.setCityName(source.getCity().getName());
         }
         if(source.getProvince()!=null){
            result.setProvinceName(source.getProvince().getName());
         }
         if(source.getArea()!=null){
            result.setAreaName(source.getArea().getName());
         }
        if(source.getProvince()!=null){
           result.setProvince(source.getProvince().getId());
        }


        return result;
    }
}
