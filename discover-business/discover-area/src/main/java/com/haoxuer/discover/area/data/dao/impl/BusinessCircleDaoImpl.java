package com.haoxuer.discover.area.data.dao.impl;

import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.haoxuer.discover.area.data.dao.BusinessCircleDao;
import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2018年09月12日14:42:16.
 */
@Repository

public class BusinessCircleDaoImpl extends CriteriaDaoImpl<BusinessCircle, Long> implements BusinessCircleDao {
  
  @Override
  public BusinessCircle findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }
  
  @Override
  public BusinessCircle save(BusinessCircle bean) {
    
    getSession().save(bean);
    
    
    return bean;
  }
  
  @Override
  public BusinessCircle deleteById(Long id) {
    BusinessCircle entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }
  
  @Override
  protected Class<BusinessCircle> getEntityClass() {
    return BusinessCircle.class;
  }
  
  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}