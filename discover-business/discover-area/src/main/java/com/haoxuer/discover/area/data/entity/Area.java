package com.haoxuer.discover.area.data.entity;

import com.haoxuer.discover.area.data.enums.AreaState;
import com.haoxuer.discover.area.data.enums.AreaType;
import com.haoxuer.discover.data.entity.CatalogEntity;
import com.nbsaas.codemake.annotation.CatalogClass;
import com.nbsaas.codemake.annotation.FormAnnotation;
import lombok.Data;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@FormAnnotation(title = "地区管理", model = "地区", menu = "1,41,42")
@CatalogClass
@Data
@Entity
@Table(name = "area")
public class Area extends CatalogEntity {

    public static Area fromId(Integer id) {
        Area result = new Area();
        result.setId(id);
        return result;
    }

    /**
     * 下属地区
     */
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<Area> children;
    /**
     * 父地区id
     */
    @JoinColumn(name = "pid")
    @ManyToOne(fetch = FetchType.LAZY)
    private Area parent;
    /**
     * 城市状态 1为开通，0为未开通
     */
    private AreaState state;

    /**
     * 地理经度。
     */
    private Float lng;


    /**
     * 地理纬度。
     */
    private Float lat;

    @Column(name = "area_type")
    private AreaType areaType;

    /**
     * 全程
     */
    @Column(length = 20)
    private String fullName;

    /**
     * 政府编码
     */
    @Column(length = 12)
    private String govCode;


    @Override
    public Integer getParentId() {
        Area parent = getParent();
        if (parent != null) {
            return parent.getId();
        } else {
            return null;
        }
    }

}
