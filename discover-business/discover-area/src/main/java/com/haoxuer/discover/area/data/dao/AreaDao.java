package com.haoxuer.discover.area.data.dao;


import com.haoxuer.discover.area.data.entity.Area;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年08月04日10:01:33.
 */
public interface AreaDao extends BaseDao<Area, Integer> {
  
  Area findById(Integer id);
  
  Area save(Area bean);
  
  Area updateByUpdater(Updater<Area> updater);
  
  Area deleteById(Integer id);
  
  /**
   * 根据基本名字查找
   *
   * @param name
   * @return
   */
  Area findByName(String name);
  
  /**
   * 根据全名查找
   *
   * @param name
   * @return
   */
  Area findByFullName(String name);
  
  /**
   * 根据名字或者全名查找
   *
   * @param name
   * @return
   */
  Area name(String name,String ...parents);
  
  Area findByCode(String code);
  
  
}