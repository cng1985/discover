package com.haoxuer.discover.area.data.enums;

public enum AreaType {
  
  country, province, city, district, street;
  
  @Override
  public String toString() {
    if (name().equals("country")) {
      return "国家";
    } else if (name().equals("province")) {
      return "省";
    } else if (name().equals("city")) {
      return "市";
    } else if (name().equals("district")) {
      return "区县";
    } else if (name().equals("street")) {
      return "街道";
    }
    return super.toString();
  }
}
