package com.haoxuer.discover.area.data.service.impl;

import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.haoxuer.discover.area.data.dao.BusinessCircleDao;
import com.haoxuer.discover.area.data.service.BusinessCircleService;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2018年09月12日14:42:16.
 */


@Scope("prototype")
@Service
@Transactional
public class BusinessCircleServiceImpl implements BusinessCircleService {
  
  private BusinessCircleDao dao;
  
  
  @Override
  @Transactional(readOnly = true)
  public BusinessCircle findById(Long id) {
    return dao.findById(id);
  }
  
  
  @Override
  @Transactional
  public BusinessCircle save(BusinessCircle bean) {
    dao.save(bean);
    return bean;
  }
  
  @Override
  @Transactional
  public BusinessCircle update(BusinessCircle bean) {
    Updater<BusinessCircle> updater = new Updater<BusinessCircle>(bean);
    return dao.updateByUpdater(updater);
  }
  
  @Override
  @Transactional
  public BusinessCircle deleteById(Long id) {
    BusinessCircle bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }
  
  @Override
  @Transactional
  public BusinessCircle[] deleteByIds(Long[] ids) {
    BusinessCircle[] beans = new BusinessCircle[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }
  
  
  @Autowired
  public void setDao(BusinessCircleDao dao) {
    this.dao = dao;
  }
  
  @Override
  public Page<BusinessCircle> page(Pageable pageable) {
    return dao.page(pageable);
  }
  
  
  @Override
  public Page<BusinessCircle> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }
  
  @Override
  public List<BusinessCircle> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}