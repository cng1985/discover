package com.haoxuer.discover.area.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;


@FormAnnotation(title = "商圈管理",model = "商圈",menu = "1,41,42")
@Data
@Entity
@Table(name = "area_business_circle")
public class BusinessCircle extends AbstractEntity {

  @SearchItem(label = "商圈名称",name = "name")
  @FormField(title = "商圈名称",col = 22,required = true,grid = true,width = "300")
  @Column(length =30)
  private String name;


  @SearchItem(label = "省份",name = "province",key = "province.id",classType = "Integer",operator = "eq",show = false)
  @FormField(title = "省份",grid = true,ignore = true,sort = true)
  @FieldName
  @FieldConvert(classType = "Integer")
  @ManyToOne(fetch = FetchType.LAZY)
  private Area province;

  @SearchItem(label = "城市",name = "city",key = "city.id",classType = "Integer",operator = "eq",show = false)
  @FormField(title = "城市",grid = true,ignore = true,sort = true)
  @FieldName
  @FieldConvert(classType = "Integer")
  @ManyToOne(fetch = FetchType.LAZY)
  private Area city;

  @SearchItem(label = "区县",name = "area",key = "area.id",classType = "Integer",operator = "eq",show = false)
  @FormField(title = "区县",grid = true,ignore = true,type = InputType.select,sort = true)
  @FieldName
  @FieldConvert(classType = "Integer")
  @ManyToOne(fetch = FetchType.LAZY)
  private Area area;

  @FormField(title = "中心地址",col = 22,required = true,grid = true,width = "3000",type = InputType.map)
  private String address;
  /**
   * 地理经度。
   */
  private Float lng;
  
  /**
   * 地理纬度。
   */
  private Float lat;
  
}
