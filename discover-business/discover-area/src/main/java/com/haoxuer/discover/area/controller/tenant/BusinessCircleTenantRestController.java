package com.haoxuer.discover.area.controller.tenant;

import com.haoxuer.discover.area.api.apis.BusinessCircleApi;
import com.haoxuer.discover.area.api.domain.list.BusinessCircleList;
import com.haoxuer.discover.area.api.domain.page.BusinessCirclePage;
import com.haoxuer.discover.area.api.domain.request.*;
import com.haoxuer.discover.area.api.domain.response.BusinessCircleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/businesscircle")
@RestController
public class BusinessCircleTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("businesscircle")
    @RequiresUser
    @RequestMapping("create")
    public BusinessCircleResponse create(BusinessCircleDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("businesscircle")
    @RequiresUser
    @RequestMapping("update")
    public BusinessCircleResponse update(BusinessCircleDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("businesscircle")
    @RequiresUser
    @RequestMapping("delete")
    public BusinessCircleResponse delete(BusinessCircleDataRequest request) {
        init(request);
        BusinessCircleResponse result = new BusinessCircleResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("businesscircle")
    @RequiresUser
    @RequestMapping("view")
    public BusinessCircleResponse view(BusinessCircleDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("businesscircle")
    @RequiresUser
    @RequestMapping("list")
    public BusinessCircleList list(BusinessCircleSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("businesscircle")
    @RequiresUser
    @RequestMapping("search")
    public BusinessCirclePage search(BusinessCircleSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private BusinessCircleApi api;

}
