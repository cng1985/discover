package com.haoxuer.discover.area.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.area.data.enums.AreaType;
import com.haoxuer.discover.area.data.enums.AreaState;
import java.util.Date;

/**
*
* Created by imake on 2021年03月28日12:28:38.
*/

@Data
public class AreaDataRequest extends BaseRequest {

    private Integer id;

     private String code;

     private Float lng;

     private String fullName;

     private Date addDate;

     private String govCode;

     private Integer levelInfo;

     private Integer sortNum;

     private AreaType areaType;

     private String ids;

     private Integer lft;

     private Date lastDate;

     private String name;

     private AreaState state;

     private Float lat;

     private Integer rgt;


}