package com.haoxuer.discover.area.rest.convert;

import com.haoxuer.discover.area.api.domain.simple.BusinessCircleSimple;
import com.haoxuer.discover.area.data.entity.BusinessCircle;
import com.haoxuer.discover.data.rest.core.Conver;
public class BusinessCircleSimpleConvert implements Conver<BusinessCircleSimple, BusinessCircle> {


    @Override
    public BusinessCircleSimple conver(BusinessCircle source) {
        BusinessCircleSimple result = new BusinessCircleSimple();

            result.setId(source.getId());
            if(source.getArea()!=null){
               result.setArea(source.getArea().getId());
            }
            if(source.getCity()!=null){
               result.setCity(source.getCity().getId());
            }
             result.setAddress(source.getAddress());
             result.setLng(source.getLng());
             if(source.getCity()!=null){
                result.setCityName(source.getCity().getName());
             }
             result.setLastDate(source.getLastDate());
             result.setName(source.getName());
             if(source.getProvince()!=null){
                result.setProvinceName(source.getProvince().getName());
             }
             if(source.getArea()!=null){
                result.setAreaName(source.getArea().getName());
             }
            if(source.getProvince()!=null){
               result.setProvince(source.getProvince().getId());
            }
             result.setAddDate(source.getAddDate());
             result.setLat(source.getLat());

        return result;
    }
}
