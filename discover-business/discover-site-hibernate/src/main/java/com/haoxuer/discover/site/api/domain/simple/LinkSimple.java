package com.haoxuer.discover.site.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年02月04日19:06:33.
*/
@Data
public class LinkSimple implements Serializable {

    private Long id;

     private String linkTypeName;
     private Integer sortNum;
     private String name;
     private String icon;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private Integer linkType;
     private String target;
     private String url;


}
