package com.haoxuer.discover.site.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年05月16日15:44:12.
*/

@Data
public class LinkTypeSearchRequest extends BasePageRequest {

    //父分类
     @Search(name = "parent.id",operator = Filter.Operator.eq)
     private Integer parent;



    private int fetch;

    @Search(name = "levelInfo",operator = Filter.Operator.eq)
    private Integer level;

    private String sortField;


    private String sortMethod;
}