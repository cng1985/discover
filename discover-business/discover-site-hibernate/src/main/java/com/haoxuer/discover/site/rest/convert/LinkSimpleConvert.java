package com.haoxuer.discover.site.rest.convert;

import com.haoxuer.discover.site.api.domain.simple.LinkSimple;
import com.haoxuer.discover.site.data.entity.Link;
import com.haoxuer.discover.data.rest.core.Conver;
public class LinkSimpleConvert implements Conver<LinkSimple, Link> {


    @Override
    public LinkSimple conver(Link source) {
        LinkSimple result = new LinkSimple();

            result.setId(source.getId());
             if(source.getLinkType()!=null){
                result.setLinkTypeName(source.getLinkType().getName());
             }
             result.setSortNum(source.getSortNum());
             result.setName(source.getName());
             result.setIcon(source.getIcon());
             result.setAddDate(source.getAddDate());
            if(source.getLinkType()!=null){
               result.setLinkType(source.getLinkType().getId());
            }
             result.setTarget(source.getTarget());
             result.setUrl(source.getUrl());

        return result;
    }
}
