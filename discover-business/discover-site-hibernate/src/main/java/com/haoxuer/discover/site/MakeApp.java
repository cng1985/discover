package com.haoxuer.discover.site;

import com.haoxuer.discover.site.data.entity.LinkType;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernateSimple.TemplateHibernateSimpleDir;
import com.nbsaas.codemake.templates.elementuiForm.ElementUIFormDir;

/**
 * Hello world!
 */
public class MakeApp {
  public static void main(String[] args) {

    //getCodeMake().makes(Link.class);
    getCodeMake().makes(LinkType.class);

  }

  private static CodeMake getCodeMake() {
    CodeMake make = new CodeMake(ElementUIFormDir.class, TemplateHibernateSimpleDir.class);


    make.setDao(true);
    make.setView(false);
    make.setAction(true);
    make.setRest(true);
    make.setSo(false);
    make.setService(false);
    return make;
  }
}
