package com.haoxuer.discover.site.rest.convert;

import com.haoxuer.discover.site.api.domain.response.LinkResponse;
import com.haoxuer.discover.site.data.entity.Link;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.site.data.entity.LinkType;

import java.util.ArrayList;

public class LinkResponseConvert implements Conver<LinkResponse, Link> {
    @Override
    public LinkResponse conver(Link source) {
        LinkResponse result = new LinkResponse();
        BeanDataUtils.copyProperties(source, result);

        if (source.getLinkType() != null) {
            result.setLinkType(source.getLinkType().getId());
            result.setLinkTypeName(source.getLinkType().getName());
            LinkType temp = source.getLinkType();
            result.setTreeData(new ArrayList<>());
            while (temp != null) {
                result.getTreeData().add("" + temp.getId());
                temp = temp.getParent();
            }
        }


        return result;
    }
}
