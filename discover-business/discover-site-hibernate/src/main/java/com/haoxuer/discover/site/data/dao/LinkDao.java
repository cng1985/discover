package com.haoxuer.discover.site.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.site.data.entity.Link;

/**
* Created by imake on 2021年02月04日19:06:33.
*/
public interface LinkDao extends BaseDao<Link,Long>{

	 Link findById(Long id);

	 Link save(Link bean);

	 Link updateByUpdater(Updater<Link> updater);

	 Link deleteById(Long id);
}