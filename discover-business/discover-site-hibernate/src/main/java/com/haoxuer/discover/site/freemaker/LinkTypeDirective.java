package com.haoxuer.discover.site.freemaker;

import com.haoxuer.discover.site.data.service.LinkTypeService;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.site.data.entity.LinkType;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;


public class LinkTypeDirective extends ListDirective<LinkType> {


  @Override
  public List<LinkType> list() {

    Integer size = getInt("size");
    Integer id = getInt("id");
    Pageable pagex = new Pageable();
    pagex.setPageSize(size);
    pagex.setPageNumber(1);
    pagex.getFilters().add(Filter.eq("parent.id", id));
    return linkTypeService.list(0, size, pagex.getFilters(), pagex.getOrders());
  }

  @Autowired
  LinkTypeService linkTypeService;
}