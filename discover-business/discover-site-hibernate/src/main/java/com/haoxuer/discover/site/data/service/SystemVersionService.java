package com.haoxuer.discover.site.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.site.data.entity.SystemVersion;
import java.util.List;

/**
 * Created by imake on 2017年08月15日13:28:19.
 */
public interface SystemVersionService {

  SystemVersion findById(Long id);

  SystemVersion save(SystemVersion bean);

  SystemVersion update(SystemVersion bean);

  SystemVersion deleteById(Long id);

  SystemVersion[] deleteByIds(Long[] ids);

  Page<SystemVersion> page(Pageable pageable);

  Page<SystemVersion> page(Pageable pageable, Object search);


  List<SystemVersion> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}