package com.haoxuer.discover.site.data.entity;

import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.CatalogEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 链接分类
 * <p>
 * Created by ada on 2017/6/7.
 */
@CatalogClass
@FormAnnotation(title = "链接分类管理",model = "链接分类",menu = "1,2,6")
@Data
@Entity
@Table(name = "site_link_type")
public class LinkType extends CatalogEntity {



    @FieldConvert(classType = "Integer")
    @FieldName
    @FormField(title = "父分类")
    @SearchItem(label = "父分类", operator = "eq", name = "parent", key = "parent.id", classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pid")
    private LinkType parent;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    private List<LinkType> children;

    @Override
    public Integer getParentId() {
        if (parent != null) {
            return parent.getId();
        }
        return null;
    }

}
