package com.haoxuer.discover.site.api.domain.page;


import com.haoxuer.discover.site.api.domain.simple.LinkTypeSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月16日15:44:12.
*/

@Data
public class LinkTypePage  extends ResponsePage<LinkTypeSimple> {

}