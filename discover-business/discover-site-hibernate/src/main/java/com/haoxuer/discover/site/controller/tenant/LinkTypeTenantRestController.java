package com.haoxuer.discover.site.controller.tenant;

import com.haoxuer.discover.site.api.apis.LinkTypeApi;
import com.haoxuer.discover.site.api.domain.list.LinkTypeList;
import com.haoxuer.discover.site.api.domain.page.LinkTypePage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkTypeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/linktype")
@RestController
public class LinkTypeTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("linktype")
    @RequiresUser
    @RequestMapping("create")
    public LinkTypeResponse create(LinkTypeDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("linktype")
    @RequiresUser
    @RequestMapping("update")
    public LinkTypeResponse update(LinkTypeDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("linktype")
    @RequiresUser
    @RequestMapping("delete")
    public LinkTypeResponse delete(LinkTypeDataRequest request) {
        init(request);
        LinkTypeResponse result = new LinkTypeResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("linktype")
    @RequiresUser
    @RequestMapping("view")
    public LinkTypeResponse view(LinkTypeDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("linktype")
    @RequiresUser
    @RequestMapping("list")
    public LinkTypeList list(LinkTypeSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("linktype")
    @RequiresUser
    @RequestMapping("search")
    public LinkTypePage search(LinkTypeSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private LinkTypeApi api;

}
