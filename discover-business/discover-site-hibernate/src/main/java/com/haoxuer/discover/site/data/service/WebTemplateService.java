package com.haoxuer.discover.site.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.site.data.entity.WebTemplate;
import java.util.List;

/**
 * Created by imake on 2017年11月14日18:53:51.
 */
public interface WebTemplateService {

  WebTemplate findById(String id);

  WebTemplate save(WebTemplate bean);

  WebTemplate update(WebTemplate bean);

  WebTemplate deleteById(String id);

  WebTemplate[] deleteByIds(String[] ids);

  Page<WebTemplate> page(Pageable pageable);

  Page<WebTemplate> page(Pageable pageable, Object search);


  List<WebTemplate> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}