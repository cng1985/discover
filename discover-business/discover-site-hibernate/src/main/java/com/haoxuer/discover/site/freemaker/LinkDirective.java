package com.haoxuer.discover.site.freemaker;

import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.site.api.apis.LinkApi;
import com.haoxuer.discover.site.api.domain.list.LinkList;
import com.haoxuer.discover.site.api.domain.request.LinkSearchRequest;
import com.haoxuer.discover.site.api.domain.simple.LinkSimple;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class LinkDirective extends ListDirective<LinkSimple> {

    @Override
    public List<LinkSimple> list() {
        LinkSearchRequest request = new LinkSearchRequest();
        request.setLinkType(getInt("linkType"));
        request.setSize(getInt("size", 20));
        LinkList list = api.list(request);
        return list.getList();
    }

    @Autowired
    LinkApi api;
}