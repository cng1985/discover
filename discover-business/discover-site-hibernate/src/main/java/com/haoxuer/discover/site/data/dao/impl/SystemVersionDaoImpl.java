package com.haoxuer.discover.site.data.dao.impl;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.site.data.dao.SystemVersionDao;
import com.haoxuer.discover.site.data.entity.SystemVersion;
import java.util.Date;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2017年08月15日13:28:19.
 */
@Repository

public class SystemVersionDaoImpl extends CriteriaDaoImpl<SystemVersion, Long> implements SystemVersionDao {

  @Override
  public SystemVersion findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public SystemVersion save(SystemVersion bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public SystemVersion deleteById(Long id) {
    SystemVersion entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<SystemVersion> getEntityClass() {
    return SystemVersion.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public Long next(String sequence) {
    SystemVersion v = null;
    List<SystemVersion> versions = findByProperty("sequence", sequence);
    if (versions == null || versions.size() == 0) {
      v = new SystemVersion();
      v.setSequence(sequence);
      v.setAddDate(new Date());
      v.setLastDate(new Date());
      v.setStep(1);
      v.setVersionNum(0l);
      save(v);
    } else {
      v = versions.get(0);
    }

    Long dbnum = v.getVersionNum();
    if (dbnum == null) {
      dbnum = 1l;
      v.setVersionNum(dbnum);
    }
    Long result = v.getVersionNum() + v.getStep();
    v.setVersionNum(result);
    return result;
  }

}