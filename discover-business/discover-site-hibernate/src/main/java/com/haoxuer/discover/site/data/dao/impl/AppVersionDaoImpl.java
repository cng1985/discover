package com.haoxuer.discover.site.data.dao.impl;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.site.data.dao.AppVersionDao;
import com.haoxuer.discover.site.data.entity.AppVersion;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2017年07月25日11:33:24.
 */
@Repository

public class AppVersionDaoImpl extends CriteriaDaoImpl<AppVersion, Long> implements AppVersionDao {

  @Override
  public AppVersion findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public AppVersion last(Long app) {
    Finder fidner = Finder.create();
    fidner.append(" from AppVersion app where app.app.id=:appid order by app.id desc");
    fidner.setParam("appid", app);
    return findOne(fidner);
  }

  @Override
  public AppVersion save(AppVersion bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public AppVersion deleteById(Long id) {
    AppVersion entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  protected Class<AppVersion> getEntityClass() {
    return AppVersion.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }
}