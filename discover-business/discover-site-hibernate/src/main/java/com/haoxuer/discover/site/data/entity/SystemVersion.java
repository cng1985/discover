package com.haoxuer.discover.site.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity - 配置版本管理
 */
@Data
@Entity
@Table(name = "config_data_version")
public class SystemVersion extends AbstractEntity {
  
  /**
   * 最高版本数据
   */
  @Column(unique = true)
  private Long versionNum;
  
  /**
   * 版本数据
   */
  private String sequence;
  
  /**
   * 步长 默认为1
   */
  private Integer step;
  

}
