package com.haoxuer.discover.site.data.dao;


import com.haoxuer.discover.site.data.entity.App;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;

/**
 * Created by imake on 2017年07月25日11:33:24.
 */
public interface AppDao extends BaseDao<App, Long> {

  public App findById(Long id);

  public App save(App bean);

  public App updateByUpdater(Updater<App> updater);

  public App deleteById(Long id);


  Long count(Filter... filters);

  App findByPackage(String packageName);


}