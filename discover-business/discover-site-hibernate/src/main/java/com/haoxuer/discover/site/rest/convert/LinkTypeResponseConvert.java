package com.haoxuer.discover.site.rest.convert;

import com.haoxuer.discover.site.api.domain.response.LinkTypeResponse;
import com.haoxuer.discover.site.data.entity.LinkType;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class LinkTypeResponseConvert implements Conver<LinkTypeResponse, LinkType> {
    @Override
    public LinkTypeResponse conver(LinkType source) {
        LinkTypeResponse result = new LinkTypeResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getParent()!=null){
           result.setParent(source.getParent().getId());
        }
         if(source.getParent()!=null){
            result.setParentName(source.getParent().getName());
         }


        return result;
    }
}
