package com.haoxuer.discover.site.data.dao;


import com.haoxuer.discover.site.data.entity.WebTemplate;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

import java.util.Map;

/**
 * Created by imake on 2017年11月14日18:53:51.
 */
public interface WebTemplateDao extends BaseDao<WebTemplate, String> {
  
  WebTemplate findById(String id);
  
  WebTemplate save(WebTemplate bean);
  
  WebTemplate updateByUpdater(Updater<WebTemplate> updater);
  
  WebTemplate deleteById(String id);
  
  String text(String templateid, Map<String, Object> keys);
  
}