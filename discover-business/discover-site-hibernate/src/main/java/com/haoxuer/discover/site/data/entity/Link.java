package com.haoxuer.discover.site.data.entity;

import com.haoxuer.discover.data.entity.AbstractEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 友情链接
 * Created by ada on 2017/6/7.
 */
@FormAnnotation(title = "友情链接管理",model = "友情链接",menu = "1,2,7")
@Data
@Entity
@Table(name = "site_link")
public class Link extends AbstractEntity {


    /**
     * 名称
     */
    @SearchItem(label = "链接名称",name = "name")
    @FormField(title = "链接名称", grid = true, required = true,width = "230",col = 22)
    @Column(length = 50)
    private String name;


    /**
     *
     */
    @FormField(title = "链接地址", grid = true, required = true,width = "260",col = 22)
    private String url;

    @FieldName
    @FieldConvert(classType = "Integer")
    @FormField(title = "链接类型", grid = true,type = InputType.select,option = "linkType",col = 22)
    @ManyToOne(fetch = FetchType.LAZY)
    private LinkType linkType;


    /**
     * 图标
     */
    private String icon;

    /**
     * 排序号码
     */
    @FormField(title = "排序号", grid = true, type = InputType.el_input_number,col = 22,width = "1111")
    private Integer sortNum;


    /**
     * 链接打开方式
     * _blank 新窗口
     * _self 当前窗口
     * _parent 父窗口
     * _top
     */

    @FormField(title = "链接打开方式", type = InputType.el_radio_group,col = 22)
    private String target;


}
