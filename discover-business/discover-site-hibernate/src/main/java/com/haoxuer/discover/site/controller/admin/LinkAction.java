package com.haoxuer.discover.site.controller.admin;

import com.haoxuer.discover.controller.BaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
*
* Created by imake on 2021年02月04日19:06:33.
*/

@Scope("prototype")
@Controller
public class LinkAction extends BaseAction {


	@RequiresPermissions("link")
	@RequestMapping("/admin/link/view_list")
	public String list() {
		return getView("link/list");
	}

}