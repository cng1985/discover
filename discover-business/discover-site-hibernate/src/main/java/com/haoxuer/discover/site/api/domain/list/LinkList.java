package com.haoxuer.discover.site.api.domain.list;


import com.haoxuer.discover.site.api.domain.simple.LinkSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年02月04日19:06:33.
*/

@Data
public class LinkList  extends ResponseList<LinkSimple> {

}