package com.haoxuer.discover.site.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.site.data.entity.LinkType;

/**
* Created by imake on 2021年05月16日15:44:11.
*/
public interface LinkTypeDao extends BaseDao<LinkType,Integer>{

	 LinkType findById(Integer id);

	 LinkType save(LinkType bean);

	 LinkType updateByUpdater(Updater<LinkType> updater);

	 LinkType deleteById(Integer id);
}