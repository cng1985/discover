package com.haoxuer.discover.site.controller.admin;


import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.site.data.entity.App;
import com.haoxuer.discover.site.data.service.AppService;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "admin")
public class AppAction {
  private static final Logger log = LoggerFactory.getLogger(AppAction.class);

  public static final String REDIRECT_LIST_HTML = "redirect:/admin/app/view_list.htm";


  @RequiresPermissions("appversion")
  @RequestMapping("/app/view_list")
  public String list(Pageable pageable, HttpServletRequest request, ModelMap model) {

    if (pageable == null) {
      pageable = new Pageable();
    }
    if (pageable.getOrders() == null || pageable.getOrders().size() == 0) {
      pageable.getOrders().add(Order.desc("id"));
    }
    Page<App> pagination = manager.page(pageable);
    model.addAttribute("list", pagination.getContent());
    model.addAttribute("page", pagination);
    return "/admin/app/list";
  }

  @RequiresPermissions("appversion")
  @RequestMapping("/app/view_add")
  public String add(ModelMap model) {

    model.addAttribute("appKey", manager.key());
    model.addAttribute("appSecret", manager.secret());

    return "/admin/app/add";
  }

  @RequiresPermissions("appversion")
  @RequestMapping("/app/view_edit")
  public String edit(Pageable pageable, Long id, Integer pageNo, HttpServletRequest request, ModelMap model) {
    model.addAttribute("model", manager.findById(id));
    model.addAttribute("pageNo", pageNo);
    model.addAttribute("page", pageable);
    return "/admin/app/edit";
  }

  @RequiresPermissions("appversion")
  @RequestMapping("/app/model_save")
  public String save(App bean, HttpServletRequest request, ModelMap model) {

    String view = "redirect:view_list.htm";
    try {
      bean = manager.save(bean);
      log.info("save object id={}", bean.getId());
    } catch (Exception e) {
      e.printStackTrace();
      model.addAttribute("erro", e.getMessage());
      view = "/admin/app/add";
    }
    return view;
  }

  @RequiresPermissions("appversion")
  @RequestMapping("/app/model_update")
  public String update(Pageable pageable, App bean, HttpServletRequest request, ModelMap model) {

    String view = "redirect:/admin/app/view_list.htm?pageNumber=" + pageable.getPageNumber();
    try {
      bean = manager.update(bean);
    } catch (Exception e) {
      e.printStackTrace();
      model.addAttribute("erro", e.getMessage());
      model.addAttribute("model", bean);
      model.addAttribute("page", pageable);
      view = "/admin/app/edit";
    }
    return view;
  }

  @RequiresPermissions("appversion")
  @RequestMapping("/app/model_delete")
  public String delete(Pageable pageable, Long id, HttpServletRequest request, ModelMap model) {

    manager.deleteById(id);

    return "redirect:/admin/app/view_list.htm?pageNumber=" + pageable.getPageNumber();
  }

  @RequiresPermissions("appversion")
  @RequestMapping("/app/model_deletes")
  public String deletes(Pageable pageable, Long[] ids, HttpServletRequest request, ModelMap model) {

    manager.deleteByIds(ids);

    return "redirect:/admin/app/view_list.htm?pageNumber=" + pageable.getPageNumber();
  }

  @Autowired
  private AppService manager;
}