package com.haoxuer.discover.site.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年02月04日19:06:33.
*/

@Data
public class LinkResponse extends ResponseObject {

    private Long id;

     private String linkTypeName;

     private Integer sortNum;

     private String name;

     private String icon;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private Integer linkType;

     private List<String> treeData;

     private String target;

     private String url;


}