package com.haoxuer.discover.site.controller.admin;

import com.haoxuer.discover.controller.BaseAction;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
*
* Created by imake on 2021年05月16日15:44:11.
*/

@Scope("prototype")
@Controller
public class LinkTypeAction extends BaseAction {


	@RequiresPermissions("linktype")
	@RequestMapping("/admin/linktype/view_list")
	public String list() {
		return getView("linktype/list");
	}

}