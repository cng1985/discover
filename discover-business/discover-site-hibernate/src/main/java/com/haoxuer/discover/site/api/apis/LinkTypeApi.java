package com.haoxuer.discover.site.api.apis;


import com.haoxuer.discover.site.api.domain.list.LinkTypeList;
import com.haoxuer.discover.site.api.domain.page.LinkTypePage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkTypeResponse;

public interface LinkTypeApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    LinkTypeResponse create(LinkTypeDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    LinkTypeResponse update(LinkTypeDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    LinkTypeResponse delete(LinkTypeDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     LinkTypeResponse view(LinkTypeDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    LinkTypeList list(LinkTypeSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    LinkTypePage search(LinkTypeSearchRequest request);

}