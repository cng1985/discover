package com.haoxuer.discover.site.rest.resource;

import com.haoxuer.discover.site.api.apis.LinkTypeApi;
import com.haoxuer.discover.site.api.domain.list.LinkTypeList;
import com.haoxuer.discover.site.api.domain.page.LinkTypePage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkTypeResponse;
import com.haoxuer.discover.site.data.dao.LinkTypeDao;
import com.haoxuer.discover.site.data.entity.LinkType;
import com.haoxuer.discover.site.rest.convert.LinkTypeResponseConvert;
import com.haoxuer.discover.site.rest.convert.LinkTypeSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.site.data.dao.LinkTypeDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class LinkTypeResource implements LinkTypeApi {

    @Autowired
    private LinkTypeDao dataDao;

    @Autowired
    private LinkTypeDao parentDao;

    @Override
    public LinkTypeResponse create(LinkTypeDataRequest request) {
        LinkTypeResponse result = new LinkTypeResponse();

        LinkType bean = new LinkType();
        handleData(request, bean);
        dataDao.save(bean);
        result = new LinkTypeResponseConvert().conver(bean);
        return result;
    }

    @Override
    public LinkTypeResponse update(LinkTypeDataRequest request) {
        LinkTypeResponse result = new LinkTypeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        LinkType bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new LinkTypeResponseConvert().conver(bean);
        return result;
    }

    private void handleData(LinkTypeDataRequest request, LinkType bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getParent()!=null){
               bean.setParent(parentDao.findById(request.getParent()));
            }

    }

    @Override
    public LinkTypeResponse delete(LinkTypeDataRequest request) {
        LinkTypeResponse result = new LinkTypeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public LinkTypeResponse view(LinkTypeDataRequest request) {
        LinkTypeResponse result=new LinkTypeResponse();
        LinkType bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new LinkTypeResponseConvert().conver(bean);
        return result;
    }
    @Override
    public LinkTypeList list(LinkTypeSearchRequest request) {
        LinkTypeList result = new LinkTypeList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<LinkType> organizations = dataDao.list(0, request.getSize(), filters, orders);
        LinkTypeSimpleConvert convert=new LinkTypeSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public LinkTypePage search(LinkTypeSearchRequest request) {
        LinkTypePage result=new LinkTypePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<LinkType> page=dataDao.page(pageable);
        LinkTypeSimpleConvert convert=new LinkTypeSimpleConvert();
        convert.setFetch(request.getFetch());
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
