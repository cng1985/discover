package com.haoxuer.discover.site.controller.rest;

import com.haoxuer.discover.site.api.apis.LinkApi;
import com.haoxuer.discover.site.api.domain.list.LinkList;
import com.haoxuer.discover.site.api.domain.page.LinkPage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/link")
@RestController
public class LinkRestController extends BaseRestController {


    @RequestMapping("create")
    public LinkResponse create(LinkDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public LinkResponse update(LinkDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public LinkResponse delete(LinkDataRequest request) {
        init(request);
        return api.delete(request);
    }

    @RequestMapping("view")
    public LinkResponse view(LinkDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public LinkList list(LinkSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public LinkPage search(LinkSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private LinkApi api;

}
