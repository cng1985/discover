package com.haoxuer.discover.site.data.dao.impl;

import com.haoxuer.discover.site.data.entity.App;
import com.haoxuer.discover.data.core.BaseDaoImpl;
import com.haoxuer.discover.data.core.Finder;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.site.data.dao.AppDao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by imake on 2017年07月25日11:33:24.
 */
@Repository

public class AppDaoImpl extends BaseDaoImpl<App, Long> implements AppDao {

  @Override
  public App findById(Long id) {
    if (id == null) {
      return null;
    }
    return get(id);
  }

  @Override
  public App save(App bean) {

    getSession().save(bean);


    return bean;
  }

  @Override
  public App deleteById(Long id) {
    App entity = super.get(id);
    if (entity != null) {
      getSession().delete(entity);
    }
    return entity;
  }

  @Override
  public Long count(Filter... filters) {
    List<Filter> filterList = new ArrayList<Filter>();
    if (filters != null) {
      for (Filter filter : filters) {
        filterList.add(filter);
      }
    }
    Finder finder = makeFinder(filterList, null);
    return countQuery(finder);
  }

  @Override
  protected Class<App> getEntityClass() {
    return App.class;
  }

  @Autowired
  public void setSuperSessionFactory(SessionFactory sessionFactory) {
    super.setSessionFactory(sessionFactory);
  }

  @Override
  public App findByPackage(String packageName) {

    Finder finder = Finder.create();
    finder.append("from App app where app.packageName =:packageName and app.system = 'android'");
    finder.setParam("packageName", packageName);

    return findOne(finder);
  }

}