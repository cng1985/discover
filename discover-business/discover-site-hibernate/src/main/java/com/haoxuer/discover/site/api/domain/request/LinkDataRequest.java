package com.haoxuer.discover.site.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年02月04日19:06:33.
*/

@Data
public class LinkDataRequest extends BaseRequest {

    private Long id;

     private Integer sortNum;

     private String name;

     private String icon;

     private Integer linkType;

     private String target;

     private String url;


}