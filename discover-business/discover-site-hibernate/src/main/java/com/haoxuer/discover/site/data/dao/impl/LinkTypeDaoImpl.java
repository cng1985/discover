package com.haoxuer.discover.site.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.site.data.dao.LinkTypeDao;
import com.haoxuer.discover.site.data.entity.LinkType;
import com.haoxuer.discover.data.core.CatalogDaoImpl;

/**
* Created by imake on 2021年05月16日15:44:11.
*/
@Repository

public class LinkTypeDaoImpl extends CatalogDaoImpl<LinkType, Integer> implements LinkTypeDao {

	@Override
	public LinkType findById(Integer id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public LinkType save(LinkType bean) {

		add(bean);
		
		
		return bean;
	}

    @Override
	public LinkType deleteById(Integer id) {
		LinkType entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<LinkType> getEntityClass() {
		return LinkType.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}