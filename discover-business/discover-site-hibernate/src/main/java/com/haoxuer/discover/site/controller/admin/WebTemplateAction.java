package com.haoxuer.discover.site.controller.admin;

import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.site.data.entity.WebTemplate;
import com.haoxuer.discover.site.data.service.WebTemplateService;
import com.haoxuer.discover.site.data.so.WebTemplateSo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by imake on 2017年11月14日18:53:51.
 */


@Scope("prototype")
@Controller
public class WebTemplateAction {

  public static final String MODEL = "model";

  private static final Logger log = LoggerFactory.getLogger(WebTemplateAction.class);

  @Autowired
  private WebTemplateService manager;

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/view_list")
  public String list(Pageable pageable, WebTemplateSo so, ModelMap model) {

    if (pageable != null) {
      if (pageable.getOrders() == null || pageable.getOrders().isEmpty()) {
        pageable.getOrders().add(Order.desc("id"));
      }
    }
    pageable.getFilters().addAll(FilterUtils.getFilters(so));
    Page<WebTemplate> pagination = manager.page(pageable);
    model.addAttribute("list", pagination.getContent());
    model.addAttribute("page", pagination);
    model.addAttribute("so", so);
    return "/admin/webtemplate/list";
  }

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/view_add")
  public String add(ModelMap model) {
    return "/admin/webtemplate/add";
  }

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/view_edit")
  public String edit(Pageable pageable, String id, ModelMap model) {
    model.addAttribute(MODEL, manager.findById(id));
    model.addAttribute("page", pageable);
    return "/admin/webtemplate/edit";
  }

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/view_view")
  public String view(String id, ModelMap model) {
    model.addAttribute(MODEL, manager.findById(id));
    return "/admin/webtemplate/view";
  }

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/model_save")
  public String save(WebTemplate bean, ModelMap model) {

    String view = "redirect:view_list.htm";
    try {
      manager.save(bean);
      log.info("save object id={}", bean.getId());
    } catch (Exception e) {
      log.error("保存失败", e);
      model.addAttribute("erro", e.getMessage());
      view = "/admin/webtemplate/add";
    }
    return view;
  }

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/model_update")
  public String update(Pageable pageable, WebTemplate bean, RedirectAttributes redirectAttributes, ModelMap model) {

    String view = "redirect:/admin/webtemplate/view_list.htm";
    try {
      manager.update(bean);
      redirectAttributes.addAttribute("pageNumber", pageable.getPageNumber());
    } catch (Exception e) {
      log.error("更新失败", e);
      model.addAttribute("erro", e.getMessage());
      model.addAttribute(MODEL, bean);
      model.addAttribute("page", pageable);
      view = "/admin/webtemplate/edit";
    }
    return view;
  }

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/model_delete")
  public String delete(Pageable pageable, String id, RedirectAttributes redirectAttributes) {

    String view = "redirect:/admin/webtemplate/view_list.htm";

    try {
      redirectAttributes.addAttribute("pageNumber", pageable.getPageNumber());
      manager.deleteById(id);
    } catch (DataIntegrityViolationException e) {
      log.error("删除失败", e);
      redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
    }

    return view;
  }

  @RequiresPermissions("webtemplate")
  @RequestMapping("/admin/webtemplate/model_deletes")
  public String deletes(Pageable pageable, String[] ids, RedirectAttributes redirectAttributes) {

    String view = "redirect:/admin/webtemplate/view_list.htm";

    try {
      redirectAttributes.addAttribute("pageNumber", pageable.getPageNumber());
      manager.deleteByIds(ids);
    } catch (DataIntegrityViolationException e) {
      log.error("批量删除失败", e);
      redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
    }
    return view;
  }

}