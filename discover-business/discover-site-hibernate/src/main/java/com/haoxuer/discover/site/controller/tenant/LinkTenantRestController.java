package com.haoxuer.discover.site.controller.tenant;

import com.haoxuer.discover.site.api.apis.LinkApi;
import com.haoxuer.discover.site.api.domain.list.LinkList;
import com.haoxuer.discover.site.api.domain.page.LinkPage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/link")
@RestController
public class LinkTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("link")
    @RequiresUser
    @RequestMapping("create")
    public LinkResponse create(LinkDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("link")
    @RequiresUser
    @RequestMapping("update")
    public LinkResponse update(LinkDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("link")
    @RequiresUser
    @RequestMapping("delete")
    public LinkResponse delete(LinkDataRequest request) {
        init(request);
        return api.delete(request);
    }

	@RequiresPermissions("link")
    @RequiresUser
    @RequestMapping("view")
    public LinkResponse view(LinkDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("link")
    @RequiresUser
    @RequestMapping("list")
    public LinkList list(LinkSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("link")
    @RequiresUser
    @RequestMapping("search")
    public LinkPage search(LinkSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private LinkApi api;

}
