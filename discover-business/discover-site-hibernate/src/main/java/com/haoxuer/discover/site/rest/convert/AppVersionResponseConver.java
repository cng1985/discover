package com.haoxuer.discover.site.rest.convert;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.site.data.entity.AppVersion;
import com.haoxuer.discover.site.domain.response.AppVersionResponse;

public class AppVersionResponseConver implements Conver<AppVersionResponse, AppVersion> {
  @Override
  public AppVersionResponse conver(AppVersion source) {
    AppVersionResponse result = new AppVersionResponse();
    result.setDownUrl(source.getDownUrl());
    result.setId(source.getId());
    result.setNote(source.getNote());
    result.setVersionCode(source.getVersionCode());
    result.setVersionName(source.getVersionName());
    return result;
  }
}
