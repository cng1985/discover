package com.haoxuer.discover.site.rest.resource;

import com.haoxuer.discover.site.api.apis.LinkApi;
import com.haoxuer.discover.site.api.domain.list.LinkList;
import com.haoxuer.discover.site.api.domain.page.LinkPage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkResponse;
import com.haoxuer.discover.site.data.dao.LinkDao;
import com.haoxuer.discover.site.data.entity.Link;
import com.haoxuer.discover.site.rest.convert.LinkResponseConvert;
import com.haoxuer.discover.site.rest.convert.LinkSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.site.data.dao.LinkTypeDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class LinkResource implements LinkApi {

    @Autowired
    private LinkDao dataDao;

    @Autowired
    private LinkTypeDao linkTypeDao;

    @Override
    public LinkResponse create(LinkDataRequest request) {
        LinkResponse result = new LinkResponse();

        Link bean = new Link();
        handleData(request, bean);
        dataDao.save(bean);
        result = new LinkResponseConvert().conver(bean);
        return result;
    }

    @Override
    public LinkResponse update(LinkDataRequest request) {
        LinkResponse result = new LinkResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        Link bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new LinkResponseConvert().conver(bean);
        return result;
    }

    private void handleData(LinkDataRequest request, Link bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getLinkType()!=null){
               bean.setLinkType(linkTypeDao.findById(request.getLinkType()));
            }

    }

    @Override
    public LinkResponse delete(LinkDataRequest request) {
        LinkResponse result = new LinkResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public LinkResponse view(LinkDataRequest request) {
        LinkResponse result=new LinkResponse();
        Link bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new LinkResponseConvert().conver(bean);
        return result;
    }
    @Override
    public LinkList list(LinkSearchRequest request) {
        LinkList result = new LinkList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<Link> organizations = dataDao.list(0, request.getSize(), filters, orders);
        LinkSimpleConvert convert=new LinkSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public LinkPage search(LinkSearchRequest request) {
        LinkPage result=new LinkPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<Link> page=dataDao.page(pageable);
        LinkSimpleConvert convert=new LinkSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
