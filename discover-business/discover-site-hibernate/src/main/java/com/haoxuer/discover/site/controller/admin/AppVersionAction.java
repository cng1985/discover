package com.haoxuer.discover.site.controller.admin;

import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.site.data.entity.AppVersion;
import com.haoxuer.discover.site.data.service.AppService;
import com.haoxuer.discover.site.data.service.AppVersionService;
import com.haoxuer.discover.site.data.so.AppVersionSo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by imake on 2017年07月25日11:33:24.
 */
@Controller
public class AppVersionAction {

  public static final String MODEL = "model";

  private static final Logger log = LoggerFactory.getLogger(AppVersionAction.class);

  @Autowired
  private AppVersionService manager;


  @Autowired
  private AppService appService;

  @RequiresPermissions("appversion")
  @RequestMapping("/admin/appversion/view_add")
  public String add(ModelMap model, Long id) {
    model.addAttribute(MODEL, appService.findById(id));
    return "/admin/appversion/add";
  }


  @RequiresPermissions("appversion")
  @RequestMapping("/admin/appversion/model_save")
  public String save(AppVersion bean, ModelMap model) {

    String view = AppAction.REDIRECT_LIST_HTML;
    try {
      manager.save(bean);
      log.info("save object id={}", bean.getId());
    } catch (Exception e) {
      log.error("保存失败", e);
      model.addAttribute(MODEL, appService.findById(bean.getApp().getId()));
      model.addAttribute("erro", "添加版本信息失败");
      view = "/admin/appversion/add";
    }
    return view;
  }


}