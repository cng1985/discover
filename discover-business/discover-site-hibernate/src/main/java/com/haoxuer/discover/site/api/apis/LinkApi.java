package com.haoxuer.discover.site.api.apis;


import com.haoxuer.discover.site.api.domain.list.LinkList;
import com.haoxuer.discover.site.api.domain.page.LinkPage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkResponse;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

public interface LinkApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    @CacheEvict(value = "catalogCache", allEntries = true)
    LinkResponse create(LinkDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    @CacheEvict(value = "catalogCache", allEntries = true)
    LinkResponse update(LinkDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    @CacheEvict(value = "catalogCache", allEntries = true)
    LinkResponse delete(LinkDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
    @Cacheable(value = "catalogCache", keyGenerator = "sysKeyGenerator")
    LinkResponse view(LinkDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    @Cacheable(value = "catalogCache", keyGenerator = "sysKeyGenerator")
    LinkList list(LinkSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    @Cacheable(value = "catalogCache", keyGenerator = "sysKeyGenerator")
    LinkPage search(LinkSearchRequest request);

}