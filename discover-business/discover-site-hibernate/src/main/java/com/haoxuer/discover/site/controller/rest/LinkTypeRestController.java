package com.haoxuer.discover.site.controller.rest;

import com.haoxuer.discover.site.api.apis.LinkTypeApi;
import com.haoxuer.discover.site.api.domain.list.LinkTypeList;
import com.haoxuer.discover.site.api.domain.page.LinkTypePage;
import com.haoxuer.discover.site.api.domain.request.*;
import com.haoxuer.discover.site.api.domain.response.LinkTypeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/linktype")
@RestController
public class LinkTypeRestController extends BaseRestController {


    @RequestMapping("create")
    public LinkTypeResponse create(LinkTypeDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public LinkTypeResponse update(LinkTypeDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public LinkTypeResponse delete(LinkTypeDataRequest request) {
        init(request);
        LinkTypeResponse result = new LinkTypeResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public LinkTypeResponse view(LinkTypeDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public LinkTypeList list(LinkTypeSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public LinkTypePage search(LinkTypeSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private LinkTypeApi api;

}
