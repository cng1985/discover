package com.haoxuer.discover.site.api.domain.page;


import com.haoxuer.discover.site.api.domain.simple.LinkSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年02月04日19:06:33.
*/

@Data
public class LinkPage  extends ResponsePage<LinkSimple> {

}