package com.haoxuer.discover.site.data.dao;


import com.haoxuer.discover.site.data.entity.AppVersion;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年07月25日11:33:24.
 */
public interface AppVersionDao extends BaseDao<AppVersion, Long> {

   AppVersion findById(Long id);

  AppVersion last(Long app);


  AppVersion save(AppVersion bean);

   AppVersion updateByUpdater(Updater<AppVersion> updater);

   AppVersion deleteById(Long id);
}