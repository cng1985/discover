package com.haoxuer.discover.site.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年02月04日19:06:33.
*/

@Data
public class LinkSearchRequest extends BasePageRequest {

    //链接名称
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;


    @Search(name = "linkType.id",operator = Filter.Operator.eq)
    private Integer linkType;


    private String sortField;


    private String sortMethod;
}