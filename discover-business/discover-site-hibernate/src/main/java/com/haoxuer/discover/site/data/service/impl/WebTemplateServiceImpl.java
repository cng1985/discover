package com.haoxuer.discover.site.data.service.impl;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.site.data.dao.WebTemplateDao;
import com.haoxuer.discover.site.data.entity.WebTemplate;
import com.haoxuer.discover.site.data.service.WebTemplateService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by imake on 2017年11月14日18:53:51.
 */


@Scope("prototype")
@Service
@Transactional
public class WebTemplateServiceImpl implements WebTemplateService {

  private WebTemplateDao dao;


  @Override
  @Transactional(readOnly = true)
  public WebTemplate findById(String id) {
    return dao.findById(id);
  }


  @Override
  @Transactional
  public WebTemplate save(WebTemplate bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public WebTemplate update(WebTemplate bean) {
    Updater<WebTemplate> updater = new Updater<WebTemplate>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public WebTemplate deleteById(String id) {
    WebTemplate bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public WebTemplate[] deleteByIds(String[] ids) {
    WebTemplate[] beans = new WebTemplate[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(WebTemplateDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<WebTemplate> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<WebTemplate> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<WebTemplate> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }
}