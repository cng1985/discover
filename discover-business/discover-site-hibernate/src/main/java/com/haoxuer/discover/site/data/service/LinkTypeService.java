package com.haoxuer.discover.site.data.service;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.site.data.entity.LinkType;
import java.util.List;

/**
 * Created by imake on 2017年08月28日16:29:13.
 */
public interface LinkTypeService {

  LinkType findById(Integer id);

  LinkType save(LinkType bean);

  LinkType update(LinkType bean);

  LinkType deleteById(Integer id);

  LinkType[] deleteByIds(Integer[] ids);

  Page<LinkType> page(Pageable pageable);

  Page<LinkType> page(Pageable pageable, Object search);

  List<LinkType> findByTops(Integer pid);

  List<LinkType> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}