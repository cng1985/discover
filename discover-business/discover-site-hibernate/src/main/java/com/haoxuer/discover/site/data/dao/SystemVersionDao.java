package com.haoxuer.discover.site.data.dao;


import com.haoxuer.discover.site.data.entity.SystemVersion;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
 * Created by imake on 2017年08月15日13:28:19.
 */
public interface SystemVersionDao extends BaseDao<SystemVersion, Long> {

  SystemVersion findById(Long id);

  SystemVersion save(SystemVersion bean);

  SystemVersion updateByUpdater(Updater<SystemVersion> updater);

  SystemVersion deleteById(Long id);

  Long next(String sequence);

}