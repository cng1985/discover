package com.haoxuer.discover.site.controller.rest;

import com.haoxuer.discover.rest.base.RequestTokenObject;
import com.haoxuer.discover.rest.base.ResponseTokenObject;
import com.haoxuer.discover.site.api.AppHandler;
import com.haoxuer.discover.site.domain.request.AppRequest;
import com.haoxuer.discover.site.domain.request.VersionRequest;
import com.haoxuer.discover.site.domain.response.AppVersionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ada on 2017/7/25.
 */

@RestController
@RequestMapping(value = "rest")
public class AppRestController {


  @RequestMapping(value = "/app/findToken")
  public ResponseTokenObject findToken(AppRequest request) {
    return handler.findToken(request);
  }

  @RequestMapping(value = "/app/refreshToken")
  public ResponseTokenObject refreshToken(RequestTokenObject request) {
    return handler.refreshToken(request);
  }

  @RequestMapping(value = "/app/version")
  public AppVersionResponse version(VersionRequest request) {
    return handler.version(request);
  }

  @Autowired
  AppHandler handler;

}
