package com.haoxuer.discover.bbs.freemaker;

import com.haoxuer.discover.bbs.data.service.ForumService;
import com.haoxuer.discover.bbs.data.entity.Forum;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.user.utils.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by cng19 on 2017/6/26.
 */
public class ForumDirective extends ListDirective<Forum> {


    public List<Forum> list() {
        List<Filter> filters = ListUtils.list(Filter.eq("parent.id", getInt("id")));
        List<Order> orders=ListUtils.list(Order.asc("code"));
        return forumService.list(0, getInt("size"),filters,orders );
    }

    @Autowired
    ForumService forumService;
}
