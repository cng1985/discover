package com.haoxuer.discover.bbs.data.dao;


import com.haoxuer.discover.bbs.data.entity.ForumPostText;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年11月27日09:47:03.
*/
public interface ForumPostTextDao extends BaseDao<ForumPostText,Long>{

	 ForumPostText findById(Long id);

	 ForumPostText save(ForumPostText bean);

	 ForumPostText updateByUpdater(Updater<ForumPostText> updater);

	 ForumPostText deleteById(Long id);
}