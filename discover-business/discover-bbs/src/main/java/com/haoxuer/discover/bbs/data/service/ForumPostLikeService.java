package com.haoxuer.discover.bbs.data.service;

import com.haoxuer.discover.bbs.data.entity.ForumPostLike;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumPostLikeService {

	ForumPostLike findById(Long id);

	ForumPostLike save(ForumPostLike bean);

	ForumPostLike update(ForumPostLike bean);

	ForumPostLike deleteById(Long id);
	
	ForumPostLike[] deleteByIds(Long[] ids);
	
	Page<ForumPostLike> page(Pageable pageable);
	
	Page<ForumPostLike> page(Pageable pageable, Object search);


	List<ForumPostLike> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}