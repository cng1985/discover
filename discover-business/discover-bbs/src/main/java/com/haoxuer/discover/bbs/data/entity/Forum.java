package com.haoxuer.discover.bbs.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.CatalogEntity;

import javax.persistence.*;

/**
 * 论坛模块
 *
 * Created by cng19 on 2017/6/22.
 */


@Entity
@Table(name = "bbs_froum")
public class Forum extends CatalogEntity {


    /**
     * 用户
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    /**
     * 数量
     */
    private Long nums;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pid")
    private Forum parent;

    public Forum getParent() {
        return parent;
    }

    public void setParent(Forum parent) {
        this.parent = parent;
    }

    public Integer getParentId() {
        if (parent != null) {
            return parent.getId();
        } else {
            return null;

        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getNums() {
        return nums;
    }

    public void setNums(Long nums) {
        this.nums = nums;
    }
}
