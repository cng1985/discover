package com.haoxuer.discover.bbs.data.service;

import com.haoxuer.discover.bbs.data.entity.ForumLike;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumLikeService {

	ForumLike findById(Long id);

	ForumLike save(ForumLike bean);

	ForumLike update(ForumLike bean);

	ForumLike deleteById(Long id);
	
	ForumLike[] deleteByIds(Long[] ids);
	
	Page<ForumLike> page(Pageable pageable);
	
	Page<ForumLike> page(Pageable pageable, Object search);


	List<ForumLike> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}