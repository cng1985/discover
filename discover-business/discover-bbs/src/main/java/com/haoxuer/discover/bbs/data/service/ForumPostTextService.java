package com.haoxuer.discover.bbs.data.service;

import com.haoxuer.discover.bbs.data.entity.ForumPostText;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年11月27日09:47:03.
*/
public interface ForumPostTextService {

	ForumPostText findById(Long id);

	ForumPostText save(ForumPostText bean);

	ForumPostText update(ForumPostText bean);

	ForumPostText deleteById(Long id);
	
	ForumPostText[] deleteByIds(Long[] ids);
	
	Page<ForumPostText> page(Pageable pageable);
	
	Page<ForumPostText> page(Pageable pageable, Object search);


	List<ForumPostText> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}