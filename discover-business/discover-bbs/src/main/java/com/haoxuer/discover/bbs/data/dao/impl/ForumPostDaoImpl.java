package com.haoxuer.discover.bbs.data.dao.impl;

import com.haoxuer.discover.bbs.data.dao.ForumDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.bbs.data.dao.ForumPostDao;
import com.haoxuer.discover.bbs.data.entity.ForumPost;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
@Repository

public class ForumPostDaoImpl extends CriteriaDaoImpl<ForumPost, Long> implements ForumPostDao {

	@Autowired
	ForumDao forumDao;

	@Override
	public ForumPost findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ForumPost save(ForumPost bean) {

        getSession().save(bean);

		Integer cid = -1;
		if (bean.getForum() != null) {
			cid = bean.getForum().getId();
		}
		if (cid > 0) {
			forumDao.updateNums(cid);
		}
		return bean;
	}

    @Override
	public ForumPost deleteById(Long id) {
		ForumPost entity = super.get(id);
		Integer forum=-1;
		if (entity.getForum()!=null){
			forum=entity.getForum().getId();
		}
		if (entity != null) {
			getSession().delete(entity);
		}
		if (forum > 0) {
			forumDao.updateNums(forum);
		}
		return entity;
	}
	
	@Override
	protected Class<ForumPost> getEntityClass() {
		return ForumPost.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}