package com.haoxuer.discover.bbs.data.dao;


import com.haoxuer.discover.bbs.data.entity.ForumPostLike;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumPostLikeDao extends BaseDao<ForumPostLike,Long>{

	 ForumPostLike findById(Long id);

	 ForumPostLike save(ForumPostLike bean);

	 ForumPostLike updateByUpdater(Updater<ForumPostLike> updater);

	 ForumPostLike deleteById(Long id);
}