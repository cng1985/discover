package com.haoxuer.discover.bbs.data.dao;


import com.haoxuer.discover.bbs.data.entity.Forum;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumDao extends BaseDao<Forum,Integer>{

	 Forum findById(Integer id);

	 Forum save(Forum bean);

	 Forum updateByUpdater(Updater<Forum> updater);

	 Forum deleteById(Integer id);

	/**
	 * 从子节点更新到根节点
	 *
	 * @param id
	 *            子节点id
	 * @return 更新节点层数
	 */
	Integer updateNums(Integer id);
}