package com.haoxuer.discover.bbs.data.dao;


import com.haoxuer.discover.bbs.data.entity.ForumLike;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumLikeDao extends BaseDao<ForumLike,Long>{

	 ForumLike findById(Long id);

	 ForumLike save(ForumLike bean);

	 ForumLike updateByUpdater(Updater<ForumLike> updater);

	 ForumLike deleteById(Long id);
}