package com.haoxuer.discover.bbs.data.service.impl;

import com.haoxuer.discover.bbs.data.service.ForumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.bbs.data.dao.ForumDao;
import com.haoxuer.discover.bbs.data.entity.Forum;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2017年11月27日09:47:02.
*/


@Scope("prototype")
@Service
@Transactional
public class ForumServiceImpl implements ForumService {

	private ForumDao dao;


	@Override
	@Transactional(readOnly = true)
	public Forum findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public List<Forum> findByTops(Integer pid) {
		LinkedList<Forum> result = new LinkedList<Forum>();
		Forum catalog = dao.findById(pid);
	    if(catalog != null){
			while ( catalog != null && catalog.getParent() != null ) {
				result.addFirst(catalog);
				catalog = dao.findById(catalog.getParentId());
			}
			result.addFirst(catalog);
	    }
		return result;
	}


    @Override
    public List<Forum> child(Integer id,Integer max) {
        List<Order> orders=new ArrayList<Order>();
        orders.add(Order.asc("code"));
        List<Filter> list=new ArrayList<Filter>();
        list.add(Filter.eq("parent.id",id));
        return dao.list(0,max,list,orders);
	}

	@Override
    @Transactional
	public Forum save(Forum bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public Forum update(Forum bean) {
		Updater<Forum> updater = new Updater<Forum>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public Forum deleteById(Integer id) {
		Forum bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public Forum[] deleteByIds(Integer[] ids) {
		Forum[] beans = new Forum[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ForumDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<Forum> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<Forum> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<Forum> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}