package com.haoxuer.discover.bbs.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.bbs.data.dao.ForumPostCommentDao;
import com.haoxuer.discover.bbs.data.entity.ForumPostComment;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
@Repository

public class ForumPostCommentDaoImpl extends CriteriaDaoImpl<ForumPostComment, Long> implements ForumPostCommentDao {

	@Override
	public ForumPostComment findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ForumPostComment save(ForumPostComment bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ForumPostComment deleteById(Long id) {
		ForumPostComment entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ForumPostComment> getEntityClass() {
		return ForumPostComment.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}