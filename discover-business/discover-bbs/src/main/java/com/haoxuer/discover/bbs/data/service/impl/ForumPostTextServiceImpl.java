package com.haoxuer.discover.bbs.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.bbs.data.dao.ForumPostTextDao;
import com.haoxuer.discover.bbs.data.entity.ForumPostText;
import com.haoxuer.discover.bbs.data.service.ForumPostTextService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2017年11月27日09:47:03.
*/


@Scope("prototype")
@Service
@Transactional
public class ForumPostTextServiceImpl implements ForumPostTextService {

	private ForumPostTextDao dao;


	@Override
	@Transactional(readOnly = true)
	public ForumPostText findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public ForumPostText save(ForumPostText bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public ForumPostText update(ForumPostText bean) {
		Updater<ForumPostText> updater = new Updater<ForumPostText>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public ForumPostText deleteById(Long id) {
		ForumPostText bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public ForumPostText[] deleteByIds(Long[] ids) {
		ForumPostText[] beans = new ForumPostText[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ForumPostTextDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<ForumPostText> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<ForumPostText> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<ForumPostText> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}