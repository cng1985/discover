package com.haoxuer.discover.bbs.data.dao;


import com.haoxuer.discover.bbs.data.entity.ForumPostComment;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumPostCommentDao extends BaseDao<ForumPostComment,Long>{

	 ForumPostComment findById(Long id);

	 ForumPostComment save(ForumPostComment bean);

	 ForumPostComment updateByUpdater(Updater<ForumPostComment> updater);

	 ForumPostComment deleteById(Long id);
}