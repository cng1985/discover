package com.haoxuer.discover.bbs.data.service.impl;

import com.haoxuer.discover.bbs.data.service.ForumPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.bbs.data.dao.ForumPostDao;
import com.haoxuer.discover.bbs.data.entity.ForumPost;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2017年11月27日09:47:02.
*/


@Scope("prototype")
@Service
@Transactional
public class ForumPostServiceImpl implements ForumPostService {

	private ForumPostDao dao;


	@Override
	@Transactional(readOnly = true)
	public ForumPost findById(Long id) {
		ForumPost post=dao.findById(id);
		Integer views=post.getViews();
		if (views==null){
			views=0;
		}
		views++;
		post.setViews(views);
		return post;
	}


	@Override
    @Transactional
	public ForumPost save(ForumPost bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public ForumPost update(ForumPost bean) {
		Updater<ForumPost> updater = new Updater<ForumPost>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public ForumPost deleteById(Long id) {
		ForumPost bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public ForumPost[] deleteByIds(Long[] ids) {
		ForumPost[] beans = new ForumPost[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ForumPostDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<ForumPost> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<ForumPost> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<ForumPost> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}