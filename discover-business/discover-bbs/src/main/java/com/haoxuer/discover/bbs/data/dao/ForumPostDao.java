package com.haoxuer.discover.bbs.data.dao;


import com.haoxuer.discover.bbs.data.entity.ForumPost;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumPostDao extends BaseDao<ForumPost,Long>{

	 ForumPost findById(Long id);

	 ForumPost save(ForumPost bean);

	 ForumPost updateByUpdater(Updater<ForumPost> updater);

	 ForumPost deleteById(Long id);
}