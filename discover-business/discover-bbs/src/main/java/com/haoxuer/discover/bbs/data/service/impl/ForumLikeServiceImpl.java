package com.haoxuer.discover.bbs.data.service.impl;

import com.haoxuer.discover.bbs.data.service.ForumLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.bbs.data.dao.ForumLikeDao;
import com.haoxuer.discover.bbs.data.entity.ForumLike;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2017年11月27日09:47:02.
*/


@Scope("prototype")
@Service
@Transactional
public class ForumLikeServiceImpl implements ForumLikeService {

	private ForumLikeDao dao;


	@Override
	@Transactional(readOnly = true)
	public ForumLike findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public ForumLike save(ForumLike bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public ForumLike update(ForumLike bean) {
		Updater<ForumLike> updater = new Updater<ForumLike>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public ForumLike deleteById(Long id) {
		ForumLike bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public ForumLike[] deleteByIds(Long[] ids) {
		ForumLike[] beans = new ForumLike[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ForumLikeDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<ForumLike> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<ForumLike> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<ForumLike> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}