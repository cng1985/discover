package com.haoxuer.discover.bbs.freemaker;

import com.haoxuer.discover.bbs.data.service.ForumPostService;
import com.haoxuer.discover.bbs.data.service.ForumService;
import com.haoxuer.discover.bbs.data.entity.Forum;
import com.haoxuer.discover.bbs.data.entity.ForumPost;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ada on 2017/7/1.
 */
public class ForumPostListDirective extends ListDirective<ForumPost> {
  @Override
  public List<ForumPost> list() {
    Integer id = getInt("forum");
    Forum forum = forumService.findById(id);
    if (forum == null) {
      return null;
    }
    List<Filter> filters =new ArrayList<>();
    filters.add(Filter.ge("forum.lft", forum.getLft()));
    filters.add(Filter.le("forum.rgt", forum.getRgt()));
    String catalog = getString("catalog","hot");
    List<Order> orders=new ArrayList<>();
    if ("new".equals(catalog)){
      orders.add(Order.desc("id"));
    }
    if ("hot".equals(catalog)){
      orders.add(Order.desc("views"));
    }
    if ("likes".equals(catalog)){
      orders.add(Order.desc("likes"));
    }
    return postService.list(0, getInt("size"), filters, orders);
  }

  @Autowired
  ForumPostService postService;

  @Autowired
  ForumService forumService;
}
