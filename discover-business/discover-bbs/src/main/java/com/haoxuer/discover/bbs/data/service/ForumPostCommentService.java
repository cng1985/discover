package com.haoxuer.discover.bbs.data.service;

import com.haoxuer.discover.bbs.data.entity.ForumPostComment;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年11月27日09:47:03.
*/
public interface ForumPostCommentService {

	ForumPostComment findById(Long id);

	ForumPostComment save(ForumPostComment bean);

	ForumPostComment update(ForumPostComment bean);

	ForumPostComment deleteById(Long id);
	
	ForumPostComment[] deleteByIds(Long[] ids);
	
	Page<ForumPostComment> page(Pageable pageable);
	
	Page<ForumPostComment> page(Pageable pageable, Object search);


	List<ForumPostComment> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}