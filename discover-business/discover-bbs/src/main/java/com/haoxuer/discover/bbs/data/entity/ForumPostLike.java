package com.haoxuer.discover.bbs.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.*;

/**
 * 对帖子点赞
 * Created by cng19 on 2017/6/22.
 */


@Entity
@Table(name = "bbs_froum_post_like")
public class ForumPostLike  extends AbstractEntity {

    /**
     * 帖子
     */
    @OneToOne(fetch = FetchType.LAZY)
    private ForumPost post;

    /**
     * 用户
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public ForumPost getPost() {
        return post;
    }

    public void setPost(ForumPost post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
