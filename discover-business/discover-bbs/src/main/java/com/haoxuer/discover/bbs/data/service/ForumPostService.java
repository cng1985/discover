package com.haoxuer.discover.bbs.data.service;

import com.haoxuer.discover.bbs.data.entity.ForumPost;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumPostService {

	ForumPost findById(Long id);

	ForumPost save(ForumPost bean);

	ForumPost update(ForumPost bean);

	ForumPost deleteById(Long id);
	
	ForumPost[] deleteByIds(Long[] ids);
	
	Page<ForumPost> page(Pageable pageable);
	
	Page<ForumPost> page(Pageable pageable, Object search);


	List<ForumPost> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}