package com.haoxuer.discover.bbs.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.bbs.data.dao.ForumPostCommentDao;
import com.haoxuer.discover.bbs.data.entity.ForumPostComment;
import com.haoxuer.discover.bbs.data.service.ForumPostCommentService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2017年11月27日09:47:03.
*/


@Scope("prototype")
@Service
@Transactional
public class ForumPostCommentServiceImpl implements ForumPostCommentService {

	private ForumPostCommentDao dao;


	@Override
	@Transactional(readOnly = true)
	public ForumPostComment findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public ForumPostComment save(ForumPostComment bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public ForumPostComment update(ForumPostComment bean) {
		Updater<ForumPostComment> updater = new Updater<ForumPostComment>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public ForumPostComment deleteById(Long id) {
		ForumPostComment bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public ForumPostComment[] deleteByIds(Long[] ids) {
		ForumPostComment[] beans = new ForumPostComment[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ForumPostCommentDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<ForumPostComment> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<ForumPostComment> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<ForumPostComment> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}