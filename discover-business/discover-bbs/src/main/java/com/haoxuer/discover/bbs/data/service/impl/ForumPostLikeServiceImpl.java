package com.haoxuer.discover.bbs.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.bbs.data.dao.ForumPostLikeDao;
import com.haoxuer.discover.bbs.data.entity.ForumPostLike;
import com.haoxuer.discover.bbs.data.service.ForumPostLikeService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2017年11月27日09:47:02.
*/


@Scope("prototype")
@Service
@Transactional
public class ForumPostLikeServiceImpl implements ForumPostLikeService {

	private ForumPostLikeDao dao;


	@Override
	@Transactional(readOnly = true)
	public ForumPostLike findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public ForumPostLike save(ForumPostLike bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public ForumPostLike update(ForumPostLike bean) {
		Updater<ForumPostLike> updater = new Updater<ForumPostLike>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public ForumPostLike deleteById(Long id) {
		ForumPostLike bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public ForumPostLike[] deleteByIds(Long[] ids) {
		ForumPostLike[] beans = new ForumPostLike[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(ForumPostLikeDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<ForumPostLike> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<ForumPostLike> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<ForumPostLike> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}