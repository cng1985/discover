package com.haoxuer.discover.bbs.data.service;

import com.haoxuer.discover.bbs.data.entity.Forum;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年11月27日09:47:02.
*/
public interface ForumService {

	Forum findById(Integer id);

	Forum save(Forum bean);

	Forum update(Forum bean);

	Forum deleteById(Integer id);
	
	Forum[] deleteByIds(Integer[] ids);
	
	Page<Forum> page(Pageable pageable);
	
	Page<Forum> page(Pageable pageable, Object search);

	List<Forum> findByTops(Integer pid);


    List<Forum> child(Integer id,Integer max);

	List<Forum> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}