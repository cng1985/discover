package com.haoxuer.discover.bbs.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.*;

/**
 * Created by cng19 on 2017/6/22.
 */


@Entity
@Table(name = "bbs_froum_post")
public class ForumPost extends AbstractEntity{


    /**
     * 用户
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    /**
     * 点赞数量
     */
    private Integer likes;

    /**
     * 查看次数
     */
    private Integer views;


    @ManyToOne(fetch = FetchType.LAZY)
    private Forum forum;

    @OneToOne(mappedBy = "post",fetch = FetchType.LAZY)
    private ForumPostText postText;

    public ForumPostText getPostText() {
        return postText;
    }

    public void setPostText(ForumPostText postText) {
        this.postText = postText;
    }

    private String name;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }
}
