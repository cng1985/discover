package com.haoxuer.discover.score.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;


@FormAnnotation(title = "积分规则")
@Entity
@Table(name = "sys_score_rule")
public class ScoreRule extends AbstractEntity {


  @FormField(title = "规则名称", sortNum = "2", grid = true, col = 12)
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
