package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.simple.TradeAccountSimple;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.data.rest.core.Conver;
public class TradeAccountSimpleConvert implements Conver<TradeAccountSimple, TradeAccount> {


    @Override
    public TradeAccountSimple conver(TradeAccount source) {
        TradeAccountSimple result = new TradeAccountSimple();

            result.setId(source.getId());
             result.setSalt(source.getSalt());
             result.setAccountType(source.getAccountType());
             result.setName(source.getName());
             result.setCheckValue(source.getCheckValue());
             result.setAddDate(source.getAddDate());
             result.setAmount(source.getAmount());
             result.setSerialNo(source.getSerialNo());

             result.setAccountTypeName(source.getAccountType()+"");
        return result;
    }
}
