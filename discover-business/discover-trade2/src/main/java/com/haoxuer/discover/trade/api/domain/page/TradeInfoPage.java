package com.haoxuer.discover.trade.api.domain.page;


import com.haoxuer.discover.trade.api.domain.simple.TradeInfoSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年03月20日16:39:57.
*/

@Data
public class TradeInfoPage  extends ResponsePage<TradeInfoSimple> {

}