package com.haoxuer.discover.trade.rest.resource;

import com.haoxuer.discover.trade.api.apis.TradeStreamApi;
import com.haoxuer.discover.trade.api.domain.list.TradeStreamList;
import com.haoxuer.discover.trade.api.domain.page.TradeStreamPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeStreamResponse;
import com.haoxuer.discover.trade.data.dao.TradeStreamDao;
import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.haoxuer.discover.trade.rest.convert.TradeStreamResponseConvert;
import com.haoxuer.discover.trade.rest.convert.TradeStreamSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.trade.data.dao.TradeInfoDao;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import java.util.ArrayList;
import java.util.List;



@Transactional
@Component
public class TradeStreamResource implements TradeStreamApi {

    @Autowired
    private TradeStreamDao dataDao;

    @Autowired
    private TradeInfoDao infoDao;
    @Autowired
    private TradeAccountDao accountDao;

    @Override
    public TradeStreamResponse create(TradeStreamDataRequest request) {
        TradeStreamResponse result = new TradeStreamResponse();

        TradeStream bean = new TradeStream();
        handleData(request, bean);
        dataDao.save(bean);
        result = new TradeStreamResponseConvert().conver(bean);
        return result;
    }

    @Override
    public TradeStreamResponse update(TradeStreamDataRequest request) {
        TradeStreamResponse result = new TradeStreamResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        TradeStream bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new TradeStreamResponseConvert().conver(bean);
        return result;
    }

    private void handleData(TradeStreamDataRequest request, TradeStream bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getInfo()!=null){
               bean.setInfo(infoDao.findById(request.getInfo()));
            }
            if(request.getAccount()!=null){
               bean.setAccount(accountDao.findById(request.getAccount()));
            }

    }

    @Override
    public TradeStreamResponse delete(TradeStreamDataRequest request) {
        TradeStreamResponse result = new TradeStreamResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public TradeStreamResponse view(TradeStreamDataRequest request) {
        TradeStreamResponse result=new TradeStreamResponse();
        TradeStream bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new TradeStreamResponseConvert().conver(bean);
        return result;
    }
    @Override
    public TradeStreamList list(TradeStreamSearchRequest request) {
        TradeStreamList result = new TradeStreamList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<TradeStream> organizations = dataDao.list(0, request.getSize(), filters, orders);
        TradeStreamSimpleConvert convert=new TradeStreamSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public TradeStreamPage search(TradeStreamSearchRequest request) {
        TradeStreamPage result=new TradeStreamPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<TradeStream> page=dataDao.page(pageable);
        TradeStreamSimpleConvert convert=new TradeStreamSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
