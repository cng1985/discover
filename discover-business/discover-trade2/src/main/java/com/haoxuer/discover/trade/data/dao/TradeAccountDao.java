package com.haoxuer.discover.trade.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.trade.api.domain.request.TradeRequest;
import com.haoxuer.discover.trade.api.domain.response.TradeResponse;
import  com.haoxuer.discover.trade.data.entity.TradeAccount;

/**
* Created by imake on 2021年03月20日16:39:57.
*/
public interface TradeAccountDao extends BaseDao<TradeAccount,Long>{

	 TradeAccount findById(Long id);

	 TradeAccount save(TradeAccount bean);

	 TradeAccount updateByUpdater(Updater<TradeAccount> updater);

	 TradeAccount deleteById(Long id);


	/**
	 * 正常账户，余额不能为负数
	 * @return
	 */
	TradeAccount initNormal();

	/**
	 * 特殊账户,余额可以为负数
	 * @return
	 */
	TradeAccount initSpecial();


	TradeResponse trade(TradeRequest request);
}