package com.haoxuer.discover.trade.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

/**
*
* Created by imake on 2021年03月25日17:08:53.
*/

@Data
public class TradeStreamDataRequest extends BaseRequest {

    private Long id;

     private BigDecimal preAmount;

     private String note;

     private Integer changeType;

     private BigDecimal afterAmount;

     private Long info;

     private BigDecimal amount;

     private Integer serialNo;

     private Long account;


}