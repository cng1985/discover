package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.response.BasicTradeAccountResponse;
import com.haoxuer.discover.trade.data.entity.BasicTradeAccount;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class BasicTradeAccountResponseConvert implements Conver<BasicTradeAccountResponse, BasicTradeAccount> {
    @Override
    public BasicTradeAccountResponse conver(BasicTradeAccount source) {
        BasicTradeAccountResponse result = new BasicTradeAccountResponse();
        BeanDataUtils.copyProperties(source, result);

        if (source.getAccount() != null) {
            result.setAccountName(source.getAccount().getName());
            result.setAccountBalance(source.getAccount().getAmount());
        }
        if (source.getAccount() != null) {
            result.setAccount(source.getAccount().getId());
        }


        return result;
    }
}
