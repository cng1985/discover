package com.haoxuer.discover.trade.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年03月25日17:08:53.
*/

@Data
public class TradeAccountExceptionResponse extends ResponseObject {

    private Long id;

     private String oldCheckValue;

     private BigDecimal oldAmount;

     private String accountName;

     private String checkValue;

     private String oldSalt;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private BigDecimal amount;

     private Long account;


}