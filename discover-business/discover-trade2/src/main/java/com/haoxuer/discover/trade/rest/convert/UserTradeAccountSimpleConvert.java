package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.simple.UserTradeAccountSimple;
import com.haoxuer.discover.trade.data.entity.UserTradeAccount;
import com.haoxuer.discover.data.rest.core.Conver;
public class UserTradeAccountSimpleConvert implements Conver<UserTradeAccountSimple, UserTradeAccount> {


    @Override
    public UserTradeAccountSimple conver(UserTradeAccount source) {
        UserTradeAccountSimple result = new UserTradeAccountSimple();

            result.setId(source.getId());
             result.setAddDate(source.getAddDate());
             result.setKey(source.getKey());

        return result;
    }
}
