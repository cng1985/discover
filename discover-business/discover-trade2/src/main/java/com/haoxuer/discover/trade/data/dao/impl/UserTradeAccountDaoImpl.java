package com.haoxuer.discover.trade.data.dao.impl;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.trade.data.dao.UserTradeAccountDao;
import com.haoxuer.discover.trade.data.entity.UserTradeAccount;

/**
* Created by imake on 2021年03月20日16:39:57.
*/
@Repository

public class UserTradeAccountDaoImpl extends CriteriaDaoImpl<UserTradeAccount, Long> implements UserTradeAccountDao {

	@Autowired
	private TradeAccountDao accountDao;

	@Override
	public UserTradeAccount findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}
	@Override
	public TradeAccount account(Long user, String key) {
		UserTradeAccount account = one(Filter.eq("key", key), Filter.eq("user.id", user));
		if (account == null) {
			account = new UserTradeAccount();
			account.setAccount(accountDao.initNormal());
			account.setKey(key);
			account.setUser(User.fromId(user));
			save(account);
		}
		return account.getAccount();
	}
	@Override
	public UserTradeAccount save(UserTradeAccount bean) {


        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public UserTradeAccount deleteById(Long id) {
		UserTradeAccount entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<UserTradeAccount> getEntityClass() {
		return UserTradeAccount.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}