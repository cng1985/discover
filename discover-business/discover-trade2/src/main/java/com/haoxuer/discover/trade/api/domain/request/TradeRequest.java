package com.haoxuer.discover.trade.api.domain.request;

import com.haoxuer.discover.trade.data.enums.ChangeType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class TradeRequest implements Serializable {

  private Long from;

  private Long to;

  private ChangeType changeType;

  private BigDecimal amount;

  private String note;

}
