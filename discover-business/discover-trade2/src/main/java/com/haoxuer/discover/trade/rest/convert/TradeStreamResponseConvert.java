package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.response.TradeStreamResponse;
import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class TradeStreamResponseConvert implements Conver<TradeStreamResponse, TradeStream> {
    @Override
    public TradeStreamResponse conver(TradeStream source) {
        TradeStreamResponse result = new TradeStreamResponse();
        BeanDataUtils.copyProperties(source,result);

         if(source.getAccount()!=null){
            result.setAccountName(source.getAccount().getName());
         }
        if(source.getInfo()!=null){
           result.setInfo(source.getInfo().getId());
        }
        if(source.getAccount()!=null){
           result.setAccount(source.getAccount().getId());
        }


        return result;
    }
}
