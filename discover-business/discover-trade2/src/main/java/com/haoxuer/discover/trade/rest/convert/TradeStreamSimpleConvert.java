package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.simple.TradeStreamSimple;
import com.haoxuer.discover.trade.data.entity.TradeStream;
import com.haoxuer.discover.data.rest.core.Conver;
public class TradeStreamSimpleConvert implements Conver<TradeStreamSimple, TradeStream> {


    @Override
    public TradeStreamSimple conver(TradeStream source) {
        TradeStreamSimple result = new TradeStreamSimple();

            result.setId(source.getId());
             result.setPreAmount(source.getPreAmount());
             result.setNote(source.getNote());
             result.setChangeType(source.getChangeType());
             if(source.getAccount()!=null){
                result.setAccountName(source.getAccount().getName());
             }
             result.setAfterAmount(source.getAfterAmount());
            if(source.getInfo()!=null){
               result.setInfo(source.getInfo().getId());
            }
             result.setAddDate(source.getAddDate());
             result.setAmount(source.getAmount());
             result.setSerialNo(source.getSerialNo());
            if(source.getAccount()!=null){
               result.setAccount(source.getAccount().getId());
            }

        return result;
    }
}
