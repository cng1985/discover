package com.haoxuer.discover.trade.rest.resource;

import com.haoxuer.discover.trade.api.apis.UserTradeAccountApi;
import com.haoxuer.discover.trade.api.domain.list.UserTradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.UserTradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.UserTradeAccountResponse;
import com.haoxuer.discover.trade.data.dao.UserTradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.entity.UserTradeAccount;
import com.haoxuer.discover.trade.rest.convert.UserTradeAccountResponseConvert;
import com.haoxuer.discover.trade.rest.convert.UserTradeAccountSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

import java.util.ArrayList;
import java.util.List;


@Transactional
@Component
public class UserTradeAccountResource implements UserTradeAccountApi {

    @Autowired
    private UserTradeAccountDao dataDao;


    @Override
    public TradeAccount account(Long user, String key) {
        return dataDao.account(user, key);
    }

    @Override
    public UserTradeAccountResponse create(UserTradeAccountDataRequest request) {
        UserTradeAccountResponse result = new UserTradeAccountResponse();

        UserTradeAccount bean = new UserTradeAccount();
        handleData(request, bean);
        dataDao.save(bean);
        result = new UserTradeAccountResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserTradeAccountResponse update(UserTradeAccountDataRequest request) {
        UserTradeAccountResponse result = new UserTradeAccountResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        UserTradeAccount bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new UserTradeAccountResponseConvert().conver(bean);
        return result;
    }

    private void handleData(UserTradeAccountDataRequest request, UserTradeAccount bean) {
        BeanDataUtils.copyProperties(request, bean);

    }

    @Override
    public UserTradeAccountResponse delete(UserTradeAccountDataRequest request) {
        UserTradeAccountResponse result = new UserTradeAccountResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public UserTradeAccountResponse view(UserTradeAccountDataRequest request) {
        UserTradeAccountResponse result = new UserTradeAccountResponse();
        UserTradeAccount bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new UserTradeAccountResponseConvert().conver(bean);
        return result;
    }

    @Override
    public UserTradeAccountList list(UserTradeAccountSearchRequest request) {
        UserTradeAccountList result = new UserTradeAccountList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<UserTradeAccount> organizations = dataDao.list(0, request.getSize(), filters, orders);
        UserTradeAccountSimpleConvert convert = new UserTradeAccountSimpleConvert();
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public UserTradeAccountPage search(UserTradeAccountSearchRequest request) {
        UserTradeAccountPage result = new UserTradeAccountPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.desc("" + request.getSortField()));
        } else {
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<UserTradeAccount> page = dataDao.page(pageable);
        UserTradeAccountSimpleConvert convert = new UserTradeAccountSimpleConvert();
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }
}
