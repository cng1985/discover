package com.haoxuer.discover.trade.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.trade.data.entity.TradeStream;

import java.math.BigDecimal;

/**
 * Created by imake on 2021年03月20日16:39:57.
 */
public interface TradeStreamDao extends BaseDao<TradeStream, Long> {

    TradeStream findById(Long id);

    TradeStream save(TradeStream bean);

    TradeStream updateByUpdater(Updater<TradeStream> updater);

    TradeStream deleteById(Long id);

    BigDecimal lastMoney(Long id);
}