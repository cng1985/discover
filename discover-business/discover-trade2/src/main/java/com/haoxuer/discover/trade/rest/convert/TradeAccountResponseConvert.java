package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.response.TradeAccountResponse;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class TradeAccountResponseConvert implements Conver<TradeAccountResponse, TradeAccount> {
    @Override
    public TradeAccountResponse conver(TradeAccount source) {
        TradeAccountResponse result = new TradeAccountResponse();
        BeanDataUtils.copyProperties(source,result);


         result.setAccountTypeName(source.getAccountType()+"");

        return result;
    }
}
