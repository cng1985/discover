package com.haoxuer.discover.trade.api.apis;


import com.haoxuer.discover.trade.api.domain.list.TradeAccountExceptionList;
import com.haoxuer.discover.trade.api.domain.page.TradeAccountExceptionPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeAccountExceptionResponse;

public interface TradeAccountExceptionApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    TradeAccountExceptionResponse create(TradeAccountExceptionDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    TradeAccountExceptionResponse update(TradeAccountExceptionDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    TradeAccountExceptionResponse delete(TradeAccountExceptionDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     TradeAccountExceptionResponse view(TradeAccountExceptionDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    TradeAccountExceptionList list(TradeAccountExceptionSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    TradeAccountExceptionPage search(TradeAccountExceptionSearchRequest request);

}