package com.haoxuer.discover.trade.api.apis;


import com.haoxuer.discover.trade.api.domain.list.TradeStreamList;
import com.haoxuer.discover.trade.api.domain.page.TradeStreamPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeStreamResponse;

public interface TradeStreamApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    TradeStreamResponse create(TradeStreamDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    TradeStreamResponse update(TradeStreamDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    TradeStreamResponse delete(TradeStreamDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     TradeStreamResponse view(TradeStreamDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    TradeStreamList list(TradeStreamSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    TradeStreamPage search(TradeStreamSearchRequest request);

}