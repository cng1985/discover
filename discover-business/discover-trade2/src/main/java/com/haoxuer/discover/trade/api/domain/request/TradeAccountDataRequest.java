package com.haoxuer.discover.trade.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.trade.data.enums.AccountType;
import java.math.BigDecimal;
import java.util.Date;

/**
*
* Created by imake on 2021年03月25日17:08:53.
*/

@Data
public class TradeAccountDataRequest extends BaseRequest {

    private Long id;

     private String salt;

     private AccountType accountType;

     private String name;

     private String checkValue;

     private BigDecimal amount;

     private Integer serialNo;


}