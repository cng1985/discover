package com.haoxuer.discover.trade.api.domain.page;


import com.haoxuer.discover.trade.api.domain.simple.BasicTradeAccountSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年03月25日17:18:36.
*/

@Data
public class BasicTradeAccountPage  extends ResponsePage<BasicTradeAccountSimple> {

}