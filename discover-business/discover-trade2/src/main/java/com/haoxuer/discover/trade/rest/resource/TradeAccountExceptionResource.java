package com.haoxuer.discover.trade.rest.resource;

import com.haoxuer.discover.trade.api.apis.TradeAccountExceptionApi;
import com.haoxuer.discover.trade.api.domain.list.TradeAccountExceptionList;
import com.haoxuer.discover.trade.api.domain.page.TradeAccountExceptionPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeAccountExceptionResponse;
import com.haoxuer.discover.trade.data.dao.TradeAccountExceptionDao;
import com.haoxuer.discover.trade.data.entity.TradeAccountException;
import com.haoxuer.discover.trade.rest.convert.TradeAccountExceptionResponseConvert;
import com.haoxuer.discover.trade.rest.convert.TradeAccountExceptionSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import java.util.ArrayList;
import java.util.List;



@Transactional
@Component
public class TradeAccountExceptionResource implements TradeAccountExceptionApi {

    @Autowired
    private TradeAccountExceptionDao dataDao;

    @Autowired
    private TradeAccountDao accountDao;

    @Override
    public TradeAccountExceptionResponse create(TradeAccountExceptionDataRequest request) {
        TradeAccountExceptionResponse result = new TradeAccountExceptionResponse();

        TradeAccountException bean = new TradeAccountException();
        handleData(request, bean);
        dataDao.save(bean);
        result = new TradeAccountExceptionResponseConvert().conver(bean);
        return result;
    }

    @Override
    public TradeAccountExceptionResponse update(TradeAccountExceptionDataRequest request) {
        TradeAccountExceptionResponse result = new TradeAccountExceptionResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        TradeAccountException bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new TradeAccountExceptionResponseConvert().conver(bean);
        return result;
    }

    private void handleData(TradeAccountExceptionDataRequest request, TradeAccountException bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getAccount()!=null){
               bean.setAccount(accountDao.findById(request.getAccount()));
            }

    }

    @Override
    public TradeAccountExceptionResponse delete(TradeAccountExceptionDataRequest request) {
        TradeAccountExceptionResponse result = new TradeAccountExceptionResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public TradeAccountExceptionResponse view(TradeAccountExceptionDataRequest request) {
        TradeAccountExceptionResponse result=new TradeAccountExceptionResponse();
        TradeAccountException bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new TradeAccountExceptionResponseConvert().conver(bean);
        return result;
    }
    @Override
    public TradeAccountExceptionList list(TradeAccountExceptionSearchRequest request) {
        TradeAccountExceptionList result = new TradeAccountExceptionList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<TradeAccountException> organizations = dataDao.list(0, request.getSize(), filters, orders);
        TradeAccountExceptionSimpleConvert convert=new TradeAccountExceptionSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public TradeAccountExceptionPage search(TradeAccountExceptionSearchRequest request) {
        TradeAccountExceptionPage result=new TradeAccountExceptionPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<TradeAccountException> page=dataDao.page(pageable);
        TradeAccountExceptionSimpleConvert convert=new TradeAccountExceptionSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
