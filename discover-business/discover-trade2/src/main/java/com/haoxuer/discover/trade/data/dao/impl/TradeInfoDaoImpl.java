package com.haoxuer.discover.trade.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.trade.data.dao.TradeInfoDao;
import com.haoxuer.discover.trade.data.entity.TradeInfo;

/**
* Created by imake on 2021年03月20日16:39:57.
*/
@Repository

public class TradeInfoDaoImpl extends CriteriaDaoImpl<TradeInfo, Long> implements TradeInfoDao {

	@Override
	public TradeInfo findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public TradeInfo save(TradeInfo bean) {


        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public TradeInfo deleteById(Long id) {
		TradeInfo entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<TradeInfo> getEntityClass() {
		return TradeInfo.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}