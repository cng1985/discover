package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.response.TradeInfoResponse;
import com.haoxuer.discover.trade.data.entity.TradeInfo;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class TradeInfoResponseConvert implements Conver<TradeInfoResponse, TradeInfo> {
    @Override
    public TradeInfoResponse conver(TradeInfo source) {
        TradeInfoResponse result = new TradeInfoResponse();
        BeanDataUtils.copyProperties(source,result);

         if(source.getTo()!=null){
            result.setToName(source.getTo().getName());
         }
        if(source.getFrom()!=null){
           result.setFrom(source.getFrom().getId());
        }
         if(source.getFrom()!=null){
            result.setFromName(source.getFrom().getName());
         }
        if(source.getTo()!=null){
           result.setTo(source.getTo().getId());
        }


        return result;
    }
}
