package com.haoxuer.discover.trade.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;


/**
*
* Created by imake on 2021年03月25日17:18:36.
*/

@Data
public class BasicTradeAccountSearchRequest extends BasePageRequest {

    //系统key
     @Search(name = "key",operator = Filter.Operator.like)
     private String key;

    //资金账号
     @Search(name = "account.id",operator = Filter.Operator.like)
     private Long account;




    private String sortField;


    private String sortMethod;
}