package com.haoxuer.discover.trade.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

/**
*
* Created by imake on 2021年03月25日17:08:53.
*/

@Data
public class TradeAccountExceptionDataRequest extends BaseRequest {

    private Long id;

     private String oldCheckValue;

     private BigDecimal oldAmount;

     private String checkValue;

     private String oldSalt;

     private BigDecimal amount;

     private Long account;


}