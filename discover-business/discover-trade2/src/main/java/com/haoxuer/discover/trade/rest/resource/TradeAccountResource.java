package com.haoxuer.discover.trade.rest.resource;

import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.trade.api.apis.TradeAccountApi;
import com.haoxuer.discover.trade.api.domain.list.TradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.TradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeAccountResponse;
import com.haoxuer.discover.trade.api.domain.response.TradeResponse;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.rest.convert.TradeAccountResponseConvert;
import com.haoxuer.discover.trade.rest.convert.TradeAccountSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

import java.util.ArrayList;
import java.util.List;


@Transactional
@Component
public class TradeAccountResource implements TradeAccountApi {

    @Autowired
    private TradeAccountDao dataDao;


    @Override
    public TradeAccountResponse create(TradeAccountDataRequest request) {
        TradeAccountResponse result = new TradeAccountResponse();

        TradeAccount bean = new TradeAccount();
        handleData(request, bean);
        dataDao.save(bean);
        result = new TradeAccountResponseConvert().conver(bean);
        return result;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Override
    public TradeResponse trade(TradeRequest request) {
        return dataDao.trade(request);
    }

    @Override
    public TradeAccountResponse update(TradeAccountDataRequest request) {
        TradeAccountResponse result = new TradeAccountResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        TradeAccount bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new TradeAccountResponseConvert().conver(bean);
        return result;
    }

    private void handleData(TradeAccountDataRequest request, TradeAccount bean) {
        BeanDataUtils.copyProperties(request, bean);

    }

    @Override
    public TradeAccountResponse delete(TradeAccountDataRequest request) {
        TradeAccountResponse result = new TradeAccountResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public TradeAccountResponse view(TradeAccountDataRequest request) {
        TradeAccountResponse result = new TradeAccountResponse();
        TradeAccount bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new TradeAccountResponseConvert().conver(bean);
        return result;
    }

    @Override
    public TradeAccountList list(TradeAccountSearchRequest request) {
        TradeAccountList result = new TradeAccountList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<TradeAccount> organizations = dataDao.list(0, request.getSize(), filters, orders);
        TradeAccountSimpleConvert convert = new TradeAccountSimpleConvert();
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public TradeAccountPage search(TradeAccountSearchRequest request) {
        TradeAccountPage result = new TradeAccountPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.desc("" + request.getSortField()));
        } else {
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<TradeAccount> page = dataDao.page(pageable);
        TradeAccountSimpleConvert convert = new TradeAccountSimpleConvert();
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }

    @Override
    public TradeAccount initNormal() {
        return dataDao.initNormal();
    }

    @Override
    public TradeAccount initSpecial() {
        return dataDao.initSpecial();
    }
}
