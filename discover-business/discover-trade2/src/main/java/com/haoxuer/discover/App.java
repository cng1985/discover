package com.haoxuer.discover;

import com.haoxuer.discover.trade.data.entity.BasicTradeAccount;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);

        make.setDao(false);
        make.setService(false);
        make.setAction(true);
        make.setView(false);
        make.setRest(true);
        make.setApi(false);
        //make.makes("com.haoxuer.discover.trade.data.entity");
        make.make(BasicTradeAccount.class);
    }
}
