package com.haoxuer.discover.trade.api.apis;


import com.haoxuer.discover.trade.api.domain.list.BasicTradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.BasicTradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.BasicTradeAccountResponse;
import com.haoxuer.discover.trade.data.entity.TradeAccount;

public interface BasicTradeAccountApi {


    /**
     * 正常账户，余额不能为负数
     * @return
     */
    TradeAccount normal(String key);

    /**
     * 特殊账户,余额可以为负数
     * @return
     */
    TradeAccount special(String key);

    /**
     * 创建
     *
     * @param request
     * @return
     */
    BasicTradeAccountResponse create(BasicTradeAccountDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    BasicTradeAccountResponse update(BasicTradeAccountDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    BasicTradeAccountResponse delete(BasicTradeAccountDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     BasicTradeAccountResponse view(BasicTradeAccountDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    BasicTradeAccountList list(BasicTradeAccountSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    BasicTradeAccountPage search(BasicTradeAccountSearchRequest request);

}