package com.haoxuer.discover.trade.controller.tenant;

import com.haoxuer.discover.trade.api.apis.BasicTradeAccountApi;
import com.haoxuer.discover.trade.api.domain.list.BasicTradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.BasicTradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.BasicTradeAccountResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/basictradeaccount")
@RestController
public class BasicTradeAccountTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("basictradeaccount")
    @RequiresUser
    @RequestMapping("create")
    public BasicTradeAccountResponse create(BasicTradeAccountDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("basictradeaccount")
    @RequiresUser
    @RequestMapping("update")
    public BasicTradeAccountResponse update(BasicTradeAccountDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("basictradeaccount")
    @RequiresUser
    @RequestMapping("delete")
    public BasicTradeAccountResponse delete(BasicTradeAccountDataRequest request) {
        init(request);
        BasicTradeAccountResponse result = new BasicTradeAccountResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("basictradeaccount")
    @RequiresUser
    @RequestMapping("view")
    public BasicTradeAccountResponse view(BasicTradeAccountDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("basictradeaccount")
    @RequiresUser
    @RequestMapping("list")
    public BasicTradeAccountList list(BasicTradeAccountSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("basictradeaccount")
    @RequiresUser
    @RequestMapping("search")
    public BasicTradeAccountPage search(BasicTradeAccountSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private BasicTradeAccountApi api;

}
