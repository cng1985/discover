package com.haoxuer.discover.trade.api.domain.simple;


import java.io.Serializable;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年03月25日17:18:36.
*/
@Data
public class BasicTradeAccountSimple implements Serializable {

    private Long id;

     private String accountName;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String key;
     private Long account;

     private BigDecimal accountBalance;


}
