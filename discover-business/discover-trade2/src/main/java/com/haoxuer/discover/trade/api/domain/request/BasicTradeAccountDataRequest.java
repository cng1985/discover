package com.haoxuer.discover.trade.api.domain.request;


import com.haoxuer.discover.trade.data.enums.AccountType;
import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年03月25日17:18:36.
*/

@Data
public class BasicTradeAccountDataRequest extends BaseRequest {

    private Long id;

     private String key;

    private String name;

    private Long account;

     //normal, special;
    private AccountType accountType;



}