package com.haoxuer.discover.trade.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by BigWorld on 2021年03月25日17:08:53.
*/
@Data
public class UserTradeAccountSimple implements Serializable {

    private Long id;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String key;


}
