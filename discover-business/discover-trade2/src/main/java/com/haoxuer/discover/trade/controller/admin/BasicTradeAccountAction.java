package com.haoxuer.discover.trade.controller.admin;

import com.haoxuer.discover.controller.BaseAction;
import com.haoxuer.discover.trade.api.apis.BasicTradeAccountApi;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
*
* Created by imake on 2021年03月25日17:08:53.
*/


@Scope("prototype")
@Controller
public class BasicTradeAccountAction extends BaseAction {

	public static final String MODEL = "model";

	public static final String REDIRECT_LIST_HTML = "redirect:/admin/basictradeaccount/view_list.htm";

	private static final Logger log = LoggerFactory.getLogger(BasicTradeAccountAction.class);

	@Autowired
	private BasicTradeAccountApi api;

	@RequiresPermissions("basictradeaccount")
	@RequestMapping("/admin/basictradeaccount/view_list")
	public String list(ModelMap model) {
		return getView("basictradeaccount/list");
	}

}