package com.haoxuer.discover.trade.rest.resource;

import com.haoxuer.discover.trade.api.apis.BasicTradeAccountApi;
import com.haoxuer.discover.trade.api.domain.list.BasicTradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.BasicTradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.BasicTradeAccountResponse;
import com.haoxuer.discover.trade.data.dao.BasicTradeAccountDao;
import com.haoxuer.discover.trade.data.entity.BasicTradeAccount;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.enums.AccountType;
import com.haoxuer.discover.trade.rest.convert.BasicTradeAccountResponseConvert;
import com.haoxuer.discover.trade.rest.convert.BasicTradeAccountSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


@Transactional
@Component
public class BasicTradeAccountResource implements BasicTradeAccountApi {

    @Autowired
    private BasicTradeAccountDao dataDao;

    @Autowired
    private TradeAccountDao accountDao;

    @Override
    public TradeAccount normal(String key) {
        return dataDao.normal(key);
    }

    @Override
    public TradeAccount special(String key) {
        return dataDao.special(key);
    }

    @Override
    public BasicTradeAccountResponse create(BasicTradeAccountDataRequest request) {
        BasicTradeAccountResponse result = new BasicTradeAccountResponse();
        if (request.getAccountType() == null) {
            request.setAccountType(AccountType.normal);
        }
        if (request.getKey() == null) {
            result.setCode(502);
            result.setMsg("该key为空");
            return result;
        }

        BasicTradeAccount account = dataDao.one(Filter.eq("key", request.getKey()));
        if (account != null) {
            result.setCode(501);
            result.setMsg("该key已存在");
            return result;
        }

        BasicTradeAccount bean = new BasicTradeAccount();
        TradeAccount tradeAccount = null;
        if (request.getAccountType() == AccountType.normal) {
            tradeAccount = accountDao.initNormal();
        } else {
            tradeAccount = accountDao.initSpecial();
        }
        tradeAccount.setName(request.getName());
        bean.setKey(request.getKey());
        bean.setAccount(tradeAccount);
        dataDao.save(bean);
        result = new BasicTradeAccountResponseConvert().conver(bean);
        return result;
    }

    @Override
    public BasicTradeAccountResponse update(BasicTradeAccountDataRequest request) {
        BasicTradeAccountResponse result = new BasicTradeAccountResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        BasicTradeAccount bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        if (StringUtils.hasText(request.getName())) {
            TradeAccount account = bean.getAccount();
            if (account == null) {
                account = accountDao.initNormal();
                bean.setAccount(account);
            }
            account.setName(request.getName());
        }
        result = new BasicTradeAccountResponseConvert().conver(bean);
        return result;
    }

    private void handleData(BasicTradeAccountDataRequest request, BasicTradeAccount bean) {
        BeanDataUtils.copyProperties(request, bean);
        if (request.getAccount() != null) {
            bean.setAccount(accountDao.findById(request.getAccount()));
        }

    }

    @Override
    public BasicTradeAccountResponse delete(BasicTradeAccountDataRequest request) {
        BasicTradeAccountResponse result = new BasicTradeAccountResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public BasicTradeAccountResponse view(BasicTradeAccountDataRequest request) {
        BasicTradeAccountResponse result = new BasicTradeAccountResponse();
        BasicTradeAccount bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result = new BasicTradeAccountResponseConvert().conver(bean);
        return result;
    }

    @Override
    public BasicTradeAccountList list(BasicTradeAccountSearchRequest request) {
        BasicTradeAccountList result = new BasicTradeAccountList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())) {
            orders.add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            orders.add(Order.desc("" + request.getSortField()));
        } else {
            orders.add(Order.desc("id"));
        }
        List<BasicTradeAccount> organizations = dataDao.list(0, request.getSize(), filters, orders);
        BasicTradeAccountSimpleConvert convert = new BasicTradeAccountSimpleConvert();
        ConverResourceUtils.converList(result, organizations, convert);
        return result;
    }

    @Override
    public BasicTradeAccountPage search(BasicTradeAccountSearchRequest request) {
        BasicTradeAccountPage result = new BasicTradeAccountPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.asc("" + request.getSortField()));
        } else if ("desc".equals(request.getSortMethod())) {
            pageable.getOrders().add(Order.desc("" + request.getSortField()));
        } else {
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<BasicTradeAccount> page = dataDao.page(pageable);
        BasicTradeAccountSimpleConvert convert = new BasicTradeAccountSimpleConvert();
        ConverResourceUtils.converPage(result, page, convert);
        return result;
    }
}
