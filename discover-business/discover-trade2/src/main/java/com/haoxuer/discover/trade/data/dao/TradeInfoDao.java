package com.haoxuer.discover.trade.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.trade.data.entity.TradeInfo;

/**
* Created by imake on 2021年03月20日16:39:57.
*/
public interface TradeInfoDao extends BaseDao<TradeInfo,Long>{

	 TradeInfo findById(Long id);

	 TradeInfo save(TradeInfo bean);

	 TradeInfo updateByUpdater(Updater<TradeInfo> updater);

	 TradeInfo deleteById(Long id);
}