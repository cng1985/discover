package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.simple.BasicTradeAccountSimple;
import com.haoxuer.discover.trade.data.entity.BasicTradeAccount;
import com.haoxuer.discover.data.rest.core.Conver;

public class BasicTradeAccountSimpleConvert implements Conver<BasicTradeAccountSimple, BasicTradeAccount> {


    @Override
    public BasicTradeAccountSimple conver(BasicTradeAccount source) {
        BasicTradeAccountSimple result = new BasicTradeAccountSimple();

        result.setId(source.getId());
        if (source.getAccount() != null) {
            result.setAccountName(source.getAccount().getName());
            result.setAccountBalance(source.getAccount().getAmount());
        }
        result.setAddDate(source.getAddDate());
        result.setKey(source.getKey());
        if (source.getAccount() != null) {
            result.setAccount(source.getAccount().getId());
        }

        return result;
    }
}
