package com.haoxuer.discover.trade.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年03月25日17:08:53.
*/

@Data
public class TradeStreamResponse extends ResponseObject {

    private Long id;

     private BigDecimal preAmount;

     private String note;

     private Integer changeType;

     private String accountName;

     private BigDecimal afterAmount;

     private Long info;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private BigDecimal amount;

     private Integer serialNo;

     private Long account;


}