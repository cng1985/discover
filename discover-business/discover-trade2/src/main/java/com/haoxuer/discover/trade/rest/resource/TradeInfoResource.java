package com.haoxuer.discover.trade.rest.resource;

import com.haoxuer.discover.trade.api.apis.TradeInfoApi;
import com.haoxuer.discover.trade.api.domain.list.TradeInfoList;
import com.haoxuer.discover.trade.api.domain.page.TradeInfoPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeInfoResponse;
import com.haoxuer.discover.trade.data.dao.TradeInfoDao;
import com.haoxuer.discover.trade.data.entity.TradeInfo;
import com.haoxuer.discover.trade.rest.convert.TradeInfoResponseConvert;
import com.haoxuer.discover.trade.rest.convert.TradeInfoSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import java.util.ArrayList;
import java.util.List;



@Transactional
@Component
public class TradeInfoResource implements TradeInfoApi {

    @Autowired
    private TradeInfoDao dataDao;

    @Autowired
    private TradeAccountDao toDao;
    @Autowired
    private TradeAccountDao fromDao;

    @Override
    public TradeInfoResponse create(TradeInfoDataRequest request) {
        TradeInfoResponse result = new TradeInfoResponse();

        TradeInfo bean = new TradeInfo();
        handleData(request, bean);
        dataDao.save(bean);
        result = new TradeInfoResponseConvert().conver(bean);
        return result;
    }

    @Override
    public TradeInfoResponse update(TradeInfoDataRequest request) {
        TradeInfoResponse result = new TradeInfoResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        TradeInfo bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new TradeInfoResponseConvert().conver(bean);
        return result;
    }

    private void handleData(TradeInfoDataRequest request, TradeInfo bean) {
        BeanDataUtils.copyProperties(request,bean);
            if(request.getFrom()!=null){
               bean.setFrom(fromDao.findById(request.getFrom()));
            }
            if(request.getTo()!=null){
               bean.setTo(toDao.findById(request.getTo()));
            }

    }

    @Override
    public TradeInfoResponse delete(TradeInfoDataRequest request) {
        TradeInfoResponse result = new TradeInfoResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public TradeInfoResponse view(TradeInfoDataRequest request) {
        TradeInfoResponse result=new TradeInfoResponse();
        TradeInfo bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new TradeInfoResponseConvert().conver(bean);
        return result;
    }
    @Override
    public TradeInfoList list(TradeInfoSearchRequest request) {
        TradeInfoList result = new TradeInfoList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<TradeInfo> organizations = dataDao.list(0, request.getSize(), filters, orders);
        TradeInfoSimpleConvert convert=new TradeInfoSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public TradeInfoPage search(TradeInfoSearchRequest request) {
        TradeInfoPage result=new TradeInfoPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<TradeInfo> page=dataDao.page(pageable);
        TradeInfoSimpleConvert convert=new TradeInfoSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
