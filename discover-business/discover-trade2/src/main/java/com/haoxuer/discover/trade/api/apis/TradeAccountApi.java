package com.haoxuer.discover.trade.api.apis;


import com.haoxuer.discover.rest.base.ResponseObject;
import com.haoxuer.discover.trade.api.domain.list.TradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.TradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeAccountResponse;
import com.haoxuer.discover.trade.api.domain.response.TradeResponse;
import com.haoxuer.discover.trade.data.entity.TradeAccount;

public interface TradeAccountApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    TradeAccountResponse create(TradeAccountDataRequest request);


    /**
     * 资金交易
     *
     * @param request
     * @return
     */
    TradeResponse trade(TradeRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    TradeAccountResponse update(TradeAccountDataRequest request);

    /**
     * 删除
     *
     * @param request
     * @return
     */
    TradeAccountResponse delete(TradeAccountDataRequest request);


    /**
     * 详情
     *
     * @param request
     * @return
     */
    TradeAccountResponse view(TradeAccountDataRequest request);


    /**
     * 集合功能
     *
     * @param request
     * @return
     */
    TradeAccountList list(TradeAccountSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    TradeAccountPage search(TradeAccountSearchRequest request);

    /**
     * 正常账户，余额不能为负数
     * @return
     */
    TradeAccount initNormal();

    /**
     * 特殊账户,余额可以为负数
     * @return
     */
    TradeAccount initSpecial();

}