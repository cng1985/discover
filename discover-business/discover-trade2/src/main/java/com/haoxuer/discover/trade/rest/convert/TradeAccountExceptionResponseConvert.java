package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.response.TradeAccountExceptionResponse;
import com.haoxuer.discover.trade.data.entity.TradeAccountException;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class TradeAccountExceptionResponseConvert implements Conver<TradeAccountExceptionResponse, TradeAccountException> {
    @Override
    public TradeAccountExceptionResponse conver(TradeAccountException source) {
        TradeAccountExceptionResponse result = new TradeAccountExceptionResponse();
        BeanDataUtils.copyProperties(source,result);

         if(source.getAccount()!=null){
            result.setAccountName(source.getAccount().getName());
         }
        if(source.getAccount()!=null){
           result.setAccount(source.getAccount().getId());
        }


        return result;
    }
}
