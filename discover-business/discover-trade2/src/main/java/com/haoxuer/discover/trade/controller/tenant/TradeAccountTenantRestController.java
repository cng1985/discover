package com.haoxuer.discover.trade.controller.tenant;

import com.haoxuer.discover.trade.api.apis.TradeAccountApi;
import com.haoxuer.discover.trade.api.domain.list.TradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.TradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeAccountResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/tradeaccount")
@RestController
public class TradeAccountTenantRestController extends BaseTenantRestController {


	@RequiresPermissions("tradeaccount")
    @RequiresUser
    @RequestMapping("create")
    public TradeAccountResponse create(TradeAccountDataRequest request) {
        init(request);
        return api.create(request);
    }

	@RequiresPermissions("tradeaccount")
    @RequiresUser
    @RequestMapping("update")
    public TradeAccountResponse update(TradeAccountDataRequest request) {
        init(request);
        return api.update(request);
    }

	@RequiresPermissions("tradeaccount")
    @RequiresUser
    @RequestMapping("delete")
    public TradeAccountResponse delete(TradeAccountDataRequest request) {
        init(request);
        TradeAccountResponse result = new TradeAccountResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

	@RequiresPermissions("tradeaccount")
    @RequiresUser
    @RequestMapping("view")
    public TradeAccountResponse view(TradeAccountDataRequest request) {
       init(request);
       return api.view(request);
   }

	@RequiresPermissions("tradeaccount")
    @RequiresUser
    @RequestMapping("list")
    public TradeAccountList list(TradeAccountSearchRequest request) {
        init(request);
        return api.list(request);
    }

	@RequiresPermissions("tradeaccount")
    @RequiresUser
    @RequestMapping("search")
    public TradeAccountPage search(TradeAccountSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private TradeAccountApi api;

}
