package com.haoxuer.discover.trade.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.trade.data.dao.TradeAccountExceptionDao;
import com.haoxuer.discover.trade.data.entity.TradeAccountException;

/**
* Created by imake on 2021年03月20日16:39:57.
*/
@Repository

public class TradeAccountExceptionDaoImpl extends CriteriaDaoImpl<TradeAccountException, Long> implements TradeAccountExceptionDao {

	@Override
	public TradeAccountException findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public TradeAccountException save(TradeAccountException bean) {


        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public TradeAccountException deleteById(Long id) {
		TradeAccountException entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<TradeAccountException> getEntityClass() {
		return TradeAccountException.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}