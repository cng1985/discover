package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.response.UserTradeAccountResponse;
import com.haoxuer.discover.trade.data.entity.UserTradeAccount;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class UserTradeAccountResponseConvert implements Conver<UserTradeAccountResponse, UserTradeAccount> {
    @Override
    public UserTradeAccountResponse conver(UserTradeAccount source) {
        UserTradeAccountResponse result = new UserTradeAccountResponse();
        BeanDataUtils.copyProperties(source,result);



        return result;
    }
}
