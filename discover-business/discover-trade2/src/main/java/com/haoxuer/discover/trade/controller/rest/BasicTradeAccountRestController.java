package com.haoxuer.discover.trade.controller.rest;

import com.haoxuer.discover.trade.api.apis.BasicTradeAccountApi;
import com.haoxuer.discover.trade.api.domain.list.BasicTradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.BasicTradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.BasicTradeAccountResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/basictradeaccount")
@RestController
public class BasicTradeAccountRestController extends BaseRestController {


    @RequestMapping("create")
    public BasicTradeAccountResponse create(BasicTradeAccountDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public BasicTradeAccountResponse update(BasicTradeAccountDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public BasicTradeAccountResponse delete(BasicTradeAccountDataRequest request) {
        init(request);
        BasicTradeAccountResponse result = new BasicTradeAccountResponse();
        try {
           result = api.delete(request);
        } catch (Exception e) {
           result.setCode(501);
           result.setMsg("删除失败!");
        }
        return result;
    }

    @RequestMapping("view")
    public BasicTradeAccountResponse view(BasicTradeAccountDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public BasicTradeAccountList list(BasicTradeAccountSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public BasicTradeAccountPage search(BasicTradeAccountSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private BasicTradeAccountApi api;

}
