package com.haoxuer.discover.trade.api.apis;


import com.haoxuer.discover.trade.api.domain.list.TradeInfoList;
import com.haoxuer.discover.trade.api.domain.page.TradeInfoPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeInfoResponse;

public interface TradeInfoApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    TradeInfoResponse create(TradeInfoDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    TradeInfoResponse update(TradeInfoDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    TradeInfoResponse delete(TradeInfoDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     TradeInfoResponse view(TradeInfoDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    TradeInfoList list(TradeInfoSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    TradeInfoPage search(TradeInfoSearchRequest request);

}