package com.haoxuer.discover.trade.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;


@Data
@FormAnnotation(title = "资金账户")
@Entity
@Table(name = "sys_trade_user_account")
public class UserTradeAccount extends AbstractEntity {

  @Column(name = "trade_key",length = 50)
  private String key;

  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  @ManyToOne(fetch = FetchType.LAZY)
  private TradeAccount account;

}
