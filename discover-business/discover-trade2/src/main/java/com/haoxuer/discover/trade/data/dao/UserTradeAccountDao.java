package com.haoxuer.discover.trade.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import  com.haoxuer.discover.trade.data.entity.UserTradeAccount;

/**
* Created by imake on 2021年03月20日16:39:57.
*/
public interface UserTradeAccountDao extends BaseDao<UserTradeAccount,Long>{

	 UserTradeAccount findById(Long id);

	 UserTradeAccount save(UserTradeAccount bean);

	 UserTradeAccount updateByUpdater(Updater<UserTradeAccount> updater);

	 UserTradeAccount deleteById(Long id);

	TradeAccount account(Long user, String key);

}