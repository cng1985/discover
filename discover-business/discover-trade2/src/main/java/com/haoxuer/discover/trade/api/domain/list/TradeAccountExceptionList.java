package com.haoxuer.discover.trade.api.domain.list;


import com.haoxuer.discover.trade.api.domain.simple.TradeAccountExceptionSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年03月20日16:39:57.
*/

@Data
public class TradeAccountExceptionList  extends ResponseList<TradeAccountExceptionSimple> {

}