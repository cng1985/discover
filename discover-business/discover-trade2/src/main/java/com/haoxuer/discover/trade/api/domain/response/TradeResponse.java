package com.haoxuer.discover.trade.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.trade.data.enums.AccountType;

/**
 * Created by imake on 2021年03月25日17:08:53.
 */

@Data
public class TradeResponse extends ResponseObject {

    private Long id;

    private String salt;

    private AccountType accountType;

    private String name;

    private String checkValue;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date addDate;

    private BigDecimal amount;

    private Long from;

    private Long to;

    private Long fromStream;

    private Long toStream;

    private Integer serialNo;


    private String accountTypeName;
}