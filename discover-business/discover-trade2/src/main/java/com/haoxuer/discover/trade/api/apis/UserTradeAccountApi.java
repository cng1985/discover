package com.haoxuer.discover.trade.api.apis;


import com.haoxuer.discover.trade.api.domain.list.UserTradeAccountList;
import com.haoxuer.discover.trade.api.domain.page.UserTradeAccountPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.UserTradeAccountResponse;
import com.haoxuer.discover.trade.data.entity.TradeAccount;

public interface UserTradeAccountApi {


    public TradeAccount account(Long user, String key);

    /**
     * 创建
     *
     * @param request
     * @return
     */
    UserTradeAccountResponse create(UserTradeAccountDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    UserTradeAccountResponse update(UserTradeAccountDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    UserTradeAccountResponse delete(UserTradeAccountDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     UserTradeAccountResponse view(UserTradeAccountDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    UserTradeAccountList list(UserTradeAccountSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    UserTradeAccountPage search(UserTradeAccountSearchRequest request);

}