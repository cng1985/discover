package com.haoxuer.discover.trade.controller.tenant;

import com.haoxuer.discover.trade.api.apis.TradeStreamApi;
import com.haoxuer.discover.trade.api.domain.list.TradeStreamList;
import com.haoxuer.discover.trade.api.domain.page.TradeStreamPage;
import com.haoxuer.discover.trade.api.domain.request.*;
import com.haoxuer.discover.trade.api.domain.response.TradeStreamResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@RequestMapping("/tenantRest/tradestream")
@RestController
public class TradeStreamTenantRestController extends BaseTenantRestController {



    @RequiresUser
    @RequestMapping("search")
    public TradeStreamPage search(TradeStreamSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private TradeStreamApi api;

}
