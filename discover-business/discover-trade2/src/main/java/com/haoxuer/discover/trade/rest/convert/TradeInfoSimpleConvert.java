package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.simple.TradeInfoSimple;
import com.haoxuer.discover.trade.data.entity.TradeInfo;
import com.haoxuer.discover.data.rest.core.Conver;
public class TradeInfoSimpleConvert implements Conver<TradeInfoSimple, TradeInfo> {


    @Override
    public TradeInfoSimple conver(TradeInfo source) {
        TradeInfoSimple result = new TradeInfoSimple();

            result.setId(source.getId());
             if(source.getTo()!=null){
                result.setToName(source.getTo().getName());
             }
            if(source.getFrom()!=null){
               result.setFrom(source.getFrom().getId());
            }
             if(source.getFrom()!=null){
                result.setFromName(source.getFrom().getName());
             }
            if(source.getTo()!=null){
               result.setTo(source.getTo().getId());
            }
             result.setAddDate(source.getAddDate());
             result.setAmount(source.getAmount());

        return result;
    }
}
