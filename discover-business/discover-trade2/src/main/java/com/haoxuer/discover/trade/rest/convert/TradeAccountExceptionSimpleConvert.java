package com.haoxuer.discover.trade.rest.convert;

import com.haoxuer.discover.trade.api.domain.simple.TradeAccountExceptionSimple;
import com.haoxuer.discover.trade.data.entity.TradeAccountException;
import com.haoxuer.discover.data.rest.core.Conver;
public class TradeAccountExceptionSimpleConvert implements Conver<TradeAccountExceptionSimple, TradeAccountException> {


    @Override
    public TradeAccountExceptionSimple conver(TradeAccountException source) {
        TradeAccountExceptionSimple result = new TradeAccountExceptionSimple();

            result.setId(source.getId());
             result.setOldCheckValue(source.getOldCheckValue());
             result.setOldAmount(source.getOldAmount());
             if(source.getAccount()!=null){
                result.setAccountName(source.getAccount().getName());
             }
             result.setCheckValue(source.getCheckValue());
             result.setOldSalt(source.getOldSalt());
             result.setAddDate(source.getAddDate());
             result.setAmount(source.getAmount());
            if(source.getAccount()!=null){
               result.setAccount(source.getAccount().getId());
            }

        return result;
    }
}
