package com.haoxuer.discover.pays.api.domain.response;

import com.haoxuer.discover.pays.api.domain.simple.WeiXinPaySimple;
import com.haoxuer.discover.rest.base.ResponseObject;

public class OrderResponse extends ResponseObject {
  
  private WeiXinPaySimple pay;
  
  private String no;

  
  public WeiXinPaySimple getPay() {
    return pay;
  }
  
  public void setPay(WeiXinPaySimple pay) {
    this.pay = pay;
  }
  
  public String getNo() {
    return no;
  }
  
  public void setNo(String no) {
    this.no = no;
  }
}
