package com.haoxuer.discover.pays.api.apis;

import com.haoxuer.discover.pays.api.domain.page.RechargeCardPage;
import com.haoxuer.discover.pays.api.domain.request.RechargeMoneyRequest;
import com.haoxuer.discover.pays.api.domain.request.RechargePageRequest;
import com.haoxuer.discover.pays.api.domain.request.RechargeRequest;
import com.haoxuer.discover.pays.api.domain.response.OrderResponse;

public interface RechargeApi {

  /**
   * 充值面额列表
   *
   * @param request
   * @return
   */
  RechargeCardPage page(RechargePageRequest request);

  /**
   * 充值
   *
   * @param request
   * @return
   */
  OrderResponse recharge(RechargeRequest request);


  /**
   * 充钱
   * @param request
   * @return
   */
  OrderResponse rechargeMoney(RechargeMoneyRequest request);




}
