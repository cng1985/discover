package com.haoxuer.discover.pays.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.pays.data.dao.RechargeCardDao;
import com.haoxuer.discover.pays.data.entity.RechargeCard;
import com.haoxuer.discover.pays.data.service.RechargeCardService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月21日10:41:46.
*/


@Scope("prototype")
@Service
@Transactional
public class RechargeCardServiceImpl implements RechargeCardService {

	private RechargeCardDao dao;


	@Override
	@Transactional(readOnly = true)
	public RechargeCard findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public RechargeCard save(RechargeCard bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public RechargeCard update(RechargeCard bean) {
		Updater<RechargeCard> updater = new Updater<RechargeCard>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public RechargeCard deleteById(Long id) {
		RechargeCard bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public RechargeCard[] deleteByIds(Long[] ids) {
		RechargeCard[] beans = new RechargeCard[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(RechargeCardDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<RechargeCard> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<RechargeCard> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<RechargeCard> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}