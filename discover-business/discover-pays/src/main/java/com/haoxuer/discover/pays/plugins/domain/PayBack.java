package com.haoxuer.discover.pays.plugins.domain;

import com.haoxuer.discover.rest.base.ResponseObject;

public class PayBack extends ResponseObject {
  
  private String no;
  
  
  private String orderNo;
  
  private String payType;
  
  private String userToken;
  
  private Long commerce;
  
  public String getPayType() {
    return payType;
  }
  
  public void setPayType(String payType) {
    this.payType = payType;
  }
  
  public String getOrderNo() {
    return orderNo;
  }
  
  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }
  
  public String getNo() {
    return no;
  }
  
  public void setNo(String no) {
    this.no = no;
  }
  
  public String getUserToken() {
    return userToken;
  }
  
  public void setUserToken(String userToken) {
    this.userToken = userToken;
  }
  
  public Long getCommerce() {
    return commerce;
  }
  
  public void setCommerce(Long commerce) {
    this.commerce = commerce;
  }
}
