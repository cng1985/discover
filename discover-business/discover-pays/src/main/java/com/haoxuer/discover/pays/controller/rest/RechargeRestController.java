package com.haoxuer.discover.pays.controller.rest;

import com.haoxuer.discover.pays.api.apis.RechargeApi;
import com.haoxuer.discover.pays.api.domain.page.RechargeCardPage;
import com.haoxuer.discover.pays.api.domain.request.RechargeMoneyRequest;
import com.haoxuer.discover.pays.api.domain.request.RechargePageRequest;
import com.haoxuer.discover.pays.api.domain.request.RechargeRequest;
import com.haoxuer.discover.pays.api.domain.response.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest/rechargecard")
@RestController
public class RechargeRestController {

  @RequestMapping("/page")
  public RechargeCardPage page(RechargePageRequest request) {
    return api.page(request);
  }

  @RequestMapping("/recharge")
  public OrderResponse recharge(RechargeRequest request) {
    return api.recharge(request);
  }

  @RequestMapping("/recharge_money")
  public OrderResponse rechargeMoney(RechargeMoneyRequest request) {
    return api.rechargeMoney(request);
  }

  @Autowired
  private RechargeApi api;

}
