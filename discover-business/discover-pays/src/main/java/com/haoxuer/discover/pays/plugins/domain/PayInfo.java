package com.haoxuer.discover.pays.plugins.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayInfo implements Serializable {
  
  private BigDecimal money;
  
  private String payType;
  
  private String openid;
  
  private String body;
  
  private String ip;
  
  private String userToken;
  
  private Long commerce;
  
  public String getBody() {
    return body;
  }
  
  public void setBody(String body) {
    this.body = body;
  }
  
  public String getOpenid() {
    return openid;
  }
  
  public void setOpenid(String openid) {
    this.openid = openid;
  }
  
  public BigDecimal getMoney() {
    return money;
  }
  
  public void setMoney(BigDecimal money) {
    this.money = money;
  }
  
  public String getPayType() {
    return payType;
  }
  
  public void setPayType(String payType) {
    this.payType = payType;
  }
  
  public String getIp() {
    return ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public String getUserToken() {
    return userToken;
  }
  
  public void setUserToken(String userToken) {
    this.userToken = userToken;
  }
  
  public Long getCommerce() {
    return commerce;
  }
  
  public void setCommerce(Long commerce) {
    this.commerce = commerce;
  }
}
