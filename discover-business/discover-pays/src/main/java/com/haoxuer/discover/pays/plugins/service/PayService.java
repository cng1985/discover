package com.haoxuer.discover.pays.plugins.service;


import com.haoxuer.discover.pays.api.domain.simple.WeiXinPaySimple;
import com.haoxuer.discover.pays.plugins.domain.PayBack;
import com.haoxuer.discover.pays.plugins.domain.PayInfo;

public interface PayService {
  PayBack pay(PayInfo payinfo);

  WeiXinPaySimple handle(PayBack back);
}
