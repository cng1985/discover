package com.haoxuer.discover.pays.data.service;

import com.haoxuer.discover.pay.weixin.domain.PayBack;
import com.haoxuer.discover.pays.data.entity.Payment;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

/**
 * Created by imake on 2018年12月20日18:08:26.
 */
public interface PaymentService {

  Payment findById(Long id);

  Payment save(Payment bean);

  Payment update(Payment bean);

  Payment deleteById(Long id);

  Payment[] deleteByIds(Long[] ids);

  Page<Payment> page(Pageable pageable);

  Page<Payment> page(Pageable pageable, Object search);


  List<Payment> list(int first, Integer size, List<Filter> filters, List<Order> orders);

  Payment handle(PayBack payBack);
}