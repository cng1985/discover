package com.haoxuer.discover.pays.api.domain.request;

import com.haoxuer.discover.rest.base.RequestUserTokenPageObject;

public class PageRequest extends RequestUserTokenPageObject {
  
  private Long app;
  
  public Long getApp() {
    return app;
  }
  
  public void setApp(Long app) {
    this.app = app;
  }
}
