package com.haoxuer.discover.pays.data.dao.impl;

import com.haoxuer.discover.data.page.Filter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.pays.data.dao.PaymentDao;
import com.haoxuer.discover.pays.data.entity.Payment;

/**
* Created by imake on 2018年12月20日18:08:26.
*/
@Repository

public class PaymentDaoImpl extends CriteriaDaoImpl<Payment, Long> implements PaymentDao {

	@Override
	public Payment findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Payment save(Payment bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Payment deleteById(Long id) {
		Payment entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Payment findByNo(String no) {
		return one(Filter.eq("no",no));
	}

	@Override
	protected Class<Payment> getEntityClass() {
		return Payment.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}