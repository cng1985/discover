package com.haoxuer.discover.pays.plugins.weixinpay;


import com.haoxuer.discover.plug.api.IPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import com.haoxuer.discover.plug.data.plugs.base.PlugTemplateController;
import com.haoxuer.discover.plug.data.service.PluginConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/admin/plugin_payment/weixinpayment")
public class WeiXinPaymentController extends PlugTemplateController {
  
  @Resource(name = "weiXinPaymentPlugin")
  private WeiXinPaymentPlugin weiAppPaymentPlugin;
  
  @Resource(name = "pluginConfigServiceImpl")
  private PluginConfigService pluginConfigService;
  
  
  @Override
  public PluginConfig getPluginConfig() {
    return weiAppPaymentPlugin.getPluginConfig();
  }
  
  @Override
  public IPlugin getPlug() {
    return weiAppPaymentPlugin;
  }
  
  @Override
  public PluginConfigService getPluginConfigService() {
    return pluginConfigService;
  }
  
  @Override
  public String getView() {
    return "/admin/plugin_payment";
  }
  
  @Override
  public String getSettingView() {
    return "weixin";
  }
}
