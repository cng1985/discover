package com.haoxuer.discover.pays.rest.conver;

import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.pays.api.domain.simple.RechargeCardSimple;
import com.haoxuer.discover.pays.data.entity.RechargeCard;

public class RechargeCardSimpleConver implements Conver<RechargeCardSimple, RechargeCard> {
  @Override
  public RechargeCardSimple conver(RechargeCard source) {
    RechargeCardSimple result = new RechargeCardSimple();
    result.setId(source.getId());
    result.setGive(source.getGive());
    result.setLogo(source.getLogo());
    result.setCheckLogo(source.getCheckLogo());
    result.setMoney(source.getMoney());
    result.setName(source.getName());
    result.setRebate(source.getRebate());
    return result;
  }
}
