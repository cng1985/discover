package com.haoxuer.discover.pays.data.enums;

public enum PayState {
  
  /**
   * 等待支付
   */
  wait,
  
  /**
   * 支付成功
   */
  success,
  
  /**
   * 支付失败
   */
  failure;
  @Override
  public String toString() {
    if (name().equals("wait")) {
      return "等待支付";
    } else if (name().equals("success")) {
      return "支付成功";
    } else if (name().equals("failure")) {
      return "支付失败";
    } else {
      return super.toString();
    }
  }
}
