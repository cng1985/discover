package com.haoxuer.discover.pays.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.pays.data.entity.RechargeCatalog;

/**
* Created by imake on 2018年12月21日10:41:46.
*/
public interface RechargeCatalogDao extends BaseDao<RechargeCatalog,Long>{

	 RechargeCatalog findById(Long id);

	 RechargeCatalog save(RechargeCatalog bean);

	 RechargeCatalog updateByUpdater(Updater<RechargeCatalog> updater);

	 RechargeCatalog deleteById(Long id);
}