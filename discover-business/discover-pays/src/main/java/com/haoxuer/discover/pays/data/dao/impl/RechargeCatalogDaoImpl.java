package com.haoxuer.discover.pays.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.pays.data.dao.RechargeCatalogDao;
import com.haoxuer.discover.pays.data.entity.RechargeCatalog;

/**
* Created by imake on 2018年12月21日10:41:46.
*/
@Repository

public class RechargeCatalogDaoImpl extends CriteriaDaoImpl<RechargeCatalog, Long> implements RechargeCatalogDao {

	@Override
	public RechargeCatalog findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public RechargeCatalog save(RechargeCatalog bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public RechargeCatalog deleteById(Long id) {
		RechargeCatalog entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<RechargeCatalog> getEntityClass() {
		return RechargeCatalog.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}