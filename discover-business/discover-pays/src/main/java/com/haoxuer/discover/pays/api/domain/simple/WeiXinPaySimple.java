package com.haoxuer.discover.pays.api.domain.simple;

import java.io.Serializable;

public class WeiXinPaySimple implements Serializable {
  
  private String appid;
  
  private String partnerid;
  
  private String prepayid;
  
  private String packageInfo;
  
  private String noncestr;
  
  private String timestamp;
  
  private String sign;
  
  private String signType;
  
  public String getAppid() {
    return appid;
  }
  
  public void setAppid(String appid) {
    this.appid = appid;
  }
  
  public String getPartnerid() {
    return partnerid;
  }
  
  public void setPartnerid(String partnerid) {
    this.partnerid = partnerid;
  }
  
  public String getPrepayid() {
    return prepayid;
  }
  
  public void setPrepayid(String prepayid) {
    this.prepayid = prepayid;
  }
  
  public String getPackageInfo() {
    return packageInfo;
  }
  
  public void setPackageInfo(String packageInfo) {
    this.packageInfo = packageInfo;
  }
  
  public String getNoncestr() {
    return noncestr;
  }
  
  public void setNoncestr(String noncestr) {
    this.noncestr = noncestr;
  }
  
  public String getTimestamp() {
    return timestamp;
  }
  
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }
  
  public String getSign() {
    return sign;
  }
  
  public void setSign(String sign) {
    this.sign = sign;
  }
  
  public String getSignType() {
    return signType;
  }
  
  public void setSignType(String signType) {
    this.signType = signType;
  }
}
