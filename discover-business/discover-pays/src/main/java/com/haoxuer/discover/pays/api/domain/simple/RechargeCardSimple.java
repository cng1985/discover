package com.haoxuer.discover.pays.api.domain.simple;

import java.io.Serializable;
import java.math.BigDecimal;

public class RechargeCardSimple implements Serializable {

  private Long id;

  private String name;

  private String logo;

  private String  checkLogo;


  private BigDecimal money;

  private BigDecimal give;

  private BigDecimal rebate;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public BigDecimal getMoney() {
    return money;
  }

  public void setMoney(BigDecimal money) {
    this.money = money;
  }

  public BigDecimal getGive() {
    return give;
  }

  public void setGive(BigDecimal give) {
    this.give = give;
  }

  public BigDecimal getRebate() {
    return rebate;
  }

  public void setRebate(BigDecimal rebate) {
    this.rebate = rebate;
  }


  public String getCheckLogo() {
    return checkLogo;
  }

  public void setCheckLogo(String checkLogo) {
    this.checkLogo = checkLogo;
  }
}
