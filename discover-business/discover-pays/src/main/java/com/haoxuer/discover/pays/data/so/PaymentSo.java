package com.haoxuer.discover.pays.data.so;

import java.io.Serializable;

/**
* Created by imake on 2018年12月20日18:08:26.
*/
public class PaymentSo implements Serializable {

    private String name;

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

}
