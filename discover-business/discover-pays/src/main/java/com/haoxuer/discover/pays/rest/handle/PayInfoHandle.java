package com.haoxuer.discover.pays.rest.handle;

import com.haoxuer.discover.pays.plugins.domain.PayInfo;
import com.haoxuer.discover.user.data.dao.UserOauthTokenDao;
import com.haoxuer.discover.user.data.entity.UserOauthToken;

public class PayInfoHandle {

  private UserOauthTokenDao tokenDao;

  private Long member;

  public PayInfoHandle(UserOauthTokenDao tokenDao, Long member, String payType) {
    this.tokenDao = tokenDao;
    this.member = member;
    this.payType = payType;
  }

  private String payType;

  public PayInfo handle(){
    PayInfo result=new PayInfo();
    if (payType.contains("weiapp")) {
      UserOauthToken token = tokenDao.findByUser(member, payType);
      if (token == null) {
        throw new RuntimeException("没有通过小程序登陆");
      }
      result.setOpenid(token.getUid());
    }
   return  result;
  }
}
