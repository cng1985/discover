package com.haoxuer.discover.pays.data.service.impl;

import com.haoxuer.discover.pay.weixin.domain.PayBack;
import com.haoxuer.discover.pays.data.enums.PayState;
import com.haoxuer.discover.pays.data.handle.MoneyHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.pays.data.dao.PaymentDao;
import com.haoxuer.discover.pays.data.entity.Payment;
import com.haoxuer.discover.pays.data.service.PaymentService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.*;

import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;

import javax.annotation.Resource;


/**
 * Created by imake on 2018年12月20日18:08:26.
 */


@Scope("prototype")
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

  private PaymentDao dao;


  @Override
  @Transactional(readOnly = true)
  public Payment findById(Long id) {
    return dao.findById(id);
  }


  @Override
  @Transactional
  public Payment save(Payment bean) {
    dao.save(bean);
    return bean;
  }

  @Override
  @Transactional
  public Payment update(Payment bean) {
    Updater<Payment> updater = new Updater<Payment>(bean);
    return dao.updateByUpdater(updater);
  }

  @Override
  @Transactional
  public Payment deleteById(Long id) {
    Payment bean = dao.findById(id);
    dao.deleteById(id);
    return bean;
  }

  @Override
  @Transactional
  public Payment[] deleteByIds(Long[] ids) {
    Payment[] beans = new Payment[ids.length];
    for (int i = 0, len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }


  @Autowired
  public void setDao(PaymentDao dao) {
    this.dao = dao;
  }

  @Override
  public Page<Payment> page(Pageable pageable) {
    return dao.page(pageable);
  }


  @Override
  public Page<Payment> page(Pageable pageable, Object search) {
    List<Filter> filters = FilterUtils.getFilters(search);
    if (filters != null) {
      pageable.getFilters().addAll(filters);
    }
    return dao.page(pageable);
  }

  @Override
  public List<Payment> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
    return dao.list(first, size, filters, orders);
  }

  @Resource
  private Map<String, MoneyHandle> handles = new HashMap<String, MoneyHandle>();

  @Transactional(isolation = Isolation.SERIALIZABLE)
  @Override
  public Payment handle(PayBack payBack) {
    Payment payment = dao.findByNo(payBack.getOut_trade_no());
    if (payment != null) {
      if (payment.getPayState() == PayState.wait) {
        payment.setPayState(PayState.success);
        payment.setBankType(payBack.getBank_type());
        payment.setBussNo(payBack.getTransaction_id());
        MoneyHandle handle= handles.get(payment.getHandle());
        if (handle!=null){
          handle.handle(payment);
        }
      }
    }
    return payment;
  }
}