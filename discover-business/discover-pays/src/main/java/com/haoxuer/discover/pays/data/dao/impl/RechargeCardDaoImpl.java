package com.haoxuer.discover.pays.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.pays.data.dao.RechargeCardDao;
import com.haoxuer.discover.pays.data.entity.RechargeCard;

/**
* Created by imake on 2018年12月21日10:41:46.
*/
@Repository

public class RechargeCardDaoImpl extends CriteriaDaoImpl<RechargeCard, Long> implements RechargeCardDao {

	@Override
	public RechargeCard findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public RechargeCard save(RechargeCard bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public RechargeCard deleteById(Long id) {
		RechargeCard entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<RechargeCard> getEntityClass() {
		return RechargeCard.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}