package com.haoxuer.discover.pays.rest.resource;

import com.haoxuer.discover.config.data.dao.UserDao;
import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.pays.api.apis.RechargeApi;
import com.haoxuer.discover.pays.api.domain.page.RechargeCardPage;
import com.haoxuer.discover.pays.api.domain.request.RechargeMoneyRequest;
import com.haoxuer.discover.pays.api.domain.request.RechargePageRequest;
import com.haoxuer.discover.pays.api.domain.request.RechargeRequest;
import com.haoxuer.discover.pays.api.domain.response.OrderResponse;
import com.haoxuer.discover.pays.api.domain.simple.WeiXinPaySimple;
import com.haoxuer.discover.pays.data.dao.PaymentDao;
import com.haoxuer.discover.pays.data.dao.RechargeCardDao;
import com.haoxuer.discover.pays.data.entity.Payment;
import com.haoxuer.discover.pays.data.entity.RechargeCard;
import com.haoxuer.discover.pays.data.enums.PayState;
import com.haoxuer.discover.pays.plugins.domain.PayBack;
import com.haoxuer.discover.pays.plugins.domain.PayInfo;
import com.haoxuer.discover.pays.plugins.service.PayService;
import com.haoxuer.discover.pays.rest.conver.PageableConver;
import com.haoxuer.discover.pays.rest.conver.RechargeCardSimpleConver;
import com.haoxuer.discover.pays.rest.handle.PayInfoHandle;
import com.haoxuer.discover.user.data.dao.UserOauthTokenDao;
import com.haoxuer.discover.user.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class RechargeResource implements RechargeApi {

  @Autowired
  private RechargeCardDao cardDao;

  @Autowired
  private UserDao userDao;



  @Autowired
  private PaymentDao  paymentDao;

  @Autowired
  private RechargeCardDao rechargeCardDao;

  @Autowired
  private PayService payService;

  @Autowired
  private UserOauthTokenDao tokenDao;

  @Autowired
  UserTokenService tokenService;

  @Override
  public RechargeCardPage page(RechargePageRequest request) {
    RechargeCardPage result = new RechargeCardPage();
    Pageable pageable = new PageableConver().conver(request);
    pageable.getFilters().add(Filter.eq("catalog.id", request.getId()));
    pageable.getOrders().add(Order.asc("money"));
    Page<RechargeCard> page = cardDao.page(pageable);
    ConverResourceUtils.converPage(result, page, new RechargeCardSimpleConver());
    return result;
  }

  @Override
  public OrderResponse recharge(RechargeRequest request) {
    OrderResponse result = new OrderResponse();
    Long mid = tokenService.user(request.getUserToken());
    User user = userDao.findById(mid);
    if (user == null) {
      result.setCode(-301);
      result.setMsg("用户信息无效");
      return result;
    }
    //在线支付
    PayInfoHandle handle = new PayInfoHandle(tokenDao, mid, request.getPayType());
    PayInfo payinfo = handle.handle();
    RechargeCard card = rechargeCardDao.findById(request.getId());
    payinfo.setMoney(card.getMoney());
    payinfo.setPayType(request.getPayType());
    payinfo.setBody("充值");
    payinfo.setUserToken(request.getToken());

    PayBack back = payService.pay(payinfo);

    back.setPayType(request.getPayType());
    back.setUserToken(request.getToken());
    WeiXinPaySimple pay = payService.handle(back);
    if (back.getCode() != 0) {
      result.setCode(back.getCode());
      result.setMsg(back.getMsg());
      return result;
    }
    String no = back.getNo();
    result.setNo(no);
    result.setPay(pay);
    Payment payment = new Payment();
    payment.setNo(back.getOrderNo());
    payment.setMoney(payinfo.getMoney());
    payment.setBussType(1);
    payment.setUser(user);
    payment.setPayState(PayState.wait);
    payment.setBid(request.getId());
    payment.setHandle("rechargeCardHandle");
    paymentDao.save(payment);
    return result;
  }

  @Override
  public OrderResponse rechargeMoney(RechargeMoneyRequest request) {
    OrderResponse result = new OrderResponse();
    Long mid = tokenService.user(request.getUserToken());
    User user = userDao.findById(mid);
    if (user == null) {
      result.setCode(-301);
      result.setMsg("用户信息无效");
      return result;
    }

    //在线支付
    PayInfoHandle handle = new PayInfoHandle(tokenDao, mid, request.getPayType());
    PayInfo payinfo = handle.handle();
    payinfo.setMoney(request.getMoney());
    payinfo.setPayType(request.getPayType());
    payinfo.setBody("充值");
    payinfo.setUserToken(request.getUserToken());

    PayBack back = payService.pay(payinfo);

    back.setPayType(request.getPayType());
    back.setUserToken(request.getToken());
    WeiXinPaySimple pay = payService.handle(back);
    if (back.getCode() != 0) {
      result.setCode(back.getCode());
      result.setMsg(back.getMsg());
      return result;
    }
    String no = back.getNo();
    result.setNo(no);
    result.setPay(pay);

    Payment payment = new Payment();
    payment.setNo(back.getOrderNo());
    payment.setMoney(payinfo.getMoney());
    payment.setBussType(1);
    payment.setUser(user);
    payment.setPayState(PayState.wait);
    payment.setBid(-1L);
    payment.setHandle(request.getHandle());
    paymentDao.save(payment);
    return result;
  }
}
