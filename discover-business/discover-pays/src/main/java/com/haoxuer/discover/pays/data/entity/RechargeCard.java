package com.haoxuer.discover.pays.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;


@FormAnnotation(title = "充值卡")
@Entity
@Table(name = "pays_recharge_card")
public class RechargeCard extends AbstractEntity {


  @FormField(title = "充值卡名称", sortNum = "1", grid = true, col = 12)
  private String name;

  @ManyToOne(fetch = FetchType.LAZY)
  private RechargeCatalog catalog;

  private String logo;

  private String checkLogo;

  /**
   * 充值面额
   */
  @FormField(title = "充值面额", sortNum = "2", grid = true, col = 12)
  private BigDecimal money;

  /**
   * 赠送金额
   */
  @FormField(title = "赠送金额", sortNum = "3", grid = true, col = 12)
  private BigDecimal give;

  /**
   * 折扣
   */
  @FormField(title = "折扣", sortNum = "4", grid = true, col = 12)
  private BigDecimal rebate;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getCheckLogo() {
    return checkLogo;
  }

  public void setCheckLogo(String checkLogo) {
    this.checkLogo = checkLogo;
  }

  public BigDecimal getMoney() {
    return money;
  }

  public void setMoney(BigDecimal money) {
    this.money = money;
  }

  public BigDecimal getGive() {
    return give;
  }

  public void setGive(BigDecimal give) {
    this.give = give;
  }

  public BigDecimal getRebate() {
    return rebate;
  }

  public void setRebate(BigDecimal rebate) {
    this.rebate = rebate;
  }

  public RechargeCatalog getCatalog() {
    return catalog;
  }

  public void setCatalog(RechargeCatalog catalog) {
    this.catalog = catalog;
  }
}
