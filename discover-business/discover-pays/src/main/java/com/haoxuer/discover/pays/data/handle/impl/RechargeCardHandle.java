package com.haoxuer.discover.pays.data.handle.impl;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.pays.data.dao.RechargeCardDao;
import com.haoxuer.discover.pays.data.entity.Payment;
import com.haoxuer.discover.pays.data.entity.RechargeCard;
import com.haoxuer.discover.pays.data.handle.MoneyHandle;
import com.haoxuer.discover.trade.data.dao.BasicTradeAccountDao;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.dao.UserTradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.others.ChangeType;
import com.haoxuer.discover.trade.data.request.TradeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component("rechargeCardHandle")
public class RechargeCardHandle implements MoneyHandle {

  @Autowired
  RechargeCardDao cardDao;

  @Autowired
  UserTradeAccountDao userTradeAccountDao;

  @Autowired
  BasicTradeAccountDao basicTradeAccountDao;

  @Autowired
  TradeAccountDao tradeAccountDao;

  @Override
  public void handle(Payment payment) {
    RechargeCard card = cardDao.findById(payment.getBid());
    if (card == null) {
      return;
    }

    User user = payment.getUser();
    TradeAccount account = userTradeAccountDao.account(user.getId(), "basicAccount");
    TradeAccount system = basicTradeAccountDao.special("systemAccount");
    if (card.getMoney() != null) {
      TradeRequest request = new TradeRequest();
      request.setFrom(system.getId());
      request.setTo(account.getId());
      request.setAmount(card.getMoney());
      request.setChangeType(ChangeType.from(1, "充值"));
      tradeAccountDao.trade(request);
    }
    if (card.getGive() != null&&card.getGive().compareTo(new BigDecimal(0))>0) {
      TradeAccount give = basicTradeAccountDao.special("giveAccount");
      TradeRequest request = new TradeRequest();
      request.setFrom(give.getId());
      request.setTo(account.getId());
      request.setAmount(card.getGive());
      request.setChangeType(ChangeType.from(2, "赠送"));
      tradeAccountDao.trade(request);
    }
  }
}
