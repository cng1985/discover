package com.haoxuer.discover.pays.plugins.base;

import com.haoxuer.discover.pays.api.domain.simple.WeiXinPaySimple;
import com.haoxuer.discover.pays.plugins.domain.PayBack;
import com.haoxuer.discover.pays.plugins.domain.PayInfo;
import com.haoxuer.discover.plug.api.IPlugin;

public abstract class PaymentPlugin extends IPlugin {
  
  public abstract PayBack pay(PayInfo payinfo);
  
  public abstract WeiXinPaySimple handle(PayBack back);
  
}
