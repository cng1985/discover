package com.haoxuer.discover.pays.api.domain.request;

public class RechargeRequest extends AppRequest {

  private Long id;

  private String payType;




  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPayType() {
    if (payType==null){
      return "weiapp";
    }
    return payType;
  }

  public void setPayType(String payType) {
    this.payType = payType;
  }

}
