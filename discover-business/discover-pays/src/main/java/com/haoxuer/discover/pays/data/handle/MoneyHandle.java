package com.haoxuer.discover.pays.data.handle;

import com.haoxuer.discover.pays.data.entity.Payment;

public interface MoneyHandle {

  void handle(Payment payment);
}
