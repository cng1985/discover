package com.haoxuer.discover.pays.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.pays.data.enums.PayState;

import javax.persistence.*;
import java.math.BigDecimal;

@FormAnnotation(title = "付款单")
@Entity
@Table(name = "pays_payment")
public class Payment extends AbstractEntity {
  
  private BigDecimal money;
  
  private String no;
  
  private String bussNo;
  
  private PayState payState;
  
  /**
   * 业务对象
   */
  private Long bid;
  
  /**
   * 业务类型
   */
  private Integer bussType;

  /**
   * 充钱以后业务处理
   */
  @Column(length = 50)
  private String  handle;
  
  @Column(length = 20)
  private String bankType;

  @ManyToOne(fetch = FetchType.LAZY)
  private User user;
  
  public BigDecimal getMoney() {
    return money;
  }
  
  public void setMoney(BigDecimal money) {
    this.money = money;
  }
  
  public String getNo() {
    return no;
  }
  
  public void setNo(String no) {
    this.no = no;
  }
  
  public String getBussNo() {
    return bussNo;
  }
  
  public void setBussNo(String bussNo) {
    this.bussNo = bussNo;
  }
  
  public PayState getPayState() {
    return payState;
  }
  
  public void setPayState(PayState payState) {
    this.payState = payState;
  }
  
  public Long getBid() {
    return bid;
  }
  
  public void setBid(Long bid) {
    this.bid = bid;
  }
  

  public String getBankType() {
    return bankType;
  }
  
  public void setBankType(String bankType) {
    this.bankType = bankType;
  }

  public Integer getBussType() {
    return bussType;
  }

  public void setBussType(Integer bussType) {
    this.bussType = bussType;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getHandle() {
    return handle;
  }

  public void setHandle(String handle) {
    this.handle = handle;
  }
}
