package com.haoxuer.discover.pays.data.service;

import com.haoxuer.discover.pays.data.entity.RechargeCatalog;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月21日10:41:46.
*/
public interface RechargeCatalogService {

	RechargeCatalog findById(Long id);

	RechargeCatalog save(RechargeCatalog bean);

	RechargeCatalog update(RechargeCatalog bean);

	RechargeCatalog deleteById(Long id);
	
	RechargeCatalog[] deleteByIds(Long[] ids);
	
	Page<RechargeCatalog> page(Pageable pageable);
	
	Page<RechargeCatalog> page(Pageable pageable, Object search);


	List<RechargeCatalog> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}