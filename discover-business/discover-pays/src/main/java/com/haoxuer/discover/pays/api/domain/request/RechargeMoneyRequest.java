package com.haoxuer.discover.pays.api.domain.request;

import java.math.BigDecimal;

public class RechargeMoneyRequest extends AppRequest {


  /**
   * 处理器
   */
  private String handle;

  /**
   * 充值金额
   */
  private BigDecimal money;

  /**
   * 付款方式
   */
  private String payType;



  public String getPayType() {
    if (payType==null){
      return "weiapp";
    }
    return payType;
  }

  public void setPayType(String payType) {
    this.payType = payType;
  }

  public BigDecimal getMoney() {
    return money;
  }

  public void setMoney(BigDecimal money) {
    this.money = money;
  }

  public String getHandle() {
    if (handle==null){
      return "rechargeMoneyHandle";
    }
    return handle;
  }

  public void setHandle(String handle) {
    this.handle = handle;
  }
}
