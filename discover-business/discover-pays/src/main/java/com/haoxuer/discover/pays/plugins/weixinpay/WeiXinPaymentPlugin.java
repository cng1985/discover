package com.haoxuer.discover.pays.plugins.weixinpay;

import com.haoxuer.discover.pay.utils.MapUtils;
import com.haoxuer.discover.pay.weixin.domain.UnifiedOrderPayBack;
import com.haoxuer.discover.pay.weixin.domain.UnifiedOrderPayData;
import com.haoxuer.discover.pay.weixin.impl.PayWeiXinService;
import com.haoxuer.discover.pays.api.domain.simple.WeiXinPaySimple;
import com.haoxuer.discover.pays.plugins.base.PaymentPlugin;
import com.haoxuer.discover.pays.plugins.domain.PayBack;
import com.haoxuer.discover.pays.plugins.domain.PayInfo;
import com.haoxuer.discover.pays.plugins.weiapppay.WeiAppPaymentPlugin;
import com.haoxuer.discover.plug.data.entity.PluginConfig;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

import static com.haoxuer.discover.pay.weixin.impl.PayWeiXinService.MD5Encode;
import static com.haoxuer.discover.pays.plugins.service.impl.PayServiceImpl.getNonceStr;


@Component("weiXinPaymentPlugin")
public class WeiXinPaymentPlugin extends PaymentPlugin {
  @Override
  public String getName() {
    return "微信app支付插件";
  }
  
  @Override
  public String getVersion() {
    return "1.01";
  }
  
  @Override
  public String getAuthor() {
    return "ada";
  }
  
  @Override
  public String getSiteUrl() {
    return "";
  }
  
  @Override
  public String getInstallUrl() {
    return "admin/plugin_payment/weixinpayment/install.htm";
  }
  
  @Override
  public String getUninstallUrl() {
    return "admin/plugin_payment/weixinpayment/uninstall.htm";
  }
  
  @Override
  public String getSettingUrl() {
    return "admin/plugin_payment/weixinpayment/setting.htm";
  }

  @Override
  public String getBaseUrl() {
    return "admin/plugin_pay/";
  }

  @Override
  public String viewName() {
    return "wxPay";
  }

  @Override
  public PayBack pay(PayInfo payinfo) {
    PayBack result = new PayBack();
  
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      UnifiedOrderPayData data = new UnifiedOrderPayData();
      String appid = pluginConfig.getAttribute("appkey");
      String mchid = pluginConfig.getAttribute("mch_id");
      String key = pluginConfig.getAttribute("key");
      String notify_url=pluginConfig.getAttribute("notify_url");
      data.setAppid(appid);
      data.setMch_id(mchid);
      data.setNonce_str(getNonceStr());
    
      data.setBody(payinfo.getBody());
    
    
      data.setOut_trade_no(WeiAppPaymentPlugin.getOrderNo());
      data.setTotal_fee(payinfo.getMoney().multiply(new BigDecimal(100)).intValue());
      data.setSpbill_create_ip("123.12.12.123");
      data.setNotify_url(notify_url);
      data.setTrade_type("APP");//APP  JSAPI
     // data.setOpenid(payinfo.getOpenid());
      PayWeiXinService service = new PayWeiXinService(key);
      UnifiedOrderPayBack back = service.order(data);
      if (back.getReturn_code().equals("SUCCESS")) {
        result.setNo(back.getPrepay_id());
        result.setOrderNo(data.getOut_trade_no());
      }else{
        result.setCode(-301);
        result.setMsg(back.getReturn_msg());
      }
    }
    return result;
  }
  public static Long second(){
    return System.currentTimeMillis()/1000;
  }

  @Override
  public WeiXinPaySimple handle(PayBack back) {
    WeiXinPaySimple result = new WeiXinPaySimple();
    PluginConfig pluginConfig = getPluginConfig();
    if (pluginConfig != null) {
      String appid = pluginConfig.getAttribute("appkey");
      String mchid = pluginConfig.getAttribute("mch_id");
      String key = pluginConfig.getAttribute("key");
      String packageinfo=pluginConfig.getAttribute("packageinfo");
      result.setAppid(appid);
      result.setNoncestr(getNonceStr());
      result.setPackageInfo(packageinfo);
      result.setTimestamp("" + second());
      result.setPrepayid(back.getNo());
      result.setPartnerid(mchid);
  
      Map<String, String> map = MapUtils.getSortMap();
      map.put("appid", result.getAppid());
      map.put("partnerid", result.getPartnerid());
      map.put("prepayid", result.getPrepayid());
      map.put("package", result.getPackageInfo());
      map.put("noncestr", result.getNoncestr());
      map.put("timestamp", result.getTimestamp());
  
      String p = MapUtils.params(map);
      p = p + "&key=" + key;
      System.out.println(p);
      String sign = MD5Encode(p);
      result.setSign(sign);
    }
    return result;
  }
}
