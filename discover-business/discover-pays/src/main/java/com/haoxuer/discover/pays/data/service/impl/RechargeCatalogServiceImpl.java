package com.haoxuer.discover.pays.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.pays.data.dao.RechargeCatalogDao;
import com.haoxuer.discover.pays.data.entity.RechargeCatalog;
import com.haoxuer.discover.pays.data.service.RechargeCatalogService;

import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.context.annotation.Scope;


/**
* Created by imake on 2018年12月21日10:41:46.
*/


@Scope("prototype")
@Service
@Transactional
public class RechargeCatalogServiceImpl implements RechargeCatalogService {

	private RechargeCatalogDao dao;


	@Override
	@Transactional(readOnly = true)
	public RechargeCatalog findById(Long id) {
		return dao.findById(id);
	}


	@Override
    @Transactional
	public RechargeCatalog save(RechargeCatalog bean) {
		dao.save(bean);
		return bean;
	}

	@Override
    @Transactional
	public RechargeCatalog update(RechargeCatalog bean) {
		Updater<RechargeCatalog> updater = new Updater<RechargeCatalog>(bean);
		return dao.updateByUpdater(updater);
	}

	@Override
    @Transactional
	public RechargeCatalog deleteById(Long id) {
		RechargeCatalog bean = dao.findById(id);
        dao.deleteById(id);
		return bean;
	}

	@Override
    @Transactional	
	public RechargeCatalog[] deleteByIds(Long[] ids) {
		RechargeCatalog[] beans = new RechargeCatalog[ids.length];
		for (int i = 0,len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	@Autowired
	public void setDao(RechargeCatalogDao dao) {
		this.dao = dao;
	}

	@Override
    public Page<RechargeCatalog> page(Pageable pageable){
         return dao.page(pageable);
    }


    @Override
	public Page<RechargeCatalog> page(Pageable pageable, Object search) {
		List<Filter> filters=	FilterUtils.getFilters(search);
		if (filters!=null) {
			pageable.getFilters().addAll(filters);
		}
		return dao.page(pageable);
	}

    @Override
    public List<RechargeCatalog> list(int first, Integer size, List<Filter> filters, List<Order> orders) {
        return dao.list(first,size,filters,orders);}
}