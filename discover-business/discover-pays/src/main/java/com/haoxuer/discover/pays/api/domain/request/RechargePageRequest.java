package com.haoxuer.discover.pays.api.domain.request;

public class RechargePageRequest extends PageRequest {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
