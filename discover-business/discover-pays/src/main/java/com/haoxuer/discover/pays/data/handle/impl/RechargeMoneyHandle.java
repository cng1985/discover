package com.haoxuer.discover.pays.data.handle.impl;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.pays.data.entity.Payment;
import com.haoxuer.discover.pays.data.handle.MoneyHandle;
import com.haoxuer.discover.trade.data.dao.BasicTradeAccountDao;
import com.haoxuer.discover.trade.data.dao.TradeAccountDao;
import com.haoxuer.discover.trade.data.dao.UserTradeAccountDao;
import com.haoxuer.discover.trade.data.entity.TradeAccount;
import com.haoxuer.discover.trade.data.others.ChangeType;
import com.haoxuer.discover.trade.data.request.TradeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("rechargeMoneyHandle")
public class RechargeMoneyHandle implements MoneyHandle {


  @Autowired
  UserTradeAccountDao userTradeAccountDao;

  @Autowired
  BasicTradeAccountDao basicTradeAccountDao;

  @Autowired
  TradeAccountDao tradeAccountDao;

  @Override
  public void handle(Payment payment) {
    User user = payment.getUser();
    TradeAccount account = userTradeAccountDao.account(user.getId(), "basicAccount");
    //系统账号
    TradeAccount system = basicTradeAccountDao.special("systemAccount");
    if (payment.getMoney() != null) {
      TradeRequest request = new TradeRequest();
      request.setFrom(system.getId());
      request.setTo(account.getId());
      request.setAmount(payment.getMoney());
      request.setChangeType(ChangeType.from(1, "充值"));
      tradeAccountDao.trade(request);
    }
  }
}
