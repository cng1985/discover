package com.haoxuer.discover.pays.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.pays.data.entity.RechargeCard;

/**
* Created by imake on 2018年12月21日10:41:46.
*/
public interface RechargeCardDao extends BaseDao<RechargeCard,Long>{

	 RechargeCard findById(Long id);

	 RechargeCard save(RechargeCard bean);

	 RechargeCard updateByUpdater(Updater<RechargeCard> updater);

	 RechargeCard deleteById(Long id);
}