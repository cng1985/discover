package com.haoxuer.discover.pays.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.pays.data.entity.Payment;

/**
* Created by imake on 2018年12月20日18:08:26.
*/
public interface PaymentDao extends BaseDao<Payment,Long>{

	 Payment findById(Long id);

	 Payment save(Payment bean);

	 Payment updateByUpdater(Updater<Payment> updater);

	 Payment deleteById(Long id);

	Payment findByNo(String no);
}