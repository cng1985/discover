package com.haoxuer.discover.pays.data.service;

import com.haoxuer.discover.pays.data.entity.RechargeCard;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2018年12月21日10:41:46.
*/
public interface RechargeCardService {

	RechargeCard findById(Long id);

	RechargeCard save(RechargeCard bean);

	RechargeCard update(RechargeCard bean);

	RechargeCard deleteById(Long id);
	
	RechargeCard[] deleteByIds(Long[] ids);
	
	Page<RechargeCard> page(Pageable pageable);
	
	Page<RechargeCard> page(Pageable pageable, Object search);


	List<RechargeCard> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}