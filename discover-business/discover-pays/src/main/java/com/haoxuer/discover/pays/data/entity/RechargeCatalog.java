package com.haoxuer.discover.pays.data.entity;

import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;


@FormAnnotation(title = "充值卡类型")
@Entity
@Table(name = "pays_recharge_catalog")
public class RechargeCatalog extends AbstractEntity {

  @FormField(title = "充值卡名称", sortNum = "1", grid = true, col = 12)
  private String name;

  @Column(name = "config_key",unique =true,length = 50)
  private String key;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
