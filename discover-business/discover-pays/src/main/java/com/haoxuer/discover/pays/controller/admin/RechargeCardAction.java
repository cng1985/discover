package com.haoxuer.discover.pays.controller.admin;

import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.pays.data.so.RechargeCardSo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import  com.haoxuer.discover.pays.data.entity.RechargeCard;
import com.haoxuer.discover.pays.data.service.RechargeCardService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.context.annotation.Scope;

/**
*
* Created by imake on 2018年12月21日10:41:46.
*/


@Scope("prototype")
@Controller
public class RechargeCardAction {

	public static final String MODEL = "model";

	public static final String REDIRECT_LIST_HTML = "redirect:/admin/rechargecard/view_list.htm";

	private static final Logger log = LoggerFactory.getLogger(RechargeCardAction.class);

	@Autowired
	private RechargeCardService manager;

	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/view_list")
	public String list(Pageable pageable,RechargeCardSo so,ModelMap model) {

		if (pageable==null){
			pageable=new Pageable();
		}
		if (pageable.getOrders().isEmpty()) {
			pageable.getOrders().add(Order.desc("id"));
		}
		pageable.getFilters().addAll(FilterUtils.getFilters(so));
		Page<RechargeCard> pagination = manager.page(pageable);
		model.addAttribute("list", pagination.getContent());
		model.addAttribute("page", pagination);
		model.addAttribute("so", so);
		return "/admin/rechargecard/list";
	}

	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/view_add")
	public String add(ModelMap model) {
		return "/admin/rechargecard/add";
	}

	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/view_edit")
	public String edit(Pageable pageable,Long id, ModelMap model) {
		model.addAttribute(MODEL, manager.findById(id));
		model.addAttribute("page", pageable);
		return "/admin/rechargecard/edit";
	}

	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/view_view")
	public String view(Long id,ModelMap model) {
		model.addAttribute(MODEL, manager.findById(id));
		return "/admin/rechargecard/view";
	}

	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/model_save")
	public String save(RechargeCard bean,ModelMap model) {
	
	    String view=REDIRECT_LIST_HTML;
		try {
			manager.save(bean);
			log.info("save object id={}", bean.getId());
		} catch (Exception e) {
			log.error("保存失败",e);
			model.addAttribute("erro", e.getMessage());
			view="/admin/rechargecard/add";
		}
		return view;
	}

	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/model_update")
	public String update(Pageable pageable, RechargeCard bean, RedirectAttributes redirectAttributes, ModelMap model) {
		
		String view=REDIRECT_LIST_HTML;
		try {
			manager.update(bean);
			initRedirectData(pageable, redirectAttributes);
		} catch (Exception e) {
			log.error("更新失败",e);
			model.addAttribute("erro", e.getMessage());
			model.addAttribute(MODEL,bean);
		    model.addAttribute("page", pageable);
			view="/admin/rechargecard/edit";
		}
		return view;
	}
	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/model_delete")
	public String delete(Pageable pageable, Long id, RedirectAttributes redirectAttributes) {

		String view=REDIRECT_LIST_HTML;

		try {
			initRedirectData(pageable, redirectAttributes);
			manager.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			log.error("删除失败",e);
			redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
		}

		return view;
	}

	@RequiresPermissions("rechargecard")
	@RequestMapping("/admin/rechargecard/model_deletes")
	public String deletes(Pageable pageable, Long[] ids,RedirectAttributes redirectAttributes) {

		String view=REDIRECT_LIST_HTML;

		try{
				initRedirectData(pageable, redirectAttributes);
				manager.deleteByIds(ids);
			} catch (DataIntegrityViolationException e) {
				log.error("批量删除失败",e);
				redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
			}
		return view;
	}
	private void initRedirectData(Pageable pageable, RedirectAttributes redirectAttributes) {
		redirectAttributes.addAttribute("pageNumber",pageable.getPageNumber());
	}
}