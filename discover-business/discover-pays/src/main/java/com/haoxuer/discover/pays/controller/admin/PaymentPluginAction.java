package com.haoxuer.discover.pays.controller.admin;

import com.haoxuer.discover.pays.plugins.base.PaymentPlugin;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Controller - 支付.
 */
@Controller("paymentPluginAction")
@RequestMapping("/admin/plugin_payment")
public class PaymentPluginAction {
  
  @Resource
  private List<PaymentPlugin> paymentPlugins = new ArrayList<PaymentPlugin>();
  
  public List<PaymentPlugin> getPushPlugins() {
    Collections.sort(paymentPlugins);
    return paymentPlugins;
  }
  
  /**
   * 列表.
   */
  @RequiresPermissions("plugin_payment")
  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String list(ModelMap model) {
    model.addAttribute("payPlugins", getPushPlugins());
    return "/admin/plugin_payment/list";
  }
  
}