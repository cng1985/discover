package com.haoxuer.discover;

import com.haoxuer.discover.pays.data.entity.Payment;
import com.haoxuer.discover.pays.data.entity.RechargeCard;
import com.haoxuer.discover.pays.data.entity.RechargeCatalog;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.simple.TemplateSimpleDir;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        CodeMake make = new CodeMake(TemplateSimpleDir.class, TemplateHibernateDir.class);
        File view = new File("E:\\workspace\\iowl\\iowl_web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(RechargeCard.class);
        cs.add(RechargeCatalog.class);

        make.setDao(true);
        make.setService(true);
        make.setView(false);
        make.setAction(true);
        make.makes(cs);
        System.out.println("ok");
    }
}
