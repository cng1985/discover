package com.haoxuer.discover.activiti.data.service;

import com.haoxuer.discover.activiti.data.so.FlowSo;
import com.haoxuer.discover.activiti.data.vo.TaskHistoryVo;
import com.haoxuer.discover.activiti.data.vo.TaskVo;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;

import java.util.List;

public interface FlowService {

	/**
	 * 查新某个人的任务
	 * @param username
	 * @param page
	 * @param so
	 * @return
	 */
	Page<TaskVo> page(String username, Pageable page, FlowSo so);

	/**
	 * 查新历史记录
	 * @param busykey
	 * @return
	 */
	List<TaskHistoryVo> records(String busykey);

	/**
	 * 查询系统中的任务
	 * @param page
	 * @param so
	 * @return
	 */
	Page<TaskVo> page(Pageable page, FlowSo so);

}
