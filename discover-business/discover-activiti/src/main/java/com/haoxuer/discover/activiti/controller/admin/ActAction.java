package com.haoxuer.discover.activiti.controller.admin;

import com.haoxuer.discover.data.utils.FilterUtils;
import com.haoxuer.discover.activiti.data.so.ActSo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.activiti.data.entity.Act;
import com.haoxuer.discover.activiti.data.service.ActService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.context.annotation.Scope;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
*
* Created by imake on 2017年10月26日11:25:05.
*/


@Scope("prototype")
@Controller
public class ActAction {

	public static final String MODEL = "model";

	private static final Logger log = LoggerFactory.getLogger(ActAction.class);

	@Autowired
	private ActService manager;

	@RequiresPermissions("act")
	@RequestMapping("/admin/act/view_list")
	public String list(Pageable pageable,ActSo so,ModelMap model) {
	
		if (pageable!=null) {
			if (pageable.getOrders()==null||pageable.getOrders().isEmpty()) {
			pageable.getOrders().add(Order.desc("id"));
			}
		}
		pageable.getFilters().addAll(FilterUtils.getFilters(so));
		Page<Act> pagination = manager.page(pageable);
		model.addAttribute("list", pagination.getContent());
		model.addAttribute("page", pagination);
		model.addAttribute("so", so);
		return "/admin/act/list";
	}

	@RequiresPermissions("act")
	@RequestMapping("/admin/act/view_add")
	public String add(ModelMap model) {
		return "/admin/act/add";
	}

	@RequiresPermissions("act")
	@RequestMapping("/admin/act/view_edit")
	public String edit(Pageable pageable,Long id, ModelMap model) {
		model.addAttribute(MODEL, manager.findById(id));
		model.addAttribute("page", pageable);
		return "/admin/act/edit";
	}

	@RequiresPermissions("act")
	@RequestMapping("/admin/act/view_view")
	public String view(Long id,ModelMap model) {
		model.addAttribute(MODEL, manager.findById(id));
		return "/admin/act/view";
	}

	@RequiresPermissions("act")
	@RequestMapping("/admin/act/model_save")
	public String save(String name, String key, String description, String category, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
	
	    String view="redirect:view_list.htm";
		try {
			org.activiti.engine.repository.Model modelData = manager.create(name, key, description, category);
			response.sendRedirect(request.getContextPath() + "/act/process-editor/modeler.jsp?modelId=" + modelData.getId());
		} catch (Exception e) {
			log.error("保存失败",e);
			model.addAttribute("erro", e.getMessage());
			view="/admin/act/add";
		}
		return view;
	}

	@RequiresPermissions("act")
	@RequestMapping("/admin/act/model_update")
	public String update(Pageable pageable, Act bean, RedirectAttributes redirectAttributes, ModelMap model) {
		
		String view="redirect:/admin/act/view_list.htm";
		try {
			manager.update(bean);
			redirectAttributes.addAttribute("pageNumber",pageable.getPageNumber());
		} catch (Exception e) {
			log.error("更新失败",e);
			model.addAttribute("erro", e.getMessage());
			model.addAttribute(MODEL,bean);
		    model.addAttribute("page", pageable);
			view="/admin/act/edit";
		}
		return view;
	}
	@RequiresPermissions("act")
	@RequestMapping("/admin/act/model_delete")
	public String delete(Pageable pageable, Long id, RedirectAttributes redirectAttributes) {

		String view="redirect:/admin/act/view_list.htm";

		try {
			redirectAttributes.addAttribute("pageNumber",pageable.getPageNumber());
			manager.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			log.error("删除失败",e);
			redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
		}

		return view;
	}

	@RequiresPermissions("act")
	@RequestMapping("/admin/act/model_deletes")
	public String deletes(Pageable pageable, Long[] ids,RedirectAttributes redirectAttributes) {

		String view="redirect:/admin/act/view_list.htm";

		try{
				redirectAttributes.addAttribute("pageNumber",pageable.getPageNumber());
				manager.deleteByIds(ids);
			} catch (DataIntegrityViolationException e) {
				log.error("批量删除失败",e);
				redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
			}
		return view;
	}

}