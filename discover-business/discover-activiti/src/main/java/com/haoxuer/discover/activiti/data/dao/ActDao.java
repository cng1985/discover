package com.haoxuer.discover.activiti.data.dao;


import com.haoxuer.discover.activiti.data.entity.Act;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年10月26日11:25:05.
*/
public interface ActDao extends BaseDao<Act,Long>{

	 Act findById(Long id);

	 Act save(Act bean);

	 Act updateByUpdater(Updater<Act> updater);

	 Act deleteById(Long id);
}