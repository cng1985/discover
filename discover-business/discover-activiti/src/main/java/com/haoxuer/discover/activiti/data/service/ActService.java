package com.haoxuer.discover.activiti.data.service;

import com.haoxuer.discover.activiti.data.entity.Act;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import org.activiti.engine.repository.Model;

import java.util.List;

/**
* Created by imake on 2017年10月26日11:25:05.
*/
public interface ActService {

	Act findById(Long id);

	Act save(Act bean);

	Model create(String name, String key, String description, String category);


	Act update(Act bean);

	Act deleteById(Long id);
	
	Act[] deleteByIds(Long[] ids);
	
	Page<Act> page(Pageable pageable);
	
	Page<Act> page(Pageable pageable, Object search);


	List<Act> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}