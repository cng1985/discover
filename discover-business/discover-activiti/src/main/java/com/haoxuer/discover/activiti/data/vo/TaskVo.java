package com.haoxuer.discover.activiti.data.vo;

import java.io.Serializable;

public class TaskVo implements Serializable {

	private String name;

	@Override
	public String toString() {
		return "TaskVo [name=" + name + ", processDefinitionId=" + processDefinitionId + ", oid=" + oid + ", id=" + id
				+ ", viewUrl=" + viewUrl + ", updateUrl=" + updateUrl + ", flowName=" + flowName + "]";
	}

	private String processDefinitionId;

	private String oid;

	private String id;

	private String viewUrl;

	private String updateUrl;
	
	private String flowName;

	private String imageUrl;


	private String addDate;

	public String getAddDate() {
		return addDate;
	}

	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	/**
	 * 任务类型
	 */
	private String catalog;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getViewUrl() {
		return viewUrl;
	}

	public void setViewUrl(String viewUrl) {
		this.viewUrl = viewUrl;
	}

	public String getUpdateUrl() {
		return updateUrl;
	}

	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}

}
