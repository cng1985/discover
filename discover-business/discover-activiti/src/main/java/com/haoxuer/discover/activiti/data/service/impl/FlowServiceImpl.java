package com.haoxuer.discover.activiti.data.service.impl;

import com.haoxuer.discover.activiti.data.conver.TaskVoConver;
import com.haoxuer.discover.activiti.data.service.FlowService;
import com.haoxuer.discover.activiti.data.so.FlowSo;
import com.haoxuer.discover.activiti.data.vo.TaskHistoryVo;
import com.haoxuer.discover.activiti.data.vo.TaskVo;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.rest.core.ConverUtils;
import org.activiti.engine.HistoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class FlowServiceImpl implements FlowService {

    @Autowired
    protected TaskService taskService;


    @Autowired
    protected HistoryService historyService;

    @Override
    public Page<TaskVo> page(String username, Pageable page, FlowSo so) {
        Page<TaskVo> result = null;
        TaskQuery query = taskService.createTaskQuery().taskAssignee(username);
        if (so != null && so.getName() != null && so.getName().length() > 0) {
            //query=query.includeTaskLocalVariables();
            //query=query.includeProcessVariables();

            //query=query.taskVariableValueLike("name","%"+so.getName()+"%");
            query = query.processVariableValueLike("name", "%" + so.getName() + "%");

        }
        query = query.orderByTaskCreateTime().desc();
        Long total = query.count();
        List<TaskVo> content = new ArrayList<TaskVo>();
        result = new Page<TaskVo>(content, total, page);
        int first = (result.getPageNumber() - 1) * page.getPageSize();
        if (first < 0) {
            first = 0;
        }
        List<Task> tasks = query.listPage(first, page.getPageSize());
        Conver<TaskVo, Task> conver = new TaskVoConver(taskService);
        content = ConverUtils.coverList(tasks, conver);
        result = new Page<TaskVo>(content, total, page);
        return result;
    }

    @Override
    public Page<TaskVo> page(Pageable page, FlowSo so) {
        Page<TaskVo> result = null;
        TaskQuery query = taskService.createTaskQuery();
        if (so != null && so.getName() != null && so.getName().length() > 0) {
            query = query.processVariableValueLike("name", "%" + so.getName() + "%");
        }
        query = query.orderByTaskCreateTime().desc();
        Long total = query.count();
        List<TaskVo> content = new ArrayList<TaskVo>();
        result = new Page<TaskVo>(content, total, page);
        int first = (result.getPageNumber() - 1) * page.getPageSize();
        if (first < 0) {
            first = 0;
        }
        List<Task> tasks = query.listPage(first, page.getPageSize());
        Conver<TaskVo, Task> conver = new TaskVoConver(taskService);
        content = ConverUtils.coverList(tasks, conver);
        result = new Page<TaskVo>(content, total, page);
        return result;
    }
    @Override
    public List<TaskHistoryVo> records(String busykey) {
        List<TaskHistoryVo> result = new ArrayList<TaskHistoryVo>();
        List<HistoricTaskInstance> instances = historyService.createHistoricTaskInstanceQuery().processInstanceBusinessKey(busykey).list();
        if (instances != null) {
            for (HistoricTaskInstance historicTaskInstance : instances) {
                List<HistoricVariableInstance> v = historyService.createHistoricVariableInstanceQuery().taskId(historicTaskInstance.getId()).list();
                Map<String, Object> maxps = new HashMap<String, Object>();
                if (v != null) {
                    for (HistoricVariableInstance historicVariableInstance : v) {
                        maxps.put(historicVariableInstance.getVariableName(), historicVariableInstance.getValue());
                    }
                }
                TaskHistoryVo vo = new TaskHistoryVo();
                vo.setNote("" + maxps.get("note"));
                vo.setUser("" + maxps.get("user"));
                vo.setState("" + maxps.get("state"));
                vo.setAddDate(historicTaskInstance.getTime());
                if (vo.getState() != null && !"null".equals(vo.getState())) {
                    result.add(vo);
                }
            }
        }
        return result;
    }

}
