package com.haoxuer.discover.activiti.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ada on 2016/11/4.
 */
public class DateFormat {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static String format(Date date) {
        return simpleDateFormat.format(date);
    }

    public static String formatTime(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(date);
    }

    public static String formatTimeSimple(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    public static String formatMonth(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月");
        return df.format(date);
    }
}
