package com.haoxuer.discover.activiti.data.dao.impl;

import com.haoxuer.discover.activiti.data.entity.Act;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.activiti.data.dao.ActDao;

/**
* Created by imake on 2017年10月26日11:25:05.
*/
@Repository

public class ActDaoImpl extends CriteriaDaoImpl<Act, Long> implements ActDao {

	@Override
	public Act findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public Act save(Act bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public Act deleteById(Long id) {
		Act entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<Act> getEntityClass() {
		return Act.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}