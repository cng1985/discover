package com.haoxuer.discover.ad.controller.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.ad.api.apis.AdApi;
import com.haoxuer.discover.ad.api.domain.page.AdPage;
import com.haoxuer.discover.ad.api.domain.request.*;import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.context.annotation.Scope;
import com.haoxuer.discover.web.base.BaseAction;
/**
*
* Created by imake on 2021年01月10日18:29:10.
*/


@Scope("prototype")
@Controller
public class AdAction extends BaseAction{

	public static final String MODEL = "model";

	public static final String REDIRECT_LIST_HTML = "redirect:/admin/ad/view_list.htm";

	private static final Logger log = LoggerFactory.getLogger(AdAction.class);

	@Autowired
	private AdApi api;

	@RequiresPermissions("ad")
	@RequestMapping("/admin/ad/view_list")
	public String list(AdSearchRequest request,ModelMap model) {

        AdPage page = api.search(request);
        model.addAttribute("list", page.getList());
		model.addAttribute("page", page);
		model.addAttribute("so", request);
		return getView("ad/list");
	}

	@RequiresPermissions("ad")
	@RequestMapping("/admin/ad/view_add")
	public String add(ModelMap model) {
		return getView("ad/add");
	}

	@RequiresPermissions("ad")
	@RequestMapping("/admin/ad/view_edit")
	public String edit(Pageable pageable,AdDataRequest request,  ModelMap model) {
        model.addAttribute(MODEL, api.view(request));
		model.addAttribute("page", pageable);
		return getView("ad/edit");
	}

	@RequiresPermissions("ad")
	@RequestMapping("/admin/ad/view_view")
	public String view(AdDataRequest request,ModelMap model) {
         model.addAttribute(MODEL, api.view(request));
		return getView("ad/view");
	}

	@RequiresPermissions("ad")
	@RequestMapping("/admin/ad/model_save")
	public String save(AdDataRequest request,ModelMap model) {
	
	    String view=REDIRECT_LIST_HTML;
		try {
			 api.create(request);
		} catch (Exception e) {
			log.error("保存失败",e);
			model.addAttribute("erro", e.getMessage());
			view=getView("ad/add");
		}
		return view;
	}

	@RequiresPermissions("ad")
	@RequestMapping("/admin/ad/model_update")
	public String update(Pageable pageable, AdDataRequest request,  RedirectAttributes redirectAttributes, ModelMap model) {
		
		String view=REDIRECT_LIST_HTML;
		try {
			api.update(request);
			initRedirectData(pageable, redirectAttributes);
		} catch (Exception e) {
			log.error("更新失败",e);
			model.addAttribute("erro", e.getMessage());
			model.addAttribute(MODEL,request);
		    model.addAttribute("page", pageable);
			view=getView("ad/edit");
		}
		return view;
	}
	@RequiresPermissions("ad")
	@RequestMapping("/admin/ad/model_delete")
	public String delete(Pageable pageable, AdDataRequest request, RedirectAttributes redirectAttributes) {

		String view=REDIRECT_LIST_HTML;

		try {
			initRedirectData(pageable, redirectAttributes);
			 api.delete(request);
		} catch (DataIntegrityViolationException e) {
			log.error("删除失败",e);
			redirectAttributes.addFlashAttribute("erro", "该条数据不能删除，请先删除和他相关的类容!");
		}

		return view;
	}

	private void initRedirectData(Pageable pageable, RedirectAttributes redirectAttributes) {
		redirectAttributes.addAttribute("pageNumber",pageable.getPageNumber());
	}
}