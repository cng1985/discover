package com.haoxuer.discover.ad.api.domain.list;


import com.haoxuer.discover.ad.api.domain.simple.AdPositionSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年01月10日18:29:10.
*/

@Data
public class AdPositionList  extends ResponseList<AdPositionSimple> {

}