package com.haoxuer.discover.ad.freemarker;

import com.haoxuer.discover.ad.data.entity.Ad;
import com.haoxuer.discover.ad.data.service.AdService;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class AdDirective extends ListDirective<Ad> {

    @Autowired
    private AdService adService;

    @Override
    public List<Ad> list() {
        List<Filter> filters=new ArrayList<>();
        filters.add(Filter.eq("adPosition.id",getLong("id",-1L)));
        List<Order> orders=new ArrayList<>();
        orders.add(Order.asc("sortNum"));
        return adService.list(0,getInt("size",10),filters,orders);
    }
}
