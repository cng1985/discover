package com.haoxuer.discover.ad.data.entity;

import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.InputType;
import com.haoxuer.discover.data.entity.SortEntity;
import com.nbsaas.codemake.annotation.SearchItem;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Entity - 广告位
 * 
 * 
 * 
 */

@FormAnnotation(title = "广告位管理",menu = "1,51,52")
@Data
@Entity
@Table(name = "bs_basic_ad_position")
public class AdPosition extends SortEntity {


	public AdPosition(){
		width=200;
		height=150;
		template="";
	}

	@SearchItem(label = "标识",name = "key")
	@FormField(title = "标识", sortNum = "10", grid = true,required = true)
	@Column(length = 30,name = "data_key")
	private String key;

	/** 名称 */
	@SearchItem(label = "广告位名称",name = "name")
	@FormField(title = "广告位名称", sortNum = "10", grid = true,required = true)
	@Column(nullable = false)
	private String name;


	/** 宽度 */
	@FormField(title = "宽度", sortNum = "10", grid = true,type = InputType.el_input_number)
	@Column(nullable = false)
	private Integer width;

	/** 高度 */
	@FormField(title = "高度", sortNum = "10", grid = true,type = InputType.el_input_number)
	@Column(nullable = false)
	private Integer height;

	/** 描述 */
	@FormField(title = "备注", sortNum = "10", grid = true,type = InputType.textarea)
	private String note;

	/** 模板 */
	@Lob
	private String template;



}