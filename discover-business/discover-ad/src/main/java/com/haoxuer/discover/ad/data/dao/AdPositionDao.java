package com.haoxuer.discover.ad.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.ad.data.entity.AdPosition;

/**
* Created by imake on 2021年01月10日18:29:10.
*/
public interface AdPositionDao extends BaseDao<AdPosition,Long>{

	 AdPosition findById(Long id);

	 AdPosition save(AdPosition bean);

	 AdPosition updateByUpdater(Updater<AdPosition> updater);

	 AdPosition deleteById(Long id);
}