package com.haoxuer.discover.ad.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.ad.data.entity.Ad;

/**
* Created by imake on 2021年01月10日18:29:10.
*/
public interface AdDao extends BaseDao<Ad,Long>{

	 Ad findById(Long id);

	 Ad save(Ad bean);

	 Ad updateByUpdater(Updater<Ad> updater);

	 Ad deleteById(Long id);
}