package com.haoxuer.discover.ad.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年01月10日18:29:10.
*/

@Data
public class AdSearchRequest extends BasePageRequest {

    //标题
     @Search(name = "title",operator = Filter.Operator.like)
     private String title;

    //广告位
     @Search(name = "adPosition.id",operator = Filter.Operator.eq)
     private Long adPosition;

    //广告标识
     @Search(name = "adPosition.key",operator = Filter.Operator.like)
     private String key;




    private String sortField;


    private String sortMethod;
}