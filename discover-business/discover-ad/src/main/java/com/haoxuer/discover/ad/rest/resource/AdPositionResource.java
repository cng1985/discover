package com.haoxuer.discover.ad.rest.resource;

import com.haoxuer.discover.ad.api.apis.AdPositionApi;
import com.haoxuer.discover.ad.api.domain.list.AdPositionList;
import com.haoxuer.discover.ad.api.domain.page.AdPositionPage;
import com.haoxuer.discover.ad.api.domain.request.*;
import com.haoxuer.discover.ad.api.domain.response.AdPositionResponse;
import com.haoxuer.discover.ad.data.dao.AdPositionDao;
import com.haoxuer.discover.ad.data.entity.AdPosition;
import com.haoxuer.discover.ad.rest.convert.AdPositionResponseConvert;
import com.haoxuer.discover.ad.rest.convert.AdPositionSimpleConvert;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.data.utils.BeanDataUtils;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class AdPositionResource implements AdPositionApi {

    @Autowired
    private AdPositionDao dataDao;


    @Override
    public AdPositionResponse create(AdPositionDataRequest request) {
        AdPositionResponse result = new AdPositionResponse();

        AdPosition bean = new AdPosition();
        handleData(request, bean);
        dataDao.save(bean);
        result = new AdPositionResponseConvert().conver(bean);
        return result;
    }

    @Override
    public AdPositionResponse update(AdPositionDataRequest request) {
        AdPositionResponse result = new AdPositionResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        AdPosition bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new AdPositionResponseConvert().conver(bean);
        return result;
    }

    private void handleData(AdPositionDataRequest request, AdPosition bean) {
        BeanDataUtils.copyProperties(request,bean);

    }

    @Override
    public AdPositionResponse delete(AdPositionDataRequest request) {
        AdPositionResponse result = new AdPositionResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public AdPositionResponse view(AdPositionDataRequest request) {
        AdPositionResponse result=new AdPositionResponse();
        AdPosition bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new AdPositionResponseConvert().conver(bean);
        return result;
    }
    @Override
    public AdPositionList list(AdPositionSearchRequest request) {
        AdPositionList result = new AdPositionList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("asc".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<AdPosition> organizations = dataDao.list(0, request.getSize(), filters, orders);
        AdPositionSimpleConvert convert=new AdPositionSimpleConvert();
        ConverResourceUtils.converList(result, organizations,convert);
        return result;
    }

    @Override
    public AdPositionPage search(AdPositionSearchRequest request) {
        AdPositionPage result=new AdPositionPage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("asc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("desc".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<AdPosition> page=dataDao.page(pageable);
        AdPositionSimpleConvert convert=new AdPositionSimpleConvert();
        ConverResourceUtils.converPage(result,page,convert);
        return result;
    }
}
