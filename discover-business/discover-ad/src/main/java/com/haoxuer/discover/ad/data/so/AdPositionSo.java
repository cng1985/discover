package com.haoxuer.discover.ad.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年11月24日18:36:31.
*/
@Data
public class AdPositionSo implements Serializable {

    //广告位
     @Search(name = "name",operator = Filter.Operator.like)
     private String name;



    private String sortField;

    private String sortMethod;

}
