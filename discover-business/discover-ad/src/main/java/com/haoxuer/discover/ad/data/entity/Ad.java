package com.haoxuer.discover.ad.data.entity;

import com.haoxuer.discover.ad.data.enums.AdType;
import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.SortEntity;
import com.nbsaas.codemake.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Entity - 广告
 */
@SearchBean(items = {
        @SearchItem(label = "广告标识", name = "key", key = "adPosition.key")
})
@FormAnnotation(title = "广告管理", menu = "1,51,53")
@Data
@Entity
@Table(name = "bs_basic_ad")
public class Ad extends SortEntity {


    /**
     * 标题
     */
    @SearchItem(label = "标题", name = "title")
    @FormField(title = "标题", sortNum = "10", grid = true, required = true)
    private String title;

    /**
     * 广告位
     */
    @SearchItem(label = "广告位", name = "adPosition", key = "adPosition.id",classType = "Long",operator = "eq", type = InputType.select)
    @FieldConvert()
    @FieldName
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @FormField(title = "广告位", sortNum = "20", grid = true, required = true, type = InputType.select, option = "adPosition")
    private AdPosition adPosition;

    /**
     * 类型
     */
    @FormField(title = "广告类型", sortNum = "30", grid = false, col = 12)
    private AdType type;

    /**
     * 路径
     */
    private String path;

    /**
     * 起始日期
     */
    @FormField(title = "广告开始时间", sortNum = "40",width = "160",grid = true, col = 12, type = InputType.date, sort = true)
    private Date beginDate;

    /**
     * 结束日期
     */
    @FormField(title = "广告结束时间", sortNum = "50",width = "160", grid = true, col = 12, type = InputType.date, sort = true)
    private Date endDate;

    /**
     * 链接地址
     */
    @FormField(title = "链接地址", sortNum = "60")
    private String url;


    /**
     * 分类
     */
    private Integer catalog;

    @FormField(title = "排序号", sortNum = "70",width = "1000",grid = true, sort = true, type = InputType.el_input_number)
    private Integer sortNum;


    @FormField(title = "内容", sortNum = "80", col = 12, type = InputType.textarea)
    private String note;

    private Long bussId;


}