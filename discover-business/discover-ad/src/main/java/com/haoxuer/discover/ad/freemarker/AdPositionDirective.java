package com.haoxuer.discover.ad.freemarker;

import com.haoxuer.discover.ad.data.entity.AdPosition;
import com.haoxuer.discover.ad.data.service.AdPositionService;
import com.haoxuer.discover.common.freemarker.ListDirective;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class AdPositionDirective extends ListDirective<AdPosition> {

  @Autowired
  private AdPositionService positionService;

  @Override
  public List<AdPosition> list() {
    List<Filter> filters = new ArrayList<>();
    filters.add(Filter.eq("parent.id", getInt("id", 1)));
    List<Order> orders = new ArrayList<>();
    orders.add(Order.asc("code"));
    return positionService.list(0, getInt("size", 10), filters, orders);
  }
}
