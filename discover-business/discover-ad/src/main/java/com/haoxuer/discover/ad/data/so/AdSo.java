package com.haoxuer.discover.ad.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年11月24日18:36:31.
*/
@Data
public class AdSo implements Serializable {

    //广告位
     @Search(name = "title",operator = Filter.Operator.like)
     private String title;

    //广告位
     @Search(name = "position.id",operator = Filter.Operator.eq)
     private Long position;



    private String sortField;

    private String sortMethod;

}
