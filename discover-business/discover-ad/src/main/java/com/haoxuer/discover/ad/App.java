package com.haoxuer.discover.ad;

import com.haoxuer.discover.ad.data.entity.Ad;
import com.haoxuer.discover.ad.data.entity.AdPosition;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        // UserOauthToken.
        getCodeMake().makes(AdPosition.class);
        getCodeMake().makes(Ad.class);

    }

    private static CodeMake getCodeMake() {
        File file = new File("E:\\mvnspace\\xjob\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);
        make.setAction("com.haoxuer.discover.ad.controller.admin");
        make.setView(file);
        make.setDao(true);
        make.setService(false);
        make.setAction(true);
        make.setView(false);
        make.setApi(true);
        make.setRest(true);
        return make;
    }
}
