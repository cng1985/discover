package com.haoxuer.discover.function.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.ColType;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.function.data.enums.LanguageType;

import javax.persistence.*;


@FormAnnotation(title = "云函数")
@Entity
@Table(name = "bs_function")
public class Function extends AbstractEntity {

  @FormField(title = "函数名", sortNum = "1", grid = true, col = 12)
  private String name;

  @Column(name = "data_key",unique = true)
  @FormField(title = "远程key", sortNum = "2", grid = true, col = 12)
  private String key;

  @FormField(title = "函数介绍", sortNum = "3", grid = true, col =12)
  private String note;

  @Column(name = "data_script")
  @FormField(title = "脚本", sortNum = "4", grid = false, col = 12)
  private String script;

  private LanguageType languageType;


  /**
   * 添加人
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getScript() {
    return script;
  }

  public void setScript(String script) {
    this.script = script;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public LanguageType getLanguageType() {
    return languageType;
  }

  public void setLanguageType(LanguageType languageType) {
    this.languageType = languageType;
  }
}
