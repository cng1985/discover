package com.haoxuer.discover.function.data.enums;

public enum LanguageType {
    shell, js;

    @Override
    public String toString() {
        if (name().equals("js")) {
            return "js";
        } else if (name().equals("shell")) {
            return "beanshell";
        } else {
            return super.toString();
        }
    }
}
