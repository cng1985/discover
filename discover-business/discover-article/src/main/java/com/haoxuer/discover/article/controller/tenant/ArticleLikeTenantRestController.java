package com.haoxuer.discover.article.controller.tenant;

import com.haoxuer.discover.article.api.apis.ArticleLikeApi;
import com.haoxuer.discover.article.api.domain.list.ArticleLikeList;
import com.haoxuer.discover.article.api.domain.page.ArticleLikePage;
import com.haoxuer.discover.article.api.domain.request.*;
import com.haoxuer.discover.article.api.domain.response.ArticleLikeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.tenant.BaseTenantRestController;
import org.apache.shiro.authz.annotation.RequiresUser;

@RequestMapping("/tenantRest/articlelike")
@RestController
public class ArticleLikeTenantRestController extends BaseTenantRestController {


    @RequiresUser
    @RequestMapping("create")
    public ArticleLikeResponse create(ArticleLikeDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequiresUser
    @RequestMapping("update")
    public ArticleLikeResponse update(ArticleLikeDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequiresUser
    @RequestMapping("delete")
    public ArticleLikeResponse delete(ArticleLikeDataRequest request) {
        init(request);
        return api.delete(request);
    }

    @RequiresUser
    @RequestMapping("view")
    public ArticleLikeResponse view(ArticleLikeDataRequest request) {
       init(request);
       return api.view(request);
   }

    @RequiresUser
    @RequestMapping("list")
    public ArticleLikeList list(ArticleLikeSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequiresUser
    @RequestMapping("search")
    public ArticleLikePage search(ArticleLikeSearchRequest request) {
        init(request);
        return api.search(request);
    }

    @Autowired
    private ArticleLikeApi api;

}
