package com.haoxuer.discover.article.api.domain.request;

import com.haoxuer.discover.user.api.domain.request.BasePageRequest;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;

/**
*
* Created by imake on 2020年09月09日17:18:20.
*/

@Data
public class ArticleLikeSearchRequest extends BasePageRequest {


    private String sortField;


    private String sortMethod;
}