package com.haoxuer.discover.article.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import java.util.Date;

/**
*
* Created by imake on 2021年01月10日18:18:54.
*/

@Data
public class ArticleCatalogDataRequest extends BaseRequest {

    private Integer id;

     private String code;

     private Integer levelInfo;

     private Integer sortNum;

     private String ids;

     private Integer lft;

     private Date lastDate;

     private String name;

     private Date addDate;

     private Long amount;

     private Integer rgt;


}