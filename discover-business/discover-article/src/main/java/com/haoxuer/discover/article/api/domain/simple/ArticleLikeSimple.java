package com.haoxuer.discover.article.api.domain.simple;


import java.io.Serializable;
import lombok.Data;

/**
*
* Created by BigWorld on 2020年09月09日17:18:20.
*/

@Data
public class ArticleLikeSimple implements Serializable {

    private Long id;

     private Long user;
     private Long article;


}