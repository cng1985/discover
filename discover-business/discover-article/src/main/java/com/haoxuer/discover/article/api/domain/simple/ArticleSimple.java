package com.haoxuer.discover.article.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.haoxuer.discover.data.enums.StoreState;

/**
*
* Created by BigWorld on 2020年11月27日20:17:17.
*/

@Data
public class ArticleSimple implements Serializable {

    private Long id;

     private Integer commentNum;
     private Integer viewNum;
     private String title;
     private StoreState storeState;
     private Long document;
     private Integer catalog;
     private String catalogName;
     private String logo;
     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;
     private String introduction;
     private Integer likeNum;
     private Integer upNum;

     private String storeStateName;

}