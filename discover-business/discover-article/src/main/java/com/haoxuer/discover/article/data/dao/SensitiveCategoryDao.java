package com.haoxuer.discover.article.data.dao;


import com.haoxuer.discover.article.data.entity.SensitiveCategory;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日09:52:13.
*/
public interface SensitiveCategoryDao extends BaseDao<SensitiveCategory,Integer>{

	 SensitiveCategory findById(Integer id);

	 SensitiveCategory save(SensitiveCategory bean);

	 SensitiveCategory updateByUpdater(Updater<SensitiveCategory> updater);

	 SensitiveCategory deleteById(Integer id);
}