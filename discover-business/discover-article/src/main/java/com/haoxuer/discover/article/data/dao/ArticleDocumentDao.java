package com.haoxuer.discover.article.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.article.data.entity.ArticleDocument;

/**
* Created by imake on 2020年11月27日19:35:47.
*/
public interface ArticleDocumentDao extends BaseDao<ArticleDocument,Long>{

	 ArticleDocument findById(Long id);

	 ArticleDocument save(ArticleDocument bean);

	 ArticleDocument updateByUpdater(Updater<ArticleDocument> updater);

	 ArticleDocument deleteById(Long id);
}