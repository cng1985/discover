package com.haoxuer.discover.article.api.domain.list;


import com.haoxuer.discover.article.api.domain.simple.ArticleSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2020年11月27日20:17:17.
*/

@Data
public class ArticleList  extends ResponseList<ArticleSimple> {

}