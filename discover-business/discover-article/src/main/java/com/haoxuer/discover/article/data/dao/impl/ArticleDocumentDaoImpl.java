package com.haoxuer.discover.article.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.article.data.dao.ArticleDocumentDao;
import com.haoxuer.discover.article.data.entity.ArticleDocument;

/**
* Created by imake on 2020年11月27日19:35:47.
*/
@Repository

public class ArticleDocumentDaoImpl extends CriteriaDaoImpl<ArticleDocument, Long> implements ArticleDocumentDao {

	@Override
	public ArticleDocument findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ArticleDocument save(ArticleDocument bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ArticleDocument deleteById(Long id) {
		ArticleDocument entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ArticleDocument> getEntityClass() {
		return ArticleDocument.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}