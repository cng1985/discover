package com.haoxuer.discover.article.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.article.data.entity.ArticleLike;

/**
* Created by imake on 2020年09月09日17:18:20.
*/
public interface ArticleLikeDao extends BaseDao<ArticleLike,Long>{

	 ArticleLike findById(Long id);

	 ArticleLike save(ArticleLike bean);

	 ArticleLike updateByUpdater(Updater<ArticleLike> updater);

	 ArticleLike deleteById(Long id);
}