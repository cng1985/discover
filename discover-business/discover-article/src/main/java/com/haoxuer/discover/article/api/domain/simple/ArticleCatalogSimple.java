package com.haoxuer.discover.article.api.domain.simple;


import java.io.Serializable;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;

/**
*
* Created by BigWorld on 2021年01月10日18:18:54.
*/

@Data
public class ArticleCatalogSimple implements Serializable {
    private Integer id;
    private String value;
    private String label;
    private String name;
    private List<ArticleCatalogSimple> children;
}
