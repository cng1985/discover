package com.haoxuer.discover.article.data.entity;


import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.entity.LongEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "article_document")
public class ArticleDocument extends LongEntity {
    /**
     * 文章扩展信息
     */
    private String extData;


    private String note;
}
