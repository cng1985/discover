package com.haoxuer.discover.article.data.dao;


import com.haoxuer.discover.article.data.entity.SensitiveWord;
import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;

/**
* Created by imake on 2017年08月15日09:52:13.
*/
public interface SensitiveWordDao extends BaseDao<SensitiveWord,Long>{

	 SensitiveWord findById(Long id);

	 SensitiveWord save(SensitiveWord bean);

	 SensitiveWord updateByUpdater(Updater<SensitiveWord> updater);

	 SensitiveWord deleteById(Long id);
}