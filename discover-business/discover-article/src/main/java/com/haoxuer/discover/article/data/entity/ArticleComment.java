package com.haoxuer.discover.article.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.haoxuer.discover.data.entity.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 文章评论
 * 
 * @author 年高
 *
 */
@Entity
@Table(name = "article_comment")
public class ArticleComment extends AbstractEntity {



	private String contents;


	private String title;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Article article;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;


	public Article getArticle() {
		return article;
	}


	public String getContents() {
		return contents;
	}



	public String getTitle() {
		return title;
	}

	public User getUser() {
		return user;
	}


	public void setArticle(Article article) {
		this.article = article;
	}


	public void setContents(String contents) {
		this.contents = contents;
	}



	public void setTitle(String title) {
		this.title = title;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
