package com.haoxuer.discover.article.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年11月27日19:35:47.
*/
@Data
public class ArticleDocumentSo implements Serializable {



    private String sortField;

    private String sortMethod;

}
