package com.haoxuer.discover.article.api.domain.page;


import com.haoxuer.discover.article.api.domain.simple.ArticleCatalogSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年01月10日18:18:54.
*/

@Data
public class ArticleCatalogPage  extends ResponsePage<ArticleCatalogSimple> {

}