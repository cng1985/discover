package com.haoxuer.discover.article.api.domain.list;


import com.haoxuer.discover.article.api.domain.simple.ArticleCatalogSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年01月10日18:18:54.
*/

@Data
public class ArticleCatalogList  extends ResponseList<ArticleCatalogSimple> {

}