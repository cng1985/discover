package com.haoxuer.discover.article.api.domain.page;


import com.haoxuer.discover.article.api.domain.simple.ArticleLikeSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2020年09月09日17:18:20.
*/

@Data
public class ArticleLikePage  extends ResponsePage<ArticleLikeSimple> {

}