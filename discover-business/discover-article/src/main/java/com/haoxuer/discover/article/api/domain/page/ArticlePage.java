package com.haoxuer.discover.article.api.domain.page;


import com.haoxuer.discover.article.api.domain.simple.ArticleSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2020年11月27日20:17:17.
*/

@Data
public class ArticlePage  extends ResponsePage<ArticleSimple> {

}