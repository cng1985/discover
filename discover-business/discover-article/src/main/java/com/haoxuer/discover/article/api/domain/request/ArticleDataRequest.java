package com.haoxuer.discover.article.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;
import com.haoxuer.discover.data.enums.StoreState;
import java.util.Date;

/**
*
* Created by imake on 2020年11月27日20:17:17.
*/

@Data
public class ArticleDataRequest extends BaseRequest {

    private Long id;

     private Integer commentNum;

     private Integer viewNum;

     private String title;

     private StoreState storeState;

     private Long document;

     private Integer catalog;

     private String logo;

     private String introduction;

     private Integer likeNum;

     private Integer upNum;

     private String note;


}