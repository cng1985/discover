package com.haoxuer.discover.article.rest.convert;

import com.haoxuer.discover.article.api.domain.simple.ArticleSimple;
import com.haoxuer.discover.article.data.entity.Article;
import com.haoxuer.discover.data.rest.core.Conver;


public class ArticleSimpleConver implements Conver<ArticleSimple, Article> {
    @Override
    public ArticleSimple conver(Article source) {
        ArticleSimple result = new ArticleSimple();
        result.setId(source.getId());
         result.setCommentNum(source.getCommentNum());
         result.setViewNum(source.getViewNum());
         result.setTitle(source.getTitle());
         result.setStoreState(source.getStoreState());
        if(source.getDocument()!=null){
           result.setDocument(source.getDocument().getId());
        }
        if(source.getCatalog()!=null){
           result.setCatalog(source.getCatalog().getId());
        }
         if(source.getCatalog()!=null){
            result.setCatalogName(source.getCatalog().getName());
         }
         result.setLogo(source.getLogo());
         result.setAddDate(source.getAddDate());
         result.setIntroduction(source.getIntroduction());
         result.setLikeNum(source.getLikeNum());
         result.setUpNum(source.getUpNum());

         result.setStoreStateName(source.getStoreState()+"");

        return result;
    }
}
