package com.haoxuer.discover.article.data.request;

import java.io.Serializable;

/**
 * Created by cng19 on 2017/12/2.
 */
public class ArticleLikeRequest implements Serializable{

    /**
     * 用户
     */
    private Long user;

    /**
     * 文章id
     */
    private Long article;

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getArticle() {
        return article;
    }

    public void setArticle(Long article) {
        this.article = article;
    }
}
