package com.haoxuer.discover.article.data.entity;

import com.nbsaas.codemake.annotation.FormAnnotation;
import com.haoxuer.discover.data.entity.CatalogEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 文章分类
 * 
 * @author 年高
 *
 */
@FormAnnotation(title = "文章分类管理",menu = "1,33,34")
@Data
@Entity
@Table(name = "article_catalog")
public class ArticleCatalog extends CatalogEntity {

	/**
	 * 父节点
	 */
	@ManyToOne
	private ArticleCatalog parent;

	/**
	 * 数量
	 */
	private Long amount;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private List<ArticleCatalog> children;

	@Override
	public Integer getParentId() {
		if (parent != null) {
			return parent.getId();
		}
		return null;
	}

}
