package com.haoxuer.discover.article.rest.resource;

import com.haoxuer.discover.article.api.apis.ArticleLikeApi;
import com.haoxuer.discover.article.api.domain.list.ArticleLikeList;
import com.haoxuer.discover.article.api.domain.page.ArticleLikePage;
import com.haoxuer.discover.article.api.domain.request.*;
import com.haoxuer.discover.article.api.domain.response.ArticleLikeResponse;
import com.haoxuer.discover.article.data.dao.ArticleLikeDao;
import com.haoxuer.discover.article.data.entity.ArticleLike;
import com.haoxuer.discover.article.rest.convert.ArticleLikeResponseConver;
import com.haoxuer.discover.article.rest.convert.ArticleLikeSimpleConver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import com.haoxuer.discover.data.utils.FilterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.haoxuer.discover.user.rest.conver.PageableConver;
import com.haoxuer.discover.config.data.dao.UserDao;
import com.haoxuer.discover.article.data.dao.ArticleDao;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Component
public class ArticleLikeResource implements ArticleLikeApi {

    @Autowired
    private ArticleLikeDao dataDao;

    @Autowired
    private UserDao userDao;
    @Autowired
    private ArticleDao articleDao;

    @Override
    public ArticleLikeResponse create(ArticleLikeDataRequest request) {
        ArticleLikeResponse result = new ArticleLikeResponse();

        ArticleLike bean = new ArticleLike();
        handleData(request, bean);
        dataDao.save(bean);
        result = new ArticleLikeResponseConver().conver(bean);
        return result;
    }

    @Override
    public ArticleLikeResponse update(ArticleLikeDataRequest request) {
        ArticleLikeResponse result = new ArticleLikeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        ArticleLike bean = dataDao.findById(request.getId());
        if (bean == null) {
            result.setCode(502);
            result.setMsg("无效id");
            return result;
        }
        handleData(request, bean);
        result = new ArticleLikeResponseConver().conver(bean);
        return result;
    }

    private void handleData(ArticleLikeDataRequest request, ArticleLike bean) {

            if(request.getUser()!=null){
               bean.setUser(userDao.findById(request.getUser()));
            }
            if(request.getArticle()!=null){
               bean.setArticle(articleDao.findById(request.getArticle()));
            }

    }

    @Override
    public ArticleLikeResponse delete(ArticleLikeDataRequest request) {
        ArticleLikeResponse result = new ArticleLikeResponse();
        if (request.getId() == null) {
            result.setCode(501);
            result.setMsg("无效id");
            return result;
        }
        dataDao.deleteById(request.getId());
        return result;
    }

    @Override
    public ArticleLikeResponse view(ArticleLikeDataRequest request) {
        ArticleLikeResponse result=new ArticleLikeResponse();
        ArticleLike bean = dataDao.findById( request.getId());
        if (bean==null){
            result.setCode(1000);
            result.setMsg("无效id");
            return result;
        }
        result=new ArticleLikeResponseConver().conver(bean);
        return result;
    }
    @Override
    public ArticleLikeList list(ArticleLikeSearchRequest request) {
        ArticleLikeList result = new ArticleLikeList();

        List<Filter> filters = new ArrayList<>();
        filters.addAll(FilterUtils.getFilters(request));
        List<Order> orders = new ArrayList<>();
        if ("ascending".equals(request.getSortMethod())){
           orders.add(Order.asc(""+request.getSortField()));
        }
        else if ("descending".equals(request.getSortMethod())){
           orders.add(Order.desc(""+request.getSortField()));
        }else{
           orders.add(Order.desc("id"));
        }
        List<ArticleLike> organizations = dataDao.list(0, request.getSize(), filters, orders);
        ConverResourceUtils.converList(result, organizations, new ArticleLikeSimpleConver());
        return result;
    }

    @Override
    public ArticleLikePage search(ArticleLikeSearchRequest request) {
        ArticleLikePage result=new ArticleLikePage();
        Pageable pageable = new PageableConver().conver(request);
        pageable.getFilters().addAll(FilterUtils.getFilters(request));
        if ("ascending".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.asc(""+request.getSortField()));
        }
        else if ("descending".equals(request.getSortMethod())){
            pageable.getOrders().add(Order.desc(""+request.getSortField()));
        }else{
            pageable.getOrders().add(Order.desc("id"));
        }
        Page<ArticleLike> page=dataDao.page(pageable);
        ConverResourceUtils.converPage(result,page,new ArticleLikeSimpleConver());
        return result;
    }
}
