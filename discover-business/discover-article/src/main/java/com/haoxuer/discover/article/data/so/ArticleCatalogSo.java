package com.haoxuer.discover.article.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年11月28日18:14:19.
*/
@Data
public class ArticleCatalogSo implements Serializable {



    private String sortField;

    private String sortMethod;

}
