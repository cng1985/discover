package com.haoxuer.discover.article.data.entity;

import com.haoxuer.discover.config.data.entity.User;
import com.nbsaas.codemake.annotation.FormAnnotation;
import com.nbsaas.codemake.annotation.FormField;
import com.nbsaas.codemake.annotation.InputType;
import com.haoxuer.discover.data.entity.AbstractEntity;
import com.haoxuer.discover.data.enums.StoreState;
import com.haoxuer.discover.user.data.entity.OwnerEntity;
import com.nbsaas.codemake.annotation.FieldConvert;
import com.nbsaas.codemake.annotation.FieldName;
import com.nbsaas.codemake.annotation.SearchItem;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 文章
 *
 * @author 年高
 */
@FormAnnotation(title = "文章管理",menu = "1,33,35")
@Data
@Entity
@Table(name = "article")
public class Article extends OwnerEntity {

    public static Article fromId(Long id){
        Article result=new Article();
        result.setId(id);
        return result;
    }

    /**
     * 文章标题
     */
    @SearchItem(label = "文章标题",name = "title")
    @FormField(title = "文章标题",grid = true,width = "200")
    private String title;


    /**
     * 文章分类
     */
    @SearchItem(label = "文章分类",name = "catalog",key = "catalog.id",type = InputType.select,operator = "eq",classType = "Integer")
    @FormField(title = "文章分类",grid = true,type = InputType.select,option = "catalog")
    @FieldName
    @FieldConvert(classType = "Integer")
    @ManyToOne(fetch = FetchType.LAZY)
    private ArticleCatalog catalog;

    @FieldConvert
    @ManyToOne(fetch = FetchType.LAZY)
    private ArticleDocument document;
    /**
     * 文章评论数量
     */
    private Integer commentNum;

    /**
     * 喜欢的数量
     */
    private Integer likeNum;

    /**
     * 封面
     */
    private String logo;

    /**
     * 文章介绍
     */
    private String introduction;
    @JoinTable(name = "article_link_tag")
    @ManyToMany
    private Set<ArticleTag> tags;



    /**
     * 点赞数量
     */
    private Integer upNum;

    /**
     * 文章作者
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    /**
     * 文章查看数量
     */
    @FormField(title = "浏览量",grid = true,width = "90")
    private Integer viewNum;

    private StoreState storeState;

}
