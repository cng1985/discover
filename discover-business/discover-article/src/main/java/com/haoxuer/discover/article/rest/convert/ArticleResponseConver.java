package com.haoxuer.discover.article.rest.convert;

import com.haoxuer.discover.article.api.domain.response.ArticleResponse;
import com.haoxuer.discover.article.data.entity.Article;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class ArticleResponseConver implements Conver<ArticleResponse, Article> {
    @Override
    public ArticleResponse conver(Article source) {
        ArticleResponse result = new ArticleResponse();
        BeanDataUtils.copyProperties(source,result);

        if(source.getDocument()!=null){
           result.setDocument(source.getDocument().getId());
            result.setNote(source.getDocument().getNote());
        }
        if(source.getCatalog()!=null){
           result.setCatalog(source.getCatalog().getId());
        }
         if(source.getCatalog()!=null){
            result.setCatalogName(source.getCatalog().getName());
         }

         result.setStoreStateName(source.getStoreState()+"");

        return result;
    }
}
