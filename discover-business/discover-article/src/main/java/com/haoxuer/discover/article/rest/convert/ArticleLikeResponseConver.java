package com.haoxuer.discover.article.rest.convert;

import com.haoxuer.discover.article.api.domain.response.ArticleLikeResponse;
import com.haoxuer.discover.article.data.entity.ArticleLike;
import com.haoxuer.discover.data.rest.core.Conver;

public class ArticleLikeResponseConver implements Conver<ArticleLikeResponse, ArticleLike> {
    @Override
    public ArticleLikeResponse conver(ArticleLike source) {
        ArticleLikeResponse result = new ArticleLikeResponse();
        result.setId(source.getId());
        if(source.getUser()!=null){
           result.setUser(source.getUser().getId());
        }
        if(source.getArticle()!=null){
           result.setArticle(source.getArticle().getId());
        }


        return result;
    }
}
