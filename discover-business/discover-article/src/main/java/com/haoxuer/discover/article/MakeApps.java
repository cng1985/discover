package com.haoxuer.discover.article;

import com.haoxuer.discover.article.data.entity.Article;
import com.haoxuer.discover.article.data.entity.ArticleCatalog;
import com.haoxuer.discover.article.data.entity.ArticleDocument;
import com.haoxuer.discover.article.data.entity.ArticleLike;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernate.TemplateHibernateDir;
import com.nbsaas.codemake.templates.adminlte.TemplateAdminLTE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MakeApps {
    public static void main(String[] args) {
        CodeMake make = new CodeMake(TemplateAdminLTE.class, TemplateHibernateDir.class);

        File view = new File("E:\\mvnspace\\usite\\web\\src\\main\\webapp\\WEB-INF\\ftl\\admin");
        make.setView(view);

        List<Class<?>> cs = new ArrayList<Class<?>>();
        cs.add(ArticleCatalog.class);

        make.setDao(false);
        make.setService(false);
        make.setView(false);
        make.setAction(false);
        make.setApi(true);
        make.setRest(true);
        make.makes(cs);
        System.out.println("ok");
    }
}
