package com.haoxuer.discover.article.api.domain.list;


import com.haoxuer.discover.article.api.domain.simple.ArticleLikeSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2020年09月09日17:18:20.
*/

@Data
public class ArticleLikeList  extends ResponseList<ArticleLikeSimple> {

}