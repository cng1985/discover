package com.haoxuer.discover.article.data.service;

import com.haoxuer.discover.article.data.entity.SensitiveWord;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Order;
import com.haoxuer.discover.data.page.Page;
import com.haoxuer.discover.data.page.Pageable;
import java.util.List;

/**
* Created by imake on 2017年08月15日09:52:13.
*/
public interface SensitiveWordService {

	SensitiveWord findById(Long id);

	SensitiveWord save(SensitiveWord bean);

	SensitiveWord update(SensitiveWord bean);

	SensitiveWord deleteById(Long id);
	
	SensitiveWord[] deleteByIds(Long[] ids);
	
	Page<SensitiveWord> page(Pageable pageable);
	
	Page<SensitiveWord> page(Pageable pageable, Object search);


	List<SensitiveWord> list(int first, Integer size, List<Filter> filters, List<Order> orders);

}