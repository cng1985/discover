package com.haoxuer.discover.article.controller.rest;

import com.haoxuer.discover.article.api.apis.ArticleLikeApi;
import com.haoxuer.discover.article.api.domain.list.ArticleLikeList;
import com.haoxuer.discover.article.api.domain.page.ArticleLikePage;
import com.haoxuer.discover.article.api.domain.request.*;
import com.haoxuer.discover.article.api.domain.response.ArticleLikeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.haoxuer.discover.user.controller.rest.BaseRestController;

@RequestMapping("/rest/articlelike")
@RestController
public class ArticleLikeRestController extends BaseRestController {


    @RequestMapping("create")
    public ArticleLikeResponse create(ArticleLikeDataRequest request) {
        init(request);
        return api.create(request);
    }

    @RequestMapping("update")
    public ArticleLikeResponse update(ArticleLikeDataRequest request) {
        init(request);
        return api.update(request);
    }

    @RequestMapping("delete")
    public ArticleLikeResponse delete(ArticleLikeDataRequest request) {
        init(request);
        return api.delete(request);
    }

    @RequestMapping("view")
    public ArticleLikeResponse view(ArticleLikeDataRequest request) {
        init(request);
        return api.view(request);
    }

    @RequestMapping("list")
    public ArticleLikeList list(ArticleLikeSearchRequest request) {
        init(request);
        return api.list(request);
    }

    @RequestMapping("search")
    public ArticleLikePage search(ArticleLikeSearchRequest request) {
        init(request);
        return api.search(request);
    }



    @Autowired
    private ArticleLikeApi api;

}
