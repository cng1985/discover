package com.haoxuer.discover.article.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
*
* Created by imake on 2021年01月10日18:18:54.
*/

@Data
public class ArticleCatalogResponse extends ResponseObject {

    private Integer id;

     private Integer levelInfo;

     private Integer sortNum;

     private String ids;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date lastDate;

     private String name;

     @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
     private Date addDate;

     private Long amount;


}