package com.haoxuer.discover.article.rest.convert;

import com.haoxuer.discover.article.api.domain.simple.ArticleLikeSimple;
import com.haoxuer.discover.article.data.entity.ArticleLike;
import com.haoxuer.discover.data.rest.core.Conver;


public class ArticleLikeSimpleConver implements Conver<ArticleLikeSimple, ArticleLike> {
    @Override
    public ArticleLikeSimple conver(ArticleLike source) {
        ArticleLikeSimple result = new ArticleLikeSimple();
        result.setId(source.getId());
        if(source.getUser()!=null){
           result.setUser(source.getUser().getId());
        }
        if(source.getArticle()!=null){
           result.setArticle(source.getArticle().getId());
        }


        return result;
    }
}
