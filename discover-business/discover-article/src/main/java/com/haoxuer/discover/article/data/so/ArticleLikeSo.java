package com.haoxuer.discover.article.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;

/**
* Created by imake on 2020年09月09日17:18:20.
*/
@Data
public class ArticleLikeSo implements Serializable {



    private String sortField;

    private String sortMethod;

}
