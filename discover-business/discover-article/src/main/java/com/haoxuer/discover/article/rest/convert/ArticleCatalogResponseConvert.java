package com.haoxuer.discover.article.rest.convert;

import com.haoxuer.discover.article.api.domain.response.ArticleCatalogResponse;
import com.haoxuer.discover.article.data.entity.ArticleCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.data.utils.BeanDataUtils;

public class ArticleCatalogResponseConvert implements Conver<ArticleCatalogResponse, ArticleCatalog> {
    @Override
    public ArticleCatalogResponse conver(ArticleCatalog source) {
        ArticleCatalogResponse result = new ArticleCatalogResponse();
        BeanDataUtils.copyProperties(source,result);



        return result;
    }
}
