package com.haoxuer.discover.article.data.so;

import java.io.Serializable;
import com.haoxuer.discover.data.page.Filter;
import com.haoxuer.discover.data.page.Search;
import lombok.Data;
import java.util.Date;

/**
* Created by imake on 2020年11月27日20:17:17.
*/
@Data
public class ArticleSo implements Serializable {

    //文章标题
     @Search(name = "title",operator = Filter.Operator.like)
     private String title;

    //文章分类
     @Search(name = "catalog.id",operator = Filter.Operator.eq)
     private Integer catalog;



    private String sortField;

    private String sortMethod;

}
