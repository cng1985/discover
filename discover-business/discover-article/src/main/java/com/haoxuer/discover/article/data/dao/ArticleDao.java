package com.haoxuer.discover.article.data.dao;


import  com.haoxuer.discover.data.core.BaseDao;
import  com.haoxuer.discover.data.core.Updater;
import  com.haoxuer.discover.article.data.entity.Article;

/**
* Created by imake on 2020年11月27日20:17:17.
*/
public interface ArticleDao extends BaseDao<Article,Long>{

	 Article findById(Long id);

	 Article save(Article bean);

	 Article updateByUpdater(Updater<Article> updater);

	 Article deleteById(Long id);
}