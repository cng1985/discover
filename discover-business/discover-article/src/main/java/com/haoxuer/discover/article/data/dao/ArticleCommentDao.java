package com.haoxuer.discover.article.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.article.data.entity.ArticleComment;

/**
* Created by imake on 2017年08月15日09:52:13.
*/
public interface ArticleCommentDao extends BaseDao<ArticleComment,Long>{

	 ArticleComment findById(Long id);

	 ArticleComment save(ArticleComment bean);

	 ArticleComment updateByUpdater(Updater<ArticleComment> updater);

	 ArticleComment deleteById(Long id);
}