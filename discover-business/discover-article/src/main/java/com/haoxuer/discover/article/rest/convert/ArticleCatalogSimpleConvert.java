package com.haoxuer.discover.article.rest.convert;

import com.haoxuer.discover.article.api.domain.simple.ArticleCatalogSimple;
import com.haoxuer.discover.article.data.entity.ArticleCatalog;
import com.haoxuer.discover.data.rest.core.Conver;
import com.haoxuer.discover.config.utils.ConverResourceUtils;
import lombok.Data;


@Data
public class ArticleCatalogSimpleConvert implements Conver<ArticleCatalogSimple, ArticleCatalog> {

    private int fetch;

    @Override
    public ArticleCatalogSimple conver(ArticleCatalog source) {
        ArticleCatalogSimple result = new ArticleCatalogSimple();

         result.setId(source.getId());
         result.setLabel(source.getName());
         result.setValue(""+source.getId());
         result.setName(source.getName());
         if (fetch!=0&&source.getChildren()!=null&&source.getChildren().size()>0){
             result.setChildren(ConverResourceUtils.converList(source.getChildren(),this));
         }
        return result;
    }
}
