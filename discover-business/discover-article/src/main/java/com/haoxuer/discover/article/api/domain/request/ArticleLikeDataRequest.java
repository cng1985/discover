package com.haoxuer.discover.article.api.domain.request;


import com.haoxuer.discover.user.api.domain.request.BaseRequest;
import lombok.Data;

/**
*
* Created by imake on 2020年09月09日17:18:20.
*/

@Data
public class ArticleLikeDataRequest extends BaseRequest {

    private Long id;

     private Long article;

     private Long user;


}