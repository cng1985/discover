package com.haoxuer.discover.article.data.dao;


import com.haoxuer.discover.data.core.BaseDao;
import com.haoxuer.discover.data.core.Updater;
import com.haoxuer.discover.article.data.entity.ArticleCatalog;

/**
* Created by imake on 2017年08月15日09:52:12.
*/
public interface ArticleCatalogDao extends BaseDao<ArticleCatalog,Integer>{

	 ArticleCatalog findById(Integer id);

	 ArticleCatalog save(ArticleCatalog bean);

	 ArticleCatalog updateByUpdater(Updater<ArticleCatalog> updater);

	 ArticleCatalog deleteById(Integer id);


	/**
	 * 从子节点更新到根节点
	 *
	 * @param id
	 *            子节点id
	 * @return 更新节点层数
	 */
	Integer updateNums(Integer id);
}