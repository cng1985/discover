package com.haoxuer.discover.article.api.apis;


import com.haoxuer.discover.article.api.domain.list.ArticleLikeList;
import com.haoxuer.discover.article.api.domain.page.ArticleLikePage;
import com.haoxuer.discover.article.api.domain.request.*;
import com.haoxuer.discover.article.api.domain.response.ArticleLikeResponse;

public interface ArticleLikeApi {

    /**
     * 创建
     *
     * @param request
     * @return
     */
    ArticleLikeResponse create(ArticleLikeDataRequest request);

    /**
     * 更新
     *
     * @param request
     * @return
     */
    ArticleLikeResponse update(ArticleLikeDataRequest request);

    /**
     * 删除
     * @param request
     * @return
     */
    ArticleLikeResponse delete(ArticleLikeDataRequest request);


    /**
    * 详情
    *
    * @param request
    * @return
    */
     ArticleLikeResponse view(ArticleLikeDataRequest request);


    /**
     * 集合功能
     * @param request
     * @return
     */
    ArticleLikeList list(ArticleLikeSearchRequest request);

    /**
     * 搜索功能
     *
     * @param request
     * @return
     */
    ArticleLikePage search(ArticleLikeSearchRequest request);

}