package com.haoxuer.discover.article.data.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haoxuer.discover.data.core.CriteriaDaoImpl;
import com.haoxuer.discover.article.data.dao.ArticleLikeDao;
import com.haoxuer.discover.article.data.entity.ArticleLike;

/**
* Created by imake on 2020年09月09日17:18:20.
*/
@Repository

public class ArticleLikeDaoImpl extends CriteriaDaoImpl<ArticleLike, Long> implements ArticleLikeDao {

	@Override
	public ArticleLike findById(Long id) {
	    if (id==null) {
			return null;
		}
		return get(id);
	}

	@Override
	public ArticleLike save(ArticleLike bean) {

        getSession().save(bean);
		
		
		return bean;
	}

    @Override
	public ArticleLike deleteById(Long id) {
		ArticleLike entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}
	
	@Override
	protected Class<ArticleLike> getEntityClass() {
		return ArticleLike.class;
	}
	
	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory){
	    super.setSessionFactory(sessionFactory);
	}
}