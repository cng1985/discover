package com.haoxuer.discover.article.controller.admin;

import com.haoxuer.discover.article.data.so.ArticleSo;
import com.haoxuer.discover.controller.BaseAction;
import com.haoxuer.discover.data.page.Pageable;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
/**
*
* Created by imake on 2020年11月27日20:17:17.
*/


@Scope("prototype")
@Controller
public class ArticleAction extends BaseAction {

	@RequiresPermissions("article")
	@RequestMapping("/admin/article/view_list")
	public String list(Pageable pageable,ArticleSo so,ModelMap model) {

		return getView("article/list");
	}
}