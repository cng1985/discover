package com.haoxuer.discover.article.api.domain.response;


import com.haoxuer.discover.rest.base.ResponseObject;
import lombok.Data;

/**
*
* Created by imake on 2020年09月09日17:18:20.
*/

@Data
public class ArticleLikeResponse extends ResponseObject {

    private Long id;

     private Long user;

     private Long article;


}